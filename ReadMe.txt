Sand Dune is a 2D lightweight Entity Component based engine that's planned to be a platform for games or general applications.  It favors simplicity and flexibility over convenience.  Sand Dune uses the SFML library (http://www.sfml-dev.org/) to interface with the OS window handle, input, and graphics.  Supported functionality is listed at the bottom of this document.

Sand Dune is currently in early development, and it is not planned for public use any time soon.  This software is intended for internal development and QA.  This is not intended for users to start using this since the software is unstable and lacks functionality.

If you're a developer, be alert that this code base is in an immature state.  Outside of inline comments, there is no documentation and no website.  The architecture is also subject for drastic changes.  Now is not the time to base applications off of Sand Dune.
====================================================

Although it's planned to support other operating systems, Sand Dune does not have a MakeFile, and the OS-specific calls for Mac and Linux are stubbed.  To ease conversions to other platforms, OS-specific functions and includes are separated into platform-specific source files (see Source/Core/Headers/PlatformWindows.h for an example).  In addition, Sand Dune uses a custom wrapper objects for majority of the standard variables (see capabilities at the bottom).  Only Windows 32 bit with vc14 compiler works out of the box.
====================================================

Although modules aren't truly independent as the definition implies, functionality is divided into individual modules to make it easier for configuration.  These modules are nothing more than a grouping of source files that accomplishes a set of related objectives, and these files placed in a common folder.  The architecture has modules depending on others.  Nearly all modules depend on the Core module to benefit from the Core's object reflection, custom data types, and definitions.  Many modules may require multiple modules.  For example, the GUI module depends on the Core, Graphics, and Input modules.  Each module follows a similar pattern.  To check the module's dependencies, see the <ModuleName>.h file.  Visit the Launcher/Headers/Configuration.h file to specify which modules to enable/disable.

The following modules are planned to start/resume development in the near future:
GUI Editor
Console
Multi Threaded Module
Particles Module (Primitive particles only to test the Multi Threaded module)
====================================================


When launching the engine without editing anything in debug, it'll launch with many unit tests.  To completely disable unit tests, set CONDUCT_UNIT_TESTS to 0.  Running with unit tests will significantly increase start up time, logs will be spammed, and several objects will be drawn on screen.  Most unit tests will automatically terminate, but some will persist without specific input.
To terminate the GUIUnit test, you must click the 'End UI Test' button.
To terminate the BaseEditor unit test, you must check the 'SetToTrueToClose' bool to true.
To terminate the Input unit test (this one spams the log), press escape once to terminate the input event test, and again to terminate the mouse object test.  The mouse object test causes the mouse to draw a red clamping box when holding the left mouse button.  Right click the mouse to remove the mouse clamp.

When launching the engine without any unit tests, you should see a blank screen with a mouse cursor.
====================================================


At this time, Sand Dune has these capabilities:
Custom Variable Type Wrappers
	Sand Dune wraps variables in a custom class known as a DProperty.  DProperty is an interface for variables (to convert to strings or to allow objects have a pointer to their first extendable property).  The DProperty also helps makes variables more consistent across the application.  There's now one place to configure the comparison tolerance when comparing floats or vectors, and converting a FLOAT to an INT will always round down.  Lastly, the DProperties makes it easier to find utility functions about that specific variable type.  Variable utilities (such as rounding, finding sub strings, or vector math) can be easily found looking through their header files.  Generic templated functions are also supported.  DProperties still have the ability to interface with standard cpp types.  The following data types are wrapped in DProperties:  bool, string, int, and float.  Non standard data types are also treated as a DProperty:  matrix, range, rectangle, function pointer, 2D vector, 3D vector, file attributes, rotator, and date time.

Object Reflection
	Each object type is affiliated with a DClass object.  The DClass object has information about the object's class such as class name, inheritance, and a default instance.  Each object also has a pointer to a property extension.  Property extensions attaches functionality to any DProperty.  See EditableProperty as an example.  For performance and for simplicity, properties are not automatically attached to objects.
	Classes are not required to inherit from Object.  Some objects (such as iterators, engine components, and interfaces) do not inherit from Object for various reasons:  to avoid the diamond problem, remain lightweight, skip object registration, etc...

Nested Components
	Entity components are allowed to attach to other Entity components.  This framework allows components to extend functionality of other subcomponents.  A clear example use case of this is with UI.  A dropdown component contains a button component, 2 label components, 2 frame components, and a scrollbar component.  The scrollbar component has 2 button components and a scrollbar track component.  In addition, each subcomponent manages its own render component, and may instantiate its own input component.

Object Management
	The engine maintains its own object hash table to optimize object iteration.  In addition, the engine also garbage collects any objects that are flagged for deletion.  This allows numerous objects to be deleted at a periodic intervals, and enable the application to notify the engine when is a good time to clean up objects (such as during loading screens).

Engine Extensibility
	The engine, itself, may have its own components.  This allows individual modules and applications to maintain its own internal engine loop, and they may also instantiate its specific management objects.  For example, the Graphics module launches the its own OS window handle, draws to that window, and removes that window on engine shutdown.  In addition, the Graphics module launches and maintains the resource pools.

Graphics Module
	This module manages the application's main window handle, runs the render pipeline, houses resources pools to protect against duplicated resources, and defines basic transformation and render components.

GUI Module
	Although the GUI module is missing many UI elements you would see in an operating system, this module defines the UI framework other UI elements may benefit from.  GUIComponents defines some optimization techniques for input broadcasting and notifications of event changes (such as drawing to a new window handle, changes in draw order, and changes in transformation attributes).  The GUIEntities allow entities to manage a group of GUIComponents, and to communicate well with other GUIEntities.  The GUI module defines the following UI elements:  button, dropdown, frame, label, and vertical scrollbar.

Input Module
	The Input module defines the InputBroadcaster.  The InputBroadcaster is an entity polling events from an OS window handle, and passes events down to its registered input components until something consumes the event.  Input components are entities that can be attached to any entity.  It'll notify the owning entity of the new input event, and the owner, itself, determines if it should consume the input event or not.  Lastly, this module defines the mouse pointer object.

File Module
	The File module contains multiple utility functions and objects that are to interface with the operating system's file structure or to a particular file.

Localization Module
	The Localization module defines a simple framework to allow quick text swaps based on the current language.

Time Module
	The Time module focuses on latent function calls.  In addition, this module defines some utility functions and data types revolving about dates and time.

====================================================


License
Sand Dune's source code is released under the MIT license. Please see LICENSE.txt for complete licensing information.
SFML libraries and source code are distributed under the zlib/png license.  Please see http://www.sfml-dev.org/license.php for their license details.

====================================================


I rather not program in the dark.  I welcome criticism, comments, and questions.  Feel free to contact me at:
Ant@SpamVikings.org
-Eric "Ant" Eberhart
