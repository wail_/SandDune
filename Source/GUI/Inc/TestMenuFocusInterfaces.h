/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TestMenuFocusInterface.h
  Object that'll be testing GUIEntities.  This entity will be testing various UI elements that
  implement the FocusInterface.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef TESTMENUFOCUSINTERFACE_H
#define TESTMENUFOCUSINTERFACE_H

#include "GUIEntity.h"

#if INCLUDE_GUI
#ifdef DEBUG_MODE

namespace SD
{
	class FrameComponent;
	class ButtonComponent;
	class TreeListComponent;

	class TestMenuFocusInterface : public GUIEntity
	{
		DECLARE_CLASS(TestMenuFocusInterface)


		/*
		=====================
		  Datatypes
		=====================
		*/

	protected:
		struct SPageComponents
		{
		public:
			std::vector<GUIComponent*> Components;
		};


		/*
		=====================
		  Properties
		=====================
		*/

	protected:
		INT PageIdx;

		std::vector<SPageComponents> PageData;
		FrameComponent* MenuFrame;
		ButtonComponent* NextPage;
		ButtonComponent* PrevPage;

		ButtonComponent* FocusButton;
		DropdownComponent* FocusDropdown;
		TreeListComponent* FocusTreeList;

	private:
		std::vector<DString> DropdownEntries;

		std::vector<DString> TreeListEntries;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual void LoseFocus () override;
		virtual void GainFocus () override;

	protected:
		virtual void CleanUpInvalidPointers () override;
		virtual void ConstructUI () override;
		virtual void RefreshTransformations () override;
		virtual void InitializeFocusComponent () override;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  Sets all focus component's visibility flags based on the active page.
		 */
		virtual void RefreshPageVisibility ();


		/*
		=====================
		  Event Handlers
		=====================
		*/

	protected:
		virtual void HandleNextPageReleased (ButtonComponent* uiComponent);
		virtual void HandlePrevPageReleased (ButtonComponent* uiComponent);
		virtual void HandleButtonReleased (ButtonComponent* uiComponent);
		virtual void HandleDropdownOptionSelected (INT newIdx);
		virtual void HandleTreeListOptionSelected (INT optionIdx);
	};
}

#endif
#endif
#endif