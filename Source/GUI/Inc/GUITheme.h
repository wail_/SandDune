/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GUITheme.h
  A class that references resources that should persist throughout multiple GUI components
  even if the textures are used within one class (to have a consistent feel throughout the application).
  Subclassed themes may have additional texture types, and also additional shared
  aesthetics across UI Components (such as sounds).

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef GUITHEME_H
#define GUITHEME_H

#include "GUI.h"

#if INCLUDE_GUI

namespace SD
{
	//TODO:  Initialize all GUIComponents using registered GUIComponents as templates to copy.
	//	For example, have a button component created and registered at GUITheme, then all ButtonComponents may refer to that button to duplicate visual properties from.
	//  Have the duplicate properties function defined in GUIComponent level.
	class GUITheme : public Object
	{
		DECLARE_CLASS(GUITheme)


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* Various mouse pointer icon overrides. */
		Texture* TextPointer;
		Texture* ResizePointerVertical;
		Texture* ResizePointerHorizontal;
		Texture* ResizePointerDiagonalTopLeft;
		Texture* ResizePointerDiagonalBottomLeft;
		Texture* ScrollbarPointer; //Overrides mouse pointer when using middle mouse to scroll.  The sprite is assumed to have 3 subdivisions (scroll up, no scroll, and scroll down).

		/* Various frame textures. */
		Texture* FrameFill;
		Texture* FrameBorderTop;
		Texture* FrameBorderRight;
		Texture* FrameBorderBottom;
		Texture* FrameBorderLeft;
		Texture* FrameBorderTopRight;
		Texture* FrameBorderBottomRight;
		Texture* FrameBorderBottomLeft;
		Texture* FrameBorderTopLeft;

		/* Various button textures. */
		Texture* ButtonSingleSpriteBackground; //Sprite to use for single sprite button components (using subdivisions)
		Texture* ButtonFill; //For stretchable buttons (framed buttons), the background is filled with this sprite.
		Texture* ButtonBorderTop;
		Texture* ButtonBorderRight;
		Texture* ButtonBorderBottom;
		Texture* ButtonBorderLeft;
		Texture* ButtonBorderTopRight;
		Texture* ButtonBorderBottomRight;
		Texture* ButtonBorderBottomLeft;
		Texture* ButtonBorderTopLeft;
		//possible solution - template button?

		/* Various scrollbar textures. */
		Texture* ScrollbarTopButton;
		Texture* ScrollbarBottomButton;
		Texture* ScrollbarMiddleMouseScrollAnchor; //sprite to display where the middle mouse button was pressed
		Texture* ScrollbarTrackFill;
		Texture* ScrollbarTrackBorderTop;
		Texture* ScrollbarTrackBorderRight;
		Texture* ScrollbarTrackBorderBottom;
		Texture* ScrollbarTrackBorderLeft;
		Texture* ScrollbarTrackBorderTopRight;
		Texture* ScrollbarTrackBorderBottomRight;
		Texture* ScrollbarTrackBorderBottomLeft;
		Texture* ScrollbarTrackBorderTopLeft;

		/* Various dropdown textures. */
		Texture* DropdownCollapsedButton;
		Texture* DropdownExpandedButton;

		/* Various tree list textures. */
		Texture* TreeListAddButton;
		Texture* TreeListSubtractButton;

		Font* GUIFont;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		virtual void InitializeTheme ();


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		static GUITheme* GetGUITheme ();
	};
}

#endif
#endif