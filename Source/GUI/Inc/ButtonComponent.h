/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ButtonComponent.h
  A component that broadcasts a delegated function whenever clicked.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef BUTTONCOMPONENT_H
#define BUTTONCOMPONENT_H

#include "FrameComponent.h"

#if INCLUDE_GUI

namespace SD
{
	class LabelComponent;
	class ButtonStateComponent;

	//TODO:  Create a function on the button state that'll allow generic support for replacing state components, and linking them to the button.
	class ButtonComponent : public FrameComponent, public FocusInterface
	{
		DECLARE_CLASS(ButtonComponent)


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* Reference to the component that's responsible for displaying text over the button. */
		LabelComponent* CaptionComponent;

	protected:
		/* Reference to the component that'll determine how the appearance of this button changes over various states. */
		ButtonStateComponent* StateComponent;

		/* True if the button can be interacted. */
		bool bEnabled;

		/* Becomes true whenever the mouse pointer clicked on it but haven't released yet. */
		bool bPressedDown;

		/* Becomes true whenever the mouse pointer is hovering over this button. */
		bool bHovered;

		/* Function callback to invoke when this button was pressed */
		std::function<void(ButtonComponent* uiComponent)> OnButtonPressed;

		/* Function callback to invoke when this button was released */
		std::function<void(ButtonComponent* uiComponent)> OnButtonReleased;


		/*
		=====================
		  Inherited	
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual void BeginObject () override;
		virtual void SetWindowHandle (Window* newWindowHandle) override;

		//FocusInterface
		virtual bool CanBeFocused () const override;
		virtual void LoseFocus () override;
		virtual bool CaptureFocusedInput (const sf::Event& keyEvent) override;
		virtual bool CaptureFocusedText (const sf::Event& keyEvent) override;

	protected:
		virtual void ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;
		virtual bool ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
		virtual bool AcceptsMouseEvents (const unsigned int& mousePosX, const unsigned int& mousePosY) const;
		virtual bool ShouldHaveInputComponent () const override;
		virtual bool ShouldHaveExternalInputComponent () const override;
		virtual void HandleSizeChange () override;
		virtual void CleanUpInvalidPointers () override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Simulates pushing down on the button.
		 */
		virtual void SetButtonDown (bool bInvokeDelegate);

		/**
		  Simulates releasing the button.
		 */
		virtual void SetButtonUp (bool bInvokeDelegate);

		virtual void SetButtonPressedHandler (std::function<void(ButtonComponent* uiComponent)> newHandler);
		virtual void SetButtonReleasedHandler (std::function<void(ButtonComponent* uiComponent)> newHandler);
		virtual void SetCaptionText (DString newText);
		virtual void SetEnabled (bool bNewEnabled);

		/**
		  Removes old ButtonStateComponent in place of a new ButtonStateComponent.  Links the newly created
		  state component with this component.  Returns a pointer of the newly created ButtonStateComponent.  A nullptr will be returned
		  if the function was unable to create or set the ButtonStateComponent.  If it fails, it will not destroy the old state component.
		 */
		virtual ButtonStateComponent* ReplaceStateComponent (const DClass* newButtonStateClass);

		/**
		  Creates and sets up the CaptionComponent to be associated with this button.
		  This function does nothing if this button already contains a caption component.
		 */
		virtual void InitializeCaptionComponent (const DClass* labelClass);


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		virtual bool GetEnabled () const;
		virtual bool GetHovered () const;
		virtual bool GetPressedDown () const;
		virtual DString GetCaptionText () const;
		virtual ButtonStateComponent* GetButtonStateComponent () const;
	};
}

#endif
#endif