/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GUIDataElement.h
  An object representing information of an object that can be displayed within GUI containers
  such as dropdowns and tables.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef GUIDATAELEMENT_H
#define GUIDATAELEMENT_H

#include "GUIComponent.h"

#if INCLUDE_GUI

namespace SD
{
	/**
	  Base class for GUIDataElement so the non templated variables may point to templated types.
	 */
	class BaseGUIDataElement
	{
		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* If not empty, this is the text to display on the UI component to represent this object. */
		DString LabelText;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		virtual DString GetLabelText () const
		{
			return LabelText;
		}
	};

	template <class T>
	class GUIDataElement : public BaseGUIDataElement
	{


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* The represented object that could be a DProperty, DClass, Object, etc... */
		T* Data;


		/*
		=====================
		  Constructors
		=====================
		*/

	public:
		GUIDataElement ()
		{
			Data = nullptr;
			LabelText = TXT("");
		}

		GUIDataElement (T* inData, const DString& inLabelText)
		{
			Data = inData;
			LabelText = inLabelText;
		}

		GUIDataElement (const GUIDataElement& inCopyData)
		{
			Data = inCopyData.Data;
			LabelText = inCopyData.LabelText;
		}

		virtual ~GUIDataElement ()
		{
		
		}


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual DString GetLabelText () const override
		{
			if (!LabelText.IsEmpty())
			{
				return LabelText;
			}

			return Data->ToString();
		}


		/*
		=====================
		  Operators
		=====================
		*/

	public:
		virtual bool operator== (const GUIDataElement& other) const
		{
			//Only comparing the data is intended.
			return Data == other.Data;
		}
	};
}

#endif
#endif