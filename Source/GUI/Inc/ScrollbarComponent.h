/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ScrollbarComponent.h
  A component that tracks a scroll position and calls back any
  changes to that scroll position for its owner.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef SCROLLBARCOMPONENT_H
#define SCROLLBARCOMPONENT_H

#include "FrameComponent.h"

#if INCLUDE_GUI

namespace SD
{
	class ButtonComponent;
	class ScrollbarTrackComponent;

	class ScrollbarComponent : public GUIComponent
	{
		DECLARE_CLASS(ScrollbarComponent)


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* Reference to the track component that resides between the two scroll buttons. */
		ScrollbarTrackComponent* Track;

		/* Sprite component responsible for drawing the arrows representing the MiddleMousePosition. */
		SpriteComponent* MiddleMouseSprite;

		/* Sprite component that renders over the mouse cursor while the Middle Mouse is actively scrolling this bar. */
		SpriteComponent* PanningMiddleMouseCursor;

		ButtonComponent* TopScrollButton;
		ButtonComponent* BottomScrollButton;

		/* When either scroll buttons are pressed, this determines how many indices the scrollbar may jump. */
		INT ScrollJumpInterval;

		/* If true, then the user may scroll this scrollbar using the middle mouse button. */
		bool bEnableMiddleMouseScrolling;

		/* If MiddleMousePosition is actively scrolling this scrollbar, then this determines how much time
		is needed before the scrollbar may scroll again.  The interval varies based on the cursor's position
		relative to the MiddleMousePosition variable.  Max value is achieved when the distance is greater than
		the owner's absolute size.*/
		Range<FLOAT> MiddleMouseScrollIntervalRange;

		/* If greater than 0, then this determines how much time is needed for the MouseWheel to be held
		down for the MiddleMousePosition to be in 'release mode' where the user must release the middle mouse
		button again to turn off the middle mouse position.  Otherwise (when middle mouse button was tapped),
		then the user must click the middle mouse button again to turn off middle mouse position. */
		FLOAT MiddleMouseHoldThreshold;

		/* When pressing the top/bottom scroll buttons, this is the time needed for the user to hold before
		the scrollbar will continuously increment. */
		FLOAT ContinuousScrollDelay;

		/* This determines how much time is needed to pass to jump again when continuously scrolling. */
		FLOAT ContinuousInterval;

		/* If true, then the mouse pointer will be clamped to the owning entity whenever scrolling via middle mouse button. */
		bool bClampsMouseWhenScrolling;

	protected:
		/* Sets the current scrolling index where 0 means very top, and higher values are lower. */
		INT ScrollPosition;

		INT MaxScrollPosition;

		/* Determines how many scroll positions are visible at once.  For example, this is how many lines
		may be displayed in a TextField without having to scroll. */
		INT NumVisibleScrollPositions;

		/* True if the scroll bar may be interacted. */
		bool bEnabled;

		/* If true, then the scrollbar will hide itself when the number of visible scroll positions is less than max scroll position. */
		bool bHideWhenInsufficientScrollPos;

		/* Becomes true while the scroll buttons are held. */
		bool bHoldingScrollButtons;

		/* If nonnegative, then the scroll bar will be scrolling automatically based on the mouse's position
		relative to the MiddleMousePosition.  This toggles whenever the middle mouse button was pressed. */
		Vector2 MiddleMousePosition;

		/* Timestamp when the middle mouse button was pressed down. */
		FLOAT MiddleMouseTimeStamp;

		/* Function callback whenever the scroll position changed. */
		std::function<void(INT newScrollPosition)> OnScrollPositionChanged;

	private:
		/* Direction where the middle mouse is currently panning.  1 = up, 0 = none, -1 = down. */
		INT MiddleMousePanDirection;

		/* If true, then this component is clamping the mouse pointer. */
		bool bClampingMousePointer;


		/*
		=====================
		  Inherited	
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual void BeginObject () override;
		virtual void SetWindowHandle (Window* newWindowHandle) override;
		virtual void Destroy () override;

	protected:
		virtual void ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;
		virtual bool ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
		virtual bool ExecuteMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent) override;
		virtual bool ShouldHaveInputComponent () const override;
		virtual bool AcceptsMouseEvents (const unsigned int& mousePosX, const unsigned int& mousePosY) const override;
		virtual void RefreshRenderComponentVisibility () override;
		virtual void CleanUpInvalidPointers () override;

		virtual void HandleSizeChange () override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Returns true if the specified index is currently visible based on this scrollbar's current scroll position.
		 */
		virtual bool IsIndexVisible (const INT index) const;

		/**
		  Jumps the scroll position relative to the current scroll position.
		  Negative value typically means scrolling up.
		 */
		virtual void IncrementScrollPosition (INT amountToJump);
		virtual void SetScrollPositionChanged (std::function<void(INT newScrollPosition)> newHandler);
		virtual void SetScrollPosition (INT newScrollPosition);
		virtual void SetMaxScrollPosition (INT newMaxScrollPosition);
		virtual void SetNumVisibleScrollPositions (INT newNumVisibleScrollPositions);
		virtual void SetEnabled (bool bNewEnabled);
		virtual void SetHideWhenInsufficientScrollPos (bool bNewHideWhenInsufficientScrollPos);
		virtual void SetMiddleMousePosition (const Vector2& newPosition);

		/**
		  Quick method to adjust scroll button height to the given value.
		  Automatically adjusts the scrollbar track component to go between the buttons.
		 */
		virtual void SetScrollButtonHeight (FLOAT newScrollButtonHeight);


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		virtual INT GetScrollPosition () const;
		virtual INT GetMaxScrollPosition () const;
		virtual INT GetLastVisibleScrollPosition () const;
		virtual INT GetNumVisibleScrollPositions () const;
		virtual bool GetEnabled () const;
		virtual bool GetHideWhenInsufficientScrollPos () const;
		virtual Vector2 GetMiddleMousePosition () const;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  Hook to initialize default properties for the top/bottom scroll buttons.
		 */
		virtual void InitializeScrollButtons ();

		/**
		  Hook to initialize the Scrollbar Track Component.
		 */
		virtual void InitializeTrackComponent ();

		virtual void InitializePanningMiddleMouse ();

		/**
		  Considers all factors that determine if the top/bottom scroll buttons should be enabled or not.
		 */
		virtual void EvaluateScrollButtonEnabledness ();

		/**
		  Considers all factors that determine if the scrollbar should be visible or not.
		 */
		virtual void EvaluateScrollVisibility ();

		/**
		  Computes the time interval needed before HandleMiddleMouseMove is called again.
		 */
		virtual FLOAT CalculateMiddleMouseMoveInterval ();


		/*
		=====================
		  Event Handlers
		=====================
		*/

	protected:
		/**
		  Used to determine the vertical position offset for the track component.
		 */
		virtual FLOAT HandleCalcTrackPosOffset () const;

		/**
		  Invoked whenever either scroll button was pressed.
		 */
		virtual void HandleScrollButtonPressed (ButtonComponent* uiComponent);

		/**
		  Invoked whenever either scroll button was released.
		 */
		virtual void HandleScrollButtonReleased (ButtonComponent* uiComponent);

		/**
		  Invoked whenever the track component wishes to change the scroll index.
		 */
		virtual void HandleTrackChangedPosition (INT newScrollPosition);

		/**
		  Invoked whenever enough time elapsed to move the scrollbar since to middle mouse button was pressed.
		 */
		virtual bool HandleMiddleMouseMove ();

		/**
		  Returns false whenever this scrollbar should no longer clamp the mouse pointer.
		 */
		virtual bool HandleLimitCallback (const sf::Event::MouseMoveEvent& mouseEvent);

		/**
		  Invoked whenever enough time elapsed to move the scrollbar up since user is holding the top scroll button.
		 */
		virtual bool HandleScrollUpInterval ();

		/**
		  Invoked whenever enough time elapsed to move the scrollbar down since user is holding the bottom scroll button.
		 */
		virtual bool HandleScrollDownInterval ();
	};
}

#endif
#endif