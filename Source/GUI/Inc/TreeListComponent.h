/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TreeListComponent.h
  A component that displays a list of GUIDataElements and their relationships between parents
  and their children.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef TREELISTCOMPONENT_H
#define TREELISTCOMPONENT_H

#include "GUIComponent.h"

#if INCLUDE_GUI

#include "GUIDataElement.h"
#include "FocusInterface.h"

namespace SD
{
	class LabelComponent;
	class ButtonComponent;
	class ScrollbarComponent;

	class TreeListComponent : public GUIComponent, public FocusInterface
	{
		DECLARE_CLASS(TreeListComponent)


		/*
		=====================
		  Datatypes
		=====================
		*/

	public:
		struct SDataBranch
		{
		public:
			/* Data element that holds a pointer to the actual data.  This object (not the data it points to) will be deleted when the tree list is cleared.
			  This object is not deleted in this struct's destructor since there could be multiple SDataBranches that point to the same object.  Instead the object
			  that owns this branch (such as the TreeListComponent) should delete this object. */
			BaseGUIDataElement* Data;
			std::vector<SDataBranch*> Children;

			/* If true, then the children branches are visible. */
			bool bExpanded;

			/* Information regarding the branch where one of its children is this branch. */
			SDataBranch* ParentBranch;

			SDataBranch ()
			{
				Data = nullptr;
				bExpanded = false;
				ParentBranch = nullptr;
			}

			SDataBranch (BaseGUIDataElement* inData)
			{
				Data = inData;
				bExpanded = false;
				ParentBranch = nullptr;
			}

			SDataBranch (BaseGUIDataElement* inData, SDataBranch* inParentBranch)
			{
				Data = inData;
				bExpanded = false;
				ParentBranch = inParentBranch;
				if (inParentBranch != nullptr)
				{
					inParentBranch->Children.push_back(this);
				}
			}

			SDataBranch (BaseGUIDataElement* inData, SDataBranch* inParentBranch, std::vector<SDataBranch*> inChildren)
			{
				Data = inData;
				ParentBranch = inParentBranch;
				if (inParentBranch != nullptr)
				{
					inParentBranch->Children.push_back(this);
				}

				Children = inChildren;
				bExpanded = false;
			}

			virtual ~SDataBranch ()
			{
			}

			/**
			  Assembles the relationship between parent and child branches.
			 */
			static void LinkBranches (SDataBranch& outParent, SDataBranch& outChild);

			bool IsValid () const;

			/**
			  Recursively checks parent branches, and returns true if all parents' bExpanded flag is true.
			 */
			bool IsVisible () const;

			/**
			  Returns the number of branches between this branch and the root branch.
			 */
			INT GetNumParentBranches () const;

			/**
			  Returns the number of sub branches from this branch (this includes the children branches of this branch's children).
			 */
			INT CountNumChildren (bool bIncludeCollapsed) const;
		};
		
		/* Maps a button to a data branch. */
		struct SButtonMapping
		{
			ButtonComponent* Button;
			SDataBranch* Branch;

			SButtonMapping ()
			{
				Button = nullptr;
				Branch = nullptr;
			}

			SButtonMapping (ButtonComponent* inButton, SDataBranch* inBranch)
			{
				Button = inButton;
				Branch = inBranch;
			}
		};

	private:
		/**
		  Struct that contains specific data regarding assembling connections between branches.
		 */
		struct SBranchConnectionInfo
		{
			SDataBranch* ParentBranch;
			SDataBranch* ChildBranch;

			/* Point at which the vertical connection will connect to meet with the parent. */
			Vector2 ParentConnectPosition;

			SBranchConnectionInfo ()
			{
				ParentBranch = nullptr;
				ChildBranch = nullptr;
			}

			SBranchConnectionInfo (SDataBranch* inParentBranch, SDataBranch* inChildBranch, const Vector2& inConnectPos)
			{
				ParentBranch = inParentBranch;
				ChildBranch = inChildBranch;
				ParentConnectPosition = inConnectPos;
			}
		};


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* Callback to invoke whenever an option was selected where the parameter is the index of the list item. */
		SDFunction<void, INT> OnOptionSelected;

		/* When focused, this is the amount the browse index will jump when the control key is held. */
		INT BrowseJumpAmount;

		/* Determines how far do the children shift to the left relevative to their parents (in pixels). */
		FLOAT IndentSize;

		/* Determines the height of a single line that represents a single item. */
		FLOAT BranchHeight;

		/* The thickness (width of vertical connectors, height for horizontal connectors) in pixels. */
		FLOAT ConnectionThickness;
		sf::Color ConnectionColor;

	protected:
		/* Since the RefreshTree function is wasteful (destroys old buttons and labels), this counter keeps track of number of pending destroy entities for all trees. */
		static INT NumPendingDeleteComponents;

		/* Maximum number the NumPendingDeleteComponents may achieve before the tree list forces the garbage collector.  If negative, then this will not force garbage collection. */
		static INT MaxNumPendingDeleteComponents;

		/* List of available options.  Each option may have a pointer to an object but does not check if the object is valid. */
		std::vector<SDataBranch*> TreeList;

		/* Selected item in the tree list where if all children of the tree was expanded, this selected index increments from top to bottom (if the tree was top-down). */
		INT SelectedItemIdx;

		/* Selected item within the TreeList. */
		SDataBranch* SelectedBranch;

		/* Contrast to the SelectedItemIdx, this index shifts along the List (moving the highlight bar) without actually selecting the item. */
		INT BrowseIndex;

		/* List of buttons associated for each branch option.  These buttons are continuously spawned/destroyed whenever the list updates or the scroll position changes.
		The buttons are also sorted based on index position. */
		std::vector<SButtonMapping> ExpandButtons;

		/* Solid colors that'll represent the connections between parents and their children. */
		std::vector<SolidColorRenderComponent*> BranchConnections;
		std::vector<AbsTransformComponent*> ConnectionTransforms;

		/* List of labels where each element represents a single branch within tree. */
		std::vector<LabelComponent*> TreeLabels;

		/* Texture to use for the ExpandButtons that could expand to sub branches. */
		Texture* ExpandButtonTexture;

		/* Texture to use for the ExpandButtons that could collapse the branch. */
		Texture* CollapseButtonTexture;
		
		/* Highlight bar over the current selected item. */
		SolidColorRenderComponent* SelectedBar;

		/* Highlight bar over the current browsed item.  This solid color will also highlight over a region when hovering over the expand buttons. */
		SolidColorRenderComponent* HoverBar;

		AbsTransformComponent* SelectedTransform;
		AbsTransformComponent* HoverTransform;

		/* If true, then the HoverTransform will not update its attributes on mouse movement. */
		bool bIgnoreHoverMovement;

		ScrollbarComponent* ListScrollbar;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual void BeginObject () override;
		virtual void Destroy () override;
		virtual void SetWindowHandle (Window* newWindowHandle) override;

		virtual bool CanBeFocused () const override;
		virtual void GainFocus () override;
		virtual void LoseFocus () override;
		virtual bool CaptureFocusedInput (const sf::Event& keyEvent) override;
		virtual bool CaptureFocusedText (const sf::Event& keyEvent) override;

	protected:
		virtual void HandleSizeChange () override;
		virtual bool ShouldHaveExternalInputComponent () const override;
		virtual void ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;
		virtual bool ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;
		virtual void CleanUpInvalidPointers () override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Sorts the data branch such that all children of the branch is listed before the siblings of the branch.
		 */
		static void SortDataList (std::vector<SDataBranch*>& outDataList);

		static void SetMaxNumPendingDeleteComponents (INT newMaxNumPendingDeleteComponents);

		virtual void ExpandBranch (INT branchIdx);
		virtual void ExpandBranch (SDataBranch* branch);
		virtual void CollapseBranch (INT branchIdx);
		virtual void CollapseBranch (SDataBranch* branch);

		virtual void ExpandAll ();
		virtual void CollapseAll ();

		/**
		  Moves scroll bar position such that the specified branch index is within view.
		 */
		virtual void JumpScrollbarToViewBranch (INT branchIdx);

		virtual void SetBrowseIndex (INT newHoverIndex);

		/**
		  Sets the selected item by index value.
		 */
		virtual void SetSelectedItem (INT itemIndex);

		/**
		  Sets the selected item by branch.
		 */
		virtual void SetSelectedItem (SDataBranch* item);

		/**
		  Replaces the entire tree with the new list.
		 */
		virtual void SetTreeList (const std::vector<SDataBranch*>& newTreeList);

		/**
		  Deletes all tree elements, and destroys all sub GUI components associated with those items.
		  The data braches, themselves, are deleted.
		 */
		virtual void ClearTreeList ();		


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		virtual SolidColorRenderComponent* GetSelectedBar () const;
		virtual SolidColorRenderComponent* GetHoverBar () const;
		virtual INT GetBrowseIndex () const;

		/**
		  This function retrieves the data branch based on the specified index from this entities' TreeList.
		  The index order is sorted from root to immediate children to siblings (same order of TreeBranchIterator).
		 */
		virtual SDataBranch* GetBranchFromIdx (INT treeIdx) const;


		/*
		=====================
		  Implementation
		=====================
		*/
		
	protected:
		virtual void InitializeScrollbar ();
		virtual void InitializeSolidColors ();

		/**
		  Updates selected color transforms.  If bInvokeCallback is true, then the OnOptionSelected callback is invoked.
		 */
		virtual void UpdateSelectedItem (bool bInvokeCallback);

		/**
		  Searches through the tree label list, and returns the label that is currently within bounds of the specified window coordinates.
		 */
		virtual LabelComponent* FindLabelAtCoordinates (FLOAT x, FLOAT y) const;

		/**
		  Iterates through the tree list to count how many lines are currently visible (including the branches beyond the scroll visibility range).
		 */
		virtual INT CountNumVisibleEntries () const;

		/**
		  Iterates through the tree until it finds the branch that is the right amount of distance from from the anchor index.
		  All index values align with the index values of the TreeList vector.

		  @Param anchorBranchIdx:  The branch index to base the distance from.  Result = anchorBranch - distanceAboveAnchor
		  @Param distanceFromAnchor:  The number of branches between the anchor branch and return value.  Negative values will search for branches above the anchor branch.
		  @Param bIncludeCollapsed:  If false, then all invisible branches (via collapsed flag) will be ignored when measuring distance.
		  @Return:  The branch index that is at the right distance away from the anchor branch.  Becomes INT_INDEX_NONE if there aren't any branches at the right distance.
		 */
		virtual INT FindBranchIdxFromAnchor (INT anchorBranchIdx, INT distanceFromAnchor, bool bIncludeCollapsed) const;

		/**
		  Figures out which branch should be displayed on top of this component considering collapsed branches and scroll position.
		 */
		virtual SDataBranch* FindFirstVisibleBranch () const;

		/**
		  Iterates through the tree lists backwards and returns the first visible branch idx regardless of scroll position.
		 */
		INT FindLastVisibleBranchIdx () const;

		/**
		  Returns the expand button that maps to the branch index.
		 */
		virtual ButtonComponent* FindExpandButtonAtIdx (INT branchIdx) const;

		/**
		  Iterates through each data branch and sets their bExpanded flag equal to the specified value.
		 */
		virtual void SetExpandForAllBranches (bool bExpanded);
		
		/**
		  Recalculates each component of the tree list in a way it maintains its structure.
		 */
		virtual void RefreshComponentTransforms ();

		/**
		  Calculates the hover transform so that it covers all visible children branches for the given ExpandButton.
		 */
		virtual void CalcHoverTransform (unsigned int expandButtonIdx);

		/**
		  Updates the hover transform so that it covers over the specified tree list index.
		 */
		virtual void CalcHoverTransformAtIdx (INT branchIdx);

		/**
		  Calculates the selected bar's transformation and visibility based on the selected item and current scroll position.
		 */
		virtual void UpdateSelectedBar ();

		/**
		  Sets the given transform component to cover the specified branch.  Returns true if the transformation is visible (within range of scroll position).
		 */
		virtual bool CalcTransformAtIdx (AbsTransformComponent* transform, SDataBranch* branch);

		/**
		  Destroys all buttons, connections, and labels that make up the tree list assembly.  This does not clear the tree list vector, itself.
		 */
		virtual void DestroyTreeComponents ();

		/**
		  Replaces all button and labels to reflect the recent changes in tree structure while considering current scroll position.
		 */
		virtual void RefreshTree ();

		/**
		  Creates a pair of solid color components such that they connect with parent and child points.
		  Creates a horizontal connection if the child and parent X position is greater than connector thickness.
		 */
		virtual void CreateConnections (const Vector2& parentPt, const Vector2 childPt);


		/*
		=====================
		  Event Handlers
		=====================
		*/

	protected:
		virtual void HandleExpandButtonClicked (ButtonComponent* uiComponent);
		virtual void HandleExpandButtonRolledOver (GUIComponent* uiComponent);
		virtual void HandleExpandButtonRolledOut (GUIComponent* uiComponent);
		virtual void HandleScrollPosChanged (INT newScrollPosition);


		/*
		=====================
		  Templates
		=====================
		*/

	public:
		template <class T>
		GUIDataElement<T>* GetSelectedItem () const
		{
			SDataBranch* selectedBranch = GetBranchFromIdx(SelectedItemIdx);
			if (selectedBranch != nullptr)
			{
				return dynamic_cast<GUIDataElement<T>*>(selectedBranch->Data);
			}

			return nullptr;
		}

		/**
		  Searches through this component's branches, and selects the first branch it finds that contains matching data with specified targetData.
		  This function is not recommended if there are duplicated entries within the tree.
		 */
		template <class T>
		void SetSelectedItem (T* targetData)
		{
			if (TreeList.size() <= 0)
			{
				return;
			}

			INT oldSelectedIdx = SelectedItemIdx;
			INT iterIdx = -1;
			SelectedItemIdx = INT_INDEX_NONE;
			SelectedBranch = nullptr;

			//Find branch containing the targetData
			for (TreeBranchIterator<SDataBranch> iter(TreeList.at(0)); iter.GetSelectedBranch() != nullptr; iter++)
			{
				iterIdx++;
				GUIDataElement<T>* dataElement = dynamic_cast<GUIDataElement<T>*>(iter.GetSelectedBranch()->Data);
				if (dataElement != nullptr && dataElement->Data == targetData)
				{
					SelectedBranch = iter.GetSelectedBranch();
					SelectedItemIdx = iterIdx;
					break;
				}
			}

			UpdateSelectedItem(oldSelectedIdx != SelectedItemIdx);
		}
	};
}

#endif
#endif