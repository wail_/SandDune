/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TextFieldRenderComponent.h
  A render component that handles rendering text, cursor blinkers, and
  text highlighting for the TextFieldComponent.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef TEXTFIELDRENDERCOMPONENT_H
#define TEXTFIELDRENDERCOMPONENT_H

#include "TextRenderComponent.h"

#if INCLUDE_GUI

namespace SD
{
	class TextFieldComponent;

	class TextFieldRenderComponent : public TextRenderComponent
	{
		DECLARE_CLASS(TextFieldRenderComponent)


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* Reference to the shader that'll be responsible for rendering the highlight effect. */
		//TODO:  Implement shaders and correct all references to HighlightingShader
		//Shader* HighlightingShader;

		/* Reference to TextField that's responsible for placing the cursor positions. */
		TextFieldComponent* OwningTextField;

		/* Determines the time interval the cursor will toggle visibility. */
		FLOAT CursorBlinkInterval;

		/* Parameters that adjusts how the cursor appears. */
		sf::Color CursorColor;
		FLOAT CursorWidth;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual void Render (const Camera* targetCamera, const Window* targetWindow) override;
		virtual void CleanUpInvalidPointers () override;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  Given the cursor position, this function will find the absolute coordinates where the cursor should be drawn.
		 */
		virtual sf::Vector2f FindCursorCoordinates (INT cursorIndex);
	};
}

#endif
#endif