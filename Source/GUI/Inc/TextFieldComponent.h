/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TextFieldComponent.h
  A component where the user may edit scrollable text with mouse controls,
  and short cut controls such as copy/paste.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef TEXTFIELDCOMPONENT_H
#define TEXTFIELDCOMPONENT_H

/*
These are waiting for UI system.  TextAreaDrawComponent shouldn't care about these.
TODO: Can enter text at a given index position
TODO: Copy:Paste
TODO: Mouse control.
TODO:  Test this functionality
*/

#include "GUIComponent.h"
#include "RenderComponent.h"

#if INCLUDE_GUI

namespace SD
{
	class FrameComponent;
	class TextFieldLabelComponent;
	class TextFieldRenderComponent;
	class ScrollbarComponent;

	class TextFieldComponent : public GUIComponent
	{
		DECLARE_CLASS(TextFieldComponent)


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* When the user entered text beyond the text boundaries, and the text field needs
		scroll further, the ScrollJumpInterval determines how much scrolling is done.
		If this TextField is single line, then this determines how far does it horizontally scrolls. */
		INT ScrollJumpInterval;

	protected:
		/* Label component that'll determine how the text appears within the box. */
		TextFieldLabelComponent* TextLabel;

		/* Frame component that'll render the background. */
		//TODO:  consider moving the frame component to be a component of the label component (not over the scrollbar)
		FrameComponent* Background;

		/* Reference to the render component draw the cursor and the highlighted shader. */
		TextFieldRenderComponent* RenderComponent;

		ScrollbarComponent* Scrollbar;

		/* If true, then the user may edit the text of this field. */
		bool bEditable;

		/* If true, then the user may drag over text for copying. */
		bool bSelectable;

		/* If true, then the new line characters are prohibited. */
		bool bSingleLine;

		/* Only applicable to single line text fields, this determines how far from the beginning has the text scrolled to the right. */
		INT HorizontalScrollPosition;

		/* Contains full text including the text beyond the scroll bar.  Use TextLabel->GetTextContent() to get visible text */
		DString FullText;

		/* Index position where the cursor resides.  This also determines the "pivot" point when dragging for highlighting */
		INT CursorPosition;

		/* Line at which the CursorPosition may be located.  Becomes negative if CursorPosition is not set or if bSingleLine is true. */
		INT ActiveLineIndex;

		/* Other end of the selected text.  Text between this value and the CursorPosition is highlighted.  Disabled if negative. */
		INT HighlightEndPosition;

		//TODO:  Character limits?
		//TODO:  Set scrollbar visibility based on MaxNumLines < NumVisibleLines

	private:
		/* True if the user is dragging the mouse over the text for highlighting. */
		bool bDraggingOverText;

		/* True if the text field is currently overriding the mouse pointer's icon. */
		bool bOverridingMousePointerIcon;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		//Super functions
		virtual void InitProps () override;
		virtual void BeginObject () override;
		virtual void SetWindowHandle (Window* newWindowHandle) override;
		virtual void Destroy () override;

	protected:
		virtual void InitializeInputComponent () override;
		virtual bool ShouldHaveInputComponent () const override;

		virtual void ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) override;
		virtual bool ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) override;

		//Super functions
		virtual void CleanUpInvalidPointers () override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Returns true if the current cursor position is visible.  Returns false if cursor position is not set.
		 */
		virtual bool CursorIsVisible () const;

		/**
		  Deletes old Text label and replaces it with newTextLabel.
		 */
		virtual void ReplaceTextLabel (TextFieldLabelComponent* newTextLabel);

		/**
		  Deletes old background and replaces it with newBackground.
		 */
		virtual void ReplaceBackground (FrameComponent* newBackground);

		/**
		  Deletes old RenderComponent and replaces it with newRenderComponent.
		 */
		virtual void ReplaceRenderComponent (TextFieldRenderComponent* newRenderComponent);

		/**
		  Deletes old Scrollbar and replaces it with newScrollbar.
		  Scrollbars are optional; pass in a nullptr in parameter to disable the scrollbar.
		 */
		virtual void SetScrollbar (ScrollbarComponent* newScrollbar);

		/**
		  Overrides the current FullText with the given new string (also updates the LabelComponent's text).
		  Updating the text will not cause the scroll bar position to move unless the current position index is greater than max.
		  If bChangesAtCursor is true, then only text after the cursor will be affected with the changes.
		 */
		virtual void SetFullText (DString newContent, bool bChangesAtCursor);

		virtual void SetEditable (bool bNewEditable);

		virtual void SetSelectable (bool bNewSelectable);

		virtual void SetSingleLine (bool bNewSingleLine);

		virtual void SetHorizontalScrollPosition (INT newHorizontalScrollPosition);

		/**
		  Updates the cursor position and the scroll position if necessary.
		  Automatically clamps the newPosition to the length of the text.
		 */
		virtual void SetCursorPosition (INT newPosition);
		virtual void SetHighlightEndPosition (INT newPosition);


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		virtual LabelComponent* GetTextLabel ();
		virtual FrameComponent* GetBackground ();
		virtual TextFieldRenderComponent* GetRenderComponent ();
		virtual ScrollbarComponent* GetScrollbar ();
		virtual bool GetEditable () const;
		virtual bool GetSelectable () const;
		virtual bool GetSingleLine () const;
		virtual INT GetHorizontalScrollPosition () const;
		virtual DString GetFullText () const;
		virtual INT GetCursorPosition () const;
		virtual INT GetHighlightEndPosition () const;

		/**
		  If bSingleLine is true, then it retrieves the horizontal scroll position.
		  Otherwise, it'll retrieve the vertical scroll position from the scroll bar.
		 */
		virtual INT GetScrollPosition () const;

		/**
		  Returns the text section that's selected.
		 */
		virtual DString GetSelectedText () const;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		virtual void InitializeTextLabelComponent ();
		virtual void InitializeScrollbarComponent ();
		virtual void InitializeBackgroundComponent ();
		virtual void InitializeRenderComponent ();

		/**
		  Links all pointers and sets variables based on the subcomponents.
		 */
		virtual void LinkComponents ();

		//TODO:  Consider replacing this function with the mouse pointer stack callback instead
		virtual void UpdateMousePointerIcon (const sf::Event::MouseMoveEvent& sfmlEvent);

		/**
		  Finds the character index position based on the given absolute mouse coordinates.
		 */
		virtual INT FindCharacterIndex (INT absX, INT absY);

		/**
		  Calculates the ActiveLineIndex based on the current CursorPosition.
		 */
		virtual void CalculateActiveLineIndex ();

		/**
		  Deletes the selected text from the text field.
		 */
		virtual void DeleteSelectedText ();

		/**
		  Updates the scroll bar's scroll range to the text length.
		 */
		virtual void UpdateScrollbarLength ();

		/**
		  Updates the highlight cursor position before the cursor position is about to jump.
		  In short, it simply sets the highlight cursor position to cursor position if there is no selection.
		  Does nothing if highlight cursor position is already set.
		  If bHighlight is false, then it deselects highlighted text.
		 */
		virtual void UpdateHighlightPositionPriorToMove (bool bShouldHighlight);

		/**
		  Moves the cursor to the beginning of the previous word.
		 */
		virtual void JumpCursorToPrevWord ();

		/**
		  Moves the cursor to the beginning of the next word.
		 */
		virtual void JumpCursorToNextWord ();

		/**
		  Moves the cursor to the beginning of the line.
		 */
		virtual void JumpCursorToBeginningLine ();

		/**
		  Moves the cursor to the end of the line.
		 */
		virtual void JumpCursorToEndLine ();


		/*
		=====================
		  Event Handlers
		=====================
		*/

		virtual bool HandleCaptureInput (sf::Event keyEvent);
		virtual bool HandleCaptureText (sf::Event keyEvent);

		/**
		  Invoked whenever the scrollbar component changed scroll position.
		 */
		virtual void HandleScrollPositionChanged (INT newScrollPosition);

		virtual void HandleMaxNumLinesChanged (INT newMaxNumLines);

		/**
		  Callback to check if this TextField should continue overriding the mouse pointer.
		  Returns false if this entity should no longer override the pointer.
		 */
		virtual bool HandleMouseIconOverride (const sf::Event::MouseMoveEvent& sfmlEvent);
	};
}

#endif
#endif