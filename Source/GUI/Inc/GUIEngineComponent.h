/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GUIEngineComponent.h
  Loads default resources such as fonts and icons.  This also instantiates
  global GUI objects such as themes and a mouse pointer.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef GUIENGINECOMPONENT_H
#define GUIENGINECOMPONENT_H

//TODO:  Remove TextAreaDrawComponent
//TODO:  Replace old console with new-improved GUI Console

#include "GUI.h"

#if INCLUDE_GUI

namespace SD
{
	class GUITheme;

	class GUIEngineComponent : public EngineComponent
	{
		DECLARE_ENGINE_COMPONENT(GUIEngineComponent)


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* UI Theme to spawn when this component initializes. */
		const DClass* GUIThemeClass;

	protected:
		/* Reference to the active GUITheme that determines the overall appearance for all UI. */
		GUITheme* RegisteredGUITheme;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitializeComponent () override;
		virtual void ShutdownComponent () override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		virtual void SetGUITheme (GUITheme* newGUITheme);


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		virtual GUITheme* GetGUITheme () const;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		virtual void ImportGUIEssentialTextures ();
		virtual void ImportGUIEssentialFonts ();
		virtual const DClass* GetGUIThemeClass ();
	};
}

#endif
#endif