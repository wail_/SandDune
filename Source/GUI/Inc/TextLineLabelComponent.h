/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TextLineLabelComponent.h
  A label component that renders text within a bounding box.
  Divided from its parent class (LabelComponent), this class notifies
  the owning TextField about the changed text so that the TextField
  may adjust its cursor positions.  This also forces single line text
  that automatically trims left/right ends of the text that are beyond
  the boundary boxes (instead of word wrapping).

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef TEXTLINELABELCOMPONENT_H
#define TEXTLINELABELCOMPONENT_H

#include "TextFieldLabelComponent.h"

#if INCLUDE_GUI

namespace SD
{
	class TextLineLabelComponent : public TextFieldLabelComponent
	{
		DECLARE_CLASS(TextLineLabelComponent)


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;
		//virtual void SetTextContent (DString newContent, INT cursorPosition, bool bChangesAtCursor) override;

	protected:
		virtual void CalculateMaxNumLines () override;
		virtual void CalculateTrimmedTextHead (INT characterIndex) override;
	};
}

#endif
#endif