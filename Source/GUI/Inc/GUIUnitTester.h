/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GUIUnitTester.h
  Class that'll instantiate various UI Components so that the user
  may test their functionality without external interference.

  This unit tester does not have any crash conditions.  It's
  up to the user to test the components' functionality manually.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef GUIUNITTESTER_H
#define GUIUNITTESTER_H

#include "GUI.h"

#if INCLUDE_GUI

#ifdef DEBUG_MODE

namespace SD
{
	class GUIComponent;

	class GUIUnitTester : public UnitTester
	{
		DECLARE_CLASS(GUIUnitTester)


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual bool RunTests (EUnitTestFlags testFlags) const override;
		virtual bool MetRequirements (const std::vector<const UnitTester*>& completedTests) const override;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		virtual bool TestTreeBranchIterator (EUnitTestFlags testFlags) const;
		virtual void SpawnTestComponents (EUnitTestFlags testFlags) const;
	};
}

#endif //debug mode
#endif //INCLUDE_GUI
#endif //include guard