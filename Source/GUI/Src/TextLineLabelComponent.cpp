/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TextLineLabelComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GUIClasses.h"

#if INCLUDE_GUI

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(TextLineLabelComponent, TextFieldLabelComponent)

	void TextLineLabelComponent::InitProps ()
	{
		Super::InitProps();

		MaxNumLines = 1;
	}

#if 0
	void TextLineLabelComponent::SetTextContent (DString newContent, INT cursorPosition, bool bChangesAtCursor)
	{
		TextFieldComponent* textField = GetOwningTextField();
		if (textField == nullptr)
		{
			Warn(TXT("The owner of ") + GetUniqueName() + TXT(" is unable to find an owner that is a TextFieldComponent.  You'll not be able to set the content for this label."));
			return;
		}

		//Make sure a full refresh works, first
		if (true || cursorPosition < 0 || !bChangesAtCursor)
		{
			LabelComponent::SetTextContent(newContent);
		}

		/*
		//Hack:  disable clamping to word wrap the text as a whole (to get number of lines)
		bClampText = false;
		TextContent = newContent;
		UpdateTextContentToSFML(true);
		FullText = TextContent;

		//Drop the characters that are out of scope from boundaries
		TrimWordsUpTo(textField->GetScrollPosition());

		//Revert hack and clamp any text beyond the label boundaries
		LabelComponent::SetClampText(true);
		*/
	}
#endif

	void TextLineLabelComponent::CalculateMaxNumLines ()
	{
		MaxNumLines = 1; //Single line text labels always have only one line
	}

	void TextLineLabelComponent::CalculateTrimmedTextHead (INT characterIndex)
	{
		//safety check
		if (characterIndex > TextContent.at(0).Length() || characterIndex < 0)
		{
			return;
		}

		TrimmedTextHead = TextContent.at(0).SubString(0, characterIndex.Value);
	}
}

#endif