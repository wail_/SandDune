/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GUIComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GUIClasses.h"

#if INCLUDE_GUI

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(GUIComponent, EntityComponent)

	void GUIComponent::InitProps ()
	{
		Super::InitProps();

		Transformation = nullptr;
		Input = nullptr;
		ExternalBorderInput = nullptr;
		WindowHandle = nullptr;
		CurrentDrawOrder = GraphicsEngineComponent::Get()->DrawOrderUI;
		bVisible = true;

		bBoundWindowPollDelegate = false;
	}

	void GUIComponent::BeginObject ()
	{
		Super::BeginObject();

		if (!VALID_OBJECT(Transformation))
		{
			Transformation = AbsTransformComponent::CreateObject();
			if (!AddComponent(Transformation))
			{
				LOG1(LOG_WARNING, TXT("Unable to attaching GUI transformation component to %s.  It will not be able contain properties regarding is scale and position."), GetName());
				Transformation->Destroy();
				Transformation = nullptr;
			}

			//Initialize some values so that menus modifying these components with preplaced nested UI components (such as the buttons for a scrollbar) may properly scale when the targeted UI component is scaled
			Transformation->SetBaseSize(Vector2(64.f, 64.f));
		}

		if (VALID_OBJECT(Transformation))
		{
			Transformation->OnSizeChanged = SDFUNCTION(this, GUIComponent, HandleSizeChange, void);
			Transformation->OnPositionChanged = SDFUNCTION_1PARAM(this, GUIComponent, HandlePositionChange, void, const Vector2&);
		}

		if (WindowHandle == nullptr)
		{
			SetWindowHandle(GraphicsEngineComponent::Get()->GetWindowHandle());
		}

		RefreshRenderComponentVisibility();
		ConditionallySetInputComponent();
	}

	void GUIComponent::Destroy ()
	{
		//May need to unregister window polling delegate before destruction
		SetWindowHandle(nullptr);

		Super::Destroy();
	}

	void GUIComponent::AttachTo (Entity* newOwner)
	{
		Super::AttachTo(newOwner);

		GUIComponent* owningUI = dynamic_cast<GUIComponent*>(newOwner);

		//Mimic the properties of the owning component's window handle
		if (owningUI != nullptr)
		{
			SetWindowHandle(owningUI->GetWindowHandle());
			RefreshRenderComponentVisibility();
		}

		if (owningUI != nullptr)
		{
			GetTransform()->SetRelativeTo(owningUI->GetTransform());

			//Draw over the new UI owner
			SetDrawOrder(owningUI->CurrentDrawOrder + 1.f);
		}
	}

	void GUIComponent::CleanUpInvalidPointers ()
	{
		Super::CleanUpInvalidPointers();

		CLEANUP_INVALID_POINTER(Transformation)
		CLEANUP_INVALID_POINTER(Input)
		CLEANUP_INVALID_POINTER(WindowHandle)
	}

	bool GUIComponent::IsOutermostGUIComponent () const
	{
		EntityComponent* nextOwner = dynamic_cast<EntityComponent*>(GetOwner());
		while (nextOwner != nullptr)
		{
			if (dynamic_cast<GUIComponent*>(nextOwner) != nullptr)
			{
				return false; //Another GUIComponent is closer to the entity than this component
			}

			nextOwner = dynamic_cast<EntityComponent*>(nextOwner->GetOwner());
		}

		return true; //No owning components are GUI Components
	}

	void GUIComponent::SetDrawOrder (FLOAT newDrawOrder)
	{
		CurrentDrawOrder = newDrawOrder;

		SetRenderComponentsDrawOrder(newDrawOrder);

		for (unsigned int i = 0; i < Components.size(); i++)
		{
			GUIComponent* subComponent = dynamic_cast<GUIComponent*>(Components.at(i));

			if (VALID_OBJECT(subComponent))
			{
				//Draw order is one value higher than its parent to ensure the children are drawn over owner
				subComponent->SetDrawOrder(newDrawOrder + 1.f);
			}
		}
	}

	void GUIComponent::ConditionallySetInputComponent ()
	{
		bool bResult = ShouldHaveInputComponent();

		if (bResult && (Input == nullptr)) //Need to add InputComponent
		{
			InitializeInputComponent();
		}
		else if (!bResult && (Input != nullptr)) //Need to remove InputComponent
		{
			Input->DetachSelfFromOwner();
			Input->Destroy();
			Input = nullptr;
		}
	}

	void GUIComponent::SetupExternalBorderInput ()
	{
		if (VALID_OBJECT(ExternalBorderInput))
		{
			//Already established
			return;
		}

		ExternalBorderInput = InputComponent::CreateObject();
		if (!AddComponent(ExternalBorderInput))
		{
			LOG1(LOG_WARNING, TXT("Failed to attach an external border input component to %s."), GetUniqueName());
			ExternalBorderInput = nullptr;
			return;
		}

		Window* owningWindow = GetWindowHandle();
		if (VALID_OBJECT(owningWindow) && InputEngineComponent::Get() != nullptr)
		{
			InputBroadcaster* broadcaster = InputEngineComponent::Get()->FindBroadcasterForWindow(owningWindow);
			if (broadcaster != nullptr && broadcaster != ExternalBorderInput->GetInputMessenger())
			{
				broadcaster->AddInputComponent(ExternalBorderInput);
			}
		}

		ExternalBorderInput->MouseMoveDelegate = SDFUNCTION_3PARAM(this, GUIComponent, HandleExternalMouseMove, void, MousePointer*, const sf::Event::MouseMoveEvent&, const Vector2&);
		ExternalBorderInput->MouseClickDelegate = SDFUNCTION_3PARAM(this, GUIComponent, HandleExternalMouseClick, bool, MousePointer*, const sf::Event::MouseButtonEvent&, sf::Event::EventType);
		ExternalBorderInput->MouseWheelScrollDelegate = SDFUNCTION_2PARAM(this, GUIComponent, HandleExternalMouseWheelMove, bool, MousePointer*, const sf::Event::MouseWheelScrollEvent&);
	}

	void GUIComponent::ConditionallySetExternalBorderInput ()
	{
		(ShouldHaveExternalInputComponent()) ? SetupExternalBorderInput() : RemoveExternalBorderInput();
	}

	void GUIComponent::RemoveExternalBorderInput ()
	{
		if (VALID_OBJECT(ExternalBorderInput))
		{
			ExternalBorderInput->Destroy();
			ExternalBorderInput = nullptr;
		}
	}

	void GUIComponent::SetWindowHandle (Window* newWindowHandle)
	{
		if (bBoundWindowPollDelegate && VALID_OBJECT(WindowHandle))
		{
			WindowHandle->UnregisterPollingDelegate(SDFUNCTION_1PARAM(this, GUIComponent, HandleWindowPollEvent, void, const sf::Event));
			bBoundWindowPollDelegate = false;
		}

		WindowHandle = newWindowHandle;

		InputBroadcaster* windowBroadcaster = (InputEngineComponent::Get() != nullptr) ? InputEngineComponent::Get()->FindBroadcasterForWindow(WindowHandle) : nullptr;
		if (windowBroadcaster != nullptr)
		{
			//Accessing Input directly instead of referring to GetInput is intended.  No need for sub UIs to do this.
			if (VALID_OBJECT(Input))
			{
				windowBroadcaster->AddInputComponent(Input, Input->GetInputPriority());
			}

			if (VALID_OBJECT(ExternalBorderInput))
			{
				windowBroadcaster->AddInputComponent(ExternalBorderInput, ExternalBorderInput->GetInputPriority());
			}
		}

		//Only the parent GUI component should care about resolution changes since the ScalingProperties will scale the sub GUI components accordingly
		if (IsOutermostGUIComponent() && WindowHandle != nullptr)
		{
			WindowHandle->RegisterPollingDelegate(SDFUNCTION_1PARAM(this, GUIComponent, HandleWindowPollEvent, void, const sf::Event));
			bBoundWindowPollDelegate = true;
		}

		//Update all subGUIComponents' WindowHandle so they, too, will know which window to render to.
		for (ComponentIterator iter(this, false); iter.GetSelectedComponent() != nullptr; iter++)
		{
			if (dynamic_cast<GUIComponent*>(iter.GetSelectedComponent()) != nullptr)
			{
				dynamic_cast<GUIComponent*>(iter.GetSelectedComponent())->SetWindowHandle(newWindowHandle);
			}
		}
	}

	MousePointer* GUIComponent::GetMousePointer () const
	{
		if (VALID_OBJECT(GetWindowHandle()))
		{
			InputBroadcaster* broadcaster = InputEngineComponent::Get()->FindBroadcasterForWindow(GetWindowHandle());
			if (broadcaster != nullptr)
			{
				return broadcaster->GetAssociatedMouse();
			}
		}

		//try main mouse pointer
		return MousePointer::GetMousePointer();
	}
	
	void GUIComponent::SetVisibility (bool bNewVisibility)
	{
		bVisible = bNewVisibility;

		RefreshRenderComponentVisibility();
	}

	bool GUIComponent::IsVisible (bool bCheckVisChain) const
	{
		if (!bCheckVisChain)
		{
			return bVisible;
		}

		if (!bVisible)
		{
			return false;	
		}

		if (dynamic_cast<GUIComponent*>(Owner) != nullptr)
		{
			return dynamic_cast<GUIComponent*>(Owner)->IsVisible();
		}

		return true;
	}

	GUIComponent* GUIComponent::GetParentGUIComponent ()
	{
		GUIComponent* parentComponent = dynamic_cast<GUIComponent*>(GetOwner());
		if (!parentComponent)
		{
			return this;
		}

		return parentComponent->GetParentGUIComponent();
	}

	AbsTransformComponent* GUIComponent::GetTransform () const
	{
		return Transformation;
	}

	Window* GUIComponent::GetWindowHandle () const
	{
		return WindowHandle;
	}

	InputComponent* GUIComponent::GetInput () const
	{
		if (Input)
		{
			return Input;
		}

		if (dynamic_cast<GUIComponent*>(Owner))
		{
			return dynamic_cast<GUIComponent*>(Owner)->GetInput();
		}

		return nullptr;
	}

	void GUIComponent::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
	{

	}

	bool GUIComponent::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
	{
		return false;
	}

	bool GUIComponent::ExecuteMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
	{
		return false;
	}

	bool GUIComponent::AcceptsMouseEvents (const unsigned int& mousePosX, const unsigned int& mousePosY) const
	{
		return (Transformation->WithinBounds(FLOAT::MakeFloat(mousePosX), FLOAT::MakeFloat(mousePosY)));
	}

	void GUIComponent::InitializeInputComponent ()
	{
		Input = InputComponent::CreateObject();
		if (AddComponent(Input))
		{
			Window* owningWindow = GetWindowHandle();
			if (VALID_OBJECT(owningWindow) && InputEngineComponent::Get() != nullptr)
			{
				InputBroadcaster* broadcaster = InputEngineComponent::Get()->FindBroadcasterForWindow(owningWindow);
				if (broadcaster != nullptr && broadcaster != Input->GetInputMessenger())
				{
					broadcaster->AddInputComponent(Input);
				}
			}

			Input->MouseMoveDelegate = SDFUNCTION_3PARAM(this, GUIComponent, HandleMouseMove, void, MousePointer*, const sf::Event::MouseMoveEvent&, const Vector2&);
			Input->MouseClickDelegate = SDFUNCTION_3PARAM(this, GUIComponent, HandleMouseClick, bool, MousePointer*, const sf::Event::MouseButtonEvent&, sf::Event::EventType);
			Input->MouseWheelScrollDelegate = SDFUNCTION_2PARAM(this, GUIComponent, HandleMouseWheelMove, bool, MousePointer*, const sf::Event::MouseWheelScrollEvent&);
		}
		else
		{
			LOG1(LOG_WARNING, TXT("Failed to add input component to %s"), GetUniqueName());
			Input = nullptr; //Input is already destroyed on AddComponent if it failed
		}
	}

	bool GUIComponent::ShouldHaveInputComponent () const
	{
		//For optimization purposes:  Only register this gui component if not owned by a GUIComponent.
		//The parent gui components are responsible for passing mouse events down to its components.
		return (!VALID_OBJECT_CAST(Owner, GUIComponent*) && IsVisible());
	}

	bool GUIComponent::ShouldHaveExternalInputComponent () const
	{
		return false;
	}

	void GUIComponent::SetRenderComponentsDrawOrder (FLOAT newDrawOrder)
	{
		for (ComponentIterator iter(this, false); iter.GetSelectedComponent() != nullptr; iter++)
		{
			RenderComponent* renderObj = dynamic_cast<RenderComponent*>(iter.GetSelectedComponent());
			if (VALID_OBJECT(renderObj))
			{
				renderObj->SetDrawOrder(newDrawOrder);
			}
		}
	}

	void GUIComponent::RefreshRenderComponentVisibility ()
	{
		bool currentVisibility = IsVisible();

		//Iterate through all immediate render components to update their visibility states.
		for (ComponentIterator iter(this, false); iter.GetSelectedComponent() != nullptr; iter++)
		{
			if (dynamic_cast<RenderComponent*>(iter.GetSelectedComponent()) != nullptr)
			{
				dynamic_cast<RenderComponent*>(iter.GetSelectedComponent())->SetVisibility(currentVisibility);
			}
		}

		//Iterate through all sub GUI Components to check their visibility states
		for (ComponentIterator iter(this, false); iter.GetSelectedComponent() != nullptr; iter++)
		{
			if (dynamic_cast<GUIComponent*>(iter.GetSelectedComponent()) != nullptr)
			{
				dynamic_cast<GUIComponent*>(iter.GetSelectedComponent())->RefreshRenderComponentVisibility();
			}
		}
	}

	void GUIComponent::HandleSizeChange ()
	{

	}

	void GUIComponent::HandlePositionChange (const Vector2& deltaMove)
	{

	}

	void GUIComponent::HandleWindowPollEvent (const sf::Event newWindowEvent)
	{
		if (newWindowEvent.type == sf::Event::Resized)
		{
			HandleSizeChange();
		}
	}

	void GUIComponent::HandleMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
	{
		if (!AcceptsMouseEvents(sfmlEvent.x, sfmlEvent.y))
		{
			return;
		}

		for (unsigned int i = 0; i < Components.size(); i++)
		{
			if (VALID_OBJECT_CAST(Components.at(i), GUIComponent*))
			{
				dynamic_cast<GUIComponent*>(Components.at(i))->HandleMouseMove(mouse, sfmlEvent, deltaMove);
			}
		}

		ExecuteMouseMove(mouse, sfmlEvent, deltaMove);
	}

	bool GUIComponent::HandleMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
	{
		if (!AcceptsMouseEvents(sfmlEvent.x, sfmlEvent.y))
		{
			return false;
		}

		//Invoke children before parent since the children are typically drawn over the parent.
		for (unsigned int i = 0; i < Components.size(); i++)
		{
			if (VALID_OBJECT_CAST(Components.at(i), GUIComponent*))
			{
				if (dynamic_cast<GUIComponent*>(Components.at(i))->HandleMouseClick(mouse, sfmlEvent, eventType))
				{
					return true;
				}
			}
		}

		return (ExecuteMouseClick(mouse, sfmlEvent, eventType));
	}

	bool GUIComponent::HandleMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
	{
		if (!AcceptsMouseEvents(sfmlEvent.x, sfmlEvent.y))
		{
			return false;
		}

		for (unsigned int i = 0; i < Components.size(); i++)
		{
			if (VALID_OBJECT_CAST(Components.at(i), GUIComponent*))
			{
				if (dynamic_cast<GUIComponent*>(Components.at(i))->HandleMouseWheelMove(mouse, sfmlEvent))
				{
					return true;
				}
			}
		}

		return (ExecuteMouseWheelMove(mouse, sfmlEvent));
	}

	void GUIComponent::HandleExternalMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
	{
		if (GetTransform() && !GetTransform()->WithinBounds(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y)))
		{
			ExecuteMouseMove(mouse, sfmlEvent, deltaMove);
		}
	}

	bool GUIComponent::HandleExternalMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
	{
		if (GetTransform() && !GetTransform()->WithinBounds(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y)))
		{
			return ExecuteMouseClick(mouse, sfmlEvent, eventType);
		}

		return false;
	}

	bool GUIComponent::HandleExternalMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
	{
		if (GetTransform() && !GetTransform()->WithinBounds(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y)))
		{
			return ExecuteMouseWheelMove(mouse, sfmlEvent);
		}

		return false;
	}
}

#endif