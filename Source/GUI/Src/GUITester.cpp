/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GUITester.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GUIClasses.h"

#if INCLUDE_GUI
#ifdef DEBUG_MODE

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(GUITester, Entity)

	void GUITester::InitProps ()
	{
		Super::InitProps();

		StageDimensions = Rectangle<FLOAT>(192.f, 32.f, 792.f, 544.f);

		StageFrame = nullptr;
		PanelButtonCounter = 0;

		MaxButtonsPerPage = 9;
		ButtonBarPageIdx = 0;
		NextButtonBarPage = nullptr;
		PrevButtonBarPage = nullptr;

		TestWindowHandle = nullptr;
		TestWindowRenderer = nullptr;

		for (INT i = 0; i < 20; i++)
		{
			DropdownNumbers.push_back(i);
		}
	}

	void GUITester::Destroy ()
	{
		ClearStageComponents(); //Destroy any external entities such as GUIEntities.

		if (VALID_OBJECT(TestWindowHandle) && GraphicsEngineComponent::Get() != nullptr)
		{
			GraphicsEngineComponent::Get()->DestroyExternalWindow(TestWindowHandle);
			TestWindowHandle = nullptr;
		}

		if (VALID_OBJECT(TestWindowRenderer))
		{
			TestWindowRenderer->Destroy();
			TestWindowRenderer = nullptr;
		}

		Super::Destroy();
	}

	void GUITester::BeginUnitTest ()
	{
		TestWindowHandle = dynamic_cast<InputWindow*>(GraphicsEngineComponent::Get()->CreateExternalWindow(InputWindow::SStaticClass()));
		CHECK(TestWindowHandle != nullptr)
		TestWindowHandle->Resource = new sf::RenderWindow(sf::VideoMode(800, 600), "GUI Unit Test");
		TestWindowHandle->Resource->setMouseCursorVisible(false);
		TestWindowHandle->RegisterPollingDelegate(SDFUNCTION_1PARAM(this, GUITester, HandleWindowEvent, void, const sf::Event));

		TestWindowRenderer = RenderTarget::CreateObject();
		TestWindowRenderer->SetRenderWindow(TestWindowHandle);

		InitializeStageFrame();
		PanelButtonCounter = 0;
		CreateBaseButtons();
	}

	void GUITester::InitializeStageFrame ()
	{
		if (VALID_OBJECT(StageFrame))
		{
			return;
		}

		StageFrame = FrameComponent::CreateObject();
		if (!AddComponent(StageFrame))
		{
			StageFrame->Destroy();
			return;
		}

		StageFrame->SetWindowHandle(TestWindowHandle);
		StageFrame->SetLockedFrame(true);
		StageFrame->SetBorderThickness(1.f);
		StageFrame->RenderComponent->SetSpriteTexture(nullptr);
		StageFrame->RenderComponent->FillColor = sf::Color(48, 48, 48);
		//Render behind stage items
		StageFrame->RenderComponent->SetDrawOrder(StageFrame->RenderComponent->GetDrawOrder() - 10);
		StageFrame->GetTransform()->SetAbsCoordinates(Vector2(StageDimensions.Left, StageDimensions.Top));
		StageFrame->GetTransform()->SetBaseSize(Vector2(StageDimensions.GetWidth(), StageDimensions.GetHeight()));
	}

	void GUITester::CreateBaseButtons ()
	{
		ButtonComponent* frameButton = AddPanelButton();
		if (VALID_OBJECT(frameButton))
		{
			frameButton->SetCaptionText(TXT("Frame Component"));
			frameButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleFrameButtonClicked, void, ButtonComponent*));
		}

		ButtonComponent* labelButton = AddPanelButton();
		if (VALID_OBJECT(labelButton))
		{
			labelButton->SetCaptionText(TXT("Label Component"));
			labelButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleLabelButtonClicked, void, ButtonComponent*));
		}

		ButtonComponent* buttonButton = AddPanelButton();
		if (VALID_OBJECT(buttonButton))
		{
			buttonButton->SetCaptionText(TXT("Button Component"));
			buttonButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleButtonButtonClicked, void, ButtonComponent*));
		}

		ButtonComponent* scrollbarButton = AddPanelButton();
		if (VALID_OBJECT(scrollbarButton))
		{
			scrollbarButton->SetCaptionText(TXT("Scrollbar Component"));
			scrollbarButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleScrollbarButtonClicked, void, ButtonComponent*));
		}

		ButtonComponent* dropdownButton = AddPanelButton();
		if (VALID_OBJECT(dropdownButton))
		{
			dropdownButton->SetCaptionText(TXT("Dropdown Component"));
			dropdownButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleDropdownButtonClicked, void, ButtonComponent*));
		}

		ButtonComponent* guiEntityButton = AddPanelButton();
		if (VALID_OBJECT(guiEntityButton))
		{
			guiEntityButton->SetCaptionText(TXT("GUI Entities"));
			guiEntityButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleGUIEntityButtonClicked, void, ButtonComponent*));
		}

		ButtonComponent* hoverButton = AddPanelButton();
		if (VALID_OBJECT(hoverButton))
		{
			hoverButton->SetCaptionText(TXT("Hover Component"));
			hoverButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleHoverButtonClicked, void, ButtonComponent*));
		}

		ButtonComponent* treeButton = AddPanelButton();
		if (VALID_OBJECT(treeButton))
		{
			treeButton->SetCaptionText(TXT("Tree List Component"));
			treeButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleTreeListButtonClicked, void, ButtonComponent*));
		}

		//TODO:  Things to test:  TextLines, TextFields

		ButtonComponent* closeButton = AddPanelButton();
		if (VALID_OBJECT(closeButton))
		{
			closeButton->SetCaptionText(TXT("End UI Test"));
			closeButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleCloseClicked, void, ButtonComponent*));
		}

		if (ButtonBar.size() <= MaxButtonsPerPage)
		{
			//No need to create prev/next button since there aren't enough buttons for a second page.
			return;
		}

		//Create next and prev button page
		Vector2 buttonSize = Vector2(32.f, 24.f);
		Vector2 buttonPosition = Vector2(16.f, TestWindowHandle->GetWindowSize().Y - buttonSize.Y * 2);
		PrevButtonBarPage = ButtonComponent::CreateObject();
		if (AddComponent(PrevButtonBarPage))
		{
			PrevButtonBarPage->SetWindowHandle(TestWindowHandle);
			PrevButtonBarPage->GetTransform()->SetAbsCoordinates(buttonPosition);
			PrevButtonBarPage->GetTransform()->SetBaseSize(buttonSize);
			PrevButtonBarPage->SetBorderThickness(1.f);
			PrevButtonBarPage->SetCaptionText(TXT("<"));
			PrevButtonBarPage->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GUITester, HandlePrevPageClicked, void, ButtonComponent*));

			//Prepare button position for next page
			buttonPosition.X += buttonSize.X + 8.f;
		}
		else
		{
			PrevButtonBarPage = nullptr;
		}


		NextButtonBarPage = ButtonComponent::CreateObject();
		if (AddComponent(NextButtonBarPage))
		{
			NextButtonBarPage->SetWindowHandle(TestWindowHandle);
			NextButtonBarPage->GetTransform()->SetAbsCoordinates(buttonPosition);
			NextButtonBarPage->GetTransform()->SetBaseSize(buttonSize);
			NextButtonBarPage->SetBorderThickness(1.f);
			NextButtonBarPage->SetCaptionText(TXT(">"));
			NextButtonBarPage->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleNextPageClicked, void, ButtonComponent*));
		}
		else
		{
			NextButtonBarPage = nullptr;
		}
	}

	void GUITester::ClearStageComponents ()
	{
		for (unsigned int i = 0; i < StageComponents.size(); i++)
		{
			if (VALID_OBJECT(StageComponents.at(i)))
			{
				StageComponents.at(i)->Destroy();
			}
		}

		for (int i = 0; i < NumAlignmentFrames; i++)
		{
			AlignmentFrames[i] = nullptr;
		}

		for (unsigned int i = 0; i < GUIEntities.size(); i++)
		{
			if (VALID_OBJECT(GUIEntities.at(i)))
			{
				GUIEntities.at(i)->Destroy();
			}
		}

		ToggleButtons[0] = nullptr;
		ToggleButtons[1] = nullptr;
		ScrollbarLabel = nullptr;
		AdjustableScrollbarLabel = nullptr;
		AdjustableScrollbar = nullptr;
		ButtonBarDropdown = nullptr;
		SetFocusDropdown = nullptr;
		HoverLabel = nullptr;
		HoverButton = nullptr;
		TreeListClassBrowser = nullptr;

		StageComponents.clear();
		GUIEntities.clear();
	}

	ButtonComponent* GUITester::AddPanelButton ()
	{
		ButtonComponent* result = ButtonComponent::CreateObject();
		if (!AddComponent(result))
		{
			return nullptr;
		}

		Vector2 buttonPosition = Vector2(16.f, 32.f);
		Vector2 buttonSize = Vector2(128.f, 48.f);
		buttonPosition.Y += (56.f * PanelButtonCounter.ToFLOAT());
		result->SetWindowHandle(TestWindowHandle);
		result->GetTransform()->SetAbsCoordinates(buttonPosition);
		result->GetTransform()->SetBaseSize(buttonSize);
		ButtonBar.push_back(result);
		if (++PanelButtonCounter >= MaxButtonsPerPage)
		{
			//Reset the counter to 0 so that the next button appears on top
			PanelButtonCounter = 0;
		}

		if ((INT(ButtonBar.size() - 1) / MaxButtonsPerPage) != ButtonBarPageIdx)
		{
			//Hide this button since the current button bar page is not viewing this button.
			result->SetVisibility(false);
		}

		return result;
	}

	void GUITester::UpdateButtonBarPageIdx ()
	{
		for (unsigned int i = 0; i < ButtonBar.size(); i++)
		{
			bool bViewingButton = ((INT(i) / MaxButtonsPerPage) == ButtonBarPageIdx);
			ButtonBar.at(i)->SetVisibility(bViewingButton);
		}
	}

	void GUITester::RefreshLabeledScrollbarText (INT newScrollPosition)
	{
		DString newLabelText = TXT("");

		for (int i = 0; i < ScrollbarLabel->GetMaxNumLines(); i++)
		{
			newLabelText += (newScrollPosition + i).ToString() + TXT("\n");
		}

		ScrollbarLabel->SetTextContent(newLabelText);
	}

	void GUITester::RefreshAdjustableScrollbarText (INT newScrollPosition)
	{
		DString newLabelText = TXT("");

		for (int i = 0; i < AdjustableScrollbar->GetNumVisibleScrollPositions() && (i + newScrollPosition) < AdjustableScrollbar->GetMaxScrollPosition(); i++)
		{
			newLabelText += (newScrollPosition + i).ToString() + TXT("\n");
		}

		AdjustableScrollbarLabel->SetTextContent(newLabelText);
	}

	void GUITester::HandleWindowEvent (const sf::Event newWindowEvent)
	{
		if (newWindowEvent.type == sf::Event::Closed)
		{
			Destroy();
		}
	}

	void GUITester::HandleFrameButtonClicked (ButtonComponent* uiComponent)
	{
		UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Displaying Frame Components!"));
		ClearStageComponents();

		//Simple locked frame with fill texture and borders
		FrameComponent* frame1 = FrameComponent::CreateObject();
		if (!StageFrame->AddComponent(frame1))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]: Failed to attach frame component for testing."));
			frame1->Destroy();
		}
		else
		{
			frame1->SetLockedFrame(true);
			frame1->GetTransform()->SetAbsCoordinates(Vector2(8.f, 8.f));
			frame1->GetTransform()->SetBaseSize(Vector2(48.f, 48.f));
			frame1->RenderComponent->SetSpriteTexture(TexturePool::GetTexturePool()->FindTexture(TXT("DebuggingTexture")));
			frame1->RenderComponent->TopBorder = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderTop"));
			frame1->RenderComponent->TopRightCorner = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderTopRight"));
			frame1->RenderComponent->RightBorder = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderRight"));
			frame1->RenderComponent->BottomRightCorner = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderBottomRight"));
			frame1->RenderComponent->BottomBorder = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderBottom"));
			frame1->RenderComponent->BottomLeftCorner = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderBottomLeft"));
			frame1->RenderComponent->LeftBorder = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderLeft"));
			frame1->RenderComponent->TopLeftCorner = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderTopLeft"));
			StageComponents.push_back(frame1);
		}

		//Simple moveable frame with fill texture and thick borders
		FrameComponent* frame2 = FrameComponent::CreateObject();
		if (!StageFrame->AddComponent(frame2))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]: Failed to attach frame component for testing."));
			frame2->Destroy();
		}
		else
		{
			frame2->SetLockedFrame(false);
			frame2->SetBorderThickness(16.f);
			frame2->GetTransform()->SetAbsCoordinates(Vector2(80.f, 8.f));
			frame2->GetTransform()->SetBaseSize(Vector2(48.f, 48.f));
			frame2->RenderComponent->SetSpriteTexture(TexturePool::GetTexturePool()->FindTexture(TXT("DebuggingTexture")));
			frame2->RenderComponent->TopBorder = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderTop"));
			frame2->RenderComponent->TopRightCorner = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderTopRight"));
			frame2->RenderComponent->RightBorder = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderRight"));
			frame2->RenderComponent->BottomRightCorner = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderBottomRight"));
			frame2->RenderComponent->BottomBorder = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderBottom"));
			frame2->RenderComponent->BottomLeftCorner = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderBottomLeft"));
			frame2->RenderComponent->LeftBorder = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderLeft"));
			frame2->RenderComponent->TopLeftCorner = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderTopLeft"));
			StageComponents.push_back(frame2);
		}

		//locked frame with a moveable frame inside it
		FrameComponent* frame3 = FrameComponent::CreateObject();
		if (!StageFrame->AddComponent(frame3))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]: Failed to attach frame component for testing."));
			frame3->Destroy();
		}
		else
		{
			frame3->SetLockedFrame(true);
			frame3->GetTransform()->SetAbsCoordinates(Vector2(8.f, 80.f));
			frame3->GetTransform()->SetBaseSize(Vector2(96.f, 96.f));
			StageComponents.push_back(frame3);

			FrameComponent* frame4 = FrameComponent::CreateObject();
			if (!frame3->AddComponent(frame4))
			{
				UNITTESTER_LOG2(TestFlags, TXT("[GUITester]:  Failed to attach %s to %s (frame4 to frame3)."), frame4->ToString(), frame3->ToString());
				frame4->Destroy();
			}
			else
			{
				frame4->SetLockedFrame(false);
				frame4->GetTransform()->SetAbsCoordinates(frame3->GetTransform()->GetCurrentSize() * 0.5f);
				frame4->GetTransform()->SetBaseSize(frame3->GetTransform()->GetCurrentSize() * 0.5f);
				StageComponents.push_back(frame4);
			}
		}

		//Moveable frame within a moveable frame within a moveable frame within moveable frame
		FrameComponent* frame5 = FrameComponent::CreateObject();
		if (!StageFrame->AddComponent(frame5))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]: Failed to attach frame component for testing."));
			frame5->Destroy();
		}
		else
		{
			FLOAT minSize = 0.f; //Counter in how thin a frame component may be (accumulates based on border thickness of sub frame components)
			frame5->SetLockedFrame(false);
			frame5->GetTransform()->SetAbsCoordinates(Vector2(8.f, 182.f));
			frame5->GetTransform()->SetBaseSize(Vector2(StageDimensions.GetWidth() * 0.4f, StageDimensions.Bottom - frame5->GetTransform()->CalcFinalCoordinatesY() - 16.f));
			frame5->GetTransform()->OnSizeChanged = SDFUNCTION(this, GUITester, HandleFrameSizeChange, void);
			StageComponents.push_back(frame5);

			FrameComponent* frame6 = FrameComponent::CreateObject();
			if (!frame5->AddComponent(frame6))
			{
				UNITTESTER_LOG2(TestFlags, TXT("[GUITester]:  Failed to attach %s to %s (frame5 to frame6)."), frame5->ToString(), frame6->ToString());
				frame6->Destroy();
			}
			else
			{
				frame6->SetLockedFrame(false);
				frame6->GetTransform()->SetAbsCoordinates(Vector2(8.f, 8.f));
				frame6->GetTransform()->SetBaseSize(frame5->GetTransform()->GetCurrentSize() - Vector2(16.f, 16.f));
				frame6->GetTransform()->OnSizeChanged = SDFUNCTION(this, GUITester, HandleFrameSizeChange, void);
				AlignmentFrames[0] = frame6;
				StageComponents.push_back(frame6);

				FrameComponent* frame7 = FrameComponent::CreateObject();
				if (!frame6->AddComponent(frame7))
				{
					UNITTESTER_LOG2(TestFlags, TXT("[GUITester]:  Failed to attach %s to %s (frame6 to frame7)."), frame6->ToString(), frame7->ToString());
					frame7->Destroy();
				}
				else
				{
					frame7->SetLockedFrame(false);
					frame7->GetTransform()->SetAbsCoordinates(Vector2(8.f, 8.f));
					frame7->GetTransform()->SetBaseSize(frame6->GetTransform()->GetCurrentSize() - Vector2(16.f, 16.f));
					frame7->GetTransform()->OnSizeChanged = SDFUNCTION(this, GUITester, HandleFrameSizeChange, void);
					AlignmentFrames[1] = frame7;
					StageComponents.push_back(frame7);

					FrameComponent* frame8 = FrameComponent::CreateObject();
					if (!frame7->AddComponent(frame8))
					{
						UNITTESTER_LOG2(TestFlags, TXT("[GUITester]:  Failed to attach %s to %s (frame7 to frame8)."), frame7->ToString(), frame8->ToString());
						frame8->Destroy();
					}
					else
					{
						frame8->SetLockedFrame(false);
						frame8->GetTransform()->SetAbsCoordinates(Vector2(8.f, 8.f));
						frame8->GetTransform()->SetBaseSize(frame7->GetTransform()->GetCurrentSize() - Vector2(16.f, 16.f));
						frame8->GetTransform()->OnSizeChanged = SDFUNCTION(this, GUITester, HandleFrameSizeChange, void);
						AlignmentFrames[2] = frame8;
						minSize = (frame8->GetBorderThickness() * 2);
						frame8->GetTransform()->SetSizeXClamps(Range<FLOAT>(minSize, -1.f));
						frame8->GetTransform()->SetSizeYClamps(Range<FLOAT>(minSize, -1.f));
						StageComponents.push_back(frame8);
					}

					minSize += (frame7->GetBorderThickness() * 2);
					frame7->GetTransform()->SetSizeXClamps(Range<FLOAT>(minSize, -1.f));
					frame7->GetTransform()->SetSizeYClamps(Range<FLOAT>(minSize, -1.f));
				}

				minSize += (frame6->GetBorderThickness() * 2);
				frame6->GetTransform()->SetSizeXClamps(Range<FLOAT>(minSize, -1.f));
				frame6->GetTransform()->SetSizeYClamps(Range<FLOAT>(minSize, -1.f));
			}

			minSize += (frame5->GetBorderThickness() * 2);
			frame5->GetTransform()->SetSizeXClamps(Range<FLOAT>(minSize, -1.f));
			frame5->GetTransform()->SetSizeYClamps(Range<FLOAT>(minSize, -1.f));
		}

		//Moveable frame with 9 locked frame components (1 of each scaling alignment (top, top right, right, bottom right, bottom, bottom left, left, top left, and center)
		FrameComponent* frame9 = FrameComponent::CreateObject();
		if (!StageFrame->AddComponent(frame9))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]: Failed to attach frame component for testing."));
			frame9->Destroy();
		}
		else
		{
			frame9->SetLockedFrame(false);
			frame9->GetTransform()->SetAbsCoordinates(Vector2(StageDimensions.GetWidth() * 0.6f, StageDimensions.Top + 80.f));
			frame9->GetTransform()->SetBaseSize(Vector2(StageDimensions.GetWidth() * 0.4f, StageDimensions.Bottom - frame5->GetTransform()->CalcFinalCoordinatesY() - 16.f));
			frame9->GetTransform()->SetSizeXClamps(Range<FLOAT>(frame9->GetTransform()->GetCurrentSizeX() * 0.7f, -1.f));
			frame9->GetTransform()->SetSizeYClamps(Range<FLOAT>(frame9->GetTransform()->GetCurrentSizeY() * 0.7f, -1.f));
			frame9->GetTransform()->OnSizeChanged = SDFUNCTION(this, GUITester, HandleFrameSizeChange, void);
			StageComponents.push_back(frame9);

			FrameComponent* topFrame = FrameComponent::CreateObject();
			if (!frame9->AddComponent(topFrame))
			{
				UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach top frame component to frame9."));
				topFrame->Destroy();
			}
			else
			{
				topFrame->SetLockedFrame(true);
				topFrame->GetTransform()->OnSizeChanged = SDFUNCTION(this, GUITester, HandleFrameSizeChange, void);
				topFrame->GetTransform()->SetBaseSize(frame9->GetTransform()->GetCurrentSize() * 0.25f);
				//center align X, top align Y
				topFrame->GetTransform()->SnapCenterHorizontally();
				topFrame->GetTransform()->SnapTop();
				AlignmentFrames[3] = topFrame;
				StageComponents.push_back(topFrame);
			}

			FrameComponent* topRightFrame = FrameComponent::CreateObject();
			if (!frame9->AddComponent(topRightFrame))
			{
				UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach top right frame component to frame9."));
				topRightFrame->Destroy();
			}
			else
			{
				topRightFrame->SetLockedFrame(true);
				topRightFrame->GetTransform()->OnSizeChanged = SDFUNCTION(this, GUITester, HandleFrameSizeChange, void);
				topRightFrame->GetTransform()->SetBaseSize(frame9->GetTransform()->GetCurrentSize() * 0.25f);
				//right align X, top align Y
				topRightFrame->GetTransform()->SnapRight();
				topRightFrame->GetTransform()->SnapTop();
				AlignmentFrames[4] = topRightFrame;
				StageComponents.push_back(topRightFrame);
			}

			FrameComponent* rightFrame = FrameComponent::CreateObject();
			if (!frame9->AddComponent(rightFrame))
			{
				UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach right frame component to frame9."));
				rightFrame->Destroy();
			}
			else
			{
				rightFrame->SetLockedFrame(true);
				rightFrame->GetTransform()->OnSizeChanged = SDFUNCTION(this, GUITester, HandleFrameSizeChange, void);
				rightFrame->GetTransform()->SetBaseSize(frame9->GetTransform()->GetCurrentSize() * 0.25f);
				//right align X, center align Y
				rightFrame->GetTransform()->SnapRight();
				rightFrame->GetTransform()->SnapCenterVertically();
				AlignmentFrames[5] = rightFrame;
				StageComponents.push_back(rightFrame);
			}

			FrameComponent* bottomRightFrame = FrameComponent::CreateObject();
			if (!frame9->AddComponent(bottomRightFrame))
			{
				UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach bottom right frame component to frame9."));
				bottomRightFrame->Destroy();
			}
			else
			{
				bottomRightFrame->SetLockedFrame(true);
				bottomRightFrame->GetTransform()->OnSizeChanged = SDFUNCTION(this, GUITester, HandleFrameSizeChange, void);
				bottomRightFrame->GetTransform()->SetBaseSize(frame9->GetTransform()->GetCurrentSize() * 0.25f);
				//right align X, bottom align Y
				bottomRightFrame->GetTransform()->SnapRight();
				bottomRightFrame->GetTransform()->SnapBottom();
				AlignmentFrames[6] = bottomRightFrame;
				StageComponents.push_back(bottomRightFrame);
			}

			FrameComponent* bottomFrame = FrameComponent::CreateObject();
			if (!frame9->AddComponent(bottomFrame))
			{
				UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach bottom frame component to frame9."));
				bottomFrame->Destroy();
			}
			else
			{
				bottomFrame->SetLockedFrame(true);
				bottomFrame->GetTransform()->OnSizeChanged = SDFUNCTION(this, GUITester, HandleFrameSizeChange, void);
				bottomFrame->GetTransform()->SetBaseSize(frame9->GetTransform()->GetCurrentSize() * 0.25f);
				//center align X, bottom align Y
				bottomFrame->GetTransform()->SnapCenterHorizontally();
				bottomFrame->GetTransform()->SnapBottom();
				AlignmentFrames[7] = bottomFrame;
				StageComponents.push_back(bottomFrame);
			}

			FrameComponent* bottomLeftFrame = FrameComponent::CreateObject();
			if (!frame9->AddComponent(bottomLeftFrame))
			{
				UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach bottom left frame component to frame9."));
				bottomLeftFrame->Destroy();
			}
			else
			{
				bottomLeftFrame->SetLockedFrame(true);
				bottomLeftFrame->GetTransform()->OnSizeChanged = SDFUNCTION(this, GUITester, HandleFrameSizeChange, void);
				bottomLeftFrame->GetTransform()->SetBaseSize(frame9->GetTransform()->GetCurrentSize() * 0.25f);
				//left align X, bottom align Y
				bottomLeftFrame->GetTransform()->SnapLeft();
				bottomLeftFrame->GetTransform()->SnapBottom();
				AlignmentFrames[8] = bottomLeftFrame;
				StageComponents.push_back(bottomLeftFrame);
			}

			FrameComponent* leftFrame = FrameComponent::CreateObject();
			if (!frame9->AddComponent(leftFrame))
			{
				UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach left frame component to frame9."));
				leftFrame->Destroy();
			}
			else
			{
				leftFrame->SetLockedFrame(true);
				leftFrame->GetTransform()->OnSizeChanged = SDFUNCTION(this, GUITester, HandleFrameSizeChange, void);
				leftFrame->GetTransform()->SetBaseSize(frame9->GetTransform()->GetCurrentSize() * 0.25f);
				//left align X, center align Y
				leftFrame->GetTransform()->SnapLeft();
				leftFrame->GetTransform()->SnapCenterVertically();
				AlignmentFrames[9] = leftFrame;
				StageComponents.push_back(leftFrame);
			}

			FrameComponent* topLeftFrame = FrameComponent::CreateObject();
			if (!frame9->AddComponent(topLeftFrame))
			{
				UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach top left frame component to frame9."));
				topLeftFrame->Destroy();
			}
			else
			{
				topLeftFrame->SetLockedFrame(true);
				topLeftFrame->GetTransform()->OnSizeChanged = SDFUNCTION(this, GUITester, HandleFrameSizeChange, void);
				topLeftFrame->GetTransform()->SetBaseSize(frame9->GetTransform()->GetCurrentSize() * 0.25f);
				//left align X, top align Y
				topLeftFrame->GetTransform()->SnapLeft();
				topLeftFrame->GetTransform()->SnapTop();
				AlignmentFrames[10] = topLeftFrame;
				StageComponents.push_back(topLeftFrame);
			}

			FrameComponent* centerFrame = FrameComponent::CreateObject();
			if (!frame9->AddComponent(centerFrame))
			{
				UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach center frame component to frame9."));
				centerFrame->Destroy();
			}
			else
			{
				centerFrame->SetLockedFrame(true);
				centerFrame->GetTransform()->OnSizeChanged = SDFUNCTION(this, GUITester, HandleFrameSizeChange, void);
				centerFrame->GetTransform()->SetBaseSize(frame9->GetTransform()->GetCurrentSize() * 0.25f);
				//center align X, center align Y
				centerFrame->GetTransform()->SnapCenterHorizontally();
				centerFrame->GetTransform()->SnapCenterVertically();
				AlignmentFrames[11] = centerFrame;
				StageComponents.push_back(centerFrame);
			}
		}
	}

	void GUITester::HandleLabelButtonClicked (ButtonComponent* uiComponent)
	{
		UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Displaying Label Components!"));
		ClearStageComponents();

		LabelComponent* topLeft = LabelComponent::CreateObject();
		if (!StageFrame->AddComponent(topLeft))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach top left label component to stage frame."));
			topLeft->Destroy();
		}
		else
		{
			topLeft->GetTransform()->SetBaseSize(StageFrame->GetTransform()->GetBaseSize());
			topLeft->SetHorizontalAlignment(LabelComponent::HA_Left);
			topLeft->SetVerticalAlignment(LabelComponent::VA_Top);
			topLeft->SetTextContent(TXT("Top Left"));
			StageComponents.push_back(topLeft);
		}

		LabelComponent* topCenter = LabelComponent::CreateObject();
		if (!StageFrame->AddComponent(topCenter))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach top center label component to stage frame."));
			topCenter->Destroy();
		}
		else
		{
			topCenter->GetTransform()->SetBaseSize(StageFrame->GetTransform()->GetBaseSize());
			topCenter->SetHorizontalAlignment(LabelComponent::HA_Center);
			topCenter->SetVerticalAlignment(LabelComponent::VA_Top);
			topCenter->SetTextContent(TXT("Top Center"));
			StageComponents.push_back(topCenter);
		}


		LabelComponent* topRight = LabelComponent::CreateObject();
		if (!StageFrame->AddComponent(topRight))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach top right label component to stage frame."));
			topRight->Destroy();
		}
		else
		{
			topRight->GetTransform()->SetBaseSize(StageFrame->GetTransform()->GetBaseSize());
			topRight->SetHorizontalAlignment(LabelComponent::HA_Right);
			topRight->SetVerticalAlignment(LabelComponent::VA_Top);
			topRight->SetTextContent(TXT("Top Right"));
			StageComponents.push_back(topRight);
		}

		LabelComponent* centerRight = LabelComponent::CreateObject();
		if (!StageFrame->AddComponent(centerRight))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach center right label component to stage frame."));
			centerRight->Destroy();
		}
		else
		{
			centerRight->GetTransform()->SetBaseSize(StageFrame->GetTransform()->GetBaseSize());
			centerRight->SetHorizontalAlignment(LabelComponent::HA_Right);
			centerRight->SetVerticalAlignment(LabelComponent::VA_Center);
			centerRight->SetTextContent(TXT("Center Right"));
			StageComponents.push_back(centerRight);
		}

		LabelComponent* bottomRight = LabelComponent::CreateObject();
		if (!StageFrame->AddComponent(bottomRight))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach bottom right label component to stage frame."));
			bottomRight->Destroy();
		}
		else
		{
			bottomRight->GetTransform()->SetBaseSize(StageFrame->GetTransform()->GetBaseSize());
			bottomRight->SetHorizontalAlignment(LabelComponent::HA_Right);
			bottomRight->SetVerticalAlignment(LabelComponent::VA_Bottom);
			bottomRight->SetTextContent(TXT("Bottom Right"));
			StageComponents.push_back(bottomRight);
		}

		LabelComponent* bottomCenter = LabelComponent::CreateObject();
		if (!StageFrame->AddComponent(bottomCenter))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach bottom center label component to stage frame."));
			bottomCenter->Destroy();
		}
		else
		{
			bottomCenter->GetTransform()->SetBaseSize(StageFrame->GetTransform()->GetBaseSize());
			bottomCenter->SetHorizontalAlignment(LabelComponent::HA_Center);
			bottomCenter->SetVerticalAlignment(LabelComponent::VA_Bottom);
			bottomCenter->SetTextContent(TXT("Bottom Center"));
			StageComponents.push_back(bottomCenter);
		}

		LabelComponent* bottomLeft = LabelComponent::CreateObject();
		if (!StageFrame->AddComponent(bottomLeft))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach bottom left label component to stage frame."));
			bottomLeft->Destroy();
		}
		else
		{
			bottomLeft->GetTransform()->SetBaseSize(StageFrame->GetTransform()->GetBaseSize());
			bottomLeft->SetHorizontalAlignment(LabelComponent::HA_Left);
			bottomLeft->SetVerticalAlignment(LabelComponent::VA_Bottom);
			bottomLeft->SetTextContent(TXT("Bottom Left"));
			StageComponents.push_back(bottomLeft);
		}

		LabelComponent* centerLeft = LabelComponent::CreateObject();
		if (!StageFrame->AddComponent(centerLeft))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach center left label component to stage frame."));
			centerLeft->Destroy();
		}
		else
		{
			centerLeft->GetTransform()->SetBaseSize(StageFrame->GetTransform()->GetBaseSize());
			centerLeft->SetHorizontalAlignment(LabelComponent::HA_Left);
			centerLeft->SetVerticalAlignment(LabelComponent::VA_Center);
			centerLeft->SetTextContent(TXT("Center Left"));
			StageComponents.push_back(centerLeft);
		}

		LabelComponent* centerCenter = LabelComponent::CreateObject();
		if (!StageFrame->AddComponent(centerCenter))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach centered label component to stage frame."));
			centerCenter->Destroy();
		}
		else
		{
			centerCenter->GetTransform()->SetBaseSize(StageFrame->GetTransform()->GetBaseSize());
			centerCenter->SetHorizontalAlignment(LabelComponent::HA_Center);
			centerCenter->SetVerticalAlignment(LabelComponent::VA_Center);
			centerCenter->SetTextContent(TXT("Centered"));
			StageComponents.push_back(centerCenter);
		}

		LabelComponent* largeText = LabelComponent::CreateObject();
		if (!StageFrame->AddComponent(largeText))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach large text label component to stage frame."));
			largeText->Destroy();
		}
		else
		{
			largeText->GetTransform()->SetBaseSize(Vector2(256, 96));
			largeText->GetTransform()->SetAbsCoordinates(Vector2(48, 64));
			largeText->SetHorizontalAlignment(LabelComponent::HA_Left);
			largeText->SetVerticalAlignment(LabelComponent::VA_Top);
			largeText->SetTextContent(TXT("Large Text (32)"));
			largeText->SetCharacterSize(32);
			StageComponents.push_back(largeText);
		}

		LabelComponent* mediumText = LabelComponent::CreateObject();
		if (!StageFrame->AddComponent(mediumText))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach medium text label component to stage frame."));
			largeText->Destroy();
		}
		else
		{
			mediumText->GetTransform()->SetBaseSize(Vector2(384, 96));
			mediumText->GetTransform()->SetAbsCoordinates(Vector2(48, 110));
			mediumText->SetHorizontalAlignment(LabelComponent::HA_Left);
			mediumText->SetVerticalAlignment(LabelComponent::VA_Top);
			mediumText->SetTextContent(TXT("Medium Text (16)"));
			mediumText->SetCharacterSize(16);
			StageComponents.push_back(mediumText);
		}

		LabelComponent* smallText = LabelComponent::CreateObject();
		if (!StageFrame->AddComponent(smallText))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach small text label component to stage frame."));
			smallText->Destroy();
		}
		else
		{
			smallText->GetTransform()->SetBaseSize(Vector2(256, 96));
			smallText->GetTransform()->SetAbsCoordinates(Vector2(48, 144));
			smallText->SetHorizontalAlignment(LabelComponent::HA_Left);
			smallText->SetVerticalAlignment(LabelComponent::VA_Top);
			smallText->SetTextContent(TXT("Small Text (8)"));
			smallText->SetCharacterSize(8);
			StageComponents.push_back(smallText);
		}

		LabelComponent* clampedText = LabelComponent::CreateObject();
		if (!StageFrame->AddComponent(clampedText))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach clamped text label component to stage frame."));
			clampedText->Destroy();
		}
		else
		{
			FLOAT clampedTextWidth = 96.f;
			clampedText->GetTransform()->SetBaseSize(Vector2(clampedTextWidth, 96));
			clampedText->GetTransform()->SetAbsCoordinates(Vector2(StageFrame->GetTransform()->GetCurrentSize().X - clampedTextWidth, 64));
			clampedText->SetHorizontalAlignment(LabelComponent::HA_Left);
			clampedText->SetVerticalAlignment(LabelComponent::VA_Top);
			clampedText->SetWrapText(false);
			clampedText->SetClampText(true);
			clampedText->SetTextContent(TXT("Clamped Text - Clamped Text - Clamped Text - Clamped Text - Clamped Text"));
			StageComponents.push_back(clampedText);
		}

		LabelComponent* singleSpacedLine = LabelComponent::CreateObject();
		if (!StageFrame->AddComponent(singleSpacedLine))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach single spaced clamped text label component to stage frame."));
			singleSpacedLine->Destroy();
		}
		else
		{
			FLOAT clampedTextWidth = 192.f;
			singleSpacedLine->GetTransform()->SetBaseSize(Vector2(clampedTextWidth, 96));
			singleSpacedLine->GetTransform()->SetAbsCoordinates(Vector2(StageFrame->GetTransform()->GetCurrentSize().X - clampedTextWidth, 32.f + (StageFrame->GetTransform()->GetCurrentSize().Y * 0.5f)));
			singleSpacedLine->SetHorizontalAlignment(LabelComponent::HA_Left);
			singleSpacedLine->SetVerticalAlignment(LabelComponent::VA_Top);
			singleSpacedLine->SetWrapText(true);
			singleSpacedLine->SetClampText(true);
			singleSpacedLine->SetLineSpacing(0.f);
			singleSpacedLine->SetTextContent(TXT("Clamped Single Line Spacing - Clamped Single Line Spacing - Clamped Single Line Spacing - Clamped Single Line Spacing - Clamped Single Line Spacing."));
			StageComponents.push_back(singleSpacedLine);
		}

		LabelComponent* multiSpacedLine = LabelComponent::CreateObject();
		if (!StageFrame->AddComponent(multiSpacedLine))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach multi spaced unclamped text label component to stage frame."));
			multiSpacedLine->Destroy();
		}
		else
		{
			FLOAT clampedTextWidth = 160.f;
			multiSpacedLine->GetTransform()->SetBaseSize(Vector2(clampedTextWidth, 128));
			multiSpacedLine->GetTransform()->SetAbsCoordinates(Vector2((StageFrame->GetTransform()->GetCurrentSize().X * 0.5f) - clampedTextWidth, 32.f + (StageFrame->GetTransform()->GetCurrentSize().Y * 0.5f)));
			multiSpacedLine->SetHorizontalAlignment(LabelComponent::HA_Left);
			multiSpacedLine->SetVerticalAlignment(LabelComponent::VA_Top);
			multiSpacedLine->SetWrapText(true);
			multiSpacedLine->SetClampText(false);
			multiSpacedLine->SetLineSpacing(multiSpacedLine->GetCharacterSize().ToFLOAT());
			multiSpacedLine->SetTextContent(TXT("Unclamped Text Double Line Spacing - Unclamped Text Double Line Spacing - Unclamped Text Double Line Spacing - Unclamped Text Double Line Spacing - Unclamped Text Double Line Spacing - Unclamped Text Double Line Spacing - Unclamped Text Double Line Spacing - Unclamped Text Double Line Spacing."));
			StageComponents.push_back(multiSpacedLine);
		}

		LabelComponent* newLineLabel = LabelComponent::CreateObject();
		if (!StageFrame->AddComponent(newLineLabel))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach new line character test label component to stage frame."));
			newLineLabel->Destroy();
		}
		else
		{
			newLineLabel->GetTransform()->SetBaseSize(Vector2(96, 128));
			newLineLabel->GetTransform()->SetAbsCoordinates(Vector2((StageFrame->GetTransform()->GetCurrentSize().X * 0.67f), (StageFrame->GetTransform()->GetCurrentSize().Y * 0.25f)));
			newLineLabel->SetHorizontalAlignment(LabelComponent::HA_Left);
			newLineLabel->SetVerticalAlignment(LabelComponent::VA_Top);
			newLineLabel->SetWrapText(false);
			newLineLabel->SetClampText(false);
			newLineLabel->SetTextContent(TXT("New Line \n character.\n"));
			StageComponents.push_back(newLineLabel);
		}
	}

	void GUITester::HandleButtonButtonClicked (ButtonComponent* uiComponent)
	{
		UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Displaying Button Components!"));
		ClearStageComponents();

		Texture* textureStates = GUITheme::GetGUITheme()->ButtonSingleSpriteBackground;
		FLOAT buttonHeight = 24.f;
		FLOAT buttonInterval = 8.f;
		FLOAT buttonPosition = 16.f;

		ButtonComponent* basicButton = ButtonComponent::CreateObject();
		if (!StageFrame->AddComponent(basicButton))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach basic button component to stage frame."));
			basicButton->Destroy();
		}
		else
		{
			basicButton->GetTransform()->SetBaseSize(Vector2(164, buttonHeight));
			basicButton->GetTransform()->SetAbsCoordinates(Vector2(16, buttonPosition));
			basicButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleTestButtonPressed, void, ButtonComponent*));
			basicButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleTestButtonReleased, void, ButtonComponent*));
			buttonPosition += buttonHeight + buttonInterval;
			basicButton->SetCaptionText("Basic Button");
			StageComponents.push_back(basicButton);
		}

		ButtonComponent* noCaption = ButtonComponent::CreateObject();
		if (!StageFrame->AddComponent(noCaption))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach no caption button component to stage frame."));
			noCaption->Destroy();
		}
		else
		{
			noCaption->GetTransform()->SetBaseSize(Vector2(164, buttonHeight));
			noCaption->GetTransform()->SetAbsCoordinates(Vector2(16, buttonPosition));
			noCaption->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleTestButtonPressed, void, ButtonComponent*));
			noCaption->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleTestButtonReleased, void, ButtonComponent*));
			buttonPosition += buttonHeight + buttonInterval;
			noCaption->CaptionComponent->Destroy();
			noCaption->CaptionComponent = nullptr;
			StageComponents.push_back(noCaption);
		}

		LabelComponent* tip = LabelComponent::CreateObject();
		if (!StageFrame->AddComponent(tip))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach label component about the captionless button component to stage frame."));
			tip->Destroy();
		}
		else
		{
			tip->GetTransform()->SetBaseSize(Vector2(164, buttonHeight));
			tip->GetTransform()->SetAbsCoordinates(Vector2(noCaption->GetTransform()->GetAbsCoordinatesX() + noCaption->GetTransform()->GetCurrentSizeX() + 16, noCaption->GetTransform()->GetAbsCoordinatesY()));
			tip->SetTextContent(TXT("<---- No Caption"));
			StageComponents.push_back(tip);
		}

		ButtonComponent* multiStateButton = ButtonComponent::CreateObject();
		if (!StageFrame->AddComponent(multiStateButton))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach multi-state button component to stage frame."));
			multiStateButton->Destroy();
		}
		else
		{
			multiStateButton->GetTransform()->SetBaseSize(Vector2(164, buttonHeight));
			multiStateButton->GetTransform()->SetAbsCoordinates(Vector2(16, buttonPosition));
			multiStateButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleTestButtonPressed, void, ButtonComponent*));
			multiStateButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleTestButtonReleased, void, ButtonComponent*));
			buttonPosition += buttonHeight + buttonInterval;
			multiStateButton->SetCaptionText(TXT("Multi-state Button"));
			multiStateButton->RenderComponent->SetSpriteTexture(textureStates);
			multiStateButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			dynamic_cast<SingleSpriteButtonState*>(multiStateButton->GetButtonStateComponent())->Sprite = multiStateButton->RenderComponent;
			multiStateButton->GetButtonStateComponent()->RefreshState();
			StageComponents.push_back(multiStateButton);
		}

		ButtonComponent* enabledButton = ButtonComponent::CreateObject();
		if (!StageFrame->AddComponent(enabledButton))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach enabled button component to stage frame."));
			enabledButton->Destroy();
		}
		else
		{
			enabledButton->GetTransform()->SetBaseSize(Vector2(164, buttonHeight));
			enabledButton->GetTransform()->SetAbsCoordinates(Vector2(16, buttonPosition));
			enabledButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleTestButtonPressed, void, ButtonComponent*));
			enabledButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleTestButtonReleased, void, ButtonComponent*));
			buttonPosition += buttonHeight + buttonInterval;
			enabledButton->SetCaptionText(TXT("Enabled Button"));
			enabledButton->SetEnabled(true);
			enabledButton->RenderComponent->SetSpriteTexture(textureStates);
			enabledButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			dynamic_cast<SingleSpriteButtonState*>(enabledButton->GetButtonStateComponent())->Sprite = enabledButton->RenderComponent;
			enabledButton->GetButtonStateComponent()->RefreshState();
			StageComponents.push_back(enabledButton);
		}

		ButtonComponent* disabledButton = ButtonComponent::CreateObject();
		if (!StageFrame->AddComponent(disabledButton))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach disabled button component to stage frame."));
			disabledButton->Destroy();
		}
		else
		{
			disabledButton->GetTransform()->SetBaseSize(Vector2(164, buttonHeight));
			disabledButton->GetTransform()->SetAbsCoordinates(Vector2(16, buttonPosition));
			disabledButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleTestButtonPressed, void, ButtonComponent*));
			disabledButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleTestButtonReleased, void, ButtonComponent*));
			buttonPosition += buttonHeight + buttonInterval;
			disabledButton->SetCaptionText(TXT("Disabled Button"));
			disabledButton->SetEnabled(false);
			disabledButton->RenderComponent->SetSpriteTexture(textureStates);
			disabledButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			dynamic_cast<SingleSpriteButtonState*>(disabledButton->GetButtonStateComponent())->Sprite = disabledButton->RenderComponent;
			disabledButton->GetButtonStateComponent()->RefreshState();
			StageComponents.push_back(disabledButton);
		}

		ButtonComponent* toggledButton1 = ButtonComponent::CreateObject();
		if (!StageFrame->AddComponent(toggledButton1))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach toggleable button component to stage frame."));
			disabledButton->Destroy();
		}
		else
		{
			toggledButton1->GetTransform()->SetBaseSize(Vector2(164, buttonHeight));
			toggledButton1->GetTransform()->SetAbsCoordinates(Vector2(16, buttonPosition));
			toggledButton1->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleTestButtonPressed, void, ButtonComponent*));
			toggledButton1->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleToggleButtonReleased, void, ButtonComponent*));
			buttonPosition += buttonHeight + buttonInterval;
			toggledButton1->SetCaptionText(TXT("Toggleable Button"));
			toggledButton1->SetEnabled(true);
			toggledButton1->RenderComponent->SetSpriteTexture(textureStates);
			toggledButton1->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			dynamic_cast<SingleSpriteButtonState*>(toggledButton1->GetButtonStateComponent())->Sprite = toggledButton1->RenderComponent;
			toggledButton1->GetButtonStateComponent()->RefreshState();
			ToggleButtons[0] = toggledButton1;
			StageComponents.push_back(toggledButton1);
		}

		ButtonComponent* toggledButton2 = ButtonComponent::CreateObject();
		if (!StageFrame->AddComponent(toggledButton2))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach toggleable button component to stage frame."));
			disabledButton->Destroy();
		}
		else
		{
			toggledButton2->GetTransform()->SetBaseSize(Vector2(164, buttonHeight));
			toggledButton2->GetTransform()->SetAbsCoordinates(Vector2(16, buttonPosition));
			toggledButton2->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleTestButtonPressed, void, ButtonComponent*));
			toggledButton2->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleToggleButtonReleased, void, ButtonComponent*));
			buttonPosition += buttonHeight + buttonInterval;
			toggledButton2->SetCaptionText(TXT("Toggleable Button"));
			toggledButton2->SetEnabled(false);
			toggledButton2->RenderComponent->SetSpriteTexture(textureStates);
			toggledButton2->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			dynamic_cast<SingleSpriteButtonState*>(toggledButton2->GetButtonStateComponent())->Sprite = toggledButton2->RenderComponent;
			toggledButton2->GetButtonStateComponent()->RefreshState();
			ToggleButtons[1] = toggledButton2;
			StageComponents.push_back(toggledButton2);
		}

		ButtonComponent* outerButton = ButtonComponent::CreateObject();
		if (!StageFrame->AddComponent(outerButton))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach outer button component to stage frame."));
			outerButton->Destroy();
		}
		else
		{
			outerButton->GetTransform()->SetBaseSize(Vector2(164, buttonHeight * 3));
			outerButton->GetTransform()->SetAbsCoordinates(Vector2(16, buttonPosition));
			outerButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleTestButtonPressed, void, ButtonComponent*));
			outerButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleTestButtonReleased, void, ButtonComponent*));
			buttonPosition += (buttonHeight * 3) + buttonInterval;
			outerButton->SetCaptionText(TXT("Outer Button"));
			outerButton->CaptionComponent->SetVerticalAlignment(LabelComponent::VA_Top);
			outerButton->RenderComponent->SetSpriteTexture(textureStates);
			outerButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			dynamic_cast<SingleSpriteButtonState*>(outerButton->GetButtonStateComponent())->Sprite = outerButton->RenderComponent;
			outerButton->GetButtonStateComponent()->RefreshState();
			StageComponents.push_back(outerButton);

			ButtonComponent* innerButton = ButtonComponent::CreateObject();
			if (!outerButton->AddComponent(innerButton))
			{
				UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach inner button component to outer button."));
				innerButton->Destroy();
			}
			else
			{
				innerButton->GetTransform()->SetBaseSize(Vector2(152, buttonHeight));
				innerButton->GetTransform()->SetAbsCoordinates(Vector2(8.f, buttonHeight));
				innerButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleTestButtonPressed, void, ButtonComponent*));
				innerButton->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleTestButtonReleased, void, ButtonComponent*));
				innerButton->SetCaptionText(TXT("Inner Button"));
				innerButton->RenderComponent->SetSpriteTexture(textureStates);
				innerButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
				dynamic_cast<SingleSpriteButtonState*>(innerButton->GetButtonStateComponent())->Sprite = innerButton->RenderComponent;
				innerButton->GetButtonStateComponent()->RefreshState();
				StageComponents.push_back(innerButton);
			}
		}
	}

	void GUITester::HandleScrollbarButtonClicked (ButtonComponent* uiComponent)
	{
		UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Displaying Scrollbar Components!"));
		ClearStageComponents();

		//Things to test:  Normal scrollbar, scroll bar with no scroll elements, Scrollbar attached to frame to test middle mouse, scroll interval amount
		ScrollbarComponent* basicScrollbar = ScrollbarComponent::CreateObject();
		if (!StageFrame->AddComponent(basicScrollbar))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach basic scrollbar component to stage frame."));
			basicScrollbar->Destroy();
		}
		else
		{
			basicScrollbar->GetTransform()->SetBaseSize(Vector2(16.f, 256.f));
			basicScrollbar->GetTransform()->SetAbsCoordinates(Vector2(32, 16));
			basicScrollbar->SetScrollPositionChanged(SDFUNCTION_1PARAM(this, GUITester, HandleBasicScrollbarIdxChange, void, INT));
			basicScrollbar->SetMaxScrollPosition(100);
			basicScrollbar->SetNumVisibleScrollPositions(10);
			basicScrollbar->bEnableMiddleMouseScrolling = false; //Disable middle mouse scrolling since multiple scrollbars are attached to stage frame.
			StageComponents.push_back(basicScrollbar);
		}

		//this scrollbar should have adjustable variables instead
		ScrollbarComponent* emptyScrollbar = ScrollbarComponent::CreateObject();
		if (!StageFrame->AddComponent(emptyScrollbar))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach empty scrollbar component to stage frame."));
			emptyScrollbar->Destroy();
		}
		else
		{
			emptyScrollbar->GetTransform()->SetBaseSize(Vector2(16.f, 256.f));
			emptyScrollbar->GetTransform()->SetAbsCoordinates(Vector2(64, 16));
			emptyScrollbar->SetScrollPositionChanged(SDFUNCTION_1PARAM(this, GUITester, HandleBasicScrollbarIdxChange, void, INT));
			emptyScrollbar->SetMaxScrollPosition(0);
			emptyScrollbar->SetHideWhenInsufficientScrollPos(false);
			emptyScrollbar->SetNumVisibleScrollPositions(1);
			emptyScrollbar->bEnableMiddleMouseScrolling = false; //Disable middle mouse scrolling since multiple scrollbars are attached to stage frame.
			StageComponents.push_back(emptyScrollbar);
		}

		//Locked frame with a label component to the left, and scrollbar to the right that'll set the label component's text based on scroll position
		FrameComponent* testScrollbarFrame = FrameComponent::CreateObject();
		if (!StageFrame->AddComponent(testScrollbarFrame))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach test scrollbar frame component to stage frame."));
			testScrollbarFrame->Destroy();
		}
		else
		{
			testScrollbarFrame->GetTransform()->SetBaseSize(Vector2(64.f, 256.f));
			testScrollbarFrame->GetTransform()->SetAbsCoordinates(Vector2(StageFrame->GetTransform()->GetCurrentSizeX() - testScrollbarFrame->GetTransform()->GetCurrentSizeX() - StageFrame->GetBorderThickness() - testScrollbarFrame->GetTransform()->GetBaseSizeX(), StageFrame->GetTransform()->GetAbsCoordinatesY() + 16.f));
			testScrollbarFrame->SetLockedFrame(true);
			StageComponents.push_back(testScrollbarFrame);

			ScrollbarComponent* labeledScrollbar = ScrollbarComponent::CreateObject();
			if (!testScrollbarFrame->AddComponent(labeledScrollbar))
			{
				UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach labeled scrollbar component to test scrollbar frame."));
				labeledScrollbar->Destroy();
			}
			else
			{
				labeledScrollbar->GetTransform()->SetBaseSize(Vector2(16.f, testScrollbarFrame->GetTransform()->GetBaseSizeY() - (testScrollbarFrame->GetBorderThickness() * 2)));
				labeledScrollbar->GetTransform()->SetAbsCoordinates(Vector2(testScrollbarFrame->GetTransform()->GetCurrentSizeX() - labeledScrollbar->GetTransform()->GetCurrentSizeX() - testScrollbarFrame->GetBorderThickness(), testScrollbarFrame->GetBorderThickness()));
				labeledScrollbar->SetMaxScrollPosition(1000);
				labeledScrollbar->SetNumVisibleScrollPositions(10);
				labeledScrollbar->SetScrollPositionChanged(SDFUNCTION_1PARAM(this, GUITester, HandleLabeledScrollbarIdxChange, void, INT));
				StageComponents.push_back(labeledScrollbar);

				ScrollbarLabel = LabelComponent::CreateObject();
				if (!testScrollbarFrame->AddComponent(ScrollbarLabel))
				{
					UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach scrollbar label component to test scrollbar frame."));
					ScrollbarLabel->Destroy();
				}
				else
				{
					ScrollbarLabel->GetTransform()->SetBaseSize(Vector2(testScrollbarFrame->GetTransform()->GetBaseSizeX() - labeledScrollbar->GetTransform()->GetBaseSizeX() - testScrollbarFrame->GetBorderThickness(), testScrollbarFrame->GetTransform()->GetBaseSizeY() - (testScrollbarFrame->GetBorderThickness() * 2)));
					ScrollbarLabel->GetTransform()->SetAbsCoordinates(Vector2(testScrollbarFrame->GetBorderThickness(), testScrollbarFrame->GetBorderThickness()));
					ScrollbarLabel->SetHorizontalAlignment(LabelComponent::HA_Left);
					ScrollbarLabel->SetWrapText(false); //new line characters manually added
					ScrollbarLabel->SetClampText(false); //manually clamped text
					labeledScrollbar->SetNumVisibleScrollPositions(ScrollbarLabel->GetMaxNumLines());
					RefreshLabeledScrollbarText(0); //initialize text
					StageComponents.push_back(ScrollbarLabel);
				}
			}
		}

		//Locked frame with a label component to the left, and scrollbar to the right that'll set the label component's text based on scroll position
		//To the right of this frame contains various options to adjust the scrollbar properties.
		FrameComponent* adjustableScrollbarFrame = FrameComponent::CreateObject();
		if (!StageFrame->AddComponent(adjustableScrollbarFrame))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach adjustable scrollbar frame component to stage frame."));
			adjustableScrollbarFrame->Destroy();
		}
		else
		{
			adjustableScrollbarFrame->GetTransform()->SetBaseSize(Vector2(64.f, 256.f));
			adjustableScrollbarFrame->GetTransform()->SetAbsCoordinates(Vector2(96, (StageFrame->GetTransform()->GetCurrentSizeY() * 0.5f)));
			adjustableScrollbarFrame->SetLockedFrame(true);
			StageComponents.push_back(adjustableScrollbarFrame);

			AdjustableScrollbar = ScrollbarComponent::CreateObject();
			if (!adjustableScrollbarFrame->AddComponent(AdjustableScrollbar))
			{
				UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach adjustable scrollbar component to adjustable scrollbar frame."));
				AdjustableScrollbar->Destroy();
			}
			else
			{
				AdjustableScrollbar->GetTransform()->SetBaseSize(Vector2(16.f, adjustableScrollbarFrame->GetTransform()->GetBaseSizeY() - (adjustableScrollbarFrame->GetBorderThickness() * 2)));
				AdjustableScrollbar->GetTransform()->SetAbsCoordinates(Vector2(adjustableScrollbarFrame->GetTransform()->GetCurrentSizeX() - AdjustableScrollbar->GetTransform()->GetCurrentSizeX() - adjustableScrollbarFrame->GetBorderThickness(), adjustableScrollbarFrame->GetBorderThickness()));
				AdjustableScrollbar->SetMaxScrollPosition(100);
				AdjustableScrollbar->SetNumVisibleScrollPositions(10);
				AdjustableScrollbar->SetScrollPositionChanged(SDFUNCTION_1PARAM(this, GUITester, HandleAdjustableScrollbarIdxChange, void, INT));
				StageComponents.push_back(AdjustableScrollbar);

				AdjustableScrollbarLabel = LabelComponent::CreateObject();
				if (!adjustableScrollbarFrame->AddComponent(AdjustableScrollbarLabel))
				{
					UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach adjustable scrollbar's label component to adjustable scrollbar frame."));
					AdjustableScrollbarLabel->Destroy();
				}
				else
				{
					AdjustableScrollbarLabel->GetTransform()->SetBaseSize(Vector2(adjustableScrollbarFrame->GetTransform()->GetBaseSizeX() - AdjustableScrollbar->GetTransform()->GetBaseSizeX() - adjustableScrollbarFrame->GetBorderThickness(), adjustableScrollbarFrame->GetTransform()->GetBaseSizeY() - (adjustableScrollbarFrame->GetBorderThickness() * 2)));
					AdjustableScrollbarLabel->GetTransform()->SetAbsCoordinates(Vector2(adjustableScrollbarFrame->GetBorderThickness(), adjustableScrollbarFrame->GetBorderThickness()));
					AdjustableScrollbarLabel->SetHorizontalAlignment(LabelComponent::HA_Left);
					AdjustableScrollbarLabel->SetWrapText(false); //new line characters manually added
					AdjustableScrollbarLabel->SetClampText(false); //manually clamped text
					AdjustableScrollbarLabel->SetCharacterSize(8);
					AdjustableScrollbarLabel->SetLineSpacing(1);
					AdjustableScrollbar->SetNumVisibleScrollPositions(AdjustableScrollbarLabel->GetMaxNumLines());
					RefreshAdjustableScrollbarText(0); //initialize text
					StageComponents.push_back(AdjustableScrollbarLabel);
				}
			}

			Vector2 rightOfAdjustableSet = Vector2(adjustableScrollbarFrame->GetTransform()->GetRightBounds() + 4, adjustableScrollbarFrame->GetTransform()->GetAbsCoordinatesY());
			FLOAT optionHeight = 36.f;
			FLOAT heightOffset = 0.f;
			FLOAT buttonWidth = 32.f;

			//Add disclaimer that it's intended about num visible scroll positions.
			LabelComponent* disclaimer = LabelComponent::CreateObject();
			if (!StageFrame->AddComponent(disclaimer))
			{
				UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach label component to stage frame."));
				disclaimer->Destroy();
			}
			else
			{
				disclaimer->GetTransform()->SetAbsCoordinates(rightOfAdjustableSet + Vector2(0, heightOffset));
				disclaimer->GetTransform()->SetBaseSize(Vector2(300, optionHeight + 24.f));
				heightOffset += disclaimer->GetTransform()->GetCurrentSizeY();
				disclaimer->SetCharacterSize(12);
				disclaimer->SetTextContent(TXT("It's intentional for the label to potentially go beyond its height since the scrollbar is telling it how many lines to draw."));
				StageComponents.push_back(disclaimer);
			}

			//Add settings to adjust maximum scroll position
			{
				ButtonComponent* decMaxScroll = ButtonComponent::CreateObject();
				if (!StageFrame->AddComponent(decMaxScroll))
				{
					UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach decrement max scroll position button to stage frame."));
					decMaxScroll->Destroy();
				}
				else
				{
					decMaxScroll->GetTransform()->SetAbsCoordinates(rightOfAdjustableSet + Vector2(0, heightOffset));
					decMaxScroll->GetTransform()->SetBaseSize(Vector2(buttonWidth, optionHeight));
					decMaxScroll->SetCaptionText(TXT("-"));	
					decMaxScroll->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleDecMaxScrollPosReleased, void, ButtonComponent*));
					StageComponents.push_back(decMaxScroll);
				}

				ButtonComponent* incMaxScroll = ButtonComponent::CreateObject();
				if (!StageFrame->AddComponent(incMaxScroll))
				{
					UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach increment max scroll position button to stage frame."));
					incMaxScroll->Destroy();
				}
				else
				{
					incMaxScroll->GetTransform()->SetAbsCoordinates(rightOfAdjustableSet + Vector2(buttonWidth, heightOffset));
					incMaxScroll->GetTransform()->SetBaseSize(Vector2(buttonWidth, optionHeight));
					incMaxScroll->SetCaptionText(TXT("+"));	
					incMaxScroll->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleIncMaxScrollPosReleased, void, ButtonComponent*));
					StageComponents.push_back(incMaxScroll);
				}

				LabelComponent* maxScrollLabel = LabelComponent::CreateObject();
				if (!StageFrame->AddComponent(maxScrollLabel))
				{
					UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach adjust max scroll position label to stage frame."));
					maxScrollLabel->Destroy();
				}
				else
				{
					maxScrollLabel->GetTransform()->SetAbsCoordinates(rightOfAdjustableSet + Vector2(buttonWidth * 2, heightOffset));
					maxScrollLabel->GetTransform()->SetBaseSize(Vector2(256, optionHeight));
					maxScrollLabel->SetTextContent(TXT("Max Scroll Position"));
					StageComponents.push_back(maxScrollLabel);
				}

				heightOffset += optionHeight;
			}

			//Add components to adjust number of visible positions
			{
				ButtonComponent* decNumVisPos = ButtonComponent::CreateObject();
				if (!StageFrame->AddComponent(decNumVisPos))
				{
					UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach decrement number of visible scroll positions button to stage frame."));
					decNumVisPos->Destroy();
				}
				else
				{
					decNumVisPos->GetTransform()->SetAbsCoordinates(rightOfAdjustableSet + Vector2(0, heightOffset));
					decNumVisPos->GetTransform()->SetBaseSize(Vector2(buttonWidth, optionHeight));
					decNumVisPos->SetCaptionText(TXT("-"));	
					decNumVisPos->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleDecNumVisPosReleased, void, ButtonComponent*));
					StageComponents.push_back(decNumVisPos);
				}

				ButtonComponent* incNumVisPos = ButtonComponent::CreateObject();
				if (!StageFrame->AddComponent(incNumVisPos))
				{
					UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach increment number of visible scroll positions button to stage frame."));
					incNumVisPos->Destroy();
				}
				else
				{
					incNumVisPos->GetTransform()->SetAbsCoordinates(rightOfAdjustableSet + Vector2(buttonWidth, heightOffset));
					incNumVisPos->GetTransform()->SetBaseSize(Vector2(buttonWidth, optionHeight));
					incNumVisPos->SetCaptionText(TXT("+"));	
					incNumVisPos->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleIncNumVisPosReleased, void, ButtonComponent*));
					StageComponents.push_back(incNumVisPos);
				}

				LabelComponent* numVisPosLabel = LabelComponent::CreateObject();
				if (!StageFrame->AddComponent(numVisPosLabel))
				{
					UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach adjust number of visible scroll positions label to stage frame."));
					numVisPosLabel->Destroy();
				}
				else
				{
					numVisPosLabel->GetTransform()->SetAbsCoordinates(rightOfAdjustableSet + Vector2(buttonWidth * 2, heightOffset));
					numVisPosLabel->GetTransform()->SetBaseSize(Vector2(256, optionHeight));
					numVisPosLabel->SetTextContent(TXT("Number of Visible Scroll Positions"));
					StageComponents.push_back(numVisPosLabel);
				}

				heightOffset += optionHeight;
			}

			//Add components to adjust scroll interval
			{
				ButtonComponent* decScrollInterval = ButtonComponent::CreateObject();
				if (!StageFrame->AddComponent(decScrollInterval))
				{
					UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach decrement scroll button button to stage frame."));
					decScrollInterval->Destroy();
				}
				else
				{
					decScrollInterval->GetTransform()->SetAbsCoordinates(rightOfAdjustableSet + Vector2(0, heightOffset));
					decScrollInterval->GetTransform()->SetBaseSize(Vector2(buttonWidth, optionHeight));
					decScrollInterval->SetCaptionText(TXT("-"));	
					decScrollInterval->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleDecScrollIntervalReleased, void, ButtonComponent*));
					StageComponents.push_back(decScrollInterval);
				}

				ButtonComponent* incScrollInterval= ButtonComponent::CreateObject();
				if (!StageFrame->AddComponent(incScrollInterval))
				{
					UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach increment scroll interval button to stage frame."));
					incScrollInterval->Destroy();
				}
				else
				{
					incScrollInterval->GetTransform()->SetAbsCoordinates(rightOfAdjustableSet + Vector2(buttonWidth, heightOffset));
					incScrollInterval->GetTransform()->SetBaseSize(Vector2(buttonWidth, optionHeight));
					incScrollInterval->SetCaptionText(TXT("+"));	
					incScrollInterval->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleIncScrollIntervalReleased, void, ButtonComponent*));
					StageComponents.push_back(incScrollInterval);
				}

				LabelComponent* scrollIntervalLabel = LabelComponent::CreateObject();
				if (!StageFrame->AddComponent(scrollIntervalLabel))
				{
					UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach adjust scroll interval label to stage frame."));
					scrollIntervalLabel->Destroy();
				}
				else
				{
					scrollIntervalLabel->GetTransform()->SetAbsCoordinates(rightOfAdjustableSet + Vector2(buttonWidth * 2, heightOffset));
					scrollIntervalLabel->GetTransform()->SetBaseSize(Vector2(256, optionHeight));
					scrollIntervalLabel->SetTextContent(TXT("Scroll Interval"));
					StageComponents.push_back(scrollIntervalLabel);
				}

				heightOffset += optionHeight;
			}

			//Add components to toggle scrollbar's visibility based on MaxScrollPosition and NumScrollPositions
			{
				ButtonComponent* toggleScrollVis = ButtonComponent::CreateObject();
				if (!StageFrame->AddComponent(toggleScrollVis))
				{
					UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach toggle scroll visibility based on num visible scroll positions button button to stage frame."));
					toggleScrollVis->Destroy();
				}
				else
				{
					toggleScrollVis->GetTransform()->SetAbsCoordinates(rightOfAdjustableSet + Vector2(0, heightOffset));
					toggleScrollVis->GetTransform()->SetBaseSize(Vector2(buttonWidth * 4, optionHeight));
					toggleScrollVis->SetCaptionText(TXT("Hiding when no scroll Position"));	
					toggleScrollVis->SetButtonReleasedHandler(SDFUNCTION_1PARAM(this, GUITester, HandleToggleScrollVisBasedPositionReleased, void, ButtonComponent*));
					StageComponents.push_back(toggleScrollVis);
				}

				heightOffset += optionHeight;
			}
		}
	}

	void GUITester::HandleDropdownButtonClicked (ButtonComponent* uiComponent)
	{
		UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Displaying Dropdown Components!"));
		ClearStageComponents();

		DropdownComponent* basicDropdown = DropdownComponent::CreateObject();
		if (!StageFrame->AddComponent(basicDropdown))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach basic dropdown to stage frame."));
		}
		else
		{
			basicDropdown->GetTransform()->SetBaseSize(Vector2(256, 128));
			basicDropdown->GetTransform()->SetAbsCoordinates(Vector2(48, 48));
			basicDropdown->OnOptionSelected = SDFUNCTION_1PARAM(this, GUITester, HandleBasicDropdownOptionClicked, void, INT);
			basicDropdown->SetNoSelectedItemText(TEXT("<Nothing selected>"));

			//Only add a few options since the basic dropdown is not testing the scrollbar component
			for (unsigned int i = 0; i < 4 && i < DropdownNumbers.size(); i++)
			{
				GUIDataElement<INT> newEntry;
				newEntry.Data = &DropdownNumbers.at(i);
				newEntry.LabelText = TXT("Option:  ") + DropdownNumbers.at(i).ToString();
				basicDropdown->AddOption(newEntry);
			}

			StageComponents.push_back(basicDropdown);
		}

		DropdownComponent* fullBasicDropdown = DropdownComponent::CreateObject();
		if (!StageFrame->AddComponent(fullBasicDropdown))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach full basic dropdown to stage frame."));
		}
		else
		{
			fullBasicDropdown->GetTransform()->SetBaseSize(Vector2(256, 128));
			fullBasicDropdown->GetTransform()->SetAbsCoordinates(Vector2(48, 175));
			fullBasicDropdown->OnOptionSelected = SDFUNCTION_1PARAM(this, GUITester, HandleFullBasicDropdownOptionClicked, void, INT);

			for (unsigned int i = 0; i < DropdownNumbers.size(); i++)
			{
				GUIDataElement<INT> newEntry;
				newEntry.Data = &DropdownNumbers.at(i);
				newEntry.LabelText = TXT("Option:  ") + DropdownNumbers.at(i).ToString();
				fullBasicDropdown->AddOption(newEntry);
			}

			fullBasicDropdown->SetSelectedItem(4);
			StageComponents.push_back(fullBasicDropdown);
		}

		ButtonBarDropdown = DropdownComponent::CreateObject();
		if (!StageFrame->AddComponent(ButtonBarDropdown))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach button bar dropdown to stage frame."));
		}
		else
		{
			ButtonBarDropdown->GetTransform()->SetBaseSize(Vector2(256, 128));
			ButtonBarDropdown->GetTransform()->SetAbsCoordinates(Vector2(48, 384));
			ButtonBarDropdown->OnOptionSelected = SDFUNCTION_1PARAM(this, GUITester, HandleButtonBarDropdownOptionClicked, void, INT);

			for (unsigned int i = 0; i < ButtonBar.size(); i++)
			{
				GUIDataElement<ButtonComponent> newEntry;
				newEntry.Data = ButtonBar.at(i);
				newEntry.LabelText = ButtonBar.at(i)->GetCaptionText();
				ButtonBarDropdown->AddOption(newEntry);
			}

			ButtonBarDropdown->SetSelectedItem(0);
			StageComponents.push_back(ButtonBarDropdown);
		}

		//Testing if the dropdown shrinks in size for having very few options.
		DropdownComponent* shortDropdown = DropdownComponent::CreateObject();
		if (!StageFrame->AddComponent(shortDropdown))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach short dropdown to stage frame."));
		}
		else
		{
			shortDropdown->GetTransform()->SetBaseSize(Vector2(256, 128));
			shortDropdown->GetTransform()->SetAbsCoordinates(Vector2(basicDropdown->GetTransform()->GetAbsCoordinatesX() + basicDropdown->GetTransform()->GetBaseSizeX() + 16.f, basicDropdown->GetTransform()->GetAbsCoordinatesY()));

			for (unsigned int i = 0; i < 2; i++)
			{
				GUIDataElement<INT> newEntry;
				newEntry.Data = &DropdownNumbers.at(i);
				newEntry.LabelText = TXT("Option:  ") + DropdownNumbers.at(i).ToString();
				shortDropdown->AddOption(newEntry);
			}

			shortDropdown->SetSelectedItem(0);
			StageComponents.push_back(shortDropdown);
		}
	}

	void GUITester::HandleGUIEntityButtonClicked (ButtonComponent* uiComponent)
	{
		UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Displaying GUIEntities!"));
		ClearStageComponents();

		TestMenuButtonList* buttonTest = TestMenuButtonList::CreateObject();
		CHECK(buttonTest != nullptr)
		buttonTest->SetWindowHandle(TestWindowHandle);
		buttonTest->GetTransform()->SetAbsCoordinates(Vector2(16, 64));
		buttonTest->GetTransform()->SetRelativeTo(StageFrame->GetTransform());
		buttonTest->GetTransform()->SetBaseSize(Vector2(256, 256));
		GUIEntities.push_back(buttonTest);

		TestMenuFocusInterface* focusTest = TestMenuFocusInterface::CreateObject();
		CHECK(focusTest != nullptr)
		focusTest->SetWindowHandle(TestWindowHandle);
		focusTest->GetTransform()->SetBaseSize(Vector2(256, 256));
		focusTest->GetTransform()->SetRelativeTo(StageFrame->GetTransform());
		focusTest->GetTransform()->SetAbsCoordinates(Vector2(256, 96));
		GUIEntities.push_back(focusTest);

		//Create UI elements that adjusts the focus
		LabelComponent* setFocusLabel = LabelComponent::CreateObject();
		if (!StageFrame->AddComponent(setFocusLabel))
		{
			UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach set focus label component to stage frame."));
		}
		else
		{
			setFocusLabel->GetTransform()->SetBaseSize(Vector2(75.f, 16.f));
			setFocusLabel->GetTransform()->SetAbsCoordinates(Vector2());
			setFocusLabel->SetHorizontalAlignment(LabelComponent::HA_Left);
			setFocusLabel->SetVerticalAlignment(LabelComponent::VA_Top);
			setFocusLabel->SetTextContent(TXT("Set Focus"));
			setFocusLabel->SetDrawOrder(GraphicsEngineComponent::DrawOrderUI + 500.f);
			StageComponents.push_back(setFocusLabel);

			SetFocusDropdown = DropdownComponent::CreateObject();
			if (!StageFrame->AddComponent(SetFocusDropdown))
			{
				UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Failed to attach set focus dropdown component to stage frame."));
				SetFocusDropdown = nullptr;
			}
			else
			{
				SetFocusDropdown->GetTransform()->SetBaseSize(Vector2(256.f, 256.f));
				SetFocusDropdown->GetTransform()->SetAbsCoordinates(setFocusLabel->GetTransform()->GetAbsCoordinates() + Vector2(setFocusLabel->GetTransform()->GetCurrentSizeX() + 16.f, 0.f));
				SetFocusDropdown->SetDrawOrder(GraphicsEngineComponent::DrawOrderUI + 501.f);
				SetFocusDropdown->OnOptionSelected = SDFUNCTION_1PARAM(this, GUITester, HandleFocusChange, void, INT);
				for (unsigned int i = 0; i < GUIEntities.size(); i++)
				{
					GUIDataElement<GUIEntity> dropdownEntry(GUIEntities.at(i), GUIEntities.at(i)->GetName());
					SetFocusDropdown->AddOption(dropdownEntry);
				}
				SetFocusDropdown->SetSelectedItem(0);
				GUIEntities.at(0)->GainFocus();
				StageComponents.push_back(SetFocusDropdown);
			}
		}
	}

	void GUITester::HandleHoverButtonClicked (ButtonComponent* uiComponent)
	{
		UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Displaying components with Hover Components!"));
		ClearStageComponents();

		FrameComponent* frame = FrameComponent::CreateObject();
		if (StageFrame->AddComponent(frame))
		{
			frame->GetTransform()->SetAbsCoordinates(Vector2());
			frame->GetTransform()->SetBaseSize(Vector2(128, 128));
			frame->SetLockedFrame(false);
			StageComponents.push_back(frame);

			HoverComponent* frameHover = HoverComponent::CreateObject();
			if (frame->AddComponent(frameHover))
			{
				frameHover->OnRollOver = SDFUNCTION_1PARAM(this, GUITester, HandleHoverFrameRolledOver, void, GUIComponent*);
				frameHover->OnRollOut = SDFUNCTION_1PARAM(this, GUITester, HandleHoverFrameRolledOut, void, GUIComponent*);
			}
		}

		HoverLabel = LabelComponent::CreateObject();
		if (StageFrame->AddComponent(HoverLabel))
		{
			HoverLabel->GetTransform()->SetBaseSize(Vector2(128, 16));
			HoverLabel->GetTransform()->SnapBottom();
			HoverLabel->GetTransform()->SnapCenterHorizontally();
			HoverLabel->SetTextContent(TXT("Hover over me"));
			StageComponents.push_back(HoverLabel);

			HoverComponent* labelHover = HoverComponent::CreateObject();
			if (HoverLabel->AddComponent(labelHover))
			{
				labelHover->OnRollOver = SDFUNCTION_1PARAM(this, GUITester, HandleHoverLabelRolledOver, void, GUIComponent*);
				labelHover->OnRollOut = SDFUNCTION_1PARAM(this, GUITester, HandleHoverLabelRolledOut, void, GUIComponent*);
			}
		}

		HoverButton = ButtonComponent::CreateObject();
		if (StageFrame->AddComponent(HoverButton))
		{
			HoverButton->GetTransform()->SetBaseSize(Vector2(128, 16));
			HoverButton->GetTransform()->SnapRight();
			HoverButton->GetTransform()->SnapTop();
			HoverButton->SetCaptionText(TXT("Hover over me"));
			StageComponents.push_back(HoverButton);

			HoverComponent* buttonHover = HoverComponent::CreateObject();
			if (HoverButton->AddComponent(buttonHover))
			{
				buttonHover->OnRollOver = SDFUNCTION_1PARAM(this, GUITester, HandleHoverButtonRolledOver, void, GUIComponent*);
				buttonHover->OnRollOut = SDFUNCTION_1PARAM(this, GUITester, HandleHoverButtonRolledOut, void, GUIComponent*);
			}
		}
	}

	void GUITester::HandleTreeListButtonClicked (ButtonComponent* uiComponent)
	{
		UNITTESTER_LOG(TestFlags, TXT("[GUITester]:  Displaying Tree List Components!"));
		ClearStageComponents();

		TreeListClassBrowser = TreeListComponent::CreateObject();
		if (StageFrame->AddComponent(TreeListClassBrowser))
		{
			TreeListClassBrowser->GetTransform()->SetAbsCoordinates(Vector2());
			TreeListClassBrowser->GetTransform()->SetBaseSize(StageFrame->GetTransform()->GetBaseSize());
			TreeListClassBrowser->OnOptionSelected = SDFUNCTION_1PARAM(this, GUITester, HandleTreeListClassSelected, void, INT);

			//Populate tree data
			vector<TreeListComponent::SDataBranch*> branches;
			for (ClassIterator iter(Object::SStaticClass()); iter.SelectedClass != nullptr; iter++)
			{
				TreeListComponent::SDataBranch* curBranch = new TreeListComponent::SDataBranch(new GUIDataElement<const DClass>(iter.SelectedClass, iter.SelectedClass->ToString()));

				//Find parent branch
				if (iter.SelectedClass->GetSuperClass() != nullptr)
				{
					for (unsigned int i = 0; i < branches.size(); i++)
					{
						GUIDataElement<const DClass>* otherBranch = dynamic_cast<GUIDataElement<const DClass>*>(branches.at(i)->Data);
						if (otherBranch->Data == iter.SelectedClass->GetSuperClass())
						{
							TreeListComponent::SDataBranch::LinkBranches(*branches.at(i), *curBranch);
							break;
						}
					}
				}

				branches.push_back(curBranch);
			}

			//Swap the first 10 elements with the last 10 elements to briefly test the sorting function
			for (unsigned int i = 0; i < 10; i++)
			{
				TreeListComponent::SDataBranch* otherBranch = branches.at(branches.size() - 1 - i);
				branches.at(branches.size() - 1 - i) = branches.at(i);
				branches.at(i) = otherBranch;
			}

			//If this function does not correctly sort this tree list, then the tree will not correctly display on UI
			TreeListComponent::SortDataList(branches);

			TreeListClassBrowser->SetTreeList(branches);
			TreeListClassBrowser->ExpandAll();

			//Select own class
			TreeListClassBrowser->SetSelectedItem<const DClass>(GUITester::SStaticClass());
			StageComponents.push_back(TreeListClassBrowser);
		}
		else
		{
			TreeListClassBrowser = nullptr;
		}
	}

	void GUITester::HandleCloseClicked (ButtonComponent* uiComponent)
	{
		UNITTESTER_LOG(TestFlags, TXT("End UI button was clicked.  Concluding UI test."));
		Destroy();
	}

	void GUITester::HandleNextPageClicked (ButtonComponent* uiComponent)
	{
		if (++ButtonBarPageIdx > (INT(ButtonBar.size()) / MaxButtonsPerPage))
		{
			ButtonBarPageIdx = 0;
		}

		UpdateButtonBarPageIdx();
	}

	void GUITester::HandlePrevPageClicked (ButtonComponent* uiComponent)
	{
		if (--ButtonBarPageIdx < 0)
		{
			ButtonBarPageIdx = INT(ButtonBar.size()) / MaxButtonsPerPage;
		}

		UpdateButtonBarPageIdx();
	}

	void GUITester::HandleFrameSizeChange ()
	{
		if (AlignmentFrames[0] != nullptr)
		{
			AlignmentFrames[0]->GetTransform()->SnapCenterVertically();
			AlignmentFrames[0]->GetTransform()->SnapCenterHorizontally();
		}

		if (AlignmentFrames[1] != nullptr)
		{
			AlignmentFrames[1]->GetTransform()->SnapCenterVertically();
			AlignmentFrames[1]->GetTransform()->SnapCenterHorizontally();
		}

		if (AlignmentFrames[2] != nullptr)
		{
			AlignmentFrames[2]->GetTransform()->SnapCenterVertically();
			AlignmentFrames[2]->GetTransform()->SnapCenterHorizontally();
		}

		if (AlignmentFrames[3] != nullptr)
		{
			AlignmentFrames[3]->GetTransform()->SnapTop();
			AlignmentFrames[3]->GetTransform()->SnapCenterHorizontally();
		}

		if (AlignmentFrames[4] != nullptr)
		{
			AlignmentFrames[4]->GetTransform()->SnapTop();
			AlignmentFrames[4]->GetTransform()->SnapRight();
		}

		if (AlignmentFrames[5] != nullptr)
		{
			AlignmentFrames[5]->GetTransform()->SnapCenterVertically();
			AlignmentFrames[5]->GetTransform()->SnapRight();
		}

		if (AlignmentFrames[6] != nullptr)
		{
			AlignmentFrames[6]->GetTransform()->SnapBottom();
			AlignmentFrames[6]->GetTransform()->SnapRight();
		}

		if (AlignmentFrames[7] != nullptr)
		{
			AlignmentFrames[7]->GetTransform()->SnapBottom();
			AlignmentFrames[7]->GetTransform()->SnapCenterHorizontally();
		}

		if (AlignmentFrames[8] != nullptr)
		{
			AlignmentFrames[8]->GetTransform()->SnapBottom();
			AlignmentFrames[8]->GetTransform()->SnapLeft();
		}

		if (AlignmentFrames[9] != nullptr)
		{
			AlignmentFrames[9]->GetTransform()->SnapCenterVertically();
			AlignmentFrames[9]->GetTransform()->SnapLeft();
		}

		if (AlignmentFrames[10] != nullptr)
		{
			AlignmentFrames[10]->GetTransform()->SnapTop();
			AlignmentFrames[10]->GetTransform()->SnapLeft();
		}

		if (AlignmentFrames[11] != nullptr)
		{
			AlignmentFrames[11]->GetTransform()->SnapCenterVertically();
			AlignmentFrames[11]->GetTransform()->SnapCenterHorizontally();
		}
	}

	void GUITester::HandleTestButtonPressed (ButtonComponent* uiComponent)
	{
		UNITTESTER_LOG1(TestFlags, TXT("Button \"%s\" was pressed."), uiComponent->GetCaptionText());
	}

	void GUITester::HandleTestButtonReleased (ButtonComponent* uiComponent)
	{
		UNITTESTER_LOG1(TestFlags, TXT("Button \"%s\" was released."), uiComponent->GetCaptionText());
	}

	void GUITester::HandleToggleButtonReleased (ButtonComponent* uiComponent)
	{
		if (ToggleButtons[0] == uiComponent)
		{
			ToggleButtons[0]->SetEnabled(false);
			ToggleButtons[1]->SetEnabled(true);
		}
		else
		{
			ToggleButtons[0]->SetEnabled(true);
			ToggleButtons[1]->SetEnabled(false);
		}

		HandleTestButtonReleased(uiComponent);
	}

	void GUITester::HandleBasicScrollbarIdxChange (INT newScrollPosition)
	{
		UNITTESTER_LOG1(TestFlags, TXT("Basic scroll bar changed position:  %s"), newScrollPosition);
	}

	void GUITester::HandleLabeledScrollbarIdxChange (INT newScrollPosition)
	{
		RefreshLabeledScrollbarText(newScrollPosition);
	}

	void GUITester::HandleAdjustableScrollbarIdxChange (INT newScrollPosition)
	{
		RefreshAdjustableScrollbarText(newScrollPosition);
	}

	void GUITester::HandleDecMaxScrollPosReleased (ButtonComponent* uiComponent)
	{
		if (AdjustableScrollbar->GetMaxScrollPosition() <= 0)
		{
			return;
		}

		AdjustableScrollbar->SetMaxScrollPosition(AdjustableScrollbar->GetMaxScrollPosition() - 1);
		RefreshAdjustableScrollbarText(AdjustableScrollbar->GetScrollPosition());
	}

	void GUITester::HandleIncMaxScrollPosReleased (ButtonComponent* uiComponent)
	{
		AdjustableScrollbar->SetMaxScrollPosition(AdjustableScrollbar->GetMaxScrollPosition() + 1);
		RefreshAdjustableScrollbarText(AdjustableScrollbar->GetScrollPosition());
	}

	void GUITester::HandleDecNumVisPosReleased (ButtonComponent* uiComponent)
	{
		if (AdjustableScrollbar->GetNumVisibleScrollPositions() <= 0)
		{
			return;
		}

		AdjustableScrollbar->SetNumVisibleScrollPositions(AdjustableScrollbar->GetNumVisibleScrollPositions() - 1);
		RefreshAdjustableScrollbarText(AdjustableScrollbar->GetScrollPosition());
	}
	void GUITester::HandleIncNumVisPosReleased (ButtonComponent* uiComponent)
	{
		AdjustableScrollbar->SetNumVisibleScrollPositions(AdjustableScrollbar->GetNumVisibleScrollPositions() + 1);
		RefreshAdjustableScrollbarText(AdjustableScrollbar->GetScrollPosition());
	}

	void GUITester::HandleDecScrollIntervalReleased (ButtonComponent* uiComponent)
	{
		if (AdjustableScrollbar->ScrollJumpInterval <= 1)
		{
			return;
		}

		AdjustableScrollbar->ScrollJumpInterval--;
	}

	void GUITester::HandleIncScrollIntervalReleased (ButtonComponent* uiComponent)
	{
		AdjustableScrollbar->ScrollJumpInterval++;
	}

	void GUITester::HandleToggleScrollVisBasedPositionReleased (ButtonComponent* uiComponent)
	{
		AdjustableScrollbar->SetHideWhenInsufficientScrollPos(!AdjustableScrollbar->GetHideWhenInsufficientScrollPos());
		const DString newCaption = (AdjustableScrollbar->GetHideWhenInsufficientScrollPos()) ? TXT("Hiding when no scroll Position") : TXT("Disabling when no scroll Position");
		uiComponent->SetCaptionText(newCaption);
	}

	void GUITester::HandleBasicDropdownOptionClicked (INT newOptionIdx)
	{
		UNITTESTER_LOG1(TestFlags, TXT("Basic dropdown component selected new option index:  %s"), newOptionIdx);
	}

	void GUITester::HandleFullBasicDropdownOptionClicked (INT newOptionIdx)
	{
		UNITTESTER_LOG1(TestFlags, TXT("Full basic dropdown component selected new option index:  %s"), newOptionIdx);
	}

	void GUITester::HandleButtonBarDropdownOptionClicked (INT newOptionIdx)
	{
		CHECK(VALID_OBJECT(ButtonBarDropdown))

		GUIDataElement<ButtonComponent>* selectedData = ButtonBarDropdown->GetSelectedItem<ButtonComponent>();
		ButtonComponent* selectedButton = selectedData->Data;
		if (selectedButton == nullptr)
		{
			Engine::GetEngine()->FatalError(TEXT("GUITester:  Dropdown component test failed.  Failed to retrieve button component instance after selecting a new item."));
			Destroy();
			return;
		}

		UNITTESTER_LOG2(TestFlags, TXT("Button bar dropdown component selected a button component (%s) with text caption equal to \"%s\"."), selectedButton->GetUniqueName(), selectedButton->GetCaptionText());
	}

	void GUITester::HandleFocusChange (INT newOptionIdx)
	{
		CHECK(VALID_OBJECT(SetFocusDropdown))

		GUIDataElement<GUIEntity>* selectedData = SetFocusDropdown->GetSelectedItem<GUIEntity>();
		GUIEntity* selectedGUI = selectedData->Data;
		if (selectedGUI == nullptr)
		{
			LOG(LOG_WARNING, TXT("Failed to select GUIEntity from Set Focus dropdown component."));
			return;
		}

		selectedGUI->GainFocus();
	}

	void GUITester::HandleHoverFrameRolledOver (GUIComponent* uiComponent)
	{
		UNITTESTER_LOG(TestFlags, TXT("Frame hover component rolled over."));
	}

	void GUITester::HandleHoverFrameRolledOut (GUIComponent* uiComponent)
	{
		UNITTESTER_LOG(TestFlags, TXT("Frame hover component rolled out."));
	}

	void GUITester::HandleHoverLabelRolledOver (GUIComponent* uiComponent)
	{
		UNITTESTER_LOG(TestFlags, TXT("Label hover component rolled over."));
		HoverLabel->SetTextContent(TXT("Rolled Over"));
	}

	void GUITester::HandleHoverLabelRolledOut (GUIComponent* uiComponent)
	{
		UNITTESTER_LOG(TestFlags, TXT("Label hover component rolled out."));
		HoverLabel->SetTextContent(TXT("Rolled Out"));
	}

	void GUITester::HandleHoverButtonRolledOver (GUIComponent* uiComponent)
	{
		UNITTESTER_LOG(TestFlags, TXT("Button hover component rolled over."));
		HoverButton->SetCaptionText(TXT("Rolled Over"));
	}

	void GUITester::HandleHoverButtonRolledOut (GUIComponent* uiComponent)
	{
		UNITTESTER_LOG(TestFlags, TXT("Button hover component rolled out."));
		HoverButton->SetCaptionText(TXT("Rolled Out"));
	}

	void GUITester::HandleTreeListClassSelected (INT newOptionIdx)
	{
		CHECK(VALID_OBJECT(TreeListClassBrowser))

		GUIDataElement<const DClass>* selectedItem = TreeListClassBrowser->GetSelectedItem<const DClass>();
		if (selectedItem != nullptr && selectedItem->Data != nullptr)
		{
			UNITTESTER_LOG1(TestFlags, TXT("Selected class:  %s"), selectedItem->Data->ToString());
		}
		else
		{
			UNITTESTER_LOG(TestFlags, TXT("Cleared class selection."));
		}
	}
}

#endif
#endif