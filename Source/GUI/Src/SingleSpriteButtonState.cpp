/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SingleSpriteButtonState.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GUIClasses.h"

#if INCLUDE_GUI

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(SingleSpriteButtonState, ButtonStateComponent)

	void SingleSpriteButtonState::InitProps ()
	{
		Super::InitProps();

		Sprite = nullptr;
	}

	void SingleSpriteButtonState::AttachTo (Entity* newOwner)
	{
		Super::AttachTo(newOwner);

		Sprite = (VALID_OBJECT(OwningButton)) ? OwningButton->RenderComponent : nullptr;
		SetDefaultAppearance();
	}

	void SingleSpriteButtonState::DrawToWindow (Window* newRelevantWindow)
	{
		Super::DrawToWindow(newRelevantWindow);

		if (VALID_OBJECT(Sprite))
		{
			Sprite->RelevantRenderWindows.clear();
			Sprite->RelevantRenderWindows.push_back(newRelevantWindow);
		}
	}

	void SingleSpriteButtonState::SetDefaultAppearance ()
	{
		if (VALID_OBJECT(Sprite))
		{
			Sprite->SetSubDivision(1, 4, 0, 0);
		}
	}

	void SingleSpriteButtonState::SetDisableAppearance ()
	{
		if (VALID_OBJECT(Sprite))
		{
			Sprite->SetSubDivision(1, 4, 0, 1);
		}
	}

	void SingleSpriteButtonState::SetHoverAppearance ()
	{
		if (VALID_OBJECT(Sprite))
		{
			Sprite->SetSubDivision(1, 4, 0, 2);
		}
	}

	void SingleSpriteButtonState::SetDownAppearance ()
	{
		if (VALID_OBJECT(Sprite))
		{
			Sprite->SetSubDivision(1, 4, 0, 3);
		}
	}
}

#endif