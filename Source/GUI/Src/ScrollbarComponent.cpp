/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ScrollbarComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GUIClasses.h"

#if INCLUDE_GUI

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(ScrollbarComponent, GUIComponent)

	void ScrollbarComponent::InitProps ()
	{
		Super::InitProps();

		Track = nullptr;
		MiddleMouseSprite = nullptr;
		PanningMiddleMouseCursor = nullptr;
		TopScrollButton = nullptr;
		BottomScrollButton = nullptr;
		ScrollJumpInterval = 3;
		bEnableMiddleMouseScrolling = true;
		MiddleMouseScrollIntervalRange = Range<FLOAT>(0.05f, 1.f);
		MiddleMouseHoldThreshold = 0.75f;
		ContinuousScrollDelay = 0.75f;
		ContinuousInterval = 0.2f;
		bClampsMouseWhenScrolling = true;

		ScrollPosition = 0;
		MaxScrollPosition = 0;
		NumVisibleScrollPositions = 1;
		bEnabled = true;
		bHideWhenInsufficientScrollPos = true;
		bHoldingScrollButtons = false;
		MiddleMousePosition = Vector2(-1, -1);
		MiddleMouseTimeStamp = -1;
		OnScrollPositionChanged = nullptr;

		MiddleMousePanDirection = 0;
		bClampingMousePointer = false;
	}

	void ScrollbarComponent::BeginObject ()
	{
		Super::BeginObject();

		if (!VALID_OBJECT(MiddleMouseSprite))
		{
			MiddleMouseSprite = SpriteComponent::CreateObject();
			MiddleMouseSprite->SetSpriteTexture(GUITheme::GetGUITheme()->ScrollbarMiddleMouseScrollAnchor);

			AbsTransformComponent* mouseTransformation = AbsTransformComponent::CreateObject();
			if (MiddleMouseSprite->AddComponent(mouseTransformation))
			{
				MiddleMouseSprite->TransformationInterface = mouseTransformation;
				mouseTransformation->SetBaseSize(MiddleMouseSprite->GetSpriteSize());
			}

			MiddleMouseSprite->SetVisibility(false);
			AddComponent(MiddleMouseSprite);
		}

		InitializeScrollButtons();
		InitializeTrackComponent();

		//Refresh the scroll bar for draw positions and track scale
		SetNumVisibleScrollPositions(NumVisibleScrollPositions);
		SetMaxScrollPosition(MaxScrollPosition);
		SetScrollPosition(0);
	}

	void ScrollbarComponent::SetWindowHandle (Window* newWindowHandle)
	{
		Super::SetWindowHandle(newWindowHandle);

		if (WindowHandle == nullptr)
		{
			return;
		}

		if (VALID_OBJECT(MiddleMouseSprite))
		{
			MiddleMouseSprite->RelevantRenderWindows.clear();
			MiddleMouseSprite->RelevantRenderWindows.push_back(WindowHandle);
		}

		if (VALID_OBJECT(PanningMiddleMouseCursor))
		{
			PanningMiddleMouseCursor->RelevantRenderWindows.clear();
			PanningMiddleMouseCursor->RelevantRenderWindows.push_back(WindowHandle);
		}
	}

	void ScrollbarComponent::Destroy ()
	{
		bClampingMousePointer = false;

		if (MiddleMousePosition.X >= 0 || MiddleMousePosition.Y >= 0)
		{
			REMOVE_TIMER(this, ScrollbarComponent::HandleMiddleMouseMove);

			//restore mouse pointer changes
			SetMiddleMousePosition(Vector2(-1,-1));
		}

		if (bHoldingScrollButtons)
		{
			REMOVE_TIMER(this, ScrollbarComponent::HandleScrollUpInterval);
			REMOVE_TIMER(this, ScrollbarComponent::HandleScrollDownInterval);
		}

		Super::Destroy();
	}

	void ScrollbarComponent::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
	{
		Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);

		if (!VALID_OBJECT(PanningMiddleMouseCursor))
		{
			return;
		}

		AbsTransformComponent* absCoordinates = dynamic_cast<AbsTransformComponent*>(PanningMiddleMouseCursor->TransformationInterface);
		if (absCoordinates != nullptr)
		{
			absCoordinates->SetAbsCoordinates(Vector2(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y)));
		}

		INT oldPanDirection = MiddleMousePanDirection;
		FLOAT deltaPos = MiddleMousePosition.Y - FLOAT::MakeFloat(sfmlEvent.y);

		if (deltaPos < -8)
		{
			MiddleMousePanDirection = -1;
		}
		else if (deltaPos > 8)
		{
			MiddleMousePanDirection = 1;
		}
		else
		{
			MiddleMousePanDirection = 0;
		}
		
		if (MiddleMousePanDirection != oldPanDirection)
		{
			switch(MiddleMousePanDirection.Value)
			{
				case(-1):
					PanningMiddleMouseCursor->SetSubDivision(1, 2, 0, 1);
					break;
				case(0):
					PanningMiddleMouseCursor->SetSubDivision(1, 1, 0, 0);
					break;
				case(1):
					PanningMiddleMouseCursor->SetSubDivision(1, 2, 0, 0);
					break;
			}
		}
	}

	bool ScrollbarComponent::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
	{
		//Only listen to middle mouse
		if (!bEnabled || sfmlEvent.button != sf::Mouse::Middle || !IsVisible() || !VALID_OBJECT_CAST(Owner, GUIComponent*))
		{
			return false;
		}

		if (!bEnableMiddleMouseScrolling)
		{
			return false;
		}

		if (eventType == sf::Event::MouseButtonPressed)
		{
			if (MiddleMousePosition.X >= 0 || MiddleMousePosition.Y >= 0)
			{
				//Toggle off
				SetMiddleMousePosition(Vector2(-1, -1));
				return true;				
			}

			MiddleMouseTimeStamp = Engine::GetEngine()->GetElapsedTime();
			SetMiddleMousePosition(Vector2(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y)));

			return true;
		}
		else if (eventType == sf::Event::MouseButtonReleased)
		{
			if (Engine::GetEngine()->GetElapsedTime() - MiddleMouseTimeStamp > MiddleMouseHoldThreshold)
			{
				//The middle mouse button was held long enough to be considered a "hold to move" action.
				SetMiddleMousePosition(Vector2(-1, -1));
				return true;
			}
		}

		return false;
	}

	bool ScrollbarComponent::ExecuteMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
	{
		if (VALID_OBJECT_CAST(Owner, GUIComponent*))
		{
			INT scrollMultiplier = -1;

			InputComponent* owningInput = GetInput();
			if (owningInput && owningInput->GetInputMessenger() && owningInput->GetInputMessenger()->GetCtrlHeld())
			{
				scrollMultiplier *= NumVisibleScrollPositions;
			}
			
			IncrementScrollPosition(FLOAT(sfmlEvent.delta).ToINT() * scrollMultiplier);
			return true;
		}

		return false;
	}

	bool ScrollbarComponent::ShouldHaveInputComponent () const
	{
		return (Super::ShouldHaveInputComponent() && bEnabled);
	}

	bool ScrollbarComponent::AcceptsMouseEvents (const unsigned int& mousePosX, const unsigned int& mousePosY) const
	{
		if (!bEnabled)
		{
			return false;
		}

		GUIComponent* guiOwner = dynamic_cast<GUIComponent*>(GetOwner());
		return (Super::AcceptsMouseEvents(mousePosX, mousePosY) || !guiOwner || guiOwner->GetTransform()->WithinBounds(FLOAT::MakeFloat(mousePosX), FLOAT::MakeFloat(mousePosY)));
	}

	void ScrollbarComponent::RefreshRenderComponentVisibility ()
	{
		Super::RefreshRenderComponentVisibility();

		//The middle mouse sprite's visibility is manually handled rather than sharing the flag of this component's visibility
		if (VALID_OBJECT(MiddleMouseSprite))
		{
			MiddleMouseSprite->SetVisibility(MiddleMousePosition.X >= 0 || MiddleMousePosition.Y >= 0);
		}
	}

	void ScrollbarComponent::CleanUpInvalidPointers ()
	{
		Super::CleanUpInvalidPointers();

		CLEANUP_INVALID_POINTER(Track)
		CLEANUP_INVALID_POINTER(MiddleMouseSprite)
		CLEANUP_INVALID_POINTER(PanningMiddleMouseCursor)
		CLEANUP_INVALID_POINTER(TopScrollButton)
		CLEANUP_INVALID_POINTER(BottomScrollButton)
	}

	void ScrollbarComponent::HandleSizeChange ()
	{
		Super::HandleSizeChange();

		if (VALID_OBJECT(TopScrollButton))
		{
			TopScrollButton->GetTransform()->SetBaseSize(Vector2(GetTransform()->GetBaseSizeX(), GetTransform()->GetBaseSizeX()));
			TopScrollButton->GetTransform()->SnapTop();

			if (VALID_OBJECT(BottomScrollButton))
			{
				BottomScrollButton->GetTransform()->SetBaseSize(TopScrollButton->GetTransform()->GetBaseSize());
				BottomScrollButton->GetTransform()->SnapBottom();
			}

			if (VALID_OBJECT(Track))
			{
				Track->GetTransform()->SetBaseSize(GetTransform()->GetBaseSize() - Vector2(0.f, TopScrollButton->GetTransform()->GetBaseSizeY() * 2));
				Track->GetTransform()->SetAbsCoordinates(Vector2(0, TopScrollButton->GetTransform()->GetCurrentSizeY()));
			}
		}
	}

	bool ScrollbarComponent::IsIndexVisible (const INT index) const
	{
		return (index >= ScrollPosition && index < ScrollPosition + NumVisibleScrollPositions && index < MaxScrollPosition);
	}

	void ScrollbarComponent::IncrementScrollPosition (INT amountToJump)
	{
		SetScrollPosition(ScrollPosition + amountToJump);
	}

	void ScrollbarComponent::SetScrollPositionChanged (std::function<void(INT newScrollPosition)> newHandler)
	{
		OnScrollPositionChanged = newHandler;
	}

	void ScrollbarComponent::SetScrollPosition (INT newScrollPosition)
	{
		newScrollPosition = Utils::Clamp<INT>(newScrollPosition, 0, MaxScrollPosition - NumVisibleScrollPositions);
		if (newScrollPosition == ScrollPosition)
		{
			return; //No need to update scroll position if the values did not change.
		}

		ScrollPosition = newScrollPosition;

		if (VALID_OBJECT(Track))
		{
			Track->RefreshScrollbarTrack();
		}

		if (OnScrollPositionChanged != nullptr)
		{
			OnScrollPositionChanged(ScrollPosition);
		}
	}

	void ScrollbarComponent::SetMaxScrollPosition (INT newMaxScrollPosition)
	{
		MaxScrollPosition = Utils::Max<INT>(newMaxScrollPosition, 0);
		EvaluateScrollButtonEnabledness();
		if (bHideWhenInsufficientScrollPos)
		{
			EvaluateScrollVisibility();
		}

		if (VALID_OBJECT(Track))
		{
			//This will toggle the thumb's visibility if NumVisibleScrollPositions <= MaxScrollPosition
			Track->RefreshScrollbarTrack();
		}
	}

	void ScrollbarComponent::SetNumVisibleScrollPositions (INT newNumVisibleScrollPositions)
	{
		NumVisibleScrollPositions = Utils::Max<INT>(newNumVisibleScrollPositions, 1);
		EvaluateScrollButtonEnabledness();
		if (bHideWhenInsufficientScrollPos)
		{
			EvaluateScrollVisibility();
		}
		
		if (VALID_OBJECT(Track))
		{
			Track->RefreshScrollbarTrack();
		}
	}

	void ScrollbarComponent::SetEnabled (bool bNewEnabled)
	{
		bEnabled = bNewEnabled;

		if (VALID_OBJECT(Track))
		{
			Track->SetEnabled(bNewEnabled);
		}

		EvaluateScrollButtonEnabledness();

		if (!bEnabled)
		{
			SetMiddleMousePosition(Vector2(-1, -1));
		}

		ConditionallySetInputComponent();
	}

	void ScrollbarComponent::SetHideWhenInsufficientScrollPos (bool bNewHideWhenInsufficientScrollPos)
	{
		bHideWhenInsufficientScrollPos = bNewHideWhenInsufficientScrollPos;
		EvaluateScrollVisibility();
	}

	void ScrollbarComponent::SetMiddleMousePosition (const Vector2& newPosition)
	{
		if (newPosition == MiddleMousePosition || GetMousePointer() == nullptr)
		{
			return;
		}

		MiddleMousePosition = newPosition;
		AbsTransformComponent* mouseTransformation = dynamic_cast<AbsTransformComponent*>(MiddleMouseSprite->TransformationInterface);
		if (VALID_OBJECT(mouseTransformation))
		{
			mouseTransformation->SetAbsCoordinates(newPosition);
			mouseTransformation->SetPivotPoint(MiddleMouseSprite->GetSpriteSize() * 0.5f);
		}

		MiddleMouseSprite->SetVisibility(newPosition.X >= 0 && newPosition.Y >= 0);

		if (MiddleMousePosition.X < 0 && MiddleMousePosition.Y < 0)
		{
			//Restore original mouse cursor
			GetMousePointer()->SetMouseVisibility(true);
			bClampingMousePointer = false;
			if (VALID_OBJECT(PanningMiddleMouseCursor))
			{
				RemoveComponent(PanningMiddleMouseCursor);
				PanningMiddleMouseCursor->Destroy();
				PanningMiddleMouseCursor = nullptr;
			}

			REMOVE_TIMER(this, ScrollbarComponent::HandleMiddleMouseMove);
		}
		else
		{
			//We hide the mouse cursor to implement additional functionality (such as overriding appearance even when hovering over other components and subdivisions).
			GetMousePointer()->SetMouseVisibility(false);
			
			//clamp the mouse pointer so that it cannot go beyond the parent component's boundaries
			GUIComponent* parentComponent = dynamic_cast<GUIComponent*>(GetOwner());
			if (VALID_OBJECT(parentComponent) && VALID_OBJECT(Transformation) && bClampsMouseWhenScrolling)
			{
				const Vector2 parentCoordinates = parentComponent->GetTransform()->CalcFinalCoordinates();
				Rectangle<INT> limitRegion(parentCoordinates.X.ToINT(), parentCoordinates.Y.ToINT(), parentComponent->GetTransform()->CalcFinalRightBounds().ToINT(), parentComponent->GetTransform()->CalcFinalBottomBounds().ToINT());
				GetMousePointer()->PushPositionLimit(limitRegion, SDFUNCTION_1PARAM(this, ScrollbarComponent, HandleLimitCallback, bool, const sf::Event::MouseMoveEvent&));
				bClampingMousePointer = true;
			}

			InitializePanningMiddleMouse();

			//TODO:  Replace this with a Timestamp and continuously compare the interval and the timestamp in Tick since the mouse may move within timer's interval (also to make GUI module independent from time module)
			SET_TIMER(this, CalculateMiddleMouseMoveInterval(), false, ScrollbarComponent::HandleMiddleMouseMove);
		}
	}

	void ScrollbarComponent::SetScrollButtonHeight (FLOAT newScrollButtonHeight)
	{	
		if (VALID_OBJECT(TopScrollButton))
		{
			TopScrollButton->GetTransform()->SetBaseSize(Vector2(GetTransform()->GetBaseSizeX(), newScrollButtonHeight));
		}

		if (VALID_OBJECT(BottomScrollButton))
		{
			BottomScrollButton->GetTransform()->SetBaseSize(Vector2(GetTransform()->GetBaseSizeX(), newScrollButtonHeight));
		}

		if (VALID_OBJECT(Track))
		{
			//Ensure the track component is between the two buttons
			Track->GetTransform()->SetBaseSize(Vector2(Transformation->GetBaseSizeX(), Transformation->GetBaseSizeY() - (newScrollButtonHeight * 2.f)));
			Track->GetTransform()->SetAbsCoordinates(Vector2(0, newScrollButtonHeight));
		}
	}

	INT ScrollbarComponent::GetScrollPosition () const
	{
		return ScrollPosition;
	}

	INT ScrollbarComponent::GetMaxScrollPosition () const
	{
		return MaxScrollPosition;
	}

	INT ScrollbarComponent::GetLastVisibleScrollPosition () const
	{
		return ScrollPosition + NumVisibleScrollPositions - 1;
	}
	
	INT ScrollbarComponent::GetNumVisibleScrollPositions () const
	{
		return NumVisibleScrollPositions;
	}
	
	bool ScrollbarComponent::GetEnabled () const
	{
		return bEnabled;
	}

	bool ScrollbarComponent::GetHideWhenInsufficientScrollPos () const
	{
		return bHideWhenInsufficientScrollPos;
	}

	Vector2 ScrollbarComponent::GetMiddleMousePosition () const
	{
		return MiddleMousePosition;
	}

	void ScrollbarComponent::InitializeScrollButtons ()
	{
		LabelComponent* buttonCaption = nullptr;
		if (!VALID_OBJECT(TopScrollButton))
		{
			TopScrollButton = ButtonComponent::CreateObject();
			TopScrollButton->RenderComponent->SetSpriteTexture(GUITheme::GetGUITheme()->ScrollbarTopButton);
			//TODO:  Get scroll button dimensions from UITheme instead of hard coding them
			TopScrollButton->GetTransform()->SetBaseSize(Vector2(GetTransform()->GetBaseSizeX(), 16.f));
			TopScrollButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			TopScrollButton->SetButtonPressedHandler(std::bind(&ScrollbarComponent::HandleScrollButtonPressed, this, placeholders::_1));
			TopScrollButton->SetButtonReleasedHandler(std::bind(&ScrollbarComponent::HandleScrollButtonReleased, this, placeholders::_1));

			buttonCaption = TopScrollButton->CaptionComponent;
			if (buttonCaption != nullptr && TopScrollButton->RemoveComponent(buttonCaption))
			{
				buttonCaption->Destroy();
				TopScrollButton->CaptionComponent = nullptr;
				buttonCaption = nullptr;
			}
			AddComponent(TopScrollButton);
		}

		if (!VALID_OBJECT(BottomScrollButton))
		{
			BottomScrollButton = ButtonComponent::CreateObject();
			BottomScrollButton->RenderComponent->SetSpriteTexture(GUITheme::GetGUITheme()->ScrollbarBottomButton);
			BottomScrollButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
			BottomScrollButton->GetTransform()->SetBaseSize(Vector2(GetTransform()->GetBaseSizeX(), 16.f));
			BottomScrollButton->SetButtonPressedHandler(std::bind(&ScrollbarComponent::HandleScrollButtonPressed, this, placeholders::_1));
			BottomScrollButton->SetButtonReleasedHandler(std::bind(&ScrollbarComponent::HandleScrollButtonReleased, this, placeholders::_1));

			buttonCaption = BottomScrollButton->CaptionComponent;
			if (buttonCaption != nullptr && BottomScrollButton->RemoveComponent(buttonCaption))
			{
				buttonCaption->Destroy();
				BottomScrollButton->CaptionComponent = nullptr;
				buttonCaption = nullptr;
			}
			AddComponent(BottomScrollButton);
		}

		EvaluateScrollButtonEnabledness();
	}

	void ScrollbarComponent::InitializeTrackComponent ()
	{
		if (!VALID_OBJECT(Track))
		{
			Track = ScrollbarTrackComponent::CreateObject();
			Track->GetTransform()->SetBaseSize(Vector2(Transformation->GetBaseSizeX(), Transformation->GetBaseSizeY() - (TopScrollButton->GetTransform()->GetCurrentSizeY() * 2.f)));
			Track->GetTransform()->SetAbsCoordinates(Vector2(0, TopScrollButton->GetTransform()->GetCurrentSizeY()));
			Track->SetThumbChangedPositionHandler(bind(&ScrollbarComponent::HandleTrackChangedPosition, this, placeholders::_1));
			AddComponent(Track);
		}
	}

	void ScrollbarComponent::InitializePanningMiddleMouse ()
	{
		PanningMiddleMouseCursor = SpriteComponent::CreateObject();		
		if (!AddComponent(PanningMiddleMouseCursor))
		{
			PanningMiddleMouseCursor->Destroy();
			PanningMiddleMouseCursor = nullptr;
			return;
		}

		//Bind a transformation object to the mouse cursor
		AbsTransformComponent* mouseTransformation = AbsTransformComponent::CreateObject();
		if (!PanningMiddleMouseCursor->AddComponent(mouseTransformation))
		{
			return;
		}

		PanningMiddleMouseCursor->TransformationInterface = mouseTransformation;
		PanningMiddleMouseCursor->SetSpriteTexture(GUITheme::GetGUITheme()->ScrollbarPointer);
		PanningMiddleMouseCursor->SetDrawOrder(MiddleMouseSprite->GetDrawOrder() + 1);
		PanningMiddleMouseCursor->RelevantRenderWindows.empty();
		PanningMiddleMouseCursor->RelevantRenderWindows.push_back(GetWindowHandle());
		Vector2 spriteSize;

		GUITheme::GetGUITheme()->ScrollbarPointer->GetDimensions(spriteSize);
		mouseTransformation->SetBaseSize(spriteSize * Vector2(1.f, 0.5f)); //flatten the sprite to be half size horizontally
		mouseTransformation->SetAbsCoordinates(GetMousePointer()->GetMousePosition());

		//Set the pivot point to be centered based on sprite size
		mouseTransformation->SetPivotPoint(spriteSize * 0.5f);

		MiddleMousePanDirection = 0;
	}

	void ScrollbarComponent::EvaluateScrollButtonEnabledness ()
	{
		if (VALID_OBJECT(TopScrollButton))
		{
			TopScrollButton->SetEnabled(bEnabled && NumVisibleScrollPositions < MaxScrollPosition);
		}

		if (VALID_OBJECT(BottomScrollButton))
		{
			BottomScrollButton->SetEnabled(bEnabled && NumVisibleScrollPositions < MaxScrollPosition);
		}
	}

	void ScrollbarComponent::EvaluateScrollVisibility ()
	{
		SetVisibility(!bHideWhenInsufficientScrollPos || NumVisibleScrollPositions < MaxScrollPosition);
	}

	FLOAT ScrollbarComponent::CalculateMiddleMouseMoveInterval ()
	{
		if (MiddleMouseScrollIntervalRange.IsConverged())
		{
			//No use calculating the interval if the min/max are equal
			return (MiddleMouseScrollIntervalRange.Min);
		}

		INT mousePosX;
		INT mousePosY;
		GetMousePointer()->GetMousePosition(mousePosX, mousePosY);

		FLOAT relativeDistance = 0.f;
		FLOAT distRatio;
		if (MiddleMousePanDirection < 0) //scrolling down
		{
			relativeDistance = mousePosY.ToFLOAT() - MiddleMousePosition.Y;
			distRatio = relativeDistance / (Transformation->CalcFinalBottomBounds() - MiddleMousePosition.Y);
		}
		else if (MiddleMousePanDirection > 0) //scrolling up
		{
			relativeDistance = mousePosY.ToFLOAT() - MiddleMousePosition.Y;
			distRatio = relativeDistance / (Transformation->CalcFinalCoordinatesY() - MiddleMousePosition.Y);
		}
		else
		{
			return 0.5f; //check periodically
		}

		//safety check to avoid setting timer to 0 seconds
		distRatio = Utils::Clamp<FLOAT>(distRatio, 0.01f, 0.99f);

		return Utils::Lerp(1-distRatio, MiddleMouseScrollIntervalRange.Min, MiddleMouseScrollIntervalRange.Max);
	}

	FLOAT ScrollbarComponent::HandleCalcTrackPosOffset () const
	{
		if (VALID_OBJECT(TopScrollButton))
		{
			return TopScrollButton->GetTransform()->GetCurrentSizeY() * -1.f;
		}

		return 0.f;
	}

	void ScrollbarComponent::HandleScrollButtonPressed (ButtonComponent* uiComponent)
	{
		bHoldingScrollButtons = true;
		INT scrollDirection = (uiComponent == TopScrollButton) ? -1 : 1;
		IncrementScrollPosition(ScrollJumpInterval * scrollDirection);

		if (uiComponent == TopScrollButton)
		{
			SET_TIMER(this, ContinuousScrollDelay, false, ScrollbarComponent::HandleScrollUpInterval);
		}
		else
		{
			SET_TIMER(this, ContinuousScrollDelay, false, ScrollbarComponent::HandleScrollDownInterval);
		}
	}

	void ScrollbarComponent::HandleScrollButtonReleased (ButtonComponent* uiComponent)
	{
		bHoldingScrollButtons = false;
		if (!REMOVE_TIMER(this, ScrollbarComponent::HandleScrollUpInterval))
		{
			REMOVE_TIMER(this, ScrollbarComponent::HandleScrollDownInterval);
		}
	}

	void ScrollbarComponent::HandleTrackChangedPosition (INT newScrollPosition)
	{
		//Do SetScrollPosition without refreshing the track component
		newScrollPosition = Utils::Clamp<INT>(newScrollPosition, 0, MaxScrollPosition - NumVisibleScrollPositions);
		if (newScrollPosition == ScrollPosition)
		{
			return;
		}

		ScrollPosition = newScrollPosition;

		if (OnScrollPositionChanged != nullptr)
		{
			OnScrollPositionChanged(ScrollPosition);
		}
	}

	bool ScrollbarComponent::HandleMiddleMouseMove ()
	{
		if (MiddleMousePosition.X < 0 && MiddleMousePosition.Y < 0)
		{
			return true;
		}

		if (MiddleMousePanDirection != 0) //If the user is not hovering over middle mouse position
		{
			FLOAT mousePosX;
			FLOAT mousePosY;
			GetMousePointer()->GetMousePosition(mousePosX, mousePosY);

			INT scrollDirection = (mousePosY > MiddleMousePosition.Y) ? 1 : -1;
			IncrementScrollPosition(ScrollJumpInterval * scrollDirection);
		}

		SET_TIMER(this, CalculateMiddleMouseMoveInterval(), false, ScrollbarComponent::HandleMiddleMouseMove);
		return true;
	}

	bool ScrollbarComponent::HandleLimitCallback (const sf::Event::MouseMoveEvent& mouseEvent)
	{
		return (bClampingMousePointer);
	}

	bool ScrollbarComponent::HandleScrollUpInterval ()
	{
		if (VALID_OBJECT(TopScrollButton) && !TopScrollButton->GetPressedDown())
		{
			REMOVE_TIMER(this, HandleScrollUpInterval);
			return false;
		}

		IncrementScrollPosition(ScrollJumpInterval * -1);
		SET_TIMER(this, ContinuousInterval, false, ScrollbarComponent::HandleScrollUpInterval);
		return true;
	}

	bool ScrollbarComponent::HandleScrollDownInterval ()
	{
		if (VALID_OBJECT(BottomScrollButton) && !BottomScrollButton->GetPressedDown())
		{
			REMOVE_TIMER(this, HandleScrollDownInterval);
			return false;
		}

		IncrementScrollPosition(ScrollJumpInterval);
		SET_TIMER(this, ContinuousInterval, false, ScrollbarComponent::HandleScrollDownInterval);
		return true;
	}
}

#endif