/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  FrameComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GUIClasses.h"

#if INCLUDE_GUI

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(FrameComponent, GUIComponent)

	/* Various flags that are associated with a EBorder type. */
	const INT FrameComponent::HorizontalBorderFlag = 1;
	const INT FrameComponent::VerticalBorderFlag = 2;

	void FrameComponent::InitProps ()
	{
		Super::InitProps();

		bLockedFrame = false;
		HorGrabbedBorder = B_None;
		VertGrabbedBorder = B_None;
		SetBorderThickness(8.f);
		RenderComponent = nullptr;
		HoverFlags = 0;
	}

	void FrameComponent::BeginObject ()
	{
		Super::BeginObject();

		RenderComponent = BorderedSpriteComponent::CreateObject();
		RenderComponent->SetSpriteTexture(GUITheme::GetGUITheme()->FrameFill);	
		RenderComponent->TopBorder = GUITheme::GetGUITheme()->FrameBorderTop;
		RenderComponent->RightBorder = GUITheme::GetGUITheme()->FrameBorderRight;
		RenderComponent->BottomBorder = GUITheme::GetGUITheme()->FrameBorderBottom;
		RenderComponent->LeftBorder = GUITheme::GetGUITheme()->FrameBorderLeft;
		RenderComponent->TopRightCorner = GUITheme::GetGUITheme()->FrameBorderTopRight;
		RenderComponent->BottomRightCorner = GUITheme::GetGUITheme()->FrameBorderBottomRight;
		RenderComponent->BottomLeftCorner = GUITheme::GetGUITheme()->FrameBorderBottomLeft;
		RenderComponent->TopLeftCorner = GUITheme::GetGUITheme()->FrameBorderTopLeft;

		RenderComponent->BorderThickness = BorderThickness;
		AddComponent(RenderComponent);

		if (VALID_OBJECT(Transformation))
		{
			RenderComponent->SetDrawOrder(CurrentDrawOrder);
			RenderComponent->TransformationInterface = Transformation;
		}
	}

	void FrameComponent::SetWindowHandle (Window* newWindowHandle)
	{
		Super::SetWindowHandle(newWindowHandle);

		if (VALID_OBJECT(RenderComponent) && WindowHandle != nullptr)
		{
			RenderComponent->RelevantRenderWindows.clear();
			RenderComponent->RelevantRenderWindows.push_back(WindowHandle);
		}
	}

	void FrameComponent::Destroy ()
	{
		//Remove callback in case this was destroyed while user was hovering over it
		if (GetMousePointer() != nullptr)
		{
			GetMousePointer()->RemoveIconOverride(SDFUNCTION_1PARAM(this, FrameComponent, HandleMouseIconOverride, bool, const sf::Event::MouseMoveEvent&));
		}

		Super::Destroy();
	}

	void FrameComponent::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
	{
		Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);

		if (HoverFlags > 0)
		{
			mouse->ConditionallyEvaluateMouseIconOverride(SDFUNCTION_1PARAM(this, FrameComponent, HandleMouseIconOverride, bool, const sf::Event::MouseMoveEvent&));
		}

		if (bLockedFrame || !VALID_OBJECT(Transformation))
		{
			return;
		}

		//Update mouse pointer if hovering over borders
		if (HorGrabbedBorder == B_None && VertGrabbedBorder == B_None)
		{
			//Update the mouse icon if necessary
			ShouldOverrideMouseIcon(sfmlEvent);

			return;
		}

		// User is grabbing the borders.  Reposition and/or rescale the borders.
		CalculateTransformationChanges(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y));
	}

	bool FrameComponent::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
	{
		//Frames only listen to left mouse button
		if (sfmlEvent.button != sf::Mouse::Left || !VALID_OBJECT(Transformation))
		{
			return false;
		}

		//Release grabbing borders
		if (eventType == sf::Event::MouseButtonReleased)
		{
			if (HorGrabbedBorder != B_None || VertGrabbedBorder != B_None)
			{
				HorGrabbedBorder = B_None;
				VertGrabbedBorder = B_None;
				ConditionallySetExternalBorderInput();
			}

			return false;
		}

		if (bLockedFrame || !Transformation->WithinBounds(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y)))
		{
			return false;
		}

		const Vector2 finalCoordinates = GetTransform()->CalcFinalCoordinates();

		//check vertical borders
		if (FLOAT::MakeFloat(sfmlEvent.x) <= finalCoordinates.X + BorderThickness)
		{
			VertGrabbedBorder = B_Left;
		}
		else if (FLOAT::MakeFloat(sfmlEvent.x) >= Transformation->CalcFinalRightBounds() - BorderThickness)
		{
			VertGrabbedBorder = B_Right;
		}

		//check horizontal borders
		if (FLOAT::MakeFloat(sfmlEvent.y) <= finalCoordinates.Y + BorderThickness)
		{
			HorGrabbedBorder = B_Top;
		}
		else if (FLOAT::MakeFloat(sfmlEvent.y) >= Transformation->CalcFinalBottomBounds() - BorderThickness)
		{
			HorGrabbedBorder = B_Bottom;
		}

		ConditionallySetExternalBorderInput();
		return (VertGrabbedBorder != B_None || HorGrabbedBorder != B_None);
	}

	bool FrameComponent::ExecuteMouseWheelMove (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent)
	{
		return false;
	}

	bool FrameComponent::ShouldHaveExternalInputComponent () const
	{
		if (!bLockedFrame)
		{
			if (HoverFlags > 0)
			{
				return true;
			}

			if (HorGrabbedBorder != B_None || VertGrabbedBorder != B_None)
			{
				return true;
			}
		}

		return Super::ShouldHaveExternalInputComponent();
	}

	void FrameComponent::CleanUpInvalidPointers ()
	{
		Super::CleanUpInvalidPointers();

		//Double check publicly accessible pointers
		CLEANUP_INVALID_POINTER(RenderComponent)
	}

	void FrameComponent::SetLockedFrame (bool bNewLockedFrame)
	{
		bLockedFrame = bNewLockedFrame;

		if (bLockedFrame)
		{
			//Release mouse controls
			HorGrabbedBorder = B_None;
			VertGrabbedBorder = B_None;
			HoverFlags = 0;
		}

		ConditionallySetInputComponent();
		ConditionallySetExternalBorderInput();
	}

	void FrameComponent::SetBorderThickness (FLOAT newBorderThickness)
	{
		BorderThickness = Utils::Max(FLOAT(1.f), newBorderThickness);

		if (VALID_OBJECT(RenderComponent))
		{
			RenderComponent->BorderThickness = BorderThickness;
		}
	}

	bool FrameComponent::GetLockedFrame () const
	{
		return bLockedFrame;
	}

	FLOAT FrameComponent::GetBorderThickness () const
	{
		return BorderThickness;
	}

	bool FrameComponent::ShouldOverrideMouseIcon (const sf::Event::MouseMoveEvent& sfmlEvent)
	{
		if (bLockedFrame || !VALID_OBJECT(Transformation))
		{
			return false;
		}

		if (HorGrabbedBorder != B_None || VertGrabbedBorder != B_None)
		{
			return true;
		}

		INT oldHoverFlags = HoverFlags;
		HoverFlags = 0;

		if (!Transformation->WithinBounds(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y)))
		{
			return false; //not overriding pointer
		}

		FLOAT mousePosX = FLOAT::MakeFloat(sfmlEvent.x);
		FLOAT mousePosY = FLOAT::MakeFloat(sfmlEvent.y);
		Texture* newIcon = nullptr;

		const Vector2 finalCoordinates = GetTransform()->CalcFinalCoordinates();

		//check vertical borders
		bool bHoveringVerticalBorder = (mousePosX <= finalCoordinates.X + BorderThickness || mousePosX >= Transformation->CalcFinalRightBounds() - BorderThickness);
		bool bHoveringHorizontalBorder = (mousePosY <= finalCoordinates.Y + BorderThickness || mousePosY >= Transformation->CalcFinalBottomBounds() - BorderThickness);

		if (bHoveringVerticalBorder)
		{
			HoverFlags |= VerticalBorderFlag;
		}

		if (bHoveringHorizontalBorder)
		{
			HoverFlags |= HorizontalBorderFlag;
		}

		ConditionallySetExternalBorderInput();
		if (HoverFlags == 0)
		{
			return false;
		}

		if (HoverFlags != oldHoverFlags)
		{
			if (bHoveringVerticalBorder && bHoveringHorizontalBorder)
			{
				//Check if diagonal pointer should move in the \ or / direction
				bool bUpperLeft = ((mousePosX <= finalCoordinates.X + BorderThickness && mousePosY <= finalCoordinates.Y + BorderThickness) ||
						(mousePosX >= Transformation->CalcFinalRightBounds() - BorderThickness && mousePosY >= Transformation->CalcFinalBottomBounds() - BorderThickness));

				newIcon = (bUpperLeft) ? GUITheme::GetGUITheme()->ResizePointerDiagonalTopLeft : GUITheme::GetGUITheme()->ResizePointerDiagonalBottomLeft;
			}
			else if ((HoverFlags & HorizontalBorderFlag) != 0)
			{
				newIcon = GUITheme::GetGUITheme()->ResizePointerVertical;
			}
			else if ((HoverFlags & VerticalBorderFlag) != 0)
			{
				newIcon = GUITheme::GetGUITheme()->ResizePointerHorizontal;
			}

			//Push a new mouse pointer to the stack for changing directions
			//Note:  It's known that this could stack multiple elements, but they'll pop when the callback returns false.
			GetMousePointer()->PushMouseIconOverride(newIcon, SDFUNCTION_1PARAM(this, FrameComponent, HandleMouseIconOverride, bool, const sf::Event::MouseMoveEvent&));
		}

		return true;
	}

	void FrameComponent::CalculateTransformationChanges (FLOAT mousePosX, FLOAT mousePosY)
	{
		//Ensure the mouse positions aren't negative (ie:  dragging beyond window borders)
		mousePosX = Utils::Max<FLOAT>(mousePosX, 0.f);
		mousePosY = Utils::Max<FLOAT>(mousePosY, 0.f);

		Range<FLOAT> posXLimits;
		Range<FLOAT> posYLimits;
		Range<FLOAT> sizeXLimits;
		Range<FLOAT> sizeYLimits;
		CalculateTransformationLimits(posXLimits, posYLimits, sizeXLimits, sizeYLimits);

		const Vector2 finalCoordinates = GetTransform()->CalcFinalCoordinates();
		Vector2 newSize = GetTransform()->GetCurrentSize();
		Vector2 newPos = finalCoordinates;

		//handle vertical borders
		if (VertGrabbedBorder == B_Left)
		{
			newSize.X = Utils::Clamp(GetTransform()->CalcFinalRightBounds() - mousePosX, sizeXLimits.Min, sizeXLimits.Max);
			newPos.X = Utils::Clamp(mousePosX, posXLimits.Min, posXLimits.Max);
		}
		else if (VertGrabbedBorder == B_Right)
		{
			newSize.X = Utils::Clamp(mousePosX - finalCoordinates.X, sizeXLimits.Min, sizeXLimits.Max);
		}

		//handle horizontal borders
		if (HorGrabbedBorder == B_Top)
		{
			newSize.Y = Utils::Clamp(GetTransform()->CalcFinalBottomBounds() - mousePosY, sizeYLimits.Min, sizeYLimits.Max);
			newPos.Y = Utils::Clamp(mousePosY, posYLimits.Min, posYLimits.Max);
		}
		else if (HorGrabbedBorder == B_Bottom)
		{
			newSize.Y = Utils::Clamp(mousePosY - finalCoordinates.Y, sizeYLimits.Min, sizeYLimits.Max);
		}

		Vector2 relativeTo = (VALID_OBJECT(GetTransform()->GetRelativeTo())) ? GetTransform()->GetRelativeTo()->CalcFinalCoordinates() : Vector2(0,0);

		GetTransform()->SetBaseSize(newSize);
		GetTransform()->SetAbsCoordinates(newPos - relativeTo);
	}

	void FrameComponent::CalculateTransformationLimits (Range<FLOAT>& outAbsCoordinatesXLimits, Range<FLOAT>& outAbsCoordinatesYLimits, Range<FLOAT>& outSizeXLimits, Range<FLOAT>& outSizeYLimits) const
	{
		outSizeXLimits = Range<FLOAT>((BorderThickness * 2) + GetTransform()->GetSizeXClamps().Min, MAXFLOAT);
		outSizeYLimits = Range<FLOAT>((BorderThickness * 2) + GetTransform()->GetSizeYClamps().Min, MAXFLOAT);
		outAbsCoordinatesXLimits = Range<FLOAT>(0.f, GetTransform()->CalcFinalRightBounds() - outSizeXLimits.Min);
		outAbsCoordinatesYLimits = Range<FLOAT>(0.f, GetTransform()->CalcFinalBottomBounds() - outSizeYLimits.Min);

		//Adjust the max limits to ensure this frame component is within the bounds of owning component
		GUIComponent* owningComponent = dynamic_cast<GUIComponent*>(GetOwner());
		if (owningComponent)
		{
			const Vector2 bounds = owningComponent->GetTransform()->CalcFinalCoordinates();
			outAbsCoordinatesXLimits.Min = bounds.X;
			outAbsCoordinatesYLimits.Min = bounds.Y;

			if (VertGrabbedBorder == B_Left)
			{
				outSizeXLimits.Max = GetTransform()->CalcFinalRightBounds() - owningComponent->GetTransform()->CalcFinalCoordinatesX();
			}
			else if (VertGrabbedBorder == B_Right)
			{
				outSizeXLimits.Max = owningComponent->GetTransform()->CalcFinalRightBounds() - GetTransform()->CalcFinalCoordinatesX();
			}

			if (HorGrabbedBorder == B_Top)
			{
				outSizeYLimits.Max = GetTransform()->CalcFinalBottomBounds() - owningComponent->GetTransform()->CalcFinalCoordinatesY();
			}
			else if (HorGrabbedBorder == B_Bottom)
			{
				outSizeYLimits.Max = owningComponent->GetTransform()->CalcFinalBottomBounds() - GetTransform()->CalcFinalCoordinatesY();
			}

			//Ensure nested frame components do not overlap borders (this is kind of ugly, but I can't think a cleaner way of doing this without building a generic system for something rather specific).
			FrameComponent* owningFrame = dynamic_cast<FrameComponent*>(owningComponent);
			if (owningFrame != nullptr)
			{
				outAbsCoordinatesXLimits.Min += owningFrame->GetBorderThickness();
				outAbsCoordinatesYLimits.Min += owningFrame->GetBorderThickness();
				outSizeXLimits.Max -= owningFrame->GetBorderThickness();
				outSizeYLimits.Max -= owningFrame->GetBorderThickness();
			}
		}
	}

	bool FrameComponent::HandleMouseIconOverride (const sf::Event::MouseMoveEvent& sfmlEvent)
	{
		if (GetPendingDelete())
		{
			return false;
		}

		return ShouldOverrideMouseIcon(sfmlEvent);
	}
}

#endif