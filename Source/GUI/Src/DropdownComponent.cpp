/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DropdownComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GUIClasses.h"

#if INCLUDE_GUI

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(DropdownComponent, GUIComponent)

	void DropdownComponent::InitProps ()
	{
		Super::InitProps();

		SelectedItemIdx = UINT_INDEX_NONE;
		BrowseIndex = INT_INDEX_NONE;
		NoSelectedItemText = TXT("");
		bExpanded = false;
		ExpandedOptionHeight = 16.f;

		ExpandButton = nullptr;
		ButtonTextureCollapsed = nullptr;
		ButtonTextureExpanded = nullptr;
		ExpandBackground = nullptr;
		SelectedObjBackground = nullptr;
		SelectedItemLabel = nullptr;
		ListLabel = nullptr;
		HighlightBar = nullptr;
		HighlightBarTransform = nullptr;
		ListScrollbar = nullptr;

		DragToggleThresholdTime = 0.5f;
		bIgnoreEnterRelease = false;
		SearchString = TXT("");
		SearchStringTimestamp = 0.f;
		SearchStringClearTime = 1.f;
	}

	void DropdownComponent::BeginObject ()
	{
		Super::BeginObject();

		ButtonTextureCollapsed = GUITheme::GetGUITheme()->DropdownCollapsedButton;
		ButtonTextureExpanded = GUITheme::GetGUITheme()->DropdownExpandedButton;

		InitializeSelectedObjBackground();
		InitializeExpandButton();
		InitializeSelectedItemLabel();
		InitializeExpandBackground();
		InitializeListScrollbar();
		InitializeListLabel();
		InitializeHighlightBar();
		RefreshComponentTransforms();
	}

	void DropdownComponent::Destroy ()
	{
		ClearList(); //Need to deallocate the list of pointers

		Super::Destroy();
	}

	void DropdownComponent::SetWindowHandle (Window* newWindowHandle)
	{
		Super::SetWindowHandle(newWindowHandle);

		if (VALID_OBJECT(HighlightBar))
		{
			HighlightBar->RelevantRenderWindows.clear();
			HighlightBar->RelevantRenderWindows.push_back(WindowHandle);
		}
	}

	bool DropdownComponent::CanBeFocused () const
	{
		return IsVisible();
	}

	void DropdownComponent::LoseFocus ()
	{
		FocusInterface::LoseFocus();

		Collapse(); //clear selection
	}

	bool DropdownComponent::CaptureFocusedInput (const sf::Event& keyEvent)
	{
		if (keyEvent.key.code == sf::Keyboard::Return)
		{
			if (!bExpanded)
			{
				if (VALID_OBJECT(ExpandButton))
				{
					if (keyEvent.type == sf::Event::KeyPressed)
					{
						ExpandButton->SetButtonDown(true);
						bIgnoreEnterRelease = true; //Ignore next release key to prevent it from immediately collapsing the menu.
					}
				}

				return true;
			}
			else //Expanded
			{
				if (keyEvent.type == sf::Event::KeyReleased && BrowseIndex != INT_INDEX_NONE && !bIgnoreEnterRelease)
				{
					SetSelectedItem(BrowseIndex.ToUnsignedInt());
					if (VALID_OBJECT(ExpandButton))
					{
						ExpandButton->SetButtonUp(true);
					}
				}

				bIgnoreEnterRelease = false;
				return true;
			}
		}

		if (bExpanded && keyEvent.type == sf::Event::KeyPressed)
		{
			if (keyEvent.key.code == sf::Keyboard::Up)
			{
				INT newIndex = BrowseIndex - 1;
				if (newIndex < 0)
				{
					newIndex = static_cast<int>(List.size()) - 1;
				}

				SetBrowseIndex(newIndex, true);
				return true;
			}
			else if (keyEvent.key.code == sf::Keyboard::Down)
			{
				INT newIndex = BrowseIndex + 1;
				if (newIndex >= static_cast<int>(List.size()))
				{
					newIndex = 0;
				}

				SetBrowseIndex(newIndex, true);
				return true;
			}
		}

		return false;
	}

	bool DropdownComponent::CaptureFocusedText (const sf::Event& keyEvent)
	{
		if (keyEvent.text.unicode == '\r' || //Don't conflict with CaptureFocusInput (The SetSelectedItem later in this function would cause the dropdown to collapse)
				keyEvent.text.unicode == '\t')
		{
			return false;
		}

		if (Engine::GetEngine()->GetElapsedTime() - SearchStringTimestamp > SearchStringClearTime)
		{
			SearchString = TXT("");
		}

		DString newEntry = DString(sf::String(keyEvent.text.unicode));

		SearchString += newEntry;
		SearchStringTimestamp = Engine::GetEngine()->GetElapsedTime();

		unsigned int bestIndex = 0;
		INT mostMatches = 1;

		for (unsigned int i = 0; i < List.size(); i++)
		{
			INT numMatches = List.at(i)->GetLabelText().FindFirstMismatchingIdx(SearchString, false);

			if (numMatches == INT_INDEX_NONE) //Found a matching string
			{
				bestIndex = i;
				break;
			}

			if (INT(numMatches) > mostMatches)
			{
				mostMatches = numMatches;
				bestIndex = i;
			}
		}

		if (bestIndex != SelectedItemIdx)
		{
			SetSelectedItem(bestIndex);
		}

		return true;
	}

	void DropdownComponent::HandleSizeChange ()
	{
		Super::HandleSizeChange();

		RefreshComponentTransforms();
	}

	bool DropdownComponent::ShouldHaveExternalInputComponent () const
	{
		return bExpanded;
	}

	void DropdownComponent::ExecuteMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
	{
		Super::ExecuteMouseMove(mouse, sfmlEvent, deltaMove);

		if (!bExpanded || !VALID_OBJECT(ExpandBackground) || !VALID_OBJECT(HighlightBar))
		{
			return;
		}

		HighlightBar->SetVisibility(ExpandBackground->GetTransform()->WithinBounds(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y)));
		if (!HighlightBar->GetVisibility())
		{
			return;
		}

		INT optionIdx = GetOptionLine(FLOAT::MakeFloat(sfmlEvent.y));
		if (optionIdx != INT_INDEX_NONE && optionIdx.ToUnsignedInt() < List.size())
		{
			//Only update the browse index for valid positions so we don't lose our browse position for a focused dropdown even if we slightly budged the mouse.
			SetBrowseIndex(optionIdx, false);
		}
	}

	bool DropdownComponent::ExecuteMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
	{
		//Collapse this menu if the user clicks outside of menu
		if (bExpanded && VALID_OBJECT(ExpandBackground) && !ExpandBackground->GetTransform()->WithinBounds(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y)) && //These checks are to see if the user clicked outside the option list.
				(Engine::GetEngine()->GetElapsedTime() - PrevExpandTime >= DragToggleThresholdTime || eventType == sf::Event::MouseButtonPressed)) //These condition branches checks to see if the user clicked outside the bounds for a toggle action.  The mouse press event check is to handle spastic handling (clicking again immediately outside after expanding).
		{
			Collapse();
		}

		if (!bExpanded || !VALID_OBJECT(ExpandBackground) || eventType != sf::Event::MouseButtonReleased || sfmlEvent.button != sf::Mouse::Left ||
				!ExpandBackground->GetTransform()->WithinBounds(FLOAT::MakeFloat(sfmlEvent.x), FLOAT::MakeFloat(sfmlEvent.y)))
		{
			return Super::ExecuteMouseClick(mouse, sfmlEvent, eventType);
		}

		INT optionIdx = GetOptionLine(FLOAT::MakeFloat(sfmlEvent.y));
		if (optionIdx != INT_INDEX_NONE && optionIdx < static_cast<int>(List.size()))
		{
			SetSelectedItem(optionIdx.ToUnsignedInt());
			if (OnOptionSelected.IsBounded())
			{
				OnOptionSelected(optionIdx);
			}

			return true;
		}

		return Super::ExecuteMouseClick(mouse, sfmlEvent, eventType);
	}

	void DropdownComponent::CleanUpInvalidPointers ()
	{
		Super::CleanUpInvalidPointers();

		CLEANUP_INVALID_POINTER(ButtonTextureCollapsed)
		CLEANUP_INVALID_POINTER(ButtonTextureExpanded)
	}

	void DropdownComponent::Expand ()
	{
		bExpanded = true;
		SetBrowseIndex(0, false);
		PrevExpandTime = Engine::GetEngine()->GetElapsedTime();
		ConditionallySetExternalBorderInput();

		if (VALID_OBJECT(ExpandBackground))
		{
			//The background's visibility should propagate to the scrollbar and list label's visibility.
			ExpandBackground->SetVisibility(true);
		}

		if (VALID_OBJECT(ExpandButton) && VALID_OBJECT(ButtonTextureExpanded))
		{
			ExpandButton->RenderComponent->SetSpriteTexture(ButtonTextureExpanded);
		}
	}

	void DropdownComponent::Collapse ()
	{
		bExpanded = false;
		SetBrowseIndex(INT_INDEX_NONE, false);
		ConditionallySetExternalBorderInput();

		if (VALID_OBJECT(ExpandBackground))
		{
			ExpandBackground->SetVisibility(false);
		}

		if (VALID_OBJECT(ExpandButton) && VALID_OBJECT(ButtonTextureCollapsed))
		{
			ExpandButton->RenderComponent->SetSpriteTexture(ButtonTextureCollapsed);
		}
	}

	void DropdownComponent::SetSelectedItem (unsigned int itemIndex)
	{
		SelectedItemIdx = itemIndex;
		if (!VALID_OBJECT(SelectedItemLabel))
		{
			return;
		}

		if (SelectedItemIdx > List.size())
		{
			SelectedItemLabel->SetTextContent(NoSelectedItemText);
			return;
		}

		SelectedItemLabel->SetTextContent(List.at(SelectedItemIdx)->GetLabelText());
		Collapse();
	}

	void DropdownComponent::SetNoSelectedItemText (const DString& newNoSelectedItemText)
	{
		NoSelectedItemText = newNoSelectedItemText;
		if (SelectedItemIdx == UINT_INDEX_NONE && VALID_OBJECT(SelectedItemLabel))
		{
			SelectedItemLabel->SetTextContent(NoSelectedItemText);
		}
	}

	void DropdownComponent::ClearList ()
	{
		//Destroy the allocated GUIDataElement objects
		for (unsigned int i = 0; i < List.size(); i++)
		{
			delete List.at(i);
		}

		List.clear();
		SelectedItemIdx = UINT_INDEX_NONE;
		Collapse();

		if (VALID_OBJECT(SelectedItemLabel))
		{
			SelectedItemLabel->SetTextContent(NoSelectedItemText);
		}
	}

	DString DropdownComponent::GetNoSelectedItemText () const
	{
		return NoSelectedItemText;
	}

	void DropdownComponent::InitializeExpandButton ()
	{
		ExpandButton = ButtonComponent::CreateObject();
		if (!AddComponent(ExpandButton))
		{
			ExpandButton = nullptr;
			return;
		}

		ExpandButton->ReplaceStateComponent(SingleSpriteButtonState::SStaticClass());
		ExpandButton->RenderComponent->SetSpriteTexture(ButtonTextureCollapsed);
		ExpandButton->GetTransform()->SetBaseSize(Vector2(32.f, 32.f));
		ExpandButton->GetTransform()->SnapRight();
		ExpandButton->GetTransform()->SnapTop();
		ExpandButton->CaptionComponent->Destroy();
		ExpandButton->CaptionComponent = nullptr;
		ExpandButton->SetButtonPressedHandler(SDFUNCTION_1PARAM(this, DropdownComponent, HandleExpandButtonClicked, void, ButtonComponent*));
	}

	void DropdownComponent::InitializeExpandBackground ()
	{
		CHECK(VALID_OBJECT(SelectedObjBackground))

		ExpandBackground = FrameComponent::CreateObject();
		if (!AddComponent(ExpandBackground))
		{
			ExpandBackground = nullptr;
			return;
		}

		ExpandBackground->RenderComponent->TopBorder = nullptr;
		ExpandBackground->SetLockedFrame(true);
		ExpandBackground->SetVisibility(false);
	}

	void DropdownComponent::InitializeSelectedObjBackground ()
	{
		SelectedObjBackground = FrameComponent::CreateObject();
		if (!AddComponent(SelectedObjBackground))
		{
			SelectedObjBackground = nullptr;
			return;
		}

		SelectedObjBackground->SetLockedFrame(true);
	}

	void DropdownComponent::InitializeSelectedItemLabel ()
	{
		CHECK(VALID_OBJECT(SelectedObjBackground) && VALID_OBJECT(ExpandButton))

		SelectedItemLabel = LabelComponent::CreateObject();
		if (!SelectedObjBackground->AddComponent(SelectedItemLabel))
		{
			SelectedItemLabel = nullptr;
			return;
		}

		SelectedItemLabel->SetClampText(true);
		SelectedItemLabel->SetWrapText(false);
		SelectedItemLabel->SetTextContent(NoSelectedItemText);
	}

	void DropdownComponent::InitializeListLabel ()
	{
		CHECK(VALID_OBJECT(ExpandBackground) && VALID_OBJECT(ListScrollbar))

		ListLabel = LabelComponent::CreateObject();
		if (!ExpandBackground->AddComponent(ListLabel))
		{
			ListLabel = nullptr;
			return;
		}

		ListLabel->SetClampText(true);
		ListLabel->SetWrapText(false);

		ListScrollbar->SetMaxScrollPosition(static_cast<int>(List.size()));
		ListScrollbar->SetNumVisibleScrollPositions(ListLabel->GetMaxNumLines());
		PopulateListLabel();
	}

	void DropdownComponent::InitializeHighlightBar ()
	{
		CHECK(VALID_OBJECT(ListLabel))

		HighlightBar = SolidColorRenderComponent::CreateObject();
		if (!ListLabel->AddComponent(HighlightBar))
		{
			HighlightBar = nullptr;
			return;
		}

		HighlightBarTransform = AbsTransformComponent::CreateObject();
		if (ListLabel->AddComponent(HighlightBarTransform))
		{
			HighlightBarTransform->SetRelativeTo(GetTransform());
			HighlightBar->TransformationInterface = HighlightBarTransform;
		}

		HighlightBar->SolidColor = sf::Color(48, 48, 48, 128);
	}

	void DropdownComponent::InitializeListScrollbar ()
	{
		CHECK(VALID_OBJECT(ExpandBackground))

		ListScrollbar = ScrollbarComponent::CreateObject();
		if (!ExpandBackground->AddComponent(ListScrollbar))
		{
			ListScrollbar = nullptr;
			return;
		}

		ListScrollbar->SetScrollPositionChanged(SDFUNCTION_1PARAM(this, DropdownComponent, HandleExpandScrollPosChanged, void, INT));
	}

	void DropdownComponent::RefreshComponentTransforms ()
	{
		if (VALID_OBJECT(ExpandButton))
		{
			CHECK(ExpandButton->GetTransform() != nullptr)
			ExpandButton->GetTransform()->SnapRight();
			ExpandButton->GetTransform()->SnapTop();
		}

		if (VALID_OBJECT(SelectedObjBackground))
		{
			CHECK(SelectedObjBackground->GetTransform() != nullptr)

			SelectedObjBackground->GetTransform()->SetBaseSize(Vector2(GetTransform()->GetBaseSizeX() - ExpandButton->GetTransform()->GetCurrentSizeX(), 32.f));
			SelectedObjBackground->GetTransform()->SnapTop();
			SelectedObjBackground->GetTransform()->SnapLeft();
		}

		if (VALID_OBJECT(SelectedItemLabel))
		{
			CHECK(SelectedItemLabel->GetTransform() != nullptr)

			FLOAT borderThickness = SelectedObjBackground->GetBorderThickness();

			SelectedItemLabel->GetTransform()->SetBaseSize(Vector2(SelectedObjBackground->GetTransform()->GetBaseSizeX() - borderThickness, SelectedObjBackground->GetTransform()->GetBaseSizeY() - (borderThickness * 2)));
			SelectedItemLabel->GetTransform()->SetAbsCoordinates(Vector2(borderThickness, borderThickness));
			SelectedItemLabel->SetCharacterSize((SelectedItemLabel->GetTransform()->GetCurrentSizeY()).ToINT());
		}

		if (VALID_OBJECT(ExpandBackground))
		{
			CHECK(ExpandBackground->GetTransform() != nullptr)

			FLOAT expandBackgroundHeight = GetTransform()->GetBaseSizeY() - SelectedObjBackground->GetTransform()->GetBaseSizeY();
			if (VALID_OBJECT(SelectedItemLabel))
			{
				//Shrink the expand background if there are few options to choose from.
				expandBackgroundHeight = Utils::Min((SelectedItemLabel->GetCharacterSize().ToFLOAT() + SelectedItemLabel->GetLineSpacing()) * FLOAT::MakeFloat(List.size()) + ExpandBackground->GetBorderThickness(), expandBackgroundHeight);
			}

			ExpandBackground->GetTransform()->SetBaseSize(Vector2(GetTransform()->GetBaseSizeX(), expandBackgroundHeight));
			ExpandBackground->GetTransform()->SetAbsCoordinates(Vector2(0.f, SelectedObjBackground->GetTransform()->GetBaseSizeY()));
		}

		if (VALID_OBJECT(ListScrollbar))
		{
			CHECK(ListScrollbar->GetTransform())

			ListScrollbar->GetTransform()->SetBaseSize(Vector2(ExpandButton->GetTransform()->GetCurrentSizeX(), ExpandBackground->GetTransform()->GetCurrentSizeY()));
			ListScrollbar->GetTransform()->SnapRight();
		}

		if (VALID_OBJECT(ListLabel))
		{
			CHECK(ListLabel->GetTransform() != nullptr)

			FLOAT scrollbarWidth = (ListScrollbar->GetMaxScrollPosition() < ListScrollbar->GetNumVisibleScrollPositions()) ? 0.f : ListScrollbar->GetTransform()->GetCurrentSizeX();
			FLOAT borderThickness = ExpandBackground->GetBorderThickness();

			ListLabel->GetTransform()->SetBaseSize(Vector2(ExpandBackground->GetTransform()->GetBaseSizeX() - scrollbarWidth - (borderThickness * 2), ExpandBackground->GetTransform()->GetBaseSizeY()));
			ListLabel->GetTransform()->SetAbsCoordinates(Vector2(borderThickness, 0.f));
			ListLabel->SetCharacterSize(ExpandedOptionHeight.ToINT());

			ListScrollbar->SetNumVisibleScrollPositions(ListLabel->GetMaxNumLines());
			PopulateListLabel();
		}

		if (VALID_OBJECT(HighlightBar))
		{
			CHECK(HighlightBarTransform != nullptr)

			FLOAT borderThickness = ExpandBackground->GetBorderThickness();

			HighlightBarTransform->SetBaseSize(Vector2(ExpandBackground->GetTransform()->GetBaseSizeX() - (borderThickness * 2.f), ListLabel->GetCharacterSize().ToFLOAT()));
			HighlightBarTransform->SetAbsCoordinates(Vector2(0.f, 0.f)); //Y position is calculated on mouse move
			HighlightBar->SetVisibility(false); //Hide the bar in case the new dimensions are outside of mouse position.  We'll remake it visible again on mouse move.
		}
	}

	void DropdownComponent::ImplementChangeInListLength ()
	{
		CHECK(VALID_OBJECT(ListScrollbar))

		
		bool bOldVisibility = ListScrollbar->IsVisible(false);
		ListScrollbar->SetMaxScrollPosition(static_cast<int>(List.size()));
		bool bNewVisibility = ListScrollbar->GetMaxScrollPosition() > ListScrollbar->GetNumVisibleScrollPositions();

		//Scrollbar is only visible if there's something to scroll
		ListScrollbar->SetVisibility(bNewVisibility);

		//If scrollbar change visibility (due to change in width of expanded label), or if scrollbar is invisible (expanded background needs to change height for short lists).
		if (bOldVisibility != bNewVisibility || !bNewVisibility)
		{
			RefreshComponentTransforms(); //Need to refresh transforms since the width of the expanded label changed to make space for scrollbar.
		}

		PopulateListLabel();
	}

	void DropdownComponent::SetBrowseIndex (INT newBrowseIndex, bool bAllowScrolling)
	{
		BrowseIndex = newBrowseIndex;
		HighlightBar->SetVisibility(BrowseIndex != INT_INDEX_NONE && List.size() > 0);
		if (BrowseIndex == INT_INDEX_NONE || List.size() <= 0)
		{
			return;
		}

		if (bAllowScrolling && VALID_OBJECT(ListScrollbar) && !ListScrollbar->IsIndexVisible(BrowseIndex))
		{
			if (BrowseIndex < ListScrollbar->GetScrollPosition())
			{
				ListScrollbar->SetScrollPosition(BrowseIndex - ListScrollbar->ScrollJumpInterval);
			}
			else
			{
				ListScrollbar->SetScrollPosition(BrowseIndex + ListScrollbar->ScrollJumpInterval - ListScrollbar->GetNumVisibleScrollPositions());
			}
		}

		Vector2 barTransform = ExpandBackground->GetTransform()->GetAbsCoordinates();
		FLOAT optionHeight = ListLabel->GetCharacterSize().ToFLOAT() + ListLabel->GetLineSpacing();
		INT labelIdx = BrowseIndex - ListScrollbar->GetScrollPosition();
		labelIdx = Utils::Min(labelIdx, ListLabel->GetMaxNumLines() - 1); //It's possible to hover over one line beyond the list label.  Clamp it to the last line.  (-1 due to length vs index)

		barTransform.X += ExpandBackground->GetBorderThickness();
		barTransform.Y += labelIdx.ToFLOAT() * optionHeight;
		HighlightBarTransform->SetAbsCoordinates(barTransform);
	}

	INT DropdownComponent::GetOptionLine (FLOAT yCoordinate) const
	{
		CHECK(VALID_OBJECT(ListScrollbar) && VALID_OBJECT(ListLabel))

		FLOAT relativePosY = yCoordinate - ListLabel->GetTransform()->CalcFinalCoordinatesY();
		if (relativePosY < 0 || relativePosY > ListLabel->GetTransform()->GetCurrentSizeY())
		{
			return INT_INDEX_NONE; //beyond list bounds
		}

		FLOAT lineNumber = relativePosY / (ListLabel->GetCharacterSize().ToFLOAT() + ListLabel->GetLineSpacing());
		lineNumber.RoundDown();

		return (ListScrollbar->GetScrollPosition() + lineNumber.ToINT());
	}

	void DropdownComponent::PopulateListLabel ()
	{
		bool bScrollbarVisible = (VALID_OBJECT(ListScrollbar) && ListScrollbar->GetNumVisibleScrollPositions() < static_cast<int>(List.size()));
		if (VALID_OBJECT(ListScrollbar))
		{
			ListScrollbar->SetVisibility(bScrollbarVisible);
		}

		DString listText = TXT("");
		if (!bScrollbarVisible)
		{
			//straight forward loop
			for (unsigned int i = 0; i < List.size(); i++)
			{
				DString curLine = List.at(i)->GetLabelText();
				INT endChar = ListLabel->GetFont()->FindCharNearWidthLimit(curLine, ListLabel->GetCharacterSize().ToUnsignedInt(), ListLabel->GetTransform()->GetCurrentSizeX());
				if (endChar > 0 && endChar < curLine.Length() - 1)
				{
					curLine = curLine.SubString(0, endChar + 1);
				}

				listText += curLine + TXT("\n");
			}
		}
		else
		{
			unsigned int scrollPos = ListScrollbar->GetScrollPosition().ToUnsignedInt();
			for (unsigned int i = scrollPos; i < List.size() && i - scrollPos < ListScrollbar->GetNumVisibleScrollPositions().ToUnsignedInt(); i++)
			{
				DString curLine = List.at(i)->GetLabelText();
				INT endChar = ListLabel->GetFont()->FindCharNearWidthLimit(curLine, ListLabel->GetCharacterSize().ToUnsignedInt(), ListLabel->GetTransform()->GetCurrentSizeX());
				if (endChar > 0 && endChar < curLine.Length() - 1)
				{
					curLine = curLine.SubString(0, endChar + 1);
				}

				listText += curLine + TXT("\n");
			}
		}

		ListLabel->SetTextContent(listText);
	}

	void DropdownComponent::HandleExpandButtonClicked (ButtonComponent* uiComponent)
	{
		(bExpanded) ? Collapse() : Expand();
	}

	void DropdownComponent::HandleExpandScrollPosChanged (INT newScrollPosition)
	{
		PopulateListLabel();
	}
}

#endif