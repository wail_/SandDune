/*
=====================================================================
  <Any copyright stuff here>

  LabelComponentTextThread.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GUIClasses.h"

#if INCLUDE_GUI

using namespace std;

namespace SD
{
	LabelComponentTextThread::LabelComponentTextThread (LabelComponent& labelComponent)
	{
		bFinishedCalculation = false;

		ParentLabelComponent = &labelComponent;
		DisplayedText = labelComponent.GetFullText();
		Font = labelComponent.GetFont();
		CharSize = labelComponent.GetCharacterSize();
		bWrapText = labelComponent.GetWrapText();
		bClampText = labelComponent.GetClampText();
		LineSpacing = labelComponent.GetLineSpacing();
		TextTrail = TXT(""); //not yet computed
		VerticalAlignment = labelComponent.GetVerticalAlignment();
		HorizontalAlignment = labelComponent.GetHorizontalAlignment();
		MaxNumLines = labelComponent.GetMaxNumLines();
		AbsPosX = labelComponent.Attributes.GetAbsPositionX();
		AbsPosY = labelComponent.Attributes.GetAbsPositionY();
		AbsSizeX = labelComponent.Attributes.GetAbsSizeX();
		AbsSizeY = labelComponent.Attributes.GetAbsSizeY();
	}

	LabelComponentTextThread::~LabelComponentTextThread ()
	{
	}

	void LabelComponentTextThread::DeployThread ()
	{
		thread textThread = thread(&LabelComponentTextThread::ExecuteTextThread, this);
		textThread.detach();
	}

	INT LabelComponentTextThread::GetBottomBorder () const
	{
		return (AbsPosY + AbsSizeY);
	}

	INT LabelComponentTextThread::GetRightBorder () const
	{
		return (AbsPosX + AbsSizeX);
	}

	void LabelComponentTextThread::ExecuteTextThread ()
	{
		//Begin with one line
		InsertTextMeasure(DisplayedText, 0);

		ParseNewLineCharacters();
		WrapText();
		ClampText();
		AlignVertically();
		AlignHorizontally();

		UpdateLabelComponent();
	}

	void LabelComponentTextThread::InsertTextMeasure (DString content, unsigned int idx)
	{
		sf::Text newText = sf::Text(content, *(Font->GetFontResource()), CharSize.ToUnsignedInt());
		if (idx >= PendingTextUpdates.size())
		{
			idx = PendingTextUpdates.size();
			PendingTextUpdates.push_back(newText);
		}
		else
		{
			PendingTextUpdates.insert(PendingTextUpdates.begin() + idx, newText);
		}

		//Adjust text positions of newText and all texts following it
		sf::Vector2f newPosition;
		newPosition.x = AbsPosX.ToFLOAT().Value;
		for (unsigned int i = idx; i < PendingTextUpdates.size(); i++)
		{
			newPosition.y = (AbsPosY + (idx * (CharSize + LineSpacing))).ToFLOAT().Value;
			PendingTextUpdates.at(i).setPosition(newPosition);
		}
	}

	bool LabelComponentTextThread::ParseNewLineCharacters ()
	{
		for (unsigned int i = 0; i < PendingTextUpdates.size(); i++)
		{
			DString curString = PendingTextUpdates.at(i).getString();
			int curCharIndex = static_cast<int>(curString.find_first_of('\n'));

			//Exit loop if no new line characters were found, or if it's at the end
			if (curCharIndex == string::npos || curCharIndex == curString.Length() - 1)
			{
				return (static_cast<int>(PendingTextUpdates.size()) <= MaxNumLines);
			}

			//Remove text from current line since it's about to be transfered to next line
			PendingTextUpdates.at(i).setString(curString.substr(0, curCharIndex));
			InsertTextMeasure(curString.substr(curCharIndex + 1), PendingTextUpdates.size()); //push back
		}
		
		return (static_cast<int>(PendingTextUpdates.size()) <= MaxNumLines);
	}

	void LabelComponentTextThread::WrapText ()
	{
		if (!bWrapText)
		{
			return;
		}

		//153-185 milliseconds
		//Stopwatch testStopwatch("WrapTextStopwatch"); //goal 50 milliseconds
		vector<char> wrapCharacters;
		wrapCharacters.push_back(' ');
		wrapCharacters.push_back('-');

		for (unsigned int i = 0; i < PendingTextUpdates.size(); i++)
		{
			//0-4 milliseconds
			INT charIndex = FindCharacterBeyondWidth(i);

			if (charIndex <= 0)
			{
				continue; //Either All characters in this line is within width or the first character is beyond width.
			}

			//0-3 milliseconds
			INT wrapPosition = FindPreviousCharacter(wrapCharacters, i, charIndex.Value);
			if (wrapPosition == INT_INDEX_NONE)
			{
				//Was unable to find a good place to wrap text.  Wrap at the character that went beyond boundaries.
				wrapPosition = charIndex;
			}
			else
			{
				//Increment by one to keep the current character at the previous line
				wrapPosition++;
			}

			//0-6 milliseconds

			//Divide the string into two parts
			DString stringWithinBounds = PendingTextUpdates.at(i).getString();
			DString transferString = stringWithinBounds.substr(wrapPosition.ToUnsignedInt());
			stringWithinBounds = stringWithinBounds.substr(0, wrapPosition.ToUnsignedInt());

			InsertTextMeasure(transferString, i + 1);
			PendingTextUpdates.at(i).setString(stringWithinBounds);
		}
	}

	void LabelComponentTextThread::ClampText ()
	{
		TextTrail = "";

		if (!bClampText || DisplayedText == "")
		{
			return;
		}

		DString truncatedText = TXT("");

		while (PendingTextUpdates.size() > 0 && static_cast<int>(PendingTextUpdates.size()) > MaxNumLines)
		{
			//prefix truncatedText with the last line
			truncatedText = PendingTextUpdates.at(PendingTextUpdates.size() - 1).getString() + truncatedText;
			PendingTextUpdates.pop_back();
		}

		TextTrail = truncatedText;
		DisplayedText = DisplayedText.substr(0, DisplayedText.size() - TextTrail.size());
	}

	void LabelComponentTextThread::AlignVertically ()
	{
		FLOAT lastLinePos = AbsPosY + ((CharSize + LineSpacing) * static_cast<int>(PendingTextUpdates.size()));
		INT unusedSpace = GetBottomBorder() - static_cast<int>(trunc(lastLinePos.Value));
		FLOAT firstLineOffset = 0; //Distance between the top of this component's yPos and the first Text's yPos.

		//Clamp unused space in case it's negative (when the characters are larger than the component, itself
		unusedSpace = Utils::Max<INT>(0, unusedSpace);

		switch(VerticalAlignment)
		{
			case(LabelComponent::VA_Top):
				break;
			case(LabelComponent::VA_Center):
				firstLineOffset = unusedSpace / 2;
				break;

			case(LabelComponent::VA_Bottom):
				firstLineOffset = unusedSpace;
				break;
		}

		for (unsigned int i = 0; i < PendingTextUpdates.size(); i++)
		{
			sf::Vector2f newLocation = PendingTextUpdates.at(i).getPosition();
			newLocation.y = (firstLineOffset + ((CharSize + LineSpacing) * i)).Value;
			newLocation.y += AbsPosY.Value;
			PendingTextUpdates.at(i).setPosition(newLocation);
		}
	}

	void LabelComponentTextThread::AlignHorizontally ()
	{
		//Use faster/simpler algorithm when aligning text to the left
		if (HorizontalAlignment == LabelComponent::HA_Left)
		{
			for (unsigned int i = 0; i < PendingTextUpdates.size(); i++)
			{
				sf::Vector2f newLocation = PendingTextUpdates.at(i).getPosition();
				newLocation.x = AbsPosX.ToFLOAT().Value;
				PendingTextUpdates.at(i).setPosition(newLocation);
			}

			return;
		}

		for (unsigned int i = 0; i < PendingTextUpdates.size(); i++)
		{
			DString curString = PendingTextUpdates.at(i).getString();
			sf::Vector2f curCharPos = PendingTextUpdates.at(i).findCharacterPos(curString.size());
			curCharPos.x += AbsPosX.Value;
			INT unusedSpace = GetRightBorder() - static_cast<int>(trunc(curCharPos.x));
			float horizontalShift = 0;

			switch (HorizontalAlignment)
			{
				case(LabelComponent::HA_Center):
					horizontalShift = FLOAT(unusedSpace / 2).Value;
					break;

				case(LabelComponent::HA_Right):
					horizontalShift = unusedSpace.ToFLOAT().Value;
					break;
			}

			sf::Vector2f newLocation = PendingTextUpdates.at(i).getPosition();
			newLocation.x = horizontalShift + FLOAT(AbsPosX).Value;
			PendingTextUpdates.at(i).setPosition(newLocation);
		}
	}

	INT LabelComponentTextThread::FindCharacterBeyondWidth (INT textIndex) const
	{
		unsigned int lineIdx = textIndex.ToUnsignedInt();
		DString msg = PendingTextUpdates.at(lineIdx).getString();
		int sampleSize = 8;

		if (static_cast<int>(msg.size()) < sampleSize)
		{
			sampleSize = static_cast<int>(msg.size());

			//It's possible that this component is narrow or is using very large characters.
			//It's faster to simply iterate backwards
			for (unsigned int i = sampleSize; i >= 0; i--)
			{
				sf::Vector2f charLocation = PendingTextUpdates.at(lineIdx).findCharacterPos(i);
				charLocation.x += AbsPosX.Value;

				if (charLocation.x < FLOAT(GetRightBorder()).Value)
				{
					return (i == sampleSize) ? -1 : i;
				}
			}

			return -1; //This should only happen if a single character is enough to go out of bounds
		}

		sf::Vector2f lineLocation = PendingTextUpdates.at(lineIdx).getPosition();

		sf::Vector2f drawOffset = PendingTextUpdates.at(lineIdx).getPosition();
		drawOffset.x -= AbsPosX.Value;
		drawOffset.y -= AbsPosY.Value;

		//Sample a few characters to get an estimation how many characters does it take to reach the width
		sf::Vector2f charLocation = PendingTextUpdates.at(lineIdx).findCharacterPos(sampleSize);
		INT avgCharWidth = static_cast<int>(trunc((charLocation.x - lineLocation.x))) / sampleSize;
		charLocation.x = static_cast<float>(AbsPosX.Value) + drawOffset.x;
		INT charIndex = 0;

		//Guess how many characters it takes to reach the right
		charIndex = Utils::Min<INT>(AbsSizeX/avgCharWidth, static_cast<int>(msg.size() - 1));
		charLocation = PendingTextUpdates.at(lineIdx).findCharacterPos(charIndex.ToUnsignedInt());
		charLocation.x += AbsPosX.Value;

		//underestimated... keep jmping until we pass right boundaries
		while (FLOAT(charLocation.x) < GetRightBorder() && charIndex < static_cast<int>(msg.size() - 1))
		{
			charIndex = Utils::Min<INT>(charIndex + 8, static_cast<int>(msg.size() - 1));
			charLocation = PendingTextUpdates.at(lineIdx).findCharacterPos(charIndex.ToUnsignedInt());
			charLocation.x += AbsPosX.Value;
		}

		if (charIndex >= static_cast<int>(msg.size()) - 1 && charLocation.x < FLOAT(GetRightBorder()).Value)
		{
			return -1; //Everything fits within bounds
		}

		//Keep decrementing by one until we're back in bounds
		do
		{
			charIndex--;
			charLocation = PendingTextUpdates.at(lineIdx).findCharacterPos(charIndex.ToUnsignedInt());
			charLocation.x += AbsPosX.Value;
		}
		while (charLocation.x > FLOAT(GetRightBorder()).Value && charIndex > 0);

		return charIndex;
	}

	INT LabelComponentTextThread::FindPreviousCharacter (const std::vector<char>& targetCharacters, unsigned int textLine, INT startPosition) const
	{
		DString line = PendingTextUpdates.at(textLine).getString();

		if (startPosition < 0)
		{
			startPosition = static_cast<int>(line.size() - 1);
		}

		for (int i = startPosition; i >= 0; i--)
		{
			for (unsigned int j = 0; j < targetCharacters.size(); j++)
			{
				if (targetCharacters.at(j) == line.at(i))
				{
					return i;
				}
			}
		}

		return INT_INDEX_NONE;
	}

	void LabelComponentTextThread::UpdateLabelComponent ()
	{
		TextMutex.lock();
		bFinishedCalculation = true;
		TextMutex.unlock();
	}

	bool LabelComponentTextThread::CheckThreadStatus ()
	{
		if (!VALID_OBJECT(ParentLabelComponent))
		{
			//REMOVE_TIMER(this, CheckThreadStatus); //TODO:  this is not object
			return false;
		}

		TextMutex.lock();
		if (bFinishedCalculation)
		{
			//TODO:  Update ParentLabelComponent
			//TODO:  Destroy self
		}
		TextMutex.unlock();

		return false;
	}
}

#endif