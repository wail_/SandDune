/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GUITheme.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GUIClasses.h"

#if INCLUDE_GUI

namespace SD
{
	IMPLEMENT_CLASS(GUITheme, Object)

	void GUITheme::InitializeTheme ()
	{
		//Mouse Pointer Icons
		TextPointer = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.CursorText"));
		ResizePointerVertical = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.CursorResizeVertical"));
		ResizePointerHorizontal = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.CursorResizeHorizontal"));
		ResizePointerDiagonalTopLeft = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.CursorResizeTopLeft"));
		ResizePointerDiagonalBottomLeft = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.CursorResizeBottomLeft"));
		ScrollbarPointer = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.CursorScrollbarPan"));

		//Frame component textures
		FrameFill = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.DefaultFrameComponentFill"));
		FrameBorderTop = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderTop"));
		FrameBorderRight = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderRight"));
		FrameBorderBottom = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderBottom"));
		FrameBorderLeft = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderLeft"));
		FrameBorderTopRight = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderTopRight"));
		FrameBorderBottomRight = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderBottomRight"));
		FrameBorderBottomLeft = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderBottomLeft"));
		FrameBorderTopLeft = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderTopLeft"));

		//Button component textures
		ButtonSingleSpriteBackground = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.Buttons.TextureTestStates"));
		ButtonFill = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.DefaultFrameComponentFill"));
		ButtonBorderTop = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderTop"));
		ButtonBorderRight = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderRight"));
		ButtonBorderBottom = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderBottom"));
		ButtonBorderLeft = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderLeft"));
		ButtonBorderTopRight = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderTopRight"));
		ButtonBorderBottomRight = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderBottomRight"));
		ButtonBorderBottomLeft = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderBottomLeft"));
		ButtonBorderTopLeft = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.FrameBorderTopLeft"));
		
		//Scrollbar component textures
		ScrollbarTopButton = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.Buttons.UpButton"));
		ScrollbarBottomButton = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.Buttons.DownButton"));
		ScrollbarMiddleMouseScrollAnchor = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.ScrollbarMiddleMouseSprite"));
		ScrollbarTrackFill = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.ScrollbarThumbFill"));
		ScrollbarTrackBorderTop = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.ScrollbarThumbBorderTop"));
		ScrollbarTrackBorderRight = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.ScrollbarThumbBorderRight"));
		ScrollbarTrackBorderBottom = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.ScrollbarThumbBorderBottom"));
		ScrollbarTrackBorderLeft = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.ScrollbarThumbBorderLeft"));
		ScrollbarTrackBorderTopRight = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.ScrollbarThumbBorderTopRight"));
		ScrollbarTrackBorderBottomRight = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.ScrollbarThumbBorderBottomRight"));
		ScrollbarTrackBorderBottomLeft = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.ScrollbarThumbBorderBottomLeft"));
		ScrollbarTrackBorderTopLeft = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.ScrollbarThumbBorderTopLeft"));

		//Dropdown component textures
		DropdownCollapsedButton = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.Buttons.CollapsedButton"));
		DropdownExpandedButton = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.Buttons.ExpandedButton"));

		//Tree list component textures
		TreeListAddButton = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.Buttons.TreeListAddButton"));
		TreeListSubtractButton = TexturePool::GetTexturePool()->FindTexture(TXT("Interface.Buttons.TreeListSubtractButton"));

		GUIFont = FontPool::GetFontPool()->FindFont(TXT("DefaultFont"));
	}

	GUITheme* GUITheme::GetGUITheme ()
	{
		return GUIEngineComponent::Get()->GetGUITheme();
	}
}

#endif