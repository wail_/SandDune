/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GUIEntity.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GUIClasses.h"

#if INCLUDE_GUI

using namespace std;

namespace SD
{
	IMPLEMENT_ABSTRACT_CLASS(GUIEntity, Entity)

	vector<GUIEntity*> GUIEntity::GUIList;
	FLOAT GUIEntity::MaxDrawOrderPerEntity = 50.f;

	void GUIEntity::InitProps ()
	{
		Super::InitProps();

		Focus = nullptr;
		Transformation = nullptr;
	}

	void GUIEntity::BeginObject ()
	{
		Super::BeginObject();

		InitializeTransformation();
		ConstructUI(); //Construct UI before registering to apply the draw order to GUIComponents based on GUIList idx
		InitializeFocusComponent();
		RefreshTransformations();
		RegisterToGUIList();
	}

	void GUIEntity::Destroy ()
	{
		for (unsigned int i = 0; i < GUIList.size(); i++)
		{
			if (GUIList.at(i) == this)
			{
				GUIList.erase(GUIList.begin() + i);
				break;
			}
		}

		Super::Destroy();
	}

	void GUIEntity::CleanUpInvalidPointers ()
	{
		Super::CleanUpInvalidPointers();

		CLEANUP_INVALID_POINTER(Focus)
		CLEANUP_INVALID_POINTER(Transformation)
	}

	void GUIEntity::LoseFocus ()
	{
		if (VALID_OBJECT(Focus))
		{
			Focus->ClearSelection();
		}
	}

	void GUIEntity::GainFocus ()
	{
		//All other GUIEntities should lose focus
		for (unsigned int i = 0; i < GUIList.size(); i++)
		{
			if (GUIList.at(i) != this)
			{
				GUIList.at(i)->LoseFocus();
			}
		}

		//Move this GUIEntity to end of GUIList to grant it highest draw order priority
		for (unsigned int i = 0; i < GUIList.size(); i++)
		{
			if (GUIList.at(i) == this)
			{
				GUIList.erase(GUIList.begin() + i);
				GUIList.push_back(this);
				break;
			}
		}
		RefreshPriorityForAll();

		if (VALID_OBJECT(Focus))
		{
			Focus->SelectNextEntity();
		}
	}

	void GUIEntity::SetWindowHandle (Window* newWindowHandle)
	{
		if (VALID_OBJECT(Focus))
		{
			InputBroadcaster* newBroadcaster = (InputEngineComponent::Get() != nullptr) ? InputEngineComponent::Get()->FindBroadcasterForWindow(newWindowHandle) : nullptr;
			if (newBroadcaster != nullptr)
			{
				newBroadcaster->AddInputComponent(Focus->GetInput(), Focus->GetInput()->GetInputPriority());
			}
		}

		for (ComponentIterator iter(this, false); iter.GetSelectedComponent() != nullptr; iter++)
		{
			GUIComponent* guiComp = dynamic_cast<GUIComponent*>(iter.GetSelectedComponent());
			if (guiComp != nullptr)
			{
				guiComp->SetWindowHandle(newWindowHandle);
			}
		}
	}

	AbsTransformComponent* GUIEntity::GetTransform () const
	{
		return Transformation;
	}

	void GUIEntity::InitializeTransformation ()
	{
		Transformation = AbsTransformComponent::CreateObject();
		if (!AddComponent(Transformation))
		{
			Transformation = nullptr;
			return;
		}

		Transformation->OnSizeChanged = SDFUNCTION(this, GUIEntity, HandleSizeChange, void);
		Transformation->OnPositionChanged = SDFUNCTION_1PARAM(this, GUIEntity, HandlePositionChange, void, const Vector2&);
	}

	void GUIEntity::InitializeFocusComponent ()
	{
		Focus = FocusComponent::CreateObject();
		if (!AddComponent(Focus))
		{
			Focus = nullptr;
			return;
		}
	}

	unsigned int GUIEntity::CalcGUIListIdx () const
	{
		//By default, GUIEntities will be added at the end of list since typically the latest opened UI will have highest priority.
		return static_cast<unsigned int>(GUIList.size());
	}

	void GUIEntity::RegisterToGUIList ()
	{
		unsigned int listIdx = CalcGUIListIdx();
		CHECK(listIdx != UINT_INDEX_NONE && listIdx <= GUIList.size())

		GUIList.insert(GUIList.begin() + listIdx, this);

		//Refresh draw orders of GUIEntities
		RefreshPriorityForAll();
	}

	void GUIEntity::RefreshPriorityForAll ()
	{
		for (unsigned int i = 0; i < GUIList.size(); i++)
		{
			GUIList.at(i)->SetBaseDrawOrder(GraphicsEngineComponent::DrawOrderUI + (MaxDrawOrderPerEntity * FLOAT::MakeFloat(i)));
			GUIList.at(i)->SetInputPriority(INT(i));
		}
	}

	void GUIEntity::SetBaseDrawOrder (FLOAT newDrawOrder)
	{
		//Only set the draw order to immediate GUIComponents for that'll cause a chain reaction to iterate through its sub GUIComponents
		for (ComponentIterator iter(this, false); iter.GetSelectedComponent() != nullptr; iter++)
		{
			GUIComponent* guiComp = dynamic_cast<GUIComponent*>(iter.GetSelectedComponent());
			if (guiComp != nullptr)
			{
				guiComp->SetDrawOrder(newDrawOrder);
			}
		}
	}

	void GUIEntity::SetInputPriority (INT newInputPriority)
	{
		if (VALID_OBJECT(Focus))
		{
			CHECK(Focus->GetInput() != nullptr && Focus->GetInput()->GetInputMessenger() != nullptr)

			//Focus input priority should be higher than the UI's input.
			Focus->GetInput()->SetInputPriority(newInputPriority + 1);
		}

		for (ComponentIterator iter(this, false); iter.GetSelectedComponent() != nullptr; iter++)
		{
			GUIComponent* guiComp = dynamic_cast<GUIComponent*>(iter.GetSelectedComponent());
			if (guiComp != nullptr)
			{
				InputComponent* curInput = guiComp->GetInput();
				if (VALID_OBJECT(curInput))
				{
					curInput->SetInputPriority(newInputPriority);
				}
			}
		}
	}

	void GUIEntity::HandleSizeChange ()
	{
		RefreshTransformations();
	}

	void GUIEntity::HandlePositionChange (const Vector2& deltaMove)
	{
		RefreshTransformations();
	}
}

#endif