/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  BorderedSpriteComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GUIClasses.h"

#if INCLUDE_GUI

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(BorderedSpriteComponent, SpriteComponent)

	void BorderedSpriteComponent::InitProps ()
	{
		Super::InitProps();

		TopBorder = nullptr;
		RightBorder = nullptr;
		BottomBorder = nullptr;
		LeftBorder = nullptr;
		TopRightCorner = nullptr;
		BottomRightCorner = nullptr;
		BottomLeftCorner = nullptr;
		TopLeftCorner = nullptr;

		FillColor = sf::Color(128, 128, 128);
		BorderThickness = 8;
	}

	void BorderedSpriteComponent::Render (const Camera* targetCamera, const Window* targetWindow)
	{
		if (TransformationInterface == nullptr)
		{
			LOG1(LOG_WARNING, TXT("Unable to draw %s since it does not have a TransformationInterface affiliated with it."), GetUniqueName());
			return;
		}

		sf::Transformable transformationResults;
		TransformationInterface->ApplyDrawPosition(&transformationResults, targetCamera);
		TransformationInterface->ApplyDrawSize(&transformationResults, targetCamera);
		const sf::Vector2f topLeftCorner = transformationResults.getPosition();
		const sf::Vector2f transformSize = transformationResults.getScale();

		//Draw the fill first to ensure it's rendered behind the border
		if (Sprite != nullptr && SpriteTexture != nullptr)
		{
			//TODO:  Draw Sprite within borders (not behind borders)... (copy paste code from super with minor adjustments? Override through interface with conditional check?)
			Super::Render(targetCamera, targetWindow);
		}
		else
		{
			sf::RectangleShape background;
			background.setFillColor(FillColor);
			background.setPosition(topLeftCorner.x + BorderThickness.Value, topLeftCorner.y + BorderThickness.Value);
			background.setSize(sf::Vector2f(transformSize.x - (BorderThickness.Value * 2), transformSize.y - (BorderThickness.Value * 2)));

			targetWindow->Resource->draw(background);
		}

		//Draw borders
		if (TopBorder != nullptr)
		{
			Vector2 borderSpriteSize;
			TopBorder->GetDimensions(borderSpriteSize);

			sf::Sprite topSprite;
			topSprite.setTexture(*TopBorder->GetTextureResource());
			topSprite.setPosition(topLeftCorner.x + BorderThickness.Value, topLeftCorner.y);
			topSprite.setScale(((transformSize.x - BorderThickness * 2.f)/borderSpriteSize.X).Value, (BorderThickness/borderSpriteSize.Y).Value);
			targetWindow->Resource->draw(topSprite);
		}

		if (RightBorder != nullptr)
		{
			Vector2 borderSpriteSize;
			RightBorder->GetDimensions(borderSpriteSize);

			sf::Sprite rightSprite;
			rightSprite.setTexture(*RightBorder->GetTextureResource());
			rightSprite.setPosition((topLeftCorner.x + transformSize.x - BorderThickness).Value, topLeftCorner.y + BorderThickness.Value);
			rightSprite.setScale((BorderThickness/borderSpriteSize.X).Value, ((transformSize.y - BorderThickness * 2.f)/borderSpriteSize.Y).Value);
			targetWindow->Resource->draw(rightSprite);
		}

		if (BottomBorder != nullptr)
		{
			Vector2 borderSpriteSize;
			BottomBorder->GetDimensions(borderSpriteSize);

			sf::Sprite bottomSprite;
			bottomSprite.setTexture(*BottomBorder->GetTextureResource());

			//bottomSprite.setPosition(drawLocation.x, FLOAT(drawLocation.y + scaleY - BorderThickness).Value);
			bottomSprite.setPosition(topLeftCorner.x + BorderThickness.Value, (topLeftCorner.y + transformSize.y - BorderThickness).Value);
			bottomSprite.setScale(((transformSize.x - BorderThickness * 2)/borderSpriteSize.X).Value, (BorderThickness/borderSpriteSize.Y).Value);
			targetWindow->Resource->draw(bottomSprite);
		}

		if (LeftBorder != nullptr)
		{
			Vector2 borderSpriteSize;
			LeftBorder->GetDimensions(borderSpriteSize);

			sf::Sprite leftSprite;
			leftSprite.setTexture(*LeftBorder->GetTextureResource());
			leftSprite.setPosition(topLeftCorner.x, (topLeftCorner.y + BorderThickness).Value);
			leftSprite.setScale((BorderThickness/borderSpriteSize.X).Value, ((transformSize.y - BorderThickness * 2.f)/borderSpriteSize.Y).Value);
			targetWindow->Resource->draw(leftSprite);
		}

		//Draw corners
		if (TopRightCorner != nullptr)
		{
			Vector2 borderSpriteSize;
			TopRightCorner->GetDimensions(borderSpriteSize);

			sf::Sprite topRightSprite;
			topRightSprite.setTexture(*TopRightCorner->GetTextureResource());
			topRightSprite.setPosition((topLeftCorner.x + transformSize.x - BorderThickness).Value, topLeftCorner.y);
			topRightSprite.setScale((BorderThickness.Value/borderSpriteSize.X).Value, (BorderThickness/borderSpriteSize.Y).Value);
			targetWindow->Resource->draw(topRightSprite);
		}

		if (BottomRightCorner != nullptr)
		{
			Vector2 borderSpriteSize;
			BottomRightCorner->GetDimensions(borderSpriteSize);

			sf::Sprite bottomRightSprite;
			bottomRightSprite.setTexture(*BottomRightCorner->GetTextureResource());
			bottomRightSprite.setPosition((topLeftCorner.x + transformSize.x - BorderThickness).Value, (topLeftCorner.y + transformSize.y - BorderThickness).Value);
			bottomRightSprite.setScale((BorderThickness/borderSpriteSize.X).Value, (BorderThickness/borderSpriteSize.Y).Value);
			targetWindow->Resource->draw(bottomRightSprite);
		}

		if (BottomLeftCorner != nullptr)
		{
			Vector2 borderSpriteSize;
			BottomLeftCorner->GetDimensions(borderSpriteSize);

			sf::Sprite bottomLeftSprite;
			bottomLeftSprite.setTexture(*BottomLeftCorner->GetTextureResource());
			bottomLeftSprite.setPosition(topLeftCorner.x, (topLeftCorner.y + transformSize.y - BorderThickness).Value);
			bottomLeftSprite.setScale((BorderThickness/borderSpriteSize.X).Value, (BorderThickness/borderSpriteSize.Y).Value);
			targetWindow->Resource->draw(bottomLeftSprite);
		}

		if (TopLeftCorner != nullptr)
		{
			Vector2 borderSpriteSize;
			TopLeftCorner->GetDimensions(borderSpriteSize);

			sf::Sprite topLeftSprite;
			topLeftSprite.setTexture(*TopLeftCorner->GetTextureResource());
			topLeftSprite.setPosition(topLeftCorner.x, topLeftCorner.y);
			topLeftSprite.setScale((BorderThickness/borderSpriteSize.X).Value, (BorderThickness/borderSpriteSize.Y).Value);
			targetWindow->Resource->draw(topLeftSprite);
		}
	}
}

#endif