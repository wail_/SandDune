/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TextFieldLabelComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GUIClasses.h"

#if INCLUDE_GUI

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(TextFieldLabelComponent, LabelComponent)

	void TextFieldLabelComponent::InitProps ()
	{
		Super::InitProps();

		bClampText = true; //always clamp
		TrimmedTextHead = TXT("");
		NumTrimmedLines = 0;

		bIgnoreAlignment = false;
	}

	void TextFieldLabelComponent::SetTextContent (DString newContent)
	{
		NumTrimmedLines = 0;
		Super::SetTextContent(newContent);
	}

	void TextFieldLabelComponent::SetClampText (bool bNewClampText)
	{
#ifdef DEBUG_MODE
		if (!bNewClampText)
		{
			LOG(LOG_WARNING, TXT("Text is always clamped for TextFieldLabelComponents."));
		}
#endif
		bClampText = true;
	}

	DString TextFieldLabelComponent::GetFullText () const
	{
		return (TrimmedTextHead + Super::GetFullText());
	}

	void TextFieldLabelComponent::ClampText ()
	{
		INT oldNumLines = INT(static_cast<int>(RenderComponent->Texts.size()));

		Super::ClampText();

		INT curNumLines = INT(static_cast<int>(RenderComponent->Texts.size()));
		if (oldNumLines > curNumLines)
		{
			NumTrimmedLines += (oldNumLines - curNumLines);
		}
	}

	void TextFieldLabelComponent::AlignVertically ()
	{
		if (!bIgnoreAlignment)
		{
			Super::AlignVertically();
		}
	}

	void TextFieldLabelComponent::AlignHorizontally ()
	{
		if (!bIgnoreAlignment)
		{
			Super::AlignHorizontally();
		}
	}

	void TextFieldLabelComponent::SetTextContent (DString newContent, INT cursorPosition, bool bChangesAtCursor)
	{
		TextFieldComponent* textField = GetOwningTextField();
		if (textField == nullptr)
		{
			LOG1(LOG_WARNING, TXT("The owner of %s is unable to find an owner that is a TextFieldComponent.  You'll not be able to set the content for this label."), GetUniqueName());
			return;
		}

		if (textField->GetScrollPosition() <= 0)
		{
			NumTrimmedLines = 0;

			//scroll position is 0, no need for TextFieldLabelComponent's additional implementation to calculate the TrimmedTextHead property.
			TrimmedTextHead = TXT("");
			return Super::SetTextContent(newContent);
		}

		//Need to calculate the TrimmedTextHead, so set the full string to the render component to get measurements.
		//Before calling Super::SetTextContent, force ignore clamping for performance reasons
		bool bOldClamp = bClampText;
		bClampText = false;

		//Disable alignment functions since we're going to call super shortly.
		bIgnoreAlignment = true;

		Super::SetTextContent(newContent);

		//Restore hacks
		bClampText = bOldClamp;
		bIgnoreAlignment = false;

		CalculateTrimmedTextHead(textField->GetScrollPosition());

		//Cut the trimmed text from newContent
		newContent.Remove(0, TrimmedTextHead.Length());

		NumTrimmedLines = 0;

		//Update the render component with the true text
		Super::SetTextContent(newContent);

		//TODO:  Consider optimizing updates based on cursorPosition
	}

	DString TextFieldLabelComponent::GetTrimmedTextHead () const
	{
		return TrimmedTextHead;
	}

	INT TextFieldLabelComponent::GetNumTrimmedLines () const
	{
		return NumTrimmedLines;
	}

	TextFieldComponent* TextFieldLabelComponent::GetOwningTextField () const
	{
		for (EntityComponent* curEntity = dynamic_cast<EntityComponent*>(GetOwner()); VALID_OBJECT(curEntity); curEntity = dynamic_cast<EntityComponent*>(curEntity->GetOwner()))
		{
			if (dynamic_cast<TextFieldComponent*>(curEntity))
			{
				return dynamic_cast<TextFieldComponent*>(curEntity);
			}
		}

		return nullptr;
	}

	void TextFieldLabelComponent::CalculateTrimmedTextHead (INT targetLineNumber)
	{
		//safety check
		if (RenderComponent->Texts.empty() || targetLineNumber >= static_cast<int>(RenderComponent->Texts.size()))
		{
			return;
		}

		unsigned int accumulatedChars = 0;
		for (unsigned int i = 0; i < targetLineNumber; i++)
		{
			accumulatedChars += static_cast<int>(RenderComponent->Texts.at(i)->getString().getSize());
		}

		TrimmedTextHead = TextContent.at(0).SubString(0, accumulatedChars);
	}
}

#endif