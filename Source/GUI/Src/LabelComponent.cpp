/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  LabelComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GUIClasses.h"

#if INCLUDE_GUI

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(LabelComponent, GUIComponent)

	void LabelComponent::InitProps ()
	{
		Super::InitProps();

		if (VALID_OBJECT(GUITheme::GetGUITheme()))
		{
			CurrentFont = GUITheme::GetGUITheme()->GUIFont;
		}

		CharacterSize = 16;
		bAutoRefresh = true;
		bWrapText = true;
		bClampText = true;
		LineSpacing = 4;
		TrimmedTextTail = TXT("");
		RenderComponent = nullptr;
		VerticalAlignment = VA_Top;
		HorizontalAlignment = HA_Left;
		MaxNumLines = 1;
		OnMaxNumLinesChanged = nullptr;
	}

	void LabelComponent::BeginObject ()
	{
		Super::BeginObject();

		if (!VALID_OBJECT(RenderComponent))
		{
			RenderComponent = TextRenderComponent::CreateObject();
			RenderComponent->TransformationInterface = Transformation;
			AddComponent(RenderComponent);
		}

		CalculateMaxNumLines();
	}

	void LabelComponent::SetWindowHandle (Window* newWindowHandle)
	{
		Super::SetWindowHandle(newWindowHandle);

		if (VALID_OBJECT(RenderComponent) && WindowHandle != nullptr)
		{
			RenderComponent->RelevantRenderWindows.clear();
			RenderComponent->RelevantRenderWindows.push_back(WindowHandle);
		}
	}

	void LabelComponent::HandleSizeChange ()
	{
		Super::HandleSizeChange();

		CalculateMaxNumLines();

		if (bAutoRefresh)
		{
			RefreshText();
		}
	}

	void LabelComponent::CleanUpInvalidPointers ()
	{
		Super::CleanUpInvalidPointers();

		//Double check public accessible pointers
		CLEANUP_INVALID_POINTER(RenderComponent)
	}

	void LabelComponent::SetFont (const Font* newFont)
	{
		if (newFont == CurrentFont)
		{
			return;
		}

		CurrentFont = newFont;

		if (VALID_OBJECT(RenderComponent))
		{
			for (unsigned int i = 0; i < RenderComponent->Texts.size(); i++)
			{
				if (RenderComponent->Texts.at(i) != nullptr)
				{
					RenderComponent->Texts.at(i)->setFont(*(CurrentFont->GetFontResource()));
				}
			}
		}

		if (bAutoRefresh)
		{
			RefreshText();
		}
	}

	void LabelComponent::SetCharacterSize (INT newCharacterSize)
	{
		if (newCharacterSize == CharacterSize)
		{
			return;
		}

		CharacterSize = newCharacterSize;

		if (VALID_OBJECT(RenderComponent))
		{
			for (unsigned int i = 0; i < RenderComponent->Texts.size(); i++)
			{
				if (RenderComponent->Texts.at(i) != nullptr)
				{
					RenderComponent->Texts.at(i)->setCharacterSize(CharacterSize.ToUnsignedInt());
				}
			}
		}

		if (bAutoRefresh)
		{
			RefreshText();
		}
	}

	void LabelComponent::SetTextContent (DString newContent)
	{
		//Reset label's text
		TextContent.clear();
		TextContent.push_back(newContent);
		TrimmedTextTail = TXT("");

		ParseNewLineCharacters();

		WrapText();
		ClampText(); //This function updates the TextContent and TrimmedTextTrail based on the truncated text

		if (VALID_OBJECT(RenderComponent))
		{
			//Reset render component
			RenderComponent->DeleteAllText();
			RenderComponent->ClearAllDrawOffsets();

			RenderTextContent();

			AlignVertically();
			AlignHorizontally();
		}
	}

	void LabelComponent::RefreshText ()
	{
		CalculateMaxNumLines();
		SetTextContent(GetFullText());
	}

	void LabelComponent::SetAutoRefresh (bool bNewAutoRefresh)
	{
		bool bOldAutoRefresh = bAutoRefresh;
		bAutoRefresh = bNewAutoRefresh;

		if (bAutoRefresh && bAutoRefresh != bOldAutoRefresh)
		{
			RefreshText();
		}
	}

	void LabelComponent::SetWrapText (bool bNewWrapText)
	{
		bWrapText = bNewWrapText;

		if (bAutoRefresh)
		{
			RefreshText();
		}
	}

	void LabelComponent::SetClampText (bool bNewClampText)
	{
		bClampText = bNewClampText;

		if (bClampText && bAutoRefresh)
		{
			ClampText();
		}
	}

	void LabelComponent::SetLineSpacing (FLOAT newLineSpacing)
	{
		LineSpacing = newLineSpacing;

		INT oldMaxNumLines = MaxNumLines;
		CalculateMaxNumLines();

		if (!bAutoRefresh)
		{
			return;
		}

		if (oldMaxNumLines != MaxNumLines)
		{
			//MaxNumLines changed, we'll need to update the TextContent in case more or less text appears
			SetTextContent(GetFullText());
		}
		else
		{
			AlignVertically();
		}
	}

	void LabelComponent::SetVerticalAlignment (eVerticalAlignment newVerticalAlignment)
	{
		VerticalAlignment = newVerticalAlignment;

		if (bAutoRefresh)
		{
			AlignVertically();
		}
	}

	void LabelComponent::SetHorizontalAlignment (eHorizontalAlignment newHorizontalAlignment)
	{
		HorizontalAlignment = newHorizontalAlignment;

		if (bAutoRefresh)
		{
			AlignHorizontally();
		}
	}

	void LabelComponent::SetMaxNumLinesChanged (function<void(INT)> newHandler)
	{
		OnMaxNumLinesChanged = newHandler;
	}

	const Font* LabelComponent::GetFont () const
	{
		return CurrentFont;
	}

	INT LabelComponent::GetCharacterSize () const
	{
		return CharacterSize;
	}

	bool LabelComponent::GetAutoRefresh () const
	{
		return bAutoRefresh;
	}

	bool LabelComponent::GetWrapText () const
	{
		return bWrapText;
	}

	bool LabelComponent::GetClampText () const
	{
		return bClampText;
	}

	FLOAT LabelComponent::GetLineSpacing () const
	{
		return LineSpacing;
	}

	DString LabelComponent::GetTextContent () const
	{
		DString results = TXT("");

		for (unsigned int i = 0; i < TextContent.size(); i++)
		{
			results += TextContent.at(i);
		}

		return results;
	}

	vector<DString> LabelComponent::GetTextContentLines () const
	{
		return TextContent;
	}

	DString LabelComponent::GetTrimmedTextTail () const
	{
		return TrimmedTextTail;
	}

	DString LabelComponent::GetFullText () const
	{
		return (GetTextContent() + TrimmedTextTail);
	}

	LabelComponent::eVerticalAlignment LabelComponent::GetVerticalAlignment () const
	{
		return VerticalAlignment;
	}

	LabelComponent::eHorizontalAlignment LabelComponent::GetHorizontalAlignment () const
	{
		return HorizontalAlignment;
	}

	INT LabelComponent::GetMaxNumLines () const
	{
		return MaxNumLines;
	}

	bool LabelComponent::ParseNewLineCharacters ()
	{
		for (unsigned int i = 0; i < TextContent.size(); i++)
		{
			int curCharIndex = static_cast<int>(TextContent.at(i).String.find_first_of('\n'));

			//Exit loop if no new line characters were found, or if it's at the end
			if (curCharIndex == string::npos || curCharIndex >= TextContent.at(i).Length() - 1)
			{
				return (static_cast<int>(RenderComponent->Texts.size()) <= MaxNumLines);
			}

			//Transfer text after the charIndex to the next line
			TextContent.push_back(TextContent.at(i).SubString(curCharIndex + 1));
			TextContent.at(i) = TextContent.at(i).SubString(0,curCharIndex);
		}
		
		return (static_cast<int>(TextContent.size()) <= MaxNumLines);
	}

	void LabelComponent::WrapText ()
	{
		if (!bWrapText)
		{
			return;
		}

		vector<char> wrapCharacters;
		wrapCharacters.push_back(' ');
		wrapCharacters.push_back('-');

		for (unsigned int i = 0; i < TextContent.size(); i++)
		{
			INT charIndex = CurrentFont->FindCharNearWidthLimit(TextContent.at(i), CharacterSize.ToUnsignedInt(), Transformation->GetCurrentSizeX());

			if (charIndex >= TextContent.at(i).Length() - 1 || charIndex <= 0)
			{
				continue; //Either All characters in this line is within width or the first character is beyond width.
			}

			//Find the place to insert a new line character (usually between words)
			INT wrapPosition = FindPreviousCharacter(wrapCharacters, i, charIndex.Value);
			if (wrapPosition == INT_INDEX_NONE)
			{
				//Was unable to find a good place to wrap text.  Wrap at the character that went beyond boundaries.
				wrapPosition = charIndex;
			}
			else
			{
				//Increment by one to keep the current character at the previous line
				wrapPosition++;
			}

			//Divide the string into two parts
			TextContent.insert(TextContent.begin() + i + 1, TextContent.at(i).SubString(wrapPosition));
			TextContent.at(i) = TextContent.at(i).SubString(0, wrapPosition - 1);
		}
	}

	void LabelComponent::ClampText ()
	{
		TrimmedTextTail = TXT("");

		if (!bClampText || TextContent.size() <= 0)
		{
			return;
		}

		if (bWrapText)
		{
			//Chop line-by-line until number of lines is within MaxLines limit
			while (TextContent.size() > 0 && static_cast<int>(TextContent.size()) > MaxNumLines)
			{
				//prefix truncatedText with the last line
				TrimmedTextTail = TextContent.at(TextContent.size() - 1) + TrimmedTextTail;
				TextContent.pop_back();
			}
		}
		else //!bWrapText
		{
			//Simply remove the text beyond width from the last line.  90% of the time there will only be one line of text for unwrapped label components.
			//For multi-lined unwrapped label components, it's assumed that the predetermined line breaks are there for a particular reason and will not truncate that.
			INT charIdx = CurrentFont->FindCharNearWidthLimit(TextContent.at(TextContent.size() - 1), CharacterSize.ToUnsignedInt(), Transformation->GetCurrentSizeX());

			//If charIdx is in middle of TextContent
			if (charIdx > 0 && charIdx < TextContent.at(TextContent.size() - 1).Length() - 1)
			{
				TrimmedTextTail = TextContent.at(TextContent.size() - 1).SubString(charIdx + 1);
				TextContent.at(TextContent.size() - 1) = TextContent.at(TextContent.size() - 1).SubString(0, charIdx);
			}
		}
	}

	void LabelComponent::RenderTextContent ()
	{
		for (unsigned int i = 0; i < TextContent.size(); i++)
		{
			RenderComponent->Texts.push_back(new sf::Text(TextContent.at(i).String, *(CurrentFont->GetFontResource()), CharacterSize.ToUnsignedInt()));
			RenderComponent->SetDrawOffset(i, 0, (CharacterSize.ToFLOAT() + LineSpacing) * FLOAT::MakeFloat(i));
		}
	}

	void LabelComponent::AlignVertically ()
	{
		if (!VALID_OBJECT(Transformation))
		{
			return;
		}

		FLOAT lastLinePos = Transformation->CalcFinalCoordinatesY() + ((CharacterSize.ToFLOAT() + LineSpacing) * FLOAT::MakeFloat(RenderComponent->Texts.size()));
		FLOAT unusedSpace = Transformation->CalcFinalBottomBounds() - lastLinePos.Value;
		FLOAT firstLineOffset = 0; //Distance between the top of this component's yPos and the first Text's yPos.

		//Clamp unused space in case it's negative (when the characters are larger than the component, itself
		unusedSpace = Utils::Max<FLOAT>(0, unusedSpace);

		switch(VerticalAlignment)
		{
			case(VA_Top):
				break;
			case(VA_Center):
				firstLineOffset = unusedSpace / 2;
				break;

			case(VA_Bottom):
				firstLineOffset = unusedSpace;
				break;
		}

		for (unsigned int i = 0; i < RenderComponent->Texts.size(); i++)
		{
			sf::Vector2f newLocation;
			RenderComponent->GetDrawOffset(i, newLocation);
			newLocation.y = (firstLineOffset + ((CharacterSize.ToFLOAT() + LineSpacing) * FLOAT::MakeFloat(i))).Value;
			RenderComponent->SetDrawOffset(i, newLocation);
		}
	}

	void LabelComponent::AlignHorizontally ()
	{
		//Use faster/simpler algorithm when aligning text to the left
		if (HorizontalAlignment == HA_Left)
		{
			for (unsigned int i = 0; i < RenderComponent->GetDrawOffsetLength(); i++)
			{
				sf::Vector2f newLocation;
				RenderComponent->GetDrawOffset(i, newLocation);
				newLocation.x = 0;
				RenderComponent->SetDrawOffset(i, newLocation);
			}

			return;
		}

		for (unsigned int i = 0; i < RenderComponent->Texts.size(); i++)
		{
			DString curString = RenderComponent->Texts.at(i)->getString();
			sf::Vector2f curCharPos = RenderComponent->Texts.at(i)->findCharacterPos(curString.Length().Value);
			curCharPos.x += Transformation->CalcFinalCoordinatesX().Value;
			FLOAT unusedSpace = Transformation->CalcFinalRightBounds() - FLOAT(curCharPos.x);
			float horizontalShift = 0;

			switch (HorizontalAlignment)
			{
				case(HA_Center):
					horizontalShift = FLOAT(unusedSpace / 2).Value;
					break;

				case(HA_Right):
					horizontalShift = unusedSpace.Value;
					break;
			}

			sf::Vector2f newLocation;
			RenderComponent->GetDrawOffset(i, newLocation);
			newLocation.x = horizontalShift;
			RenderComponent->SetDrawOffset(i, newLocation);
		}
	}

	INT LabelComponent::FindPreviousCharacter (const std::vector<char>& targetCharacters, unsigned int textLine, INT startPosition) const
	{
		if (TextContent.at(textLine).Length() <= 0)
		{
			return INT_INDEX_NONE;
		}

		if (startPosition < 0)
		{
			startPosition = TextContent.at(textLine).Length() - 1;
		}

		for (INT i = startPosition; i >= 0; i--)
		{
			for (unsigned int j = 0; j < targetCharacters.size(); j++)
			{
				if (targetCharacters.at(j) == TextContent.at(textLine).At(i))
				{
					return i;
				}
			}
		}

		return INT_INDEX_NONE;
	}

	void LabelComponent::CalculateMaxNumLines ()
	{
		INT oldMaxLines = MaxNumLines;

		if (!VALID_OBJECT(RenderComponent))
		{
			MaxNumLines = 1;
		}
		else
		{
			FLOAT result = Transformation->GetCurrentSize().Y / (CharacterSize.ToFLOAT() + LineSpacing);
			result = trunc(result.Value);
			MaxNumLines = Utils::Max<INT>(1, result.ToINT());
		}

		if (oldMaxLines != MaxNumLines && OnMaxNumLinesChanged != nullptr)
		{
			OnMaxNumLinesChanged(MaxNumLines);
		}
	}
}

#endif