/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ButtonStateComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GUIClasses.h"

#if INCLUDE_GUI

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(ButtonStateComponent, EntityComponent)

	void ButtonStateComponent::InitProps ()
	{
		Super::InitProps();

		OwningButton = nullptr;
	}

	void ButtonStateComponent::BeginObject ()
	{
		Super::BeginObject();

		SetDefaultAppearance();
	}

	bool ButtonStateComponent::CanBeAttachedTo (Entity* ownerCandidate) const
	{
		if (!Super::CanBeAttachedTo(ownerCandidate))
		{
			return false;
		}

		if (ownerCandidate == nullptr)
		{
			return true;
		}
		
		return (dynamic_cast<ButtonComponent*>(ownerCandidate) != nullptr);
	}

	void ButtonStateComponent::AttachTo (Entity* newOwner)
	{
		Super::AttachTo(newOwner);

		OwningButton = dynamic_cast<ButtonComponent*>(newOwner);
	}

	void ButtonStateComponent::RefreshState ()
	{
		if (!VALID_OBJECT(OwningButton))
		{
			return;
		}

		if (!OwningButton->GetEnabled())
		{
			SetDisableAppearance();
		}
		else if (OwningButton->GetPressedDown())
		{
			SetDownAppearance();
		}
		else if (OwningButton->GetHovered())
		{
			SetHoverAppearance();
		}
		else
		{
			SetDefaultAppearance();
		}
	}

	void ButtonStateComponent::DrawToWindow (Window* newRelevantWindow)
	{
	
	}

	void ButtonStateComponent::SetDefaultAppearance ()
	{
	
	}

	void ButtonStateComponent::SetDisableAppearance ()
	{
	
	}

	void ButtonStateComponent::SetHoverAppearance ()
	{
	
	}

	void ButtonStateComponent::SetDownAppearance ()
	{
	
	}

	ButtonComponent* ButtonStateComponent::GetOwningButton () const
	{
		return OwningButton;
	}
}

#endif