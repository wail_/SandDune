/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Texture.h
  An object responsible for storing a reference to a texture resource.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef TEXTURE_H
#define TEXTURE_H

#include "SFMLGraphics.h"

#if INCLUDE_SFMLGRAPHICS

#define TEXTURE_LOCATION TXT("Images")

namespace SD
{
	class Texture : public Object
	{
		DECLARE_CLASS(Texture)

		/*
		=====================
		  Properties
		=====================
		*/

	protected:
		sf::Texture* TextureResource;

		INT Width;
		INT Height;
		DString TextureName;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual DString GetFriendlyName () const override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		virtual void Release ();

		/**
		  Overrides pixel data of the texture.
		  Returns true on success.
		 */
		virtual bool SetPixelData (TCHAR newPixelData[]);

		virtual void SetSmooth (bool bSmooth);

		virtual void SetRepeat (bool bRepeating);

		/**
		  Destroys the previous resource, and sets the texture resource to the given params.
		 */
		virtual void SetResource (sf::Texture* newResource);


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		virtual void GetDimensions (INT& outWidth, INT& outHeight) const;
		virtual void GetDimensions (Vector2& outDimensions) const;
		virtual Vector2 GetDimensions () const;
		virtual INT GetWidth () const;
		virtual INT GetHeight () const;
		virtual DString GetTextureName () const;

		/**
		  Returns the SFML's texture resource.
		 */
		virtual const sf::Texture* GetTextureResource () const;

		friend class TexturePool;
	};
}

#endif
#endif