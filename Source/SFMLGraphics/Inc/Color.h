/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Color.h
  An object representing a color for the SFML's color object.  This
  object is treated as a datatype.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef COLOR_H
#define COLOR_H

#include "SFMLGraphics.h"

#if INCLUDE_SFMLGRAPHICS

namespace SD
{
	class Color : public DProperty
	{
	

		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* The color object actually implementing this datatype. */
		sf::Color Source;


		/*
		=====================
		  Constructors
		=====================
		*/

	public:
		Color ();
		Color (const Color& otherColor);
		Color (const sf::Color& otherColor);

		//Construct a color datatype from INTs, INTs are automatically clamped from 0-255
		Color (INT red, INT green, INT blue, INT alpha = 255);


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual DString ToString () const override;
	};

#pragma region "Operators"
//Same as sf::Color operators.  Please refer to SFML documentation to see what these operators do
//http://www.sfml-dev.org/documentation/2.3.2/classsf_1_1Color.php
bool operator== (const Color &left, const Color &right);
 
bool operator!= (const Color &left, const Color &right);

Color operator+ (const Color &left, const Color &right);

Color operator- (const Color &left, const Color &right);

Color operator* (const Color &left, const Color &right);
 
Color& operator+= (Color &left, const Color &right);

Color& operator-= (Color &left, const Color &right);
 
Color& operator*= (Color &left, const Color &right);

#pragma endregion
}

#endif
#endif