/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Window.h
  An object representing a Window.  Contains various delegates other
  objects could register to for event handling.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef WINDOW_H
#define WINDOW_H

#include "SFMLGraphics.h"

#if INCLUDE_SFMLGRAPHICS

namespace SD
{
	class Window : public Object
	{
		DECLARE_CLASS(Window)
	

		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* If true, then this object will delete the WindowHandle object when this object is deleted.
		  If false, then the owning object is expected to clean up the resources.  Otherwise it'll cause a memory leak. */
		bool bClearsHandleOnDelete;

		/* Actual handle that's broadcasting the events, and interfaces with the OS. */
		sf::RenderWindow* Resource;

	protected:
		/* List of event handlers that may be interested to any of this window handle's events. */
		std::vector<SDFunction<void, const sf::Event>> PollingDelegates;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual void Destroy () override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Registers the owning objects event handler to receive notifications from window events.
		  The external object is responsible for calling UnregisterPollingDelegate to avoid dangling pointers.
		 */
		virtual void RegisterPollingDelegate (SDFunction<void, const sf::Event> newCallback);
		virtual void UnregisterPollingDelegate (SDFunction<void, const sf::Event> targetCallback);

		/**
		  Notifies this object to check all pending window events to broadcast delegates.
		  The Window does not automatically poll its own events since this object could be external from main loop.
		 */
		virtual void PollWindowEvents ();

		/**
		  Retrieves the OS's coordinates of the current window.  If bIncludeTitlebar is false, then it'll retrieve
		  the coordinates within the view.
		 */
		virtual void GetWindowPosition (INT& outPosX, INT& outPosY, bool bIncludeTitlebar = false) const;


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		virtual void GetWindowSize (INT& outWidth, INT& outHeight) const;
		virtual Vector2 GetWindowSize () const;
	};
}

#endif
#endif