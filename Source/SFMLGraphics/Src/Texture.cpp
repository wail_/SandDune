/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Texture.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "SFMLGraphicsClasses.h"

#if INCLUDE_SFMLGRAPHICS

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(Texture, Object)

	void Texture::InitProps ()
	{
		Super::InitProps();

		Width = 0;
		Height = 0;
		TextureName = TXT("UnknownTexture");
		TextureResource = nullptr;
	}

	DString Texture::GetFriendlyName () const
	{
		if (!TextureName.IsEmpty())
		{
			return TXT("Texture:  ") + TextureName;
		}

		return Super::GetFriendlyName();
	}

#if 0
	Texture* Texture::CreateTexture (int width, int height, const DString& textureName, TCHAR pixelData[])
	{
		//validate params
#ifdef REQPOWEROF2
		if (!Utils::IsPowerOf2(width) || !Utils::IsPowerOf2(height))
		{
			LOG3(LOG_WARNING, TXT("Cannot create a texture %s with dimensions %sx%s.  Values must be in powers of 2."), textureName, INT(width), INT(height));
			return nullptr;
		}
#endif

		if (textureName.IsEmpty())
		{
			LOG(LOG_WARNING, TXT("Cannot create a texture without specifying a name."));
			return nullptr;
		}

		if (!TexturePool::GetTexturePool())
		{
			LOG1(LOG_WARNING, TXT("Cannot create texture (%s).  Cannot find a reference to the texture pool!"), textureName);
			return nullptr;
		}

		//check for clashing names
		Texture* textureReference = LoadTexture(textureName);
		if (textureReference)
		{
			LOG1(LOG_WARNING, TXT("A texture with the name %s is already created."), textureName);
			return textureReference;
		}

		sf::Texture* newTexture = new sf::Texture();
		if (!newTexture->create(width, height))
		{
			LOG2(LOG_WARNING, TXT("Unable to create %sx%s texture."), INT(width), INT(height));
			return nullptr;
		}

		textureReference = dynamic_cast<Texture*>(Texture::CreateObject());
		if (!textureReference)
		{
			return nullptr;
		}

		textureReference->TextureResource = newTexture;
		textureReference->Width = width;
		textureReference->Height = height;
		textureReference->TextureName = textureName;
		TexturePool::GetTexturePool()->RegisterTexture(textureReference, textureName);


		if (pixelData != nullptr)
		{
			textureReference->SetPixelData(pixelData);
		}

		return textureReference;
	}

	Texture* Texture::LoadTexture (const DString& textureName)
	{
		TexturePool* texturePool = TexturePool::GetTexturePool();
		if (texturePool == nullptr)
		{
			LOG1(LOG_WARNING, TXT("Unable to load texture (%s).  Cannot obtain a reference to the texture pool!"), textureName);
			return nullptr;
		}

		return (texturePool->FindTexture(textureName));
	}
#endif

	void Texture::Release ()
	{
		if (TextureResource != nullptr)
		{
			delete TextureResource;
			TextureResource = nullptr;
		}
	}

	bool Texture::SetPixelData (TCHAR newPixelData[])
	{
		INT pixelDataLength = sizeof(newPixelData);

		if (pixelDataLength != Width * Height * 4)
		{
			LOG3(LOG_WARNING, TXT("Unable to set pixel data since the length of the pixel data does not match the size of the texture (%sx%s).  Length of pixel data is %s."), Width, Height, pixelDataLength);
			return false;
		}

		sf::Uint8* textureData = new (nothrow) sf::Uint8[Width.Value * Height.Value * 4];
		if (textureData == nullptr)
		{
			LOG1(LOG_WARNING, TXT("Unable to allocate memory to update pixel data for %s."), TextureName);
			return false;
		}

		// Copy pixelData to textureData
		for (unsigned int i = 0; i < pixelDataLength; i++)
		{
			textureData[i] = newPixelData[i];
		}

		TextureResource->update(textureData);

		delete[] textureData;
		return true;
	}

	void Texture::SetSmooth (bool bSmooth)
	{
		TextureResource->setSmooth(bSmooth);
	}

	void Texture::SetRepeat (bool bRepeating)
	{
		TextureResource->setRepeated(bRepeating);
	}

	void Texture::SetResource (sf::Texture* newResource)
	{
		if (TextureResource != nullptr)
		{
			delete TextureResource;
		}

		TextureResource = newResource;
	}

	void Texture::GetDimensions (INT &outWidth, INT &outHeight) const
	{
		outWidth = Width;
		outHeight = Height;
	}

	void Texture::GetDimensions (Vector2& outDimensions) const
	{
		outDimensions.X = Width.ToFLOAT();
		outDimensions.Y = Height.ToFLOAT();
	}

	Vector2 Texture::GetDimensions () const
	{
		return Vector2(Width.ToFLOAT(), Height.ToFLOAT());
	}

	INT Texture::GetWidth () const
	{
		return Width;
	}

	INT Texture::GetHeight () const
	{
		return Height;
	}

	DString Texture::GetTextureName () const
	{
		return TextureName;
	}

	const sf::Texture* Texture::GetTextureResource () const
	{
		return TextureResource;
	}
}

#endif