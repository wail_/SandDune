/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Window.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "SFMLGraphicsClasses.h"

#if INCLUDE_SFMLGRAPHICS

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(Window, Object)

	void Window::InitProps ()
	{
		Super::InitProps();

		bClearsHandleOnDelete = true;
		Resource = nullptr;
	}

	void Window::Destroy ()
	{
		if (bClearsHandleOnDelete)
		{
			delete Resource;
		}

		Resource = nullptr;

		Super::Destroy();
	}

	void Window::RegisterPollingDelegate (SDFunction<void, const sf::Event> newCallback)
	{
		PollingDelegates.push_back(newCallback);
	}

	void Window::UnregisterPollingDelegate (SDFunction<void, const sf::Event> targetCallback)
	{
		for (unsigned int i = 0; i < PollingDelegates.size(); i++)
		{
			if (PollingDelegates.at(i) == targetCallback)
			{
				PollingDelegates.erase(PollingDelegates.begin() + i);
				break;
			}
		}
	}

	void Window::PollWindowEvents ()
	{
		//Mouse move events are recorded here and only consider the latest mouse move position since multiple mouse move events could be executed within a single frame.
		Vector2 finalMousePos = Vector2(0,0);

		sf::Event sfEvent;
		while (Resource != nullptr && Resource->pollEvent(sfEvent)) //Reason for Resource != null check... It's possible for a polling delegate to cause this window to shutdown.
		{
			if (sfEvent.type == sf::Event::MouseMoved)
			{
				finalMousePos.X = FLOAT::MakeFloat(sfEvent.mouseMove.x);
				finalMousePos.Y = FLOAT::MakeFloat(sfEvent.mouseMove.y);
				continue; //Don't notify polling delegates yet.  There may be other pending mouse moves.
			}

			for (unsigned int i = 0; i < PollingDelegates.size(); i++)
			{
				PollingDelegates.at(i).Execute(sfEvent);
			}
		}

		if (!finalMousePos.IsEmpty())
		{
			sfEvent.type = sf::Event::MouseMoved;
			sfEvent.mouseMove.x = finalMousePos.X.ToINT().Value;
			sfEvent.mouseMove.y = finalMousePos.Y.ToINT().Value;

			for (unsigned int i = 0; i < PollingDelegates.size(); i++)
			{
				PollingDelegates.at(i).Execute(sfEvent);
			}
		}
	}

	void Window::GetWindowPosition (INT& outPosX, INT& outPosY, bool bIncludeTitlebar) const
	{
		if (!bIncludeTitlebar)
		{
			Rectangle<INT> clientWindow = OS_GetClientWindowPos(this);

			outPosX = clientWindow.Left;
			outPosY = clientWindow.Top;
			return;
		}

		sf::Vector2i windowPos = Resource->getPosition();

		outPosX = windowPos.x;
		outPosY = windowPos.y;
	}

	void Window::GetWindowSize (INT& outWidth, INT& outHeight) const
	{
		sf::Vector2u size = Resource->getSize();

		outWidth = size.x;
		outHeight = size.y;
	}

	Vector2 Window::GetWindowSize () const
	{
		sf::Vector2u size = Resource->getSize();

		return Vector2(FLOAT::MakeFloat(size.x), FLOAT::MakeFloat(size.y));
	}
}

#endif