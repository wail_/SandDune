/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MouseTester.h
  Entity that helps the InputUnitTester to validate the MousePointer.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef MOUSETESTER_H
#define MOUSETESTER_H

#include "Input.h"

#if INCLUDE_INPUT

#ifdef DEBUG_MODE
namespace SD
{
	class InputComponent;
	class InputBroadcaster;
	class MouseTesterRender;

	class MouseTester : public Entity
	{
		DECLARE_CLASS(MouseTester)


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* If true, then this entity will listen and may capture input events. */
		bool bActive;

		UnitTester::EUnitTestFlags TestFlags;

	protected:
		/* Becomes true if the user is currently setting the limit region. */
		bool bSettingLimitRegion;

		InputComponent* Input;

		/* Component responsible for rendering the rectangle for region limits. */
		MouseTesterRender* RegionRenderer;

	private:
		/* Various input flags that are pressed down. */
		bool bHolding1;
		bool bHolding2;
		bool bHolding3;
		bool bHolding4;

		/* Is true, if this tester is currently clamping mouse pointer. */
		bool bClampingMouse;

		/* Various mouse icons to use based on held numbers. */
		static Texture* Holding1Tx;
		static Texture* Holding2Tx;
		static Texture* Holding3Tx;
		static Texture* Holding4Tx;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual void BeginObject () override;
		virtual void Destroy () override;

	protected:
		virtual void PostEngineInitialize () const;
		virtual void CleanUpInvalidPointers () override;


		/*
		=====================
		  Event Handlers
		=====================
		*/

	protected:
		virtual bool HandleCaptureInput (const sf::Event& keyEvent);
		virtual void HandleMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove);
		virtual bool HandleMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType);

		/* Various event handlers that determine which mouse icon the mouse pointer should use. */
		virtual bool HandleMouseIcon1 (const sf::Event::MouseMoveEvent& sfEvent);
		virtual bool HandleMouseIcon2 (const sf::Event::MouseMoveEvent& sfEvent);
		virtual bool HandleMouseIcon3 (const sf::Event::MouseMoveEvent& sfEvent);
		virtual bool HandleMouseIcon4 (const sf::Event::MouseMoveEvent& sfEvent);

		virtual bool HandlePushLimit (const sf::Event::MouseMoveEvent& sfEvent);
	};
}

#endif
#endif
#endif