/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  InputEngineComponent.h
  This component is responsible for instantiating the InputBroadcaster
  that listens for input messages from the main application's window.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef INPUTENGINECOMPONENT_H
#define INPUTENGINECOMPONENT_H

#include "Input.h"

#if INCLUDE_INPUT

namespace SD
{
	class InputBroadcaster;
	class MousePointer;

	class InputEngineComponent : public EngineComponent
	{
		DECLARE_ENGINE_COMPONENT(InputEngineComponent)


		/*
		=====================
		  Properties
		=====================
		*/

	protected:
		/* Broadcast handler that'll be broadcasting message events from the main window. */
		InputBroadcaster* MainBroadcaster;

		/* List of all instanced broadcasters. */
		std::vector<InputBroadcaster*> InputBroadcasters;

		/* Mouse pointer affiliated with the main application window (defined in GraphicsEngineComponent). */
		MousePointer* Mouse;


		/*
		=====================
		  Constructors
		=====================
		*/

	public:
		InputEngineComponent ();

		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void PreInitializeComponent () override;
		virtual void InitializeComponent () override;
		virtual void ShutdownComponent () override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Finds an input broadcaster that's broadcasting input events from the specified window.
		 */
		virtual InputBroadcaster* FindBroadcasterForWindow (Window* window) const;

		/**
		  Unconditionally appends the new broadcaster to the list of Broadcasters.
		 */
		virtual void RegisterInputBroadcaster (InputBroadcaster* newBroadcaster);

		/**
		  Removes input broadcaster from the list of broadcasters.
		 */
		virtual void UnregisterInputBroadcaster (InputBroadcaster* oldBroadcaster);

		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		virtual InputBroadcaster* GetMainBroadcaster () const;
		virtual MousePointer* GetMouse () const;
	};
}

#endif
#endif