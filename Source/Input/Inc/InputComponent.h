/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  InputComponent.h
  A component that receives events and may potentially capture
  that input event, which hinders other components further down the stack
  from receiving the key event.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef INPUTCOMPONENT_H
#define INPUTCOMPONENT_H

#include "Input.h"

#if INCLUDE_INPUT

namespace SD
{
	class MousePointer;
	class InputBroadcaster;

	class InputComponent : public EntityComponent
	{
		DECLARE_CLASS(InputComponent)


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* Various callbacks for the input events. */
		SDFunction<bool, const sf::Event&> CaptureInputDelegate;
		SDFunction<bool, const sf::Event&> CaptureTextDelegate;
		SDFunction<void, MousePointer*, const sf::Event::MouseMoveEvent&, const Vector2&> MouseMoveDelegate;
		SDFunction<bool, MousePointer*, const sf::Event::MouseButtonEvent&, sf::Event::EventType> MouseClickDelegate;
		SDFunction<bool, MousePointer*, const sf::Event::MouseWheelScrollEvent&> MouseWheelScrollDelegate;

	protected:
		/* Broadcaster this component registered to.  This component will unregister itself from the
		broadcaster on destroy.  This is automatically set when (un)registering to a broadcaster.*/
		InputBroadcaster* InputMessenger;

		/* Input Priority this InputComponent was registered to its InputMessenger.  An InputBroadcaster directly assigns this variable whenever this component registers to it. */
		INT InputPriority;

		/* Broadcaster this component is pending to.  Needed in case this component is moving to a new broadcaster while this one was already pending from another broadcaster. */
		InputBroadcaster* PendingInputMessenger;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual void BeginObject () override;
		virtual void Destroy () override;

	protected:
		virtual void CleanUpInvalidPointers () override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Invoked whenever a key press event was sent.
		  Return true to prevent other InputComponents further down the stack from receiving this event.
		  Warning:  Keep a look out for overlaping keyEvents.  For example:  if bUnicode is unchecked, keyEvent.Text.unicode 54 will be true for both "6" and sf::Keyboard::Tilde
		 */
		virtual bool CaptureInput (const sf::Event& keyEvent);

		/**
		  Invoked whenever a text event was sent.
		  Return true to prevent other InputComponents further down the stack from receiving this event.
		  Not to be confused with CaptureInput.  This function's keyEvent contains interpretted text from sequence of keys that was pressed (ie:  Shift + a = 'A').
		 */
		virtual bool CaptureText (const sf::Event& keyEvent);

		/**
		  Invoked whenever the mouse pointer moved.
		  Use sfmlEvent.x to get the new mouse pointer cordinate relative to the left of the window.
		  Use sfmlEvent.y to get the new mouse pointer cordinate relative to the top of the window.
		  deltaMove is the distance traveled since last MouseMove event.
		 */
		virtual void MouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove);

		/**
		  Invoked whenever any mouse button was clicked.

		  Supported buttons:
		  Left, Right, Middle, XButton1, XButton2

		  eventType will either MouseButtonPressed or MouseButtonReleased
		  Return true to prevent other InputComponents further down the stack from receiving this event.
		 */
		virtual bool MouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType);

		/**
		  Invoked whenever the mouse wheel moved.
		  sfmlEvent.delta will be positive if the wheel moved up, negative when wheel moved down.
		  Return true to prevent other InputComponents further down the stack from receiving this event.
		 */
		virtual bool MouseWheelScroll (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent);

		virtual void SetInputPriority (INT newPriority);


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		virtual InputBroadcaster* GetInputMessenger () const;
		virtual INT GetInputPriority () const;

		friend class InputBroadcaster;
	};
}

#endif
#endif