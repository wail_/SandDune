/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  InputTester.h
  Entity that helps the InputUnitTester to validate the InputComponent.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef INPUTTESTER_H
#define INPUTTESTER_H

#include "Input.h"

#if INCLUDE_INPUT

#ifdef DEBUG_MODE
namespace SD
{
	class InputTester : public Entity
	{
		DECLARE_CLASS(InputTester)


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		UnitTester::EUnitTestFlags TestFlags;

	protected:
		InputBroadcaster* TestBroadcaster;
		InputComponent* TestComponent;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual void BeginObject () override;
		virtual void Destroy () override;

	protected:
		virtual void CleanUpInvalidPointers () override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		virtual void SetTestBroadcaster (InputBroadcaster* newTestBroadcaster);


		/*
		=====================
		  Event Handlers
		=====================
		*/

	protected:
		virtual bool HandleCaptureInput (const sf::Event& keyEvent);
		virtual bool HandleCaptureText (const sf::Event& keyEvent);
		virtual void HandleMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove);
		virtual bool HandleMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType);
		virtual bool HandleMouseWheelScroll (MousePointer* mouse, const sf::Event::MouseWheelScrollEvent& sfmlEvent);

	};
}

#endif
#endif
#endif