/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  InputBroadcaster.h
  Binds delegates to a Window handle to receive input events.  Then
  it'll broadcast those events to registered InputComponents until the event
  is consumed, or if it reached to the end of the stack.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

//TODO:  move to input module
//TODO:  Rename to input broadcaster
//TODO:  debugging
//TODO:  Instanced entity (for multithread support)
//TODO:  Move the bHeldShift, alt, and ctrl flags to keyboard state entity (this class should only care about broadcasting)
//TODO:  bAlt, Ctrl, Shift flags should be bitwise flags.


#ifndef INPUTBROADCASTER_H
#define INPUTBROADCASTER_H

#include "Input.h"

#if INCLUDE_CORE

namespace SD
{
	class InputComponent;

	class InputBroadcaster : public Object
	{
		DECLARE_CLASS(InputBroadcaster)


		/*
		=====================
		  Data types
		=====================
		*/

	protected:
		struct SInputPriorityMapping
		{
			InputComponent* Component;

			/* Relatively speaking, the higher the Priority, the earlier this input component will receive events. */
			INT Priority;

			SInputPriorityMapping (InputComponent* inComponent, INT inPriority)
			{
				Component = inComponent;
				Priority = inPriority;
			}
		};


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* Various debugging flags about a particular input event. */
		bool bDebugKeyEvent;
		bool bDebugTextEvent;
		bool bDebugMouseMove;
		bool bDebugMouseButton;
		bool bDebugMouseWheel;

	protected:
		/* Various input state changes. */
		static bool bCtrlHeld;
		static bool bShiftHeld;
		static bool bAltHeld;

		/* A list of registered InputComponents that may receive key events. */
		std::vector<SInputPriorityMapping> RegisteredInputComponents;

		/* Window handle this broadcaster is receiving events from. */
		Window* InputSource;

		/* Previous mouse pointer position used to calculate the delta mouse move. */
		Vector2 OldMousePosition;

		MousePointer* AssociatedMouse;

		/* If true, the the InputBroadcaster is in the middle of reading from RegisteredInputComponents (within a loop).
		If this is true, then new input components cannot be registered, and old components cannot be removed until this is turns false. */
		bool bLockedRegisteredComponents;

		/* List of components waiting to be registered when RegisteredInputComponents vector is available. */
		std::vector<SInputPriorityMapping> PendingRegisterComponents;

		/* List of components waiting to be unregistered when RegisteredInputComponents vector is available. */
		std::vector<InputComponent*> PendingUnregisterComponents;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual void BeginObject () override;
		virtual void Destroy () override;

	protected:
		virtual void CleanUpInvalidPointers () override;

		
		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Binds event callbacks to the given window.
		 */
		virtual void SetInputSource (Window* targetWindow);

		/**
		  Adds an InputComponent to the RegisteredInputs array.
		  The higher the priority, the earlier the InputComponent will appear in the RegisteredInputsArray (gets key events before other components).
		 */
		virtual void AddInputComponent (InputComponent* newComponent, INT priority = 0);

		/**
		  Reorders the input priority for specified input component.
		  Returns true if the targetComponent was found.
		 */
		virtual bool SetInputPriority (InputComponent* targetComponent, INT newPriority);

		/**
		  Removes an InputComponent from the RegisteredInputs array.
		  Returns true if the target was found and removed.
		 */
		virtual bool RemoveInputComponent (InputComponent* target);

		virtual void SetAssociatedMouse (MousePointer* newAssociatedMouse);

#ifdef DEBUG_MODE
		/**
		  Logs out the list of registered InputComponents and their priorities.
		 */
		virtual void LogRegisteredInputComponents () const;
#endif


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		virtual MousePointer* GetAssociatedMouse () const;
		static bool GetCtrlHeld ();
		static bool GetShiftHeld ();
		static bool GetAltHeld ();
		virtual Window* GetInputSource () const;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		virtual void SetLockedRegisteredComponents (bool bNewLockedRegisteredComponents);

		/**
		  Various input events to broadcast to InputComponents whenever a window event message was received.
		 */
		virtual void NewKeyEvent (sf::Event newEvent);
		virtual void NewTextEvent (sf::Event newEvent);
		virtual void NewMouseMoveEvent (sf::Event::MouseMoveEvent sfmlEvent);
		virtual void NewMouseButtonEvent (sf::Event::MouseButtonEvent sfmlEvent, sf::Event::EventType eventType);
		virtual void NewMouseWheelEvent (sf::Event::MouseWheelScrollEvent sfmlEvent);

		/**
		  Helper functions to convert various aspects of the sf::Event to DString (for logging).
		 */
		virtual DString KeyEventToString (sf::Event sfEvent) const;
		virtual DString TextEventToString (sf::Event sfEvent) const;
		virtual DString MouseMoveEventToString (sf::Event::MouseMoveEvent sfEvent) const;
		virtual DString MouseButtonEventToString (sf::Event::MouseButtonEvent sfEvent) const;
		virtual DString MouseWheelEventToString (sf::Event::MouseWheelScrollEvent sfEvent) const;

		/**
		  Updates the Input Messenger variables for the given InputComponent.
		  Not virtual since this function relies on InputComponent's friend class.
		 */
		void SetInputMessengerFor (InputComponent* target);
		void ClearInputMessengerFor (InputComponent* target);


		/*
		=====================
		  Event Handlers
		=====================
		*/

	protected:
		/**
		  Invoked whenever any event was invoked to the window this broadcaster is bound to.
		 */
		virtual void HandleWindowEvent (const sf::Event newEvent);
	};
}

#endif
#endif