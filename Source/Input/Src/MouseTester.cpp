/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MouseTester.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "InputClasses.h"

#if INCLUDE_INPUT

#ifdef DEBUG_MODE

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(MouseTester, Entity)

	Texture* MouseTester::Holding1Tx = nullptr;
	Texture* MouseTester::Holding2Tx = nullptr;
	Texture* MouseTester::Holding3Tx = nullptr;
	Texture* MouseTester::Holding4Tx = nullptr;

	void MouseTester::InitProps ()
	{
		Super::InitProps();

		bActive = true;
		bSettingLimitRegion = false;
		Input = nullptr;
		RegionRenderer = nullptr;

		bHolding1 = false;
		bHolding2 = false;
		bHolding3 = false;
		bHolding4 = false;
		bClampingMouse = false;
	}

	void MouseTester::BeginObject ()
	{
		Super::BeginObject();

		Input = InputComponent::CreateObject();
		if (VALID_OBJECT(Input))
		{
			Input->CaptureInputDelegate = SDFUNCTION_1PARAM(this, MouseTester, HandleCaptureInput, bool, const sf::Event&);
			Input->MouseMoveDelegate = SDFUNCTION_3PARAM(this, MouseTester, HandleMouseMove, void, MousePointer*, const sf::Event::MouseMoveEvent&, const Vector2&);
			Input->MouseClickDelegate = SDFUNCTION_3PARAM(this, MouseTester, HandleMouseClick, bool, MousePointer*, const sf::Event::MouseButtonEvent&, sf::Event::EventType);
			AddComponent(Input);
		}

		RegionRenderer = MouseTesterRender::CreateObject();
		if (VALID_OBJECT(RegionRenderer))
		{
			AddComponent(RegionRenderer);
		}
	}

	void MouseTester::Destroy ()
	{
		MousePointer* pointer = InputEngineComponent::Get()->GetMouse();
		if (VALID_OBJECT(pointer))
		{
			pointer->RemoveIconOverride(SDFUNCTION_1PARAM(this, MouseTester, HandleMouseIcon1, bool, const sf::Event::MouseMoveEvent&));
			pointer->RemoveIconOverride(SDFUNCTION_1PARAM(this, MouseTester, HandleMouseIcon2, bool, const sf::Event::MouseMoveEvent&));
			pointer->RemoveIconOverride(SDFUNCTION_1PARAM(this, MouseTester, HandleMouseIcon3, bool, const sf::Event::MouseMoveEvent&));
			pointer->RemoveIconOverride(SDFUNCTION_1PARAM(this, MouseTester, HandleMouseIcon4, bool, const sf::Event::MouseMoveEvent&));
			pointer->RemovePositionLimit(SDFUNCTION_1PARAM(this, MouseTester, HandlePushLimit, bool, const sf::Event::MouseMoveEvent&));
		}

		Super::Destroy();
	}

	void MouseTester::PostEngineInitialize () const
	{
		Super::PostEngineInitialize();

		Holding1Tx = TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface/Tests/CursorPointer1.png"), TXT("Interaface/Tests/CursorPointer1"));
		Holding2Tx = TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface/Tests/CursorPointer2.png"), TXT("Interaface/Tests/CursorPointer2"));
		Holding3Tx = TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface/Tests/CursorPointer3.png"), TXT("Interaface/Tests/CursorPointer3"));
		Holding4Tx = TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("Interface/Tests/CursorPointer4.png"), TXT("Interaface/Tests/CursorPointer4"));
	}

	void MouseTester::CleanUpInvalidPointers ()
	{
		Super::CleanUpInvalidPointers();

		CLEANUP_INVALID_POINTER(Input)
		CLEANUP_INVALID_POINTER(RegionRenderer)
	}

#define HOLD_NUM_SWITCH(num) \
	if (bHolding##num && keyEvent.type == sf::Event::KeyReleased) \
	{ \
		bHolding##num = false; \
		InputEngineComponent::Get()->GetMouse()->EvaluateMouseIconOverrides(); \
	} \
	else if (!bHolding##num && keyEvent.type == sf::Event::KeyPressed) \
	{ \
		bHolding##num = true; \
		InputEngineComponent::Get()->GetMouse()->PushMouseIconOverride(Holding##num##Tx, SDFUNCTION_1PARAM(this, MouseTester, HandleMouseIcon##num##, bool, const sf::Event::MouseMoveEvent&)); \
	}

	bool MouseTester::HandleCaptureInput (const sf::Event& keyEvent)
	{
		if (!bActive)
		{
			return false;
		}

		switch (keyEvent.key.code)
		{
			case(sf::Keyboard::Num1):
				HOLD_NUM_SWITCH(1);
				return true;

			case(sf::Keyboard::Num2):
				HOLD_NUM_SWITCH(2);
				return true;

			case(sf::Keyboard::Num3):
				HOLD_NUM_SWITCH(3);
				return true;

			case(sf::Keyboard::Num4):
				HOLD_NUM_SWITCH(4);
				return true;

			case(sf::Keyboard::Escape):
				if (keyEvent.type == sf::Event::KeyReleased)
				{
					UNITTESTER_LOG(TestFlags, TXT("Escape released.  Ending Mouse Tester."));
					Destroy();
				}
				return true;
		}

		return false;
	}
#undef HOLD_NUM_SWITCH

	void MouseTester::HandleMouseMove (MousePointer* mouse, const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
	{
		if (bActive && bSettingLimitRegion && VALID_OBJECT(RegionRenderer))
		{
			RegionRenderer->FlexiblePoint = Vector2(static_cast<float>(sfmlEvent.x), static_cast<float>(sfmlEvent.y));
		}
	}

	bool MouseTester::HandleMouseClick (MousePointer* mouse, const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
	{
		if (!bActive)
		{
			return false;
		}

		if (sfmlEvent.button == sf::Mouse::Button::Left)
		{
			bSettingLimitRegion = (eventType == sf::Event::MouseButtonPressed);
			if (VALID_OBJECT(RegionRenderer) && bSettingLimitRegion)
			{
				RegionRenderer->PivotPoint = Vector2(static_cast<float>(sfmlEvent.x), static_cast<float>(sfmlEvent.y));
				RegionRenderer->FlexiblePoint = RegionRenderer->PivotPoint;
			}

			if (eventType == sf::Event::MouseButtonReleased) //Apply limit
			{
				Rectangle<INT> limitRegion;
				limitRegion.Left = Utils::Min(RegionRenderer->PivotPoint.X, RegionRenderer->FlexiblePoint.X).ToINT();
				limitRegion.Top = Utils::Min(RegionRenderer->PivotPoint.Y, RegionRenderer->FlexiblePoint.Y).ToINT();
				limitRegion.Right = Utils::Max(RegionRenderer->PivotPoint.X, RegionRenderer->FlexiblePoint.X).ToINT();
				limitRegion.Bottom = Utils::Max(RegionRenderer->PivotPoint.Y, RegionRenderer->FlexiblePoint.Y).ToINT();

				InputEngineComponent::Get()->GetMouse()->PushPositionLimit(limitRegion, SDFUNCTION_1PARAM(this, MouseTester, HandlePushLimit, bool, const sf::Event::MouseMoveEvent&));
				bClampingMouse = true;
			}

			return true;
		}

		if (sfmlEvent.button == sf::Mouse::Button::Right)
		{
			if (eventType == sf::Event::MouseButtonReleased)
			{
				bClampingMouse = false;
				InputEngineComponent::Get()->GetMouse()->EvaluateClampedMousePointer();
				if (VALID_OBJECT(RegionRenderer))
				{
					//Stop rendering rectangle
					RegionRenderer->PivotPoint = Vector2(0,0);
					RegionRenderer->FlexiblePoint = Vector2(0,0);
				}
			}

			return true;
		}

		return false;
	}

	bool MouseTester::HandleMouseIcon1 (const sf::Event::MouseMoveEvent& sfEvent)
	{
		return bHolding1;
	}

	bool MouseTester::HandleMouseIcon2 (const sf::Event::MouseMoveEvent& sfEvent)
	{
		return bHolding2;
	}

	bool MouseTester::HandleMouseIcon3 (const sf::Event::MouseMoveEvent& sfEvent)
	{
		return bHolding3;
	}

	bool MouseTester::HandleMouseIcon4 (const sf::Event::MouseMoveEvent& sfEvent)
	{
		return bHolding4;
	}

	bool MouseTester::HandlePushLimit (const sf::Event::MouseMoveEvent& sfEvent)
	{
		return bClampingMouse;
	}

}

#endif
#endif