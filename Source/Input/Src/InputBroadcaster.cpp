/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  InputBroadcaster.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "InputClasses.h"

#if INCLUDE_INPUT

using namespace std;

namespace SD
{
	bool InputBroadcaster::bCtrlHeld = false;
	bool InputBroadcaster::bShiftHeld = false;
	bool InputBroadcaster::bAltHeld = false;

	IMPLEMENT_CLASS(InputBroadcaster, Object)

	void InputBroadcaster::InitProps ()
	{
		Super::InitProps();

		bDebugKeyEvent = false;
		bDebugTextEvent = false;
		bDebugMouseMove = false;
		bDebugMouseButton = false;
		bDebugMouseWheel = false;

		InputSource = nullptr;
		AssociatedMouse = nullptr;
	}

	void InputBroadcaster::BeginObject ()
	{
		Super::BeginObject();

		if (InputEngineComponent::Get() != nullptr)
		{
			InputEngineComponent::Get()->RegisterInputBroadcaster(this);
		}
	}

	void InputBroadcaster::Destroy ()
	{
		if (InputEngineComponent::Get() != nullptr)
		{
			InputEngineComponent::Get()->UnregisterInputBroadcaster(this);
		}

		SetInputSource(nullptr);

		Super::Destroy();
	}

	void InputBroadcaster::CleanUpInvalidPointers ()
	{
		Super::CleanUpInvalidPointers();

		CLEANUP_INVALID_POINTER(InputSource)
		CLEANUP_INVALID_POINTER(AssociatedMouse)

		for (unsigned int i = 0; i < RegisteredInputComponents.size(); i++)
		{
			if (RegisteredInputComponents.at(i).Component == nullptr || RegisteredInputComponents.at(i).Component->GetPendingDelete())
			{
				RegisteredInputComponents.erase(RegisteredInputComponents.begin() + i);
				i--;
			}
		}

		for (unsigned int i = 0; i < PendingRegisterComponents.size(); i++)
		{
			if (PendingRegisterComponents.at(i).Component == nullptr || PendingRegisterComponents.at(i).Component->GetPendingDelete())
			{
				PendingRegisterComponents.erase(PendingRegisterComponents.begin() + i);
				i--;
			}
		}

		CLEANUP_INVALID_POINTER_ARRAY(PendingUnregisterComponents)
	}

	void InputBroadcaster::SetInputSource (Window* targetWindow)
	{
		if (InputSource != nullptr)
		{
			InputSource->UnregisterPollingDelegate(SDFUNCTION_1PARAM(this, InputBroadcaster, HandleWindowEvent, void, const sf::Event));
			InputSource = nullptr;
		}

		if (targetWindow != nullptr)
		{
			targetWindow->RegisterPollingDelegate(SDFUNCTION_1PARAM(this, InputBroadcaster, HandleWindowEvent, void, const sf::Event));
			InputSource = targetWindow;
		}
	}

	void InputBroadcaster::AddInputComponent (InputComponent* newComponent, INT priority)
	{
		if (newComponent->PendingInputMessenger != nullptr && newComponent->PendingInputMessenger != this)
		{
			//Notify the other broadcaster not to include add this component to avoid having this broadcaster from overriding our new changes.
			InputBroadcaster* otherBroadcaster = newComponent->PendingInputMessenger;
			for (unsigned int i = 0; i < otherBroadcaster->PendingRegisterComponents.size(); i++)
			{
				if (otherBroadcaster->PendingRegisterComponents.at(i).Component == newComponent)
				{
					otherBroadcaster->PendingRegisterComponents.erase(otherBroadcaster->PendingRegisterComponents.begin() + i);
					break;
				}
			}

			newComponent->PendingInputMessenger = nullptr;
		}

		if (bLockedRegisteredComponents)
		{
			//See if this component is already pending
			for (unsigned int i = 0; i < PendingRegisterComponents.size(); i++)
			{
				if (PendingRegisterComponents.at(i).Component == newComponent)
				{
					PendingRegisterComponents.at(i).Priority = priority;
					return; //already pending
				}
			}

			PendingRegisterComponents.push_back(SInputPriorityMapping(newComponent, priority));
			newComponent->PendingInputMessenger = this;
			return;
		}

		if (newComponent->InputMessenger == this)
		{
			return; //already registered
		}
		
		if (newComponent->InputMessenger != nullptr)
		{
			//LOG2(LOG_INPUT, TXT("Unregistering %s from %s since an InputComponent may only have a single input source."), newComponent->ToString(), newComponent->InputMessenger->ToString());
			newComponent->InputMessenger->RemoveInputComponent(newComponent);
		}

		SetInputMessengerFor(newComponent);

		unsigned int i = 0;
		while (i < RegisteredInputComponents.size())
		{
			if (RegisteredInputComponents.at(i).Priority < priority)
			{
				break;
			}

			i++;
		}

		newComponent->InputPriority = priority;
		RegisteredInputComponents.insert(RegisteredInputComponents.begin() + i, SInputPriorityMapping(newComponent, priority));
	}

	bool InputBroadcaster::SetInputPriority (InputComponent* targetComponent, INT newPriority)
	{
		RemoveInputComponent(targetComponent);
		AddInputComponent(targetComponent, newPriority);

		return true;
	}

	bool InputBroadcaster::RemoveInputComponent (InputComponent* target)
	{
		if (bLockedRegisteredComponents)
		{
			//See if target is already pending removal
			for (InputComponent* curPendingComp : PendingUnregisterComponents)
			{
				if (curPendingComp == target)
				{
					return false; //Already pending removal
				}
			}

			PendingUnregisterComponents.push_back(target);
			return false;
		}

		for (unsigned int i = 0; i < RegisteredInputComponents.size(); i++)
		{
			if (RegisteredInputComponents.at(i).Component == target)
			{
				RegisteredInputComponents.erase(RegisteredInputComponents.begin() + i);
				ClearInputMessengerFor(target);
				return true;
			}
		}

		return false;
	}

	void InputBroadcaster::SetAssociatedMouse (MousePointer* newAssociatedMouse)
	{
		AssociatedMouse = newAssociatedMouse;
	}

#ifdef DEBUG_MODE
	void InputBroadcaster::LogRegisteredInputComponents () const
	{
		LOG1(LOG_DEBUG, TXT("-== Listing registered input components for %s ==-"), ToString());
		LOG(LOG_DEBUG, TXT("    [ArrayIdx]:  ObjectName (Priority Value)"));

		for (unsigned int i = 0; i < RegisteredInputComponents.size(); i++)
		{
			LOG3(LOG_DEBUG, TXT("    [%s]:  %s (%s)"), DString(i), RegisteredInputComponents.at(i).Component->ToString(), RegisteredInputComponents.at(i).Priority);
		}

		LOG(LOG_DEBUG, TXT("-== End debug list of input components ==-"));
	}
#endif

	MousePointer* InputBroadcaster::GetAssociatedMouse () const
	{
		return AssociatedMouse;
	}

	bool InputBroadcaster::GetCtrlHeld ()
	{
		return bCtrlHeld;
	}

	bool InputBroadcaster::GetShiftHeld ()
	{
		return bShiftHeld;
	}

	bool InputBroadcaster::GetAltHeld ()
	{
		return bAltHeld;
	}

	Window* InputBroadcaster::GetInputSource () const
	{
		return InputSource;
	}

	void InputBroadcaster::SetLockedRegisteredComponents (bool bNewLockedRegisteredComponents)
	{
		bLockedRegisteredComponents = bNewLockedRegisteredComponents;

		if (bLockedRegisteredComponents)
		{
			return;
		}

		//Execute all pending input components
		for (InputComponent* curInput : PendingUnregisterComponents)
		{
			RemoveInputComponent(curInput);
		}
		PendingUnregisterComponents.clear();

		for (SInputPriorityMapping curPriorityMapping : PendingRegisterComponents)
		{
			AddInputComponent(curPriorityMapping.Component, curPriorityMapping.Priority);
			curPriorityMapping.Component->PendingInputMessenger = nullptr;
		}
		PendingRegisterComponents.clear();
	}

	void InputBroadcaster::NewKeyEvent (sf::Event newEvent)
	{
		if (bDebugKeyEvent)
		{
			LOG2(LOG_INPUT, TXT("%s received new key event (%s)"), ToString(), KeyEventToString(newEvent));
		}

		bCtrlHeld = newEvent.key.control;
		bShiftHeld = newEvent.key.shift;
		bAltHeld = newEvent.key.alt;

		SetLockedRegisteredComponents(true);
		for (unsigned int i = 0; i < RegisteredInputComponents.size(); i++)
		{
			if (RegisteredInputComponents.at(i).Component->CaptureInput(newEvent))
			{
				if (bDebugKeyEvent)
				{
					LOG3(LOG_INPUT, TXT("    %s (Handler: %s) captured key event (%s)"), RegisteredInputComponents.at(i).Component->GetUniqueName(), RegisteredInputComponents.at(i).Component->CaptureInputDelegate, KeyEventToString(newEvent));
				}

				break;
			}
		}
		SetLockedRegisteredComponents(false);
	}

	void InputBroadcaster::NewTextEvent (sf::Event newEvent)
	{
		if (bDebugTextEvent)
		{
			LOG2(LOG_INPUT, TXT("%s received new text event \"%s\""), ToString(), TextEventToString(newEvent));
		}

		SetLockedRegisteredComponents(true);
		for (unsigned int i = 0; i < RegisteredInputComponents.size(); i++)
		{
			if (RegisteredInputComponents.at(i).Component->CaptureText(newEvent))
			{
				if (bDebugTextEvent)
				{
					LOG3(LOG_INPUT, TXT("    %s (Handler: %s) captured text event \"%s\""), RegisteredInputComponents.at(i).Component->GetUniqueName(), RegisteredInputComponents.at(i).Component->CaptureTextDelegate, TextEventToString(newEvent));
				}

				break;
			}
		}
		SetLockedRegisteredComponents(false);
	}

	void InputBroadcaster::NewMouseMoveEvent (sf::Event::MouseMoveEvent sfmlEvent)
	{
		if (!VALID_OBJECT(AssociatedMouse))
		{
			LOG1(LOG_WARNING, TXT("Failed to broadcast mouse move event since an associated mouse is not bound to %s"), GetUniqueName());
			return;
		}

		Vector2 deltaMove((float)sfmlEvent.x - OldMousePosition.X, (float)sfmlEvent.y - OldMousePosition.Y);
		OldMousePosition = Vector2(static_cast<float>(sfmlEvent.x), static_cast<float>(sfmlEvent.y));

		if (bDebugMouseMove)
		{
			LOG3(LOG_INPUT, TXT("%s received new mouse move event %s.   Delta mouse move is:  %s"), ToString(), MouseMoveEventToString(sfmlEvent), deltaMove);
		}

		SetLockedRegisteredComponents(true);
		for (unsigned int i = 0; i < RegisteredInputComponents.size(); i++)
		{
			RegisteredInputComponents.at(i).Component->MouseMove(AssociatedMouse, sfmlEvent, deltaMove);
		}
		SetLockedRegisteredComponents(false);
	}

	void InputBroadcaster::NewMouseButtonEvent (sf::Event::MouseButtonEvent sfmlEvent, sf::Event::EventType eventType)
	{
		if (!VALID_OBJECT(AssociatedMouse))
		{
			LOG1(LOG_WARNING, TXT("Failed to broadcast mouse button event since an associated mouse is not bound to %s"), GetUniqueName());
			return;
		}

		if (bDebugMouseButton)
		{
			LOG2(LOG_INPUT, TXT("%s received new mouse button event (%s)"), ToString(), MouseButtonEventToString(sfmlEvent));
		}

		SetLockedRegisteredComponents(true);
		for (unsigned int i = 0; i < RegisteredInputComponents.size(); i++)
		{
			if (RegisteredInputComponents.at(i).Component->MouseClick(AssociatedMouse, sfmlEvent, eventType))
			{
				if (bDebugMouseButton)
				{
					LOG3(LOG_INPUT, TXT("    %s (Handler: %s) captured mouse button event (%s)"), RegisteredInputComponents.at(i).Component->GetUniqueName(), RegisteredInputComponents.at(i).Component->MouseClickDelegate, MouseButtonEventToString(sfmlEvent));
				}

				break;
			}
		}
		SetLockedRegisteredComponents(false);
	}

	void InputBroadcaster::NewMouseWheelEvent (sf::Event::MouseWheelScrollEvent sfmlEvent)
	{
		if (!VALID_OBJECT(AssociatedMouse))
		{
			LOG1(LOG_WARNING, TXT("Failed to broadcast mouse wheel event since an associated mouse is not bound to %s"), GetUniqueName());
			return;
		}

		if (bDebugMouseWheel)
		{
			LOG2(LOG_INPUT, TXT("%s received new mouse wheel event (%s)"), ToString(), MouseWheelEventToString(sfmlEvent));
		}

		SetLockedRegisteredComponents(true);
		for (unsigned int i = 0; i < RegisteredInputComponents.size(); i++)
		{
			if (RegisteredInputComponents.at(i).Component->MouseWheelScroll(AssociatedMouse, sfmlEvent))
			{
				if (bDebugMouseWheel)
				{
					LOG3(LOG_INPUT, TXT("    %s (Handler: %s) captured mouse wheel event (%s)"), RegisteredInputComponents.at(i).Component->ToString(), RegisteredInputComponents.at(i).Component->MouseWheelScrollDelegate, MouseWheelEventToString(sfmlEvent));
				}

				break;
			}
		}
		SetLockedRegisteredComponents(false);
	}

	DString InputBroadcaster::KeyEventToString (sf::Event sfEvent) const
	{
		DString keyType = TXT("Unknown");
		switch (sfEvent.type)
		{
			case(sf::Event::EventType::KeyPressed):
				keyType = TXT("Key Pressed");
				break;
			case(sf::Event::EventType::KeyReleased):
				keyType = TXT("Key Released");
				break;

			default:
				return TXT("Not a Key Event");
		}

		return DString::Format(TXT("Type:  %s | Key code:  %s"), {keyType.ToCString(), INT(sfEvent.key.code).ToString().ToCString()});
	}

	DString InputBroadcaster::TextEventToString (sf::Event sfEvent) const
	{
		if (sfEvent.type != sf::Event::TextEntered)
		{
			return TXT("Not a Text Event");
		}

		return DString(sf::String(sfEvent.text.unicode));
	}

	DString InputBroadcaster::MouseMoveEventToString (sf::Event::MouseMoveEvent sfEvent) const
	{
		return DString::Format(TXT("(%s,%s)"), {DString(sfEvent.x).ToCString(), DString(sfEvent.y).ToCString()});
	}

	DString InputBroadcaster::MouseButtonEventToString (sf::Event::MouseButtonEvent sfEvent) const
	{
		DString buttonName = TXT("Unknown mouse button");
		switch (sfEvent.button)
		{
			case(sf::Mouse::Button::Left):
				buttonName = TXT("LMouse");
				break;
			case(sf::Mouse::Button::Middle):
				buttonName = TXT("MMouse");
				break;
			case(sf::Mouse::Button::Right):
				buttonName = TXT("RMouse");
				break;
			case(sf::Mouse::Button::XButton1):
				buttonName = TXT("FirstExtraMouseButton");
				break;
			case(sf::Mouse::Button::XButton2):
				buttonName = TXT("SecExtraMouseButton");
				break;
		}

		return buttonName;
	}

	DString InputBroadcaster::MouseWheelEventToString (sf::Event::MouseWheelScrollEvent sfEvent) const
	{
		if (sfEvent.delta > 0)
		{
			return DString::Format(TXT("Mouse wheel up %s"), {DString(sfEvent.delta).ToCString()});
		}
		else
		{
			return DString::Format(TXT("Mouse wheel down %s"), {DString(sfEvent.delta * -1).ToCString()});
		}
	}

	void InputBroadcaster::SetInputMessengerFor (InputComponent* target)
	{
		target->InputMessenger = this;
	}

	void InputBroadcaster::ClearInputMessengerFor (InputComponent* target)
	{
		target->InputMessenger = nullptr;
	}

	void InputBroadcaster::HandleWindowEvent (const sf::Event newEvent)
	{
		switch (newEvent.type)
		{
			case(sf::Event::KeyPressed) :
			case(sf::Event::KeyReleased) :
				NewKeyEvent(newEvent);
				break;

			case(sf::Event::TextEntered) :
				NewTextEvent(newEvent);
				break;

			case(sf::Event::MouseMoved) :
				NewMouseMoveEvent(newEvent.mouseMove);
				break;

			case(sf::Event::MouseButtonReleased) :
			case(sf::Event::MouseButtonPressed) :
				NewMouseButtonEvent(newEvent.mouseButton, newEvent.type);
				break;

			case(sf::Event::MouseWheelScrolled) :
				NewMouseWheelEvent(newEvent.mouseWheelScroll);
				break;
		}
	}
}

#endif