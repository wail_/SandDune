/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GUIEditorEngineComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GUIEditorClasses.h"

#if INCLUDE_GUIEDITOR

using namespace std;

namespace SD
{
	IMPLEMENT_ENGINE_COMPONENT(GUIEditorEngineComponent)

	GUIEditorEngineComponent::GUIEditorEngineComponent () : Super()
	{
		bTickingComponent = true;

		EditorLauncher = nullptr;
		EditorWindow = nullptr;
		EditorRenderTarget = nullptr;
		EditorBroadcaster = nullptr;
		EditorFrameRate = 0.034f; //~30 frames per second

		TimeRemaining = EditorFrameRate;
	}

	void GUIEditorEngineComponent::PostInitializeComponent ()
	{
		Super::PostInitializeComponent();

		//Create an entity may launch the editor sequence.
		EditorLauncher = GUIEditorContainer::CreateObject();

		//Setup OnDestroy callbacks in case something else destroyed that entity before this engine component destroyed it.
		EditorLauncher->OnDestroy = SDFUNCTION(this, GUIEditorEngineComponent, HandleEditorLauncherDestroyed, void);
	}

	void GUIEditorEngineComponent::PreTick (FLOAT deltaSec)
	{
		Super::PreTick(deltaSec);

		if (VALID_OBJECT(EditorWindow))
		{
			EditorWindow->PollWindowEvents();
		}

		if (VALID_OBJECT(EditorWindow)) //Double check is necessary since it's possible for PollWindowEvents to delete window
		{
			EditorWindow->Resource->clear(sf::Color::Black);
		}
	}

	void GUIEditorEngineComponent::PostTick (FLOAT deltaSec)
	{
		Super::PostTick(deltaSec);

		if (VALID_OBJECT(EditorWindow))
		{
			TimeRemaining  -= deltaSec;
			if (TimeRemaining <= 0)
			{
				EditorWindow->Resource->display();
				TimeRemaining = EditorFrameRate;
			}
		}
	}

	void GUIEditorEngineComponent::ShutdownComponent ()
	{
		DestroyEditorWindow();

		if (VALID_OBJECT(EditorBroadcaster))
		{
			EditorBroadcaster->Destroy();
			EditorBroadcaster = nullptr;
		}

		if (VALID_OBJECT(EditorLauncher))
		{
			EditorLauncher->OnDestroy = nullptr;
			EditorLauncher->Destroy();
			EditorLauncher = nullptr;
		}

		Super::ShutdownComponent();
	}

	void GUIEditorEngineComponent::InitializeEditorWindow ()
	{
		if (VALID_OBJECT(EditorWindow))
		{
			return; //already initialized
		}

		EditorWindow = InputWindow::CreateObject();
		EditorWindow->Resource = new sf::RenderWindow(sf::VideoMode(800, 600), GUIEDITOR_TITLE);
		EditorWindow->Resource->setMouseCursorVisible(false);
		EditorWindow->RegisterPollingDelegate(SDFUNCTION_1PARAM(this, GUIEditorEngineComponent, HandleWindowEvent, void, const sf::Event));

		//Instantiate render target that'll draw to this window
		EditorRenderTarget = RenderTarget::CreateObject();
		EditorRenderTarget->SetFrameRate(EditorFrameRate);
		EditorRenderTarget->SetRenderWindow(EditorWindow);

		TimeRemaining = EditorFrameRate;
	}

	void GUIEditorEngineComponent::DestroyEditorWindow ()
	{
		if (VALID_OBJECT(EditorBroadcaster))
		{
			EditorBroadcaster->Destroy();
			EditorBroadcaster = nullptr;
		}

		if (VALID_OBJECT(EditorWindow))
		{
			EditorWindow->Destroy();
			EditorWindow = nullptr;
		}

		if (VALID_OBJECT(EditorRenderTarget))
		{
			EditorRenderTarget->Destroy();
			EditorRenderTarget = nullptr;
		}
	}

	Window* GUIEditorEngineComponent::GetEditorWindow () const
	{
		return EditorWindow;
	}

	InputBroadcaster* GUIEditorEngineComponent::GetEditorBroadcaster () const
	{
		return EditorBroadcaster;
	}

	void GUIEditorEngineComponent::HandleEditorLauncherDestroyed ()
	{
		EditorLauncher = nullptr;
	}

	void GUIEditorEngineComponent::HandleWindowEvent (const sf::Event newEvent)
	{
		if (newEvent.type == sf::Event::Closed)
		{
			DestroyEditorWindow();
		}
	}
}

#endif