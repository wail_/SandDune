/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GUIEditorContainer.h
  An entity that interfaces user input and game events with the GUI Editor.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef GUIEDITORCONTAINER_H
#define GUIEDITORCONTAINER_H

#include "GUIEditor.h"

#if INCLUDE_BASEEDITOR

namespace SD
{
	class GUIEditorContainer : public Entity
	{
		DECLARE_CLASS(GUIEditorContainer)


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* Callback to invoke when this entity is destroyed. */
		std::function<void()> OnDestroy;
		//SDFunction<void> OnDestroy; //TODO:  Figure out how I can set OnDestroy=nullptr

	protected:
		/* Root GUI component that handles all editor functionality. */
		GUIComponent* Editor;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual void BeginObject () override;
		virtual void Destroy () override;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  Launches window handle, and initializes GUIEditor components to render to that window.
		 */
		virtual void ActivateEditor ();


		/*
		=====================
		  Event Handlers
		=====================
		*/

	protected:
		virtual bool HandleKeyInput (const sf::Event& newKeyEvent);
	};
}

#endif
#endif