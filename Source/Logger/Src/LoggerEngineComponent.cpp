/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  LoggerEngineComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "LoggerClasses.h"

#if INCLUDE_LOGGER

using namespace std;

namespace SD
{
	IMPLEMENT_ENGINE_COMPONENT(LoggerEngineComponent)

	LoggerEngineComponent::LoggerEngineComponent () : Super()
	{
		BaseLogName = PROJECT_NAME;
		LogFile = nullptr;
		LogLifeSpan = -1; //Default to negative in case the config file was not opened, and doesn't cause archived logs to be deleted.
		TimestampFormat = TXT("%name-Backup %month-%day-%year %hour.%minute.%second");
		LogHeaderFormat = TXT("[%type]:  ");
	}

	void LoggerEngineComponent::InitializeComponent ()
	{
		Super::InitializeComponent();

		//Retrieve config files
		ConfigWriter* configWriter = ConfigWriter::CreateObject();
		if (VALID_OBJECT(configWriter))
		{
			configWriter->OpenFile(CONFIG_LOCATION + DString(TXT("Logger.ini")), false);
			if (!configWriter->IsFileOpened())
			{
				LOG(LOG_WARNING, TXT("Unable to open configuration file for LoggerEngineComponent.  Using default values."));
			}
			else
			{
				LogLifeSpan = configWriter->GetProperty<INT>(TXT("LoggerEngineComponent"), TXT("LogLifeSpan"));
				TimestampFormat = configWriter->GetPropertyText(TXT("LoggerEngineComponent"), TXT("TimestampFormat"));
				LogHeaderFormat = configWriter->GetPropertyText(TXT("LoggerEngineComponent"), TXT("LogHeaderFormat"));
			}

			configWriter->Destroy();
		}

		PurgeOldLogFiles();
		SetupLogWriter();
	}

	void LoggerEngineComponent::RecordLog (DString& outFullMessage, const DString& logType)
	{
		FormatLogHeader(outFullMessage, logType);

		//Record to file writer
		if (VALID_OBJECT(LogFile))
		{
			LogFile->AddTextEntry(outFullMessage);
		}
		else
		{
			//Push log message to vector to be later recorded whenever LogFile was initialized.
			PendingLogMsgs.push_back(outFullMessage);
		}
	}

	void LoggerEngineComponent::ShutdownComponent ()
	{
		if (VALID_OBJECT(LogFile))
		{
			//Bid the user good well before departing. . .
			LOG(LOG_LOGGER, TXT("Engine is properly shutting down.  Closing log file - Good Bye!"));

			//Write out the remaining buffer to the file
			LogFile->WriteToFile(true);
			LogFile->Destroy();
			LogFile = nullptr;
			PreserveLogFile();
		}

		Super::ShutdownComponent();
	}

	void LoggerEngineComponent::PurgeOldLogFiles ()
	{
		if (LogLifeSpan <= 0)
		{
			return; //Files don't expire
		}

		DateTime today;
		today.SetToCurrentTime();
		INT todayDays = today.CalculateTotalDays().ToINT();

		vector<PrimitiveFileAttributes> pendingDeleteFiles;
		for (FileIterator fileIter(LOG_LOCATION, FileIterator::INCLUDE_FILES); !fileIter.FinishedIterating(); fileIter++)
		{
			FileAttributes selectedAttributes(fileIter.GetSelectedAttributes());

			//Only consider .log files
			if (!selectedAttributes.GetFileExists() || selectedAttributes.GetFileExtension().Compare(TXT("log"), false) != 0)
			{
				continue;
			}

			//Can't delete read only files
			if (selectedAttributes.GetReadOnly())
			{
				continue;
			}

			if (selectedAttributes.GetLastModifiedDate().CalculateTotalDays().ToINT() + LogLifeSpan < todayDays)
			{
				//Mark for deletion.  Don't delete actually here since iterator is still referencing file.
				pendingDeleteFiles.push_back(selectedAttributes);
			}
		}

		for (unsigned int i = 0; i < pendingDeleteFiles.size(); i++)
		{
			LOG1(LOG_LOGGER, TXT("Purging old log file:  %s"), pendingDeleteFiles.at(i).GetFullFileName());
			if (!OS_DeleteFile(pendingDeleteFiles.at(i).GetPath(), pendingDeleteFiles.at(i).GetFullFileName()))
			{
				LOG1(LOG_LOGGER, TXT("Failed to purge old log file:  %s"), pendingDeleteFiles.at(i).GetFullFileName());
			}
		}
	}

	void LoggerEngineComponent::SetupLogWriter ()
	{
		LogFile = TextFileWriter::CreateObject();

		if (!VALID_OBJECT(LogFile))
		{
			//Perhaps the console could write the warning.
			LOG(LOG_WARNING, TXT("Unable to instantiate a TextFileWriter object.  The LoggerEngineComponent will be unable to write messages to the system log."));
			return;
		}

		if (!LogFile->OpenFile(LOG_LOCATION + BaseLogName + TXT(".Log"), true))
		{
			LOG2(LOG_WARNING, TXT("Unable to open:  %s%s.Log!"), BASE_DIR, BaseLogName);
			return;
		}

		for (unsigned int i = 0; i < PendingLogMsgs.size(); i++)
		{
			LogFile->AddTextEntry(PendingLogMsgs.at(i));
		}
		PendingLogMsgs.clear();
	}

	void LoggerEngineComponent::FormatLogHeader (DString& outFullMessage, const DString& logType)
	{
		DString header = LogHeaderFormat;

		header.StrReplace(TXT("%type"), logType);
		header.StrReplace(TXT("%frame"), DString(Engine::GetEngine()->GetFrameCounter()));
		header.StrReplace(TXT("%time"), Engine::GetEngine()->GetElapsedTime().ToString());

		if (header.FindSubText(TXT("%")) >= 0)
		{
			DateTime now;
			now.SetToCurrentTime();
			header = now.FormatStringPadded(header);
		}

		outFullMessage = header + outFullMessage;
	}

	void LoggerEngineComponent::PreserveLogFile ()
	{
		DString address = FileUtils::ToAbsolutePath(LOG_LOCATION);
		DString newFileName;
		GenerateFileName(newFileName);

		if (!OS_CopyFile(address + BaseLogName + TXT(".Log"), address + newFileName, true))
		{
			LOG2(LOG_WARNING, TXT("Unable to save a copy of %s.Log to %s.  The logs from this session will not be archived."), BaseLogName, newFileName);
		}
	}

	void LoggerEngineComponent::GenerateFileName (DString& outFileName)
	{
		DateTime today;
		today.SetToCurrentTime();

		outFileName = TimestampFormat;
		outFileName = today.FormatStringPadded(outFileName);
		outFileName.StrReplace(TXT("%name"), BaseLogName);
	}
}

#endif