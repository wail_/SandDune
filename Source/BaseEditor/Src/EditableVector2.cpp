/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  EditableVector2.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "BaseEditorClasses.h"

#if INCLUDE_BASEEDITOR

using namespace std;

namespace SD
{
	EditableVector2::EditableVector2 () : EditableProperty(), CollapsibleProperty()
	{
		OwningProperty = nullptr;

		PropertyLineX = nullptr;
		PropertyLineY = nullptr;

		EditableX = nullptr;
		EditableY = nullptr;
	}

	EditableVector2::EditableVector2 (const EditableVector2& copyProperty) : EditableProperty(copyProperty), CollapsibleProperty(copyProperty)
	{
		//Copied vectors will maintain their own instances of editable axis.
		PropertyLineX = nullptr;
		PropertyLineY = nullptr;

		EditableX = nullptr;
		EditableY = nullptr;
	}

	EditableVector2::~EditableVector2 ()
	{
		RemoveAxisPropertyLines();
	}

	void EditableVector2::SetOwningProperty (DProperty* newOwningProperty)
	{
		EditableProperty::SetOwningProperty(newOwningProperty);

		OwningProperty = dynamic_cast<Vector2*>(newOwningProperty);
		if (OwningProperty == nullptr)
		{
			Engine::GetEngine()->FatalError(TXT("EditableVector2s may only describe Vector2 datatypes."));
			return;
		}

		InitializeAxis();
	}

	void EditableVector2::EditVariable (DString& outNewValue)
	{
		//Cannot be invoked directly.  Instead edit the axis individually
	}

	DString EditableVector2::GetStringValue () const
	{
		return GetOwningProperty()->ToString();
	}

	DString EditableVector2::ExportVariable () const
	{
		return PropertyName + TXT("=") + GetOwningProperty()->ToString();
	}

	bool EditableVector2::OverrideMouseClickEvent (const sf::Event::MouseButtonEvent& sfEvent, sf::Event::EventType eventType, INT mouseRelevance)
	{
		//Redirect to collapsible property first
		if (CollapsibleProperty::MouseEventOverCollapsibleButton(sfEvent, eventType, mouseRelevance))
		{
			return true;
		}

		if (mouseRelevance > 1)
		{
			return true; //consume mouse event but don't actually place the text cursor over value since that region is not writable.
		}

		return EditableProperty::OverrideMouseClickEvent(sfEvent, eventType, mouseRelevance);
	}

	void EditableVector2::SetPropertyEditor (PropertyLine* newPropertyEditor)
	{
		EditableProperty::SetPropertyEditor(newPropertyEditor);

		RemoveAxisPropertyLines();
		if (newPropertyEditor == nullptr)
		{
			return;
		}

		PropertyLineX = newPropertyEditor->CreateObjectOfMatchingClass();
		if (VALID_OBJECT(PropertyLineX))
		{
			PropertyLineX->GetTransform()->SetAbsCoordinates(Vector2(0.f, newPropertyEditor->GetTransform()->GetCurrentSizeY()));
			PropertyLineX->GetTransform()->SetRelativeTo(newPropertyEditor->GetTransform());
			PropertyLineX->SetActive(bExpanded);
			PropertyLineX->OnDestroy = SDFUNCTION(this, EditableVector2, HandleXLineDestroyed, void);
			PropertyLineX->OnBeginTextEdit = SDFUNCTION_1PARAM(this, EditableVector2, HandleAxisBeginTextEdit, void, PropertyLine*);

			if (EditableX != nullptr)
			{
				PropertyLineX->BindPropertyVariable(EditableX);
			}
		}

		PropertyLineY = newPropertyEditor->CreateObjectOfMatchingClass();
		if (VALID_OBJECT(PropertyLineY))
		{
			PropertyLineY->GetTransform()->SetAbsCoordinates(Vector2(0.f, newPropertyEditor->GetTransform()->GetCurrentSizeY() * 2.f));
			PropertyLineY->GetTransform()->SetRelativeTo(newPropertyEditor->GetTransform());
			PropertyLineY->SetActive(bExpanded);
			PropertyLineY->OnDestroy = SDFUNCTION(this, EditableVector2, HandleYLineDestroyed, void);
			PropertyLineY->OnBeginTextEdit = SDFUNCTION_1PARAM(this, EditableVector2, HandleAxisBeginTextEdit, void, PropertyLine*);

			if (EditableY != nullptr)
			{
				PropertyLineY->BindPropertyVariable(EditableY);
			}
		}
	}

	void EditableVector2::SetReadOnly (bool bNewReadOnly)
	{
		EditableProperty::SetReadOnly(bNewReadOnly);
		if (EditableX != nullptr)
		{
			EditableX->SetReadOnly(bNewReadOnly);
		}

		if (EditableY != nullptr)
		{
			EditableY->SetReadOnly(bNewReadOnly);
		}
	}

	INT EditableVector2::GetMaxHeight () const
	{
		return 3; //This + X + Y
	}

	void EditableVector2::HandleRender (PropertyLineRenderComponent* propertyRenderer, const sf::Transformable& renderTransform, const Window* targetWindow, INT& outRenderFlags, FLOAT& outPropNameOffset)
	{
		EditableProperty::HandleRender(propertyRenderer, renderTransform, targetWindow, outRenderFlags, outPropNameOffset);
		outPropNameOffset = CollapsibleProperty::RenderCollapsibleButton(propertyRenderer, renderTransform, targetWindow, outRenderFlags);
	}

	void EditableVector2::ExpandProperty ()
	{
		CollapsibleProperty::ExpandProperty();
		CalculateCurrentHeight();

		if (VALID_OBJECT(PropertyLineX))
		{
			PropertyLineX->SetActive(true);
		}

		if (VALID_OBJECT(PropertyLineY))
		{
			PropertyLineY->SetActive(true);
		}
	}

	void EditableVector2::CollapseProperty ()
	{
		CollapsibleProperty::CollapseProperty();
		CalculateCurrentHeight();

		if (VALID_OBJECT(PropertyLineX))
		{
			PropertyLineX->SetActive(false);
		}

		if (VALID_OBJECT(PropertyLineY))
		{
			PropertyLineY->SetActive(false);
		}
	}

	void EditableVector2::InitializeAxis ()
	{
		if (EditableX != nullptr)
		{
			delete EditableX;
		}

		if (EditableY != nullptr)
		{
			delete EditableY;
		}

		EditableX = new EditableFLOAT();
		EditableX->SetReadOnly(GetReadOnly());
		EditableX->PropertyName = TXT("X");
		EditableX->SetOwningProperty(GetXAxis());
		EditableX->OnPrePropertyChange = SDFUNCTION_1PARAM(this, EditableVector2, HandleXPreChange, void, DString&);
		EditableX->OnPostPropertyChange = SDFUNCTION_1PARAM(this, EditableVector2, HandleXPostChange, void, DString&);
		if (VALID_OBJECT(PropertyLineX))
		{
			PropertyLineX->BindPropertyVariable(EditableX);
		}

		EditableY = new EditableFLOAT();
		EditableY->SetReadOnly(GetReadOnly());
		EditableY->PropertyName = TXT("Y");
		EditableY->SetOwningProperty(GetYAxis());
		EditableY->OnPrePropertyChange = SDFUNCTION_1PARAM(this, EditableVector2, HandleYPreChange, void, DString&);
		EditableY->OnPostPropertyChange = SDFUNCTION_1PARAM(this, EditableVector2, HandleYPostChange, void, DString&);
		if (VALID_OBJECT(PropertyLineY))
		{
			PropertyLineY->BindPropertyVariable(EditableY);
		}
	}

	void EditableVector2::RemoveAxisPropertyLines ()
	{
		if (VALID_OBJECT(PropertyLineX))
		{
			PropertyLineX->Destroy();
			PropertyLineX = nullptr;
		}

		if (VALID_OBJECT(PropertyLineY))
		{
			PropertyLineY->Destroy();
			PropertyLineY = nullptr;
		}
	}

	void EditableVector2::CalculateCurrentHeight ()
	{
		SetHeight((bExpanded) ? GetMaxHeight() : 1);
	}

	DProperty* EditableVector2::GetXAxis () const
	{
		return &OwningProperty->X;
	}

	DProperty* EditableVector2::GetYAxis () const
	{
		return &OwningProperty->Y;
	}

	DProperty* EditableVector2::GetOwningProperty () const
	{
		return OwningProperty;
	}

	void EditableVector2::HandleAxisBeginTextEdit (PropertyLine* focusedLine)
	{
		//Apply edits to the axis that's currently not focused.
		(PropertyLineX != focusedLine) ? PropertyLineX->ApplyEdits() : PropertyLineY->ApplyEdits();

		//Notify parent property line that this property just gained edit focus
		if (VALID_OBJECT(PropertyEditor))
		{
			PropertyEditor->SetTextFocus(true);
		}
	}

	void EditableVector2::HandleXPreChange (DString& outPropertyValue)
	{
		if (OnXPreChange != nullptr)
		{
			OnXPreChange(outPropertyValue);
		}

		if (OnPrePropertyChange != nullptr)
		{
			OnPrePropertyChange(GetOwningProperty()->ToString());
		}
	}

	void EditableVector2::HandleXPostChange (DString& outPropertyValue)
	{
		if (OnXPostChange != nullptr)
		{
			OnXPostChange(outPropertyValue);
		}

		DString vectorString = GetOwningProperty()->ToString();
		if (OnPostPropertyChange != nullptr)
		{
			OnPostPropertyChange(vectorString);
		}

		//Update main text field (making a change in an axis should cause the main property field to update, too).
		if (VALID_OBJECT(PropertyEditor))
		{
			PropertyEditor->SetPropertyTextValue(vectorString);	
		}
	}

	void EditableVector2::HandleYPreChange (DString& outPropertyValue)
	{
		if (OnYPreChange != nullptr)
		{
			OnYPreChange(outPropertyValue);
		}

		if (OnPrePropertyChange != nullptr)
		{
			OnPrePropertyChange(GetOwningProperty()->ToString());
		}
	}

	void EditableVector2::HandleYPostChange (DString& outPropertyValue)
	{
		if (OnYPostChange != nullptr)
		{
			OnYPostChange(outPropertyValue);
		}

		DString vectorString = GetOwningProperty()->ToString();
		if (OnPostPropertyChange != nullptr)
		{
			OnPostPropertyChange(vectorString);
		}

		//Update main text field (making a change in an axis should cause the main property field to update, too).
		if (VALID_OBJECT(PropertyEditor))
		{
			PropertyEditor->SetPropertyTextValue(vectorString);	
		}
	}

	void EditableVector2::HandleXLineDestroyed ()
	{
		PropertyLineX = nullptr;
	}

	void EditableVector2::HandleYLineDestroyed ()
	{
		PropertyLineY = nullptr;
	}
}

#endif