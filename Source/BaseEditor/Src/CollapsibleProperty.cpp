/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  CollapsibleProperty.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "BaseEditorClasses.h"

#if INCLUDE_BASEEDITOR

using namespace std;

namespace SD
{
	CollapsibleProperty::CollapsibleProperty ()
	{
		bExpanded = false;
	}

	CollapsibleProperty::CollapsibleProperty (const CollapsibleProperty& copyProperty)
	{
		bExpanded = copyProperty.bExpanded;
	}

	FLOAT CollapsibleProperty::RenderCollapsibleButton (PropertyLineRenderComponent* propertyRenderer, const sf::Transformable& renderTransform, const Window* targetWindow, INT& outRenderFlags)
	{
		Texture* buttonTexture = BaseEditorEngineComponent::Get()->CollapsibleButtonTexture;
		if (buttonTexture == nullptr)
		{
			return 0.f;
		}

		sf::Vector2u textureSize = buttonTexture->GetTextureResource()->getSize();
		Vector2 sdTextureSize = buttonTexture->GetDimensions();

		//Expected to draw checkbox as a 1x2 texture where the bottom half is the checked checkbox.
		sf::IntRect sfRect;
		sfRect.width = textureSize.x;
		sfRect.height = textureSize.y/2; //Only render one of the checkboxes
		sfRect.left = 0;

		if (bExpanded)
		{
			sfRect.top = sfRect.height; //Render bottom half for checked checkbox
		}
		else
		{
			sfRect.top = 0; //Render top half
		}

		sf::Sprite sprite(*buttonTexture->GetTextureResource());
		sprite.setTextureRect(sfRect);

		Vector2 spriteScale = Vector2(renderTransform.getScale().y, renderTransform.getScale().y);
		spriteScale /= (sdTextureSize * Vector2(1.f,0.5f)); //(1,0.5) is to take account of subdivision where only a part of the texture is drawn.
		sprite.setScale(Vector2::SDtoSFML(spriteScale));
		sprite.setPosition(sf::Vector2f(renderTransform.getPosition().x + 4.f, renderTransform.getPosition().y));
		targetWindow->Resource->draw(sprite);

		//Save button attributes to detect relevant mouse click events
		CollapsibleButtonAttributes.Left = sprite.getPosition().x;
		CollapsibleButtonAttributes.Top = sprite.getPosition().y;
		CollapsibleButtonAttributes.Right = sprite.getPosition().x + (spriteScale.X * sdTextureSize.X);
		CollapsibleButtonAttributes.Bottom = sprite.getPosition().y + (spriteScale.Y * sdTextureSize.Y * 0.5f);

		return (spriteScale.X * sdTextureSize.X) + 8.f; //Add a 8 to the result to give the button some space.
	}

	bool CollapsibleProperty::MouseEventOverCollapsibleButton (const sf::Event::MouseButtonEvent& sfEvent, sf::Event::EventType eventType, INT mouseRelevance)
	{
		if (eventType != sf::Event::MouseButtonReleased || sfEvent.button != sf::Mouse::Left)
		{
			return false;
		}

		if (CoordinatesOverCollapsibleButton(FLOAT::MakeFloat(sfEvent.x), FLOAT::MakeFloat(sfEvent.y)))
		{
			(bExpanded) ? CollapseProperty() : ExpandProperty();
			return true;
		}

		return false;
	}

	bool CollapsibleProperty::CoordinatesOverCollapsibleButton (FLOAT x, FLOAT y)
	{
		return (x <= CollapsibleButtonAttributes.Right && x >= CollapsibleButtonAttributes.Left &&
			y <= CollapsibleButtonAttributes.Bottom && y >= CollapsibleButtonAttributes.Top);
	}

	void CollapsibleProperty::ExpandProperty ()
	{
		bExpanded = true;
	}

	void CollapsibleProperty::CollapseProperty ()
	{
		bExpanded = false;
	}
}

#endif