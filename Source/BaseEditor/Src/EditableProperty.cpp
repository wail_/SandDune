/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  EditableProperty.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "BaseEditorClasses.h"

#if INCLUDE_BASEEDITOR

using namespace std;

namespace SD
{
	EditableProperty::EditableProperty () : DPropertyExtension()
	{
		bReadOnly = false;
		PropertyName = TXT("Unnamed Property");

		Height = 1;
		PropertyEditor = nullptr;
	}

	EditableProperty::EditableProperty (const EditableProperty& copyProperty) : DPropertyExtension(copyProperty)
	{
		bReadOnly = copyProperty.bReadOnly;
		PropertyName = copyProperty.PropertyName;

		Height = copyProperty.Height;
		PropertyEditor = nullptr;
	}

	EditableProperty::~EditableProperty ()
	{
	}

	bool EditableProperty::OverrideMouseClickEvent (const sf::Event::MouseButtonEvent& sfEvent, sf::Event::EventType eventType, INT mouseRelevance)
	{
		return false;
	}

	void EditableProperty::SetPropertyEditor (PropertyLine* newPropertyEditor)
	{
		PropertyEditor = newPropertyEditor;
	}

	void EditableProperty::SetReadOnly (bool bNewReadOnly)
	{
		bReadOnly = bNewReadOnly;
	}

	void EditableProperty::SetHeight (INT newHeight)
	{
		Height = newHeight;
		if (OnHeightChange != nullptr)
		{
			OnHeightChange(Height);
		}
	}

	bool EditableProperty::GetReadOnly () const
	{
		return bReadOnly;
	}

	INT EditableProperty::GetHeight () const
	{
		return Height;
	}

	INT EditableProperty::GetMaxHeight () const
	{
		return 1;
	}

	void EditableProperty::HandleRender (PropertyLineRenderComponent* propertyRenderer, const sf::Transformable& renderTransform, const Window* targetWindow, INT& outRenderFlags, FLOAT& outPropNameOffset)
	{
	
	}
}

#endif