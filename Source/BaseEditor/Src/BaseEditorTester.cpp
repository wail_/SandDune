/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  BaseEditorTester.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "BaseEditorClasses.h"

#if INCLUDE_BASEEDITOR

#ifdef DEBUG_MODE

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(BaseEditorTester, Entity)

	void BaseEditorTester::InitProps ()
	{
		Super::InitProps();

		ClampedStringLimit = 24;

		INIT_EDITABLE_PROP(BOOL, SetToTrueToClose, this, false)
		SetToTrueToCloseExtension->OnPostPropertyChange = SDFUNCTION_1PARAM(this, BaseEditorTester, HandleExitBoolChange, void, DString&);
		SetToTrueToClose = false;

		INIT_EDITABLE_PROP(INT, UnitTesterINT, this, false)
		UnitTesterINTExtension->OnPostPropertyChange = SDFUNCTION_1PARAM(this, BaseEditorTester, HandlePostINTChange, void, DString&);
		UnitTesterINT = 64;

		INIT_EDITABLE_PROP(INT, UnitTesterINTClamped, this, false)
		UnitTesterINTClampedExtension->OnPrePropertyChange = SDFUNCTION_1PARAM(this, BaseEditorTester, HandlePreINTClampedChange, void, DString&);
		UnitTesterINTClampedExtension->OnPostPropertyChange = SDFUNCTION_1PARAM(this, BaseEditorTester, HandlePostINTClampedChange, void, DString&);
		UnitTesterINTClampedExtension->bMaxBounded = true;
		UnitTesterINTClampedExtension->bMinBounded = true;
		UnitTesterINTClampedExtension->MinValue = 0;
		UnitTesterINTClampedExtension->MaxValue = 100;
		UnitTesterINTClamped = 50;

		INIT_EDITABLE_PROP(INT, UnitTesterINTReadOnly, this, true)
		UnitTesterINTReadOnlyExtension->OnPostPropertyChange = SDFUNCTION_1PARAM(this, BaseEditorTester, HandlePostINTReadOnlyChange, void, DString&);
		UnitTesterINTReadOnly = 11;

		INIT_EDITABLE_PROP(FLOAT, UnitTesterFLOAT, this, false)
		UnitTesterFLOATExtension->OnPostPropertyChange = SDFUNCTION_1PARAM(this, BaseEditorTester, HandlePostFLOATChange, void, DString&);
		UnitTesterFLOAT = 0.5f;

		INIT_EDITABLE_PROP(DString, UnitTesterString, this, false)
		UnitTesterStringExtension->OnPostPropertyChange = SDFUNCTION_1PARAM(this, BaseEditorTester, HandlePostStringChange, void, DString&);
		UnitTesterString = TXT("Unlimited string");

		INIT_EDITABLE_PROP(DString, UnitTesterClampedString, this, false)
		UnitTesterClampedStringExtension->bApplyCharLimit = true;
		UnitTesterClampedStringExtension->MaxCharacters = ClampedStringLimit;
		UnitTesterClampedStringExtension->OnPostPropertyChange = SDFUNCTION_1PARAM(this, BaseEditorTester, HandlePostClampedStringChange, void, DString&);
		UnitTesterClampedString = DString::Format(TXT("Char limit %s"), {ClampedStringLimit.ToString().ToCString()});

		INIT_EDITABLE_PROP(BOOL, UnitTesterBOOL, this, false)
		UnitTesterBOOLExtension->OnPostPropertyChange = SDFUNCTION_1PARAM(this, BaseEditorTester, HandlePostBOOLChange, void, DString&);
		UnitTesterBOOL = true;

		INIT_EDITABLE_PROP(BOOL, UnitTesterBOOLReadOnly, this, true)
		UnitTesterBOOLReadOnlyExtension->OnPostPropertyChange = SDFUNCTION_1PARAM(this, BaseEditorTester, HandlePostBOOLReadOnlyChange, void, DString&);
		UnitTesterBOOLReadOnly = true;

		INIT_EDITABLE_PROP(Vector2, UnitTesterVector2, this, false)
		UnitTesterVector2Extension->OnPostPropertyChange = SDFUNCTION_1PARAM(this, BaseEditorTester, HandlePostVector2Change, void, DString&);
		UnitTesterVector2 = Vector2(128, 256);

		INIT_EDITABLE_PROP(Vector2, UnitTesterVector2ReadOnly, this, true)
		UnitTesterVector2ReadOnlyExtension->OnPostPropertyChange = SDFUNCTION_1PARAM(this, BaseEditorTester, HandlePostVector2ReadOnlyChange, void, DString&);
		UnitTesterVector2ReadOnly = Vector2(33.5f, 46.75f);

		INIT_EDITABLE_PROP(Vector3, UnitTesterVector3, this, false)
		UnitTesterVector3Extension->OnPostPropertyChange = SDFUNCTION_1PARAM(this, BaseEditorTester, HandlePostVector3Change, void, DString&);
		UnitTesterVector3 = Vector3(512.25f, 32.75f, 1024.125f);
	}

	void BaseEditorTester::BeginObject ()
	{
		Super::BeginObject();

		InstantiatePropertyLines();
	}

	void BaseEditorTester::Destroy ()
	{
		// test commit / merge
		// for (auto& i : PropertyLines)
		// {
		//		if (VALID_OBJECT(i))
		//		{
		//			i.Destroy();
		//		}
		// }
		
		for (unsigned int i = 0; i < PropertyLines.size(); i++)
		{
			if (VALID_OBJECT(PropertyLines.at(i)))
			{
				PropertyLines.at(i)->Destroy();
			}
		}

		PropertyLines.clear();

		Super::Destroy();
	}

	void BaseEditorTester::CleanUpInvalidPointers ()
	{
		Super::CleanUpInvalidPointers();

		CLEANUP_INVALID_POINTER_ARRAY(PropertyLines)
	}

	void BaseEditorTester::InstantiatePropertyLines ()
	{
		FLOAT propertyYPos = 48;
		for (PropertyIterator<EditableProperty> iter(this); iter.GetSelectedProperty() != nullptr; iter++)
		{
			PropertyLine* testProperty = PropertyLine::CreateObject();
			testProperty->GetTransform()->SetAbsCoordinates(Vector2(400, propertyYPos));
			testProperty->BindPropertyVariable(iter.GetSelectedProperty());
			testProperty->OnBeginTextEdit = SDFUNCTION_1PARAM(this, BaseEditorTester, HandleBeginTextEdit, void, PropertyLine*);
			propertyYPos += (testProperty->GetTransform()->GetCurrentSizeY() * iter.GetSelectedProperty()->GetMaxHeight().ToFLOAT());
			PropertyLines.push_back(testProperty);
		}
	}

	void BaseEditorTester::HandleBeginTextEdit (PropertyLine* focusedLine)
	{
		//Unfocus text edit from all other property lines since the focused line is now capturing input
		for (unsigned int i = 0; i < PropertyLines.size(); i++)
		{
			if (PropertyLines.at(i) != focusedLine)
			{
				PropertyLines.at(i)->ApplyEdits();
			}
		}
	}

	void BaseEditorTester::HandleExitBoolChange (DString& outPropertyValue)
	{
		if (SetToTrueToClose)
		{
			UNITTESTER_LOG(TestFlags, TXT("BaseEditorTester's exit bool was set to true.  Ending test."));
			Destroy();
		}
	}

	void BaseEditorTester::HandlePostINTChange (DString& outPropertyValue)
	{
		UNITTESTER_LOG1(TestFlags, TXT("BaseEditorTester::UnitTesterINT is now equal to %s"), UnitTesterINT);
	}

	void BaseEditorTester::HandlePreINTClampedChange (DString& outProperyValue)
	{
		UNITTESTER_LOG1(TestFlags, TXT("BaseEditorTester::UnitTesterINTClamped PreChange... value was set to %s"), UnitTesterINTClamped);
	}

	void BaseEditorTester::HandlePostINTClampedChange (DString& outProperyValue)
	{
		UNITTESTER_LOG1(TestFlags, TXT("BaseEditorTester::UnitTesterINTClamped is now equal to %s"), UnitTesterINTClamped);
	}

	void BaseEditorTester::HandlePostINTReadOnlyChange (DString& outProperyValue)
	{
		Engine::GetEngine()->FatalError(TXT("Base Editor Test failed.  Was able to make edits to a read only variable."));
	}

	void BaseEditorTester::HandlePostFLOATChange (DString& outProperyValue)
	{
		UNITTESTER_LOG1(TestFlags, TXT("BaseEditorTester::UnitTesterFLOAT is now equal to %s"), UnitTesterFLOAT);
	}

	void BaseEditorTester::HandlePostStringChange (DString& outPropertyValue)
	{
		UNITTESTER_LOG1(TestFlags, TXT("BaseEditorTester::UnitTesterString is now equal to \"%s\""), UnitTesterString);
	}

	void BaseEditorTester::HandlePostClampedStringChange (DString& outPropertyValue)
	{
		UNITTESTER_LOG1(TestFlags, TXT("BaseEditorTester::UnitTesterClampedString is now equal to \"%s\""), UnitTesterClampedString);
		if (UnitTesterClampedString.Length() > ClampedStringLimit)
		{
			Engine::GetEngine()->FatalError(TXT("Base Editor Test failed.  The clamped string was able to achieve a character length greater than its limit."));
		}
	}

	void BaseEditorTester::HandlePostBOOLChange (DString& outPropertyValue)
	{
		UNITTESTER_LOG1(TestFlags, TXT("BaseEditorTester::UnitTesterBOOL is now %s"), UnitTesterBOOL);
	}

	void BaseEditorTester::HandlePostBOOLReadOnlyChange (DString& outPropertyValue)
	{
		Engine::GetEngine()->FatalError(TXT("Base Editor Test failed.  The user was able to change a read only flag."));
	}

	void BaseEditorTester::HandlePostVector2Change (DString& outPropertyValue)
	{
		UNITTESTER_LOG1(TestFlags, TXT("BaseEditorTester::UnitTesterVector2 is now %s"), UnitTesterVector2);
	}

	void BaseEditorTester::HandlePostVector2ReadOnlyChange (DString& outPropertyValue)
	{
		Engine::GetEngine()->FatalError(TXT("Base Editor Test failed.  The user was able to change values for a read only vector."));
	}

	void BaseEditorTester::HandlePostVector3Change (DString& outPropertyValue)
	{
		UNITTESTER_LOG1(TestFlags, TXT("BaseEditorTester::UnitTesterVector3 is now %s"), UnitTesterVector3);
	}
}

#endif
#endif