/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  BaseEditorTester.h
  Entity that instantiates various PropertyLine entities that are used to test editing member variables.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef BASEEDITORTESTER_H
#define BASEEDITORTESTER_H

#include "BaseEditor.h"

#if INCLUDE_BASEEDITOR

#ifdef DEBUG_MODE

namespace SD
{
	class PropertyLine;

	class BaseEditorTester : public Entity
	{
		DECLARE_CLASS(BaseEditorTester)


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		UnitTester::EUnitTestFlags TestFlags;

	protected:
		/* List of PropertyLines instantiated from test. */
		std::vector<PropertyLine*> PropertyLines;
	
		/* For the character limit string test, this is the max num characters the string length may achieve. */
		INT ClampedStringLimit;

		/* Various editable variables a PropertLine entity will be testing. */
		BOOL SetToTrueToClose;
		INT UnitTesterINT;
		INT UnitTesterINTClamped;
		INT UnitTesterINTReadOnly;
		FLOAT UnitTesterFLOAT;
		DString UnitTesterString;
		DString UnitTesterClampedString;
		BOOL UnitTesterBOOL;
		BOOL UnitTesterBOOLReadOnly;
		Vector2 UnitTesterVector2;
		Vector2 UnitTesterVector2ReadOnly;
		Vector3 UnitTesterVector3;

	
		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual void BeginObject () override;
		virtual void Destroy () override;

	protected:
		virtual void CleanUpInvalidPointers () override;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  Creates and initializes a property line for each editable member variable for this class.
		 */
		virtual void InstantiatePropertyLines ();


		/*
		=====================
		  Event Handlers
		=====================
		*/

	protected:
		/**
		  Called whenever any of its PropertyLines gained text edit focus.
		 */
		virtual void HandleBeginTextEdit (PropertyLine* focusedLine);

		/**
		  Various notifications whenever the property line adjusted the value of its variable.
		 */
		virtual void HandleExitBoolChange (DString& outPropertyValue);
		virtual void HandlePostINTChange (DString& outProperyValue);
		virtual void HandlePreINTClampedChange (DString& outProperyValue);
		virtual void HandlePostINTClampedChange (DString& outProperyValue);
		virtual void HandlePostINTReadOnlyChange (DString& outProperyValue);
		virtual void HandlePostFLOATChange (DString& outProperyValue);
		virtual void HandlePostStringChange (DString& outPropertyValue);
		virtual void HandlePostClampedStringChange (DString& outPropertyValue);
		virtual void HandlePostBOOLChange (DString& outPropertyValue);
		virtual void HandlePostBOOLReadOnlyChange (DString& outPropertyValue);
		virtual void HandlePostVector2Change (DString& outPropertyValue);
		virtual void HandlePostVector2ReadOnlyChange (DString& outPropertyValue);
		virtual void HandlePostVector3Change (DString& outPropertyValue);
	};
}

#endif
#endif
#endif