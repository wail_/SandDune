/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  BaseEditor.h
  Contains important file includes and definitions for the Base Editor module.

  The Base Editor module defines essential utilities all editors (such as map, GUI, and particle
  editors) will be using.  This editor will define the following:
  
	-Meta data for properties (such as clamping, tooltips, and rendering).
	-Maintain object variables so that properties may be iterated.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef BASEEDITOR_H
#define BASEEDITOR_H

#include "Configuration.h"
#if INCLUDE_BASEEDITOR

#if !(INCLUDE_CORE)
#error The Base Editor module requires the Core module.  Please enable INCLUDE_CORE in Configuration.h.
#endif

#if !(INCLUDE_GRAPHICS)
#error The Base Editor module requires the Graphics module.  Please enable INCLUDE_GRAPHICS in Configuration.h.
#endif

#if !(INCLUDE_INPUT)
#error The Base Editor module requires the Input module.  Please enable INCLUDE_INPUT in Configuration.h.
#endif

#include "CoreClasses.h"
#include "GraphicsClasses.h"
#include "InputClasses.h"

#define LOG_BASEEDITOR "Base Editor"

#endif
#endif