/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GUIComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GUIClasses.h"

#if INCLUDE_GUI

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(GUIComponent, EntityComponent)

	void GUIComponent::HandleMouseMove (const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
	{
		for (unsigned int i = 0; i < ForcedMouseEventsComponents.size(); i++)
		{
			ForcedMouseEventsComponents.at(i)->HandleMouseMove(sfmlEvent, deltaMove);
		}

		if (!AcceptsMouseEvents(sfmlEvent.x, sfmlEvent.y))
		{
			return;
		}

		ExecuteMouseMove(sfmlEvent, deltaMove);

		for (unsigned int i = 0; i < Components.size(); i++)
		{
			if (VALID_OBJECT_CAST(Components.at(i), GUIComponent*))
			{
				dynamic_cast<GUIComponent*>(Components.at(i))->HandleMouseMove(sfmlEvent, deltaMove);
			}
		}
	}

	bool GUIComponent::HandleMouseClick (const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
	{
		//Handle forced events first
		for (unsigned int i = 0; i < ForcedMouseEventsComponents.size(); i++)
		{
			ForcedMouseEventsComponents.at(i)->HandleMouseClick(sfmlEvent, eventType);
		}

		if (!AcceptsMouseEvents(sfmlEvent.x, sfmlEvent.y))
		{
			return false;
		}

		//Invoke children before parent since the children are typically drawn over the parent.
		for (unsigned int i = 0; i < Components.size(); i++)
		{
			if (VALID_OBJECT_CAST(Components.at(i), GUIComponent*))
			{
				if (dynamic_cast<GUIComponent*>(Components.at(i))->HandleMouseClick(sfmlEvent, eventType))
				{
					return true;
				}
			}
		}

		return (ExecuteMouseClick(sfmlEvent, eventType));
	}

	bool GUIComponent::HandleMouseWheelMove (const sf::Event::MouseWheelScrollEvent& sfmlEvent)
	{
		for (unsigned int i = 0; i < ForcedMouseEventsComponents.size(); i++)
		{
			ForcedMouseEventsComponents.at(i)->HandleMouseWheelMove(sfmlEvent);
		}

		if (!AcceptsMouseEvents(sfmlEvent.x, sfmlEvent.y))
		{
			return false;
		}

		for (unsigned int i = 0; i < Components.size(); i++)
		{
			if (VALID_OBJECT_CAST(Components.at(i), GUIComponent*))
			{
				if (dynamic_cast<GUIComponent*>(Components.at(i))->HandleMouseWheelMove(sfmlEvent))
				{
					return true;
				}
			}
		}

		return (ExecuteMouseWheelMove(sfmlEvent));
	}

	void GUIComponent::HandleResolutionChange (INT oldResX, INT oldResY)
	{
		if (VALID_OBJECT_CAST(Owner, GUIComponent*))
		{
			return; // Do nothing since the owner will notify its children to rescale upon resolution change
		}

		//Refresh position and size
		SetRelativePosition(Attributes.GetRelPositionX(), Attributes.GetRelPositionY());
		SetRelativeSize(Attributes.GetRelSizeX(), Attributes.GetRelSizeY());
	}

	void GUIComponent::InitProps ()
	{
		Super::InitProps();

		InterfacePriority = 50;

		ScalingProperties = nullptr;
		TransformationComponent = nullptr;

		//Default attributes to take up 100% of the space
		Attributes.SetRelativeSize(1.f, 1.f);

		Attributes.SetOwningComponent(this);
		bRegisteredToInterface = false;
		bForcedMouseEvents = false;
	}

	void GUIComponent::InitProps (const Object* copyObject)
	{
		Super::InitProps(copyObject);

		ScalingProperties = nullptr;
		const GUIComponent* castCopyObject = dynamic_cast<const GUIComponent*>(copyObject);
		if (castCopyObject != nullptr)
		{
			InterfacePriority = castCopyObject->InterfacePriority;

			Attributes.SetOwningComponent(this);
			Attributes.SetAbsolutePosition(castCopyObject->Attributes.GetAbsPositionX(), castCopyObject->Attributes.GetAbsPositionY());
			Attributes.SetAbsoluteSize(castCopyObject->Attributes.GetAbsSizeX(), castCopyObject->Attributes.GetAbsSizeY());

			bRegisteredToInterface = false; //This particular object was not registered to the InputManager.... yet
			bForcedMouseEvents = castCopyObject->bForcedMouseEvents;

			ScalingComponent* otherScalingComponent = dynamic_cast<ScalingComponent*>(castCopyObject->FindSubComponent(ScalingComponent::SStaticClass(), false));
			if (VALID_OBJECT(otherScalingComponent))
			{
				ScalingProperties = otherScalingComponent->CreateObjectOfMatchingClass();
				AddComponent(ScalingProperties);
			}

			GUITransformationComponent* otherTransformationComponent = dynamic_cast<GUITransformationComponent*>(castCopyObject->FindSubComponent(GUITransformationComponent::SStaticClass(), false));
			if (VALID_OBJECT(otherTransformationComponent))
			{
				TransformationComponent = otherTransformationComponent->CreateObjectOfMatchingClass();
				AddComponent(TransformationComponent);
			}
		}
	}

	void GUIComponent::BeginObject ()
	{
		Super::BeginObject();

		if (!VALID_OBJECT(ScalingProperties))
		{
			ScalingProperties = ScalingComponent::CreateObject();
			if (!AddComponent(ScalingProperties))
			{
				LOG1(LOG_WARNING, TXT("Unable to attaching scaling component to %s.  It will not be able to scale properly whenever the owning entity changes attributes."), GetName());
				ScalingProperties->Destroy();
				ScalingProperties = nullptr;
			}
		}

		if (!VALID_OBJECT(ScalingProperties))
		{
			TransformationComponent = GUITransformationComponent::CreateObject();
			if (!AddComponent(TransformationComponent))
			{
				LOG1(LOG_WARNING, TXT("Unable to attaching GUI transformation component to %s.  It will not be able contain properties regarding is scale and position."), GetName());
				TransformationComponent->Destroy();
				TransformationComponent = nullptr;
			}
			else
			{
				TransformationComponent->AttributeChangedCallback = SDFUNCTION(this, GUIComponent, HandleTransformationChange, void);
			}
		}

		ReevaluateMouseInterface();
	}

	bool GUIComponent::SetOwner (Entity* newOwner)
	{
		bool bResult = Super::SetOwner(newOwner);

		if (bResult)
		{
			//Scale and reposition this component based on the new owner's attributes.
			Attributes.SetRelativeSize(Attributes.GetRelSizeX(), Attributes.GetRelSizeY());
			Attributes.SetRelativePosition(Attributes.GetRelPositionX(), Attributes.GetRelPositionY());
			ReevaluateMouseInterface();
		}

		return bResult;
	}

	//Draw GUIComponents in absolute coordinates regardless of entity location relative to camera.
	void GUIComponent::CalculateRenderPosition (sf::Vector2f& outResults, const Camera* targetCamera)
	{
		outResults.x = Attributes.GetAbsPositionX().ToFLOAT().Value;
		outResults.y = Attributes.GetAbsPositionY().ToFLOAT().Value;
	}

	void GUIComponent::Destroy ()
	{
		InputManager::RemoveMouseInterface(this);

		Super::Destroy();
	}

	void GUIComponent::CleanUpInvalidPointers ()
	{
		Super::CleanUpInvalidPointers();

		CLEANUP_INVALID_POINTER(ScalingProperties)

		for (unsigned int i = 0; i < ForcedMouseEventsComponents.size(); i++)
		{
			if (ForcedMouseEventsComponents.at(i) == nullptr || ForcedMouseEventsComponents.at(i)->GetPendingDelete())
			{
				ForcedMouseEventsComponents.erase(ForcedMouseEventsComponents.begin() + i);
				i--;
			}
		}
	}

	void GUIComponent::SetDrawOrder (unsigned int newDrawOrder)
	{
		for (unsigned int i = 0; i < Components.size(); i++)
		{
			GUIComponent* subComponent = dynamic_cast<GUIComponent*>(Components.at(i));

			if (VALID_OBJECT(subComponent))
			{
				//Draw order is one value higher than its parent to ensure the children are drawn over owner
				subComponent->SetDrawOrder(newDrawOrder + 1);
			}
		}
	}

	bool GUIComponent::WithinBounds (unsigned int x, unsigned int y) const
	{
		return (x >= Attributes.GetAbsPositionX() && y >= Attributes.GetAbsPositionY() &&
				x <= Attributes.GetRight() && y <= Attributes.GetBottom());
	}

	bool GUIComponent::WithinBounds (INT x, INT y) const
	{
		return (x >= Attributes.GetAbsPositionX() && y >= Attributes.GetAbsPositionY() &&
				x <= Attributes.GetRight() && y <= Attributes.GetBottom());
	}

	void GUIComponent::SetAbsolutePosition (INT newPositionX, INT newPositionY)
	{
		OldAbsPositionX = Attributes.GetAbsPositionX();
		OldAbsPositionY = Attributes.GetAbsPositionY();

		Attributes.SetAbsolutePosition(newPositionX, newPositionY);
		HandlePositionChanged();
	}

	void GUIComponent::SetRelativePosition (FLOAT newPositionX, FLOAT newPositionY)
	{
		OldAbsPositionX = Attributes.GetAbsPositionX();
		OldAbsPositionY = Attributes.GetAbsPositionY();

		Attributes.SetRelativePosition(newPositionX, newPositionY);
		HandlePositionChanged();
	}

	void GUIComponent::SetAbsoluteSize (INT newSizeX, INT newSizeY)
	{
		OldAbsSizeX = Attributes.GetAbsSizeX();
		OldAbsSizeY = Attributes.GetAbsSizeY();

		Attributes.SetAbsoluteSize(newSizeX, newSizeY);
		HandleSizeChanged();
	}

	void GUIComponent::SetRelativeSize (FLOAT newSizeX, FLOAT newSizeY)
	{
		OldAbsSizeX = Attributes.GetAbsSizeX();
		OldAbsSizeY = Attributes.GetAbsSizeY();

		Attributes.SetRelativeSize(newSizeX, newSizeY);
		HandleSizeChanged();
	}

	void GUIComponent::RefreshAttributeClamps ()
	{
		Attributes.ApplyAutoClampValues();
	}

	void GUIComponent::ReevaluateMouseInterface ()
	{
		bool bResult = ShouldRegisterInterface();

		if (bResult != bRegisteredToInterface)
		{
			bRegisteredToInterface = bResult;

			bResult ? InputManager::AddMouseInterface(this, InterfacePriority) : InputManager::RemoveMouseInterface(this);
		}
	}

	void GUIComponent::AddForcedMouseEventsComponent (GUIComponent* newTarget)
	{
		ForcedMouseEventsComponents.push_back(newTarget);
	}

	bool GUIComponent::RemoveForcedMouseEventsComponent (GUIComponent* target)
	{
		for (unsigned int i = 0; i < ForcedMouseEventsComponents.size(); i++)
		{
			if (ForcedMouseEventsComponents.at(i) == target)
			{
				ForcedMouseEventsComponents.erase(ForcedMouseEventsComponents.begin() + i);
				return true;
			}
		}

		return false;
	}

	void GUIComponent::SetForcedMouseEvents (bool bNewForcedMouseEvents)
	{
		if (bNewForcedMouseEvents == bForcedMouseEvents)
		{
			return;
		}

		bForcedMouseEvents = bNewForcedMouseEvents;
		if (GetRegisteredToInterface())
		{
			//No need to add to forced mouse events vector primarily because this component is already handling mouse events.
			return;
		}

		GUIComponent* parentComponent = GetParentGUIComponent();
		if (VALID_OBJECT(parentComponent))
		{
			(bNewForcedMouseEvents) ? parentComponent->AddForcedMouseEventsComponent(this) : parentComponent->RemoveForcedMouseEventsComponent(this);
		}
	}

	GUIComponent* GUIComponent::GetParentGUIComponent ()
	{
		GUIComponent* parentComponent = dynamic_cast<GUIComponent*>(GetOwner());
		if (!parentComponent)
		{
			return this;
		}

		return parentComponent->GetParentGUIComponent();
	}

	bool GUIComponent::GetRegisteredToInterface () const
	{
		return bRegisteredToInterface;
	}

	bool GUIComponent::GetForcedMouseEvents () const
	{
		return bForcedMouseEvents;
	}

	ScalingComponent* GUIComponent::GetScalingProperties () const
	{
		return ScalingProperties;
	}

	GUITransformationComponent* GUIComponent::GetTransformationComponent () const
	{
		return TransformationComponent;
	}

	void GUIComponent::ExecuteMouseMove (const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove)
	{
	}

	bool GUIComponent::ExecuteMouseClick (const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType)
	{
		return false;
	}

	bool GUIComponent::ExecuteMouseWheelMove (const sf::Event::MouseWheelScrollEvent& sfmlEvent)
	{
		return false;
	}

	bool GUIComponent::AcceptsMouseEvents (const unsigned int& mousePosX, const unsigned int& mousePosY) const
	{
		return (bForcedMouseEvents || WithinBounds(mousePosX, mousePosY));
	}

	bool GUIComponent::ShouldRegisterInterface ()
	{
		//For optimization purposes:  Only register this gui component if not owned by a GUIComponent.
		//The parent gui components are responsible for passing mouse events down to its components.
		return (!VALID_OBJECT_CAST(Owner, GUIComponent*) && GetVisibility());
	}

	void GUIComponent::HandlePositionChanged ()
	{
		INT deltaX = Attributes.GetAbsPositionX() - OldAbsPositionX;
		INT deltaY = Attributes.GetAbsPositionY() - OldAbsPositionY;

		//Update all children GUIComponents of their owner's new position
		for (unsigned int i = 0; i < Components.size(); i++)
		{
			if (VALID_OBJECT_CAST(Components.at(i), GUIComponent*))
			{
				GUIComponent* curComponent = dynamic_cast<GUIComponent*>(Components.at(i));

				//Update the subcomponent's min/max clamp values based on current component's new position
				curComponent->RefreshAttributeClamps();
				curComponent->SetAbsolutePosition(curComponent->Attributes.GetAbsPositionX() + deltaX, curComponent->Attributes.GetAbsPositionY() + deltaY);
			}
		}
	}

	void GUIComponent::HandleSizeChanged ()
	{
		FLOAT scaleMultiplierX = Attributes.GetAbsSizeX().ToFLOAT() / OldAbsSizeX.ToFLOAT();
		FLOAT scaleMultiplierY = Attributes.GetAbsSizeY().ToFLOAT() / OldAbsSizeY.ToFLOAT();

		//Update all children GUIComponents of their owner's new size
		for (unsigned int i = 0; i < Components.size(); i++)
		{
			if (VALID_OBJECT_CAST(Components.at(i), GUIComponent*))
			{
				GUIComponent* curComponent = dynamic_cast<GUIComponent*>(Components.at(i));
				GUIAttributes curAttributes = curComponent->Attributes;

				//Update the subcomponent's min/max clamp values based on current component's new size
				curComponent->RefreshAttributeClamps();
				curComponent->SetAbsoluteSize(curAttributes.GetAbsSizeX().ToFLOAT() * scaleMultiplierX, curAttributes.GetAbsSizeY().ToFLOAT() * scaleMultiplierY);

				//Changing size also causes subcomponents to adjust their positions (pivot point is to the upper left corner).
				curComponent->SetRelativePosition(curAttributes.GetRelPositionX(), curAttributes.GetRelPositionY());
			}
		}
	}

	void GUIComponent::HandleTransformationChange ()
	{
		//TODO:  Implement component iterator... then iterate through all immediate child GUI Components
#if 0
		if (VALID_OBJECT(ScalingProperties))
		{
			Vector2 newPosition;
			Vector2 newScale;
			Rectangle<FLOAT> bounds = Rectangle<FLOAT>(AbsCoordinates.X, AbsCoordinates.Y, GetRightBounds(), GetBottomBounds());
			ScalingProperties->ScaleAttributes(AbsCoordinates, Scale2D * Scale, bounds, newPosition, newScale);
		}
#endif
	}
}

#endif