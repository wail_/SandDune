/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  FrameRenderComponent.h
  A component that communicates with SFML to render borders and
  the fill within the borders that makes up the FrameComponent.
  The sprite from its parent class is the fill texture (optional).

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef FRAMERENDERCOMPONENT_H
#define FRAMERENDERCOMPONENT_H

#include "GUI.h"

namespace SD
{
	class FrameComponent;

	class FrameRenderComponent : public SpriteComponent
	{
		DECLARE_CLASS(FrameRenderComponent)


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* Reference to the resources that draws the borders. */
		Texture* TopBorder;
		Texture* RightBorder;
		Texture* BottomBorder;
		Texture* LeftBorder;

		/* Corner images. */
		Texture* TopRightCorner;
		Texture* BottomRightCorner;
		Texture* BottomLeftCorner;
		Texture* TopLeftCorner;

		/* If FillTexture is blank, then the interior is filled with this color. */
		sf::Color FillColor;

	protected:
		static Texture* DefaultTopBorder;
		static Texture* DefaultRightBorder;
		static Texture* DefaultBottomBorder;
		static Texture* DefaultLeftBorder;

		static Texture* DefaultTopRightCorner;
		static Texture* DefaultBottomRightCorner;
		static Texture* DefaultBottomLeftCorner;
		static Texture* DefaultTopLeftCorner;

		/* Reference to the owner FrameComponent */
		FrameComponent* OwningFrameComponent;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitializeDefaultProperties () override;
		virtual void InitializeProperties (const Object* copyObject) override;
		virtual bool SetOwner (Entity* newOwner) override;
		virtual void Render (const Camera* targetCamera) override;

	protected:
		virtual void PostEngineInitialize () const override;
	};
}

#endif