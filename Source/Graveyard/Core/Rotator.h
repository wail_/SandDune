/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Rotator.h
  A normalized 2D vector that represents a direction.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

//TODO:  Move Rotator to the 2D module
//TODO:  Change floats to INTs



//TODO:  remove stub
#include "Vector3.h"
namespace SD
{
	class Rotator : public Vector3
	{

	};
}

#if 0

#ifndef ROTATOR_H
#define ROTATOR_H

#include "Configuration.h"
#if INCLUDE_CORE

/* There will be precision errors because of PI.  When comparing two angles, the angles are viewed to be the same if the difference between them is less than this value. */
#ifndef ROTATOR_PRECISION_TOLERANCE
#define ROTATOR_PRECISION_TOLERANCE 0.001
#endif

/* Set this to false if your program prefers for Rotators to use Radians instead of degrees. */
#ifndef ROTATOR_UNIT_DEGREES
#define ROTATOR_UNIT_DEGREES 1
#endif

#if ROTATOR_UNIT_DEGREES
#define ROTATOR_90 90.f
#else
#define ROTATOR_90 1.570796327f
#endif

//TODO:  Use INTs instead of FLOATS
namespace SD
{
	class Rotator : public DProperty
	{


		/*
		=====================
		  Properties
		=====================
		*/

	protected:
		float X;
		float Y;

		/* Increments everytime either axis of this rotator changed.  Resets when normalized. */
		int OperationCounter = 0;

		/* Determines how high OperationCounter may go up to before the Rotator normalizes. (To counter accumulated floating precision errors) */
		const int NormalizingFrequency = 100;


		/*
		=====================
		  Constructors
		=====================
		*/

	public:
		Rotator ();

		Rotator (float initialAngle);

		Rotator (const float x, const float y);

		Rotator (const Rotator& copyRotator);


		
		/*
		=====================
		  Operators
		=====================
		*/

	public:
		void operator= (const Rotator& copyRotator);
		void operator= (float newRotation);
		bool operator== (const Rotator& otherRotator);
		bool operator!= (const Rotator& otherRotator);
		Rotator operator+ (const Rotator& otherRotator);
		Rotator operator+ (float adjustment);
		void operator+= (const Rotator& otherRotator);
		void operator+= (float adjustment);
		Rotator operator- (const Rotator& otherRotator);
		Rotator operator- (float adjustment);
		void operator-= (const Rotator& otherRotator);
		void operator-= (float adjustment);


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual DString ToString () const;

		
		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Sets the x and y values and normalizes rotator after updates.
		  Expects values between -1 and 1.
		 */
		virtual void SetAxis (float newX, float newY);

		float Dot (const Rotator& otherRotator) const;

		void Normalize ();

		/**
		  Returns a float representation of the rotator where 0 is <0,1>, 90 is <1,0>, 180 is <0,-1>, and -90 is <-1,0>
		 */
		virtual float ToFloat () const;


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		virtual void GetAxis (float& outX, float& outY) const;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  Increments the counter, and normalizes the rotator whenever the counter reaches beyond the NormalizingFrequency value.
		 */
		virtual void IncrementCounter (int numTimesToIncrement = 1);

		virtual void SetAxisFromFloat (float newRotation);
	};
}

#endif
#endif
#endif