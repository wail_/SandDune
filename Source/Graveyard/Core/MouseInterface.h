/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  MouseInterface.h
  An interface for any mouse events and mouse input.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef MOUSEINTERFACE_H
#define MOUSEINTERFACE_H

#include "Object.h"
#include "InputManager.h"

#if INCLUDE_CORE

namespace SD
{
	class MouseInterface
	{


		/*
		=====================
		  Implementation
		=====================
		*/

	public:
		/**
		  Invoked whenever the mouse pointer moved.
		  Use sfmlEvent.x to get the new mouse pointer cordinate relative to the left of the game window.
		  Use sfmlEvent.y to get the new mouse pointer cordinate relative to the top of the game window.
		  deltaMove is the distance traveled since last MouseMove event.
		  Return true to capture event, preventing the event from being passed to other MouseInterfaces with lower priority.
		 */
		virtual void HandleMouseMove (const sf::Event::MouseMoveEvent& sfmlEvent, const Vector2& deltaMove) = 0;

		/**
		  Invoked whenever any mouse button was clicked.

		  Supported buttons:
		  Left, Right, Middle, XButton1, XButton2

		  eventType will either MouseButtonPressed or MouseButtonReleased
		  Return true to capture event, preventing the event from being passed to other MouseInterfaces with lower priority.
		 */
		virtual bool HandleMouseClick (const sf::Event::MouseButtonEvent& sfmlEvent, sf::Event::EventType eventType) = 0;

		/**
		  Invoked whenever the mouse wheel moved.
		  sfmlEvent.delta will be positive if the wheel moved up, negative when wheel moved down.
		  Return true to capture event, preventing the event from being passed to other MouseInterfaces with lower priority.
		 */
		virtual bool HandleMouseWheelMove (const sf::Event::MouseWheelEvent& sfmlEvent) = 0;
	};
}

#endif
#endif