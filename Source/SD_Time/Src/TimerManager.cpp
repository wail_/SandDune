/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TimerManager.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "TimeClasses.h"

#if INCLUDE_SD_TIME

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(TimerManager, Entity)

	TimerManager* TimerManager::MainTimerManager = nullptr;

	void TimerManager::BeginObject ()
	{
		Super::BeginObject();

		if (MainTimerManager == nullptr)
		{
			MainTimerManager = this;
		}

		TickComponent* tick = TickComponent::CreateObject();
		
		if (!VALID_OBJECT(tick))
		{
			//this should never happen
			Engine::GetEngine()->FatalError(TXT("Unable to create a tick component for ") + GetUniqueName());
			return;
		}

		tick->SetTickHandler(SDFUNCTION_1PARAM(this, TimerManager, HandleTick, void, FLOAT));
		AddComponent(tick);
	}

	void TimerManager::Destroy ()
	{
		if (MainTimerManager == this)
		{
			MainTimerManager = nullptr;
		}

		Super::Destroy();
	}

	void TimerManager::CleanUpInvalidPointers ()
	{
		Super::CleanUpInvalidPointers();

		for (unsigned int i = 0; i < Timers.size(); i++)
		{
			if (!Timers.at(i).OwningObject || Timers.at(i).OwningObject->GetPendingDelete())
			{
				Timers.at(i).OwningObject = nullptr;
				Timers.at(i).bPendingDelete = true;
			}
		}
	}

	TimerManager* TimerManager::GetTimerManager ()
	{
		return MainTimerManager;
	}

	bool TimerManager::SetTimer (const Object* targetObject, FLOAT timeDelay, bool bRepeat, function<bool()> functionCallback, const DString& functionIdentifier)
	{
		if (functionCallback == nullptr)
		{
			return false;
		}

		bRepeat = (bRepeat && timeDelay > 0);

		for (unsigned int i = 0; i < Timers.size(); i++)
		{
			if (Timers.at(i).EventName == functionIdentifier)
			{
				Timers.at(i).TimeRemaining = timeDelay;
				Timers.at(i).TimeInterval = (bRepeat) ? timeDelay : 0;
				Timers.at(i).bPendingDelete = false; //Revert pending delete if it was about to be deleted

				return true;
			}
		}

		STimerData newData;
		newData.OwningObject = targetObject;
		newData.EventHandler = functionCallback;
		newData.EventName = functionIdentifier;
		newData.TimeInterval = (bRepeat) ? timeDelay : 0;
		newData.TimeRemaining = timeDelay;
		Timers.push_back(newData);

		return false;
	}

	bool TimerManager::RemoveTimer (const Object* targetObject, const DString& functionIdentifier)
	{
		for (unsigned int i = 0; i < Timers.size(); i++)
		{
			if (Timers.at(i).OwningObject == targetObject && Timers.at(i).EventName == functionIdentifier)
			{
				//Add this entry to the list of entries to delete next tick (since this could be called within tick).
				Timers.at(i).bPendingDelete = true;
				return true;
			}
		}

		return false;
	}

	FLOAT TimerManager::GetTimeRemaining (const Object* targetObject, const DString& functionIdentifier) const
	{
		for (unsigned int i = 0; i < Timers.size(); i++)
		{
			if (Timers.at(i).OwningObject == targetObject && Timers.at(i).EventName == functionIdentifier)
			{
				return Timers.at(i).TimeRemaining;
			}
		}

		return 0;
	}

	void TimerManager::EvaluateTimerObjects ()
	{
		for (unsigned int i = 0; i < Timers.size(); i++)
		{
			if (Timers.at(i).bPendingDelete)
			{
				continue; //already marked.  Skip check
			}

			if (!IsValidTimerData(i))
			{
				Timers.at(i).bPendingDelete = true;
			}
		}
	}

	bool TimerManager::IsValidTimerData (unsigned int timerIdx) const
	{
		return (VALID_OBJECT(Timers.at(timerIdx).OwningObject));
	}

	void TimerManager::ApplyPendingDelete ()
	{
		for (unsigned int i = 0; i < Timers.size(); i++)
		{
			if (Timers.at(i).bPendingDelete)
			{
				Timers.erase(Timers.begin() + i);
				i--;
			}
		}
	}

	void TimerManager::HandleTick (FLOAT deltaSec)
	{
		EvaluateTimerObjects(); //Check if any timers should be marked for deletion.
		ApplyPendingDelete(); //Remove all PendingDelete timers

		for (unsigned int i = 0; i < Timers.size(); i++)
		{
			Timers.at(i).TimeRemaining -= deltaSec;

			//In case the timer was invoked multiple times this tick due to short interval or long deltaSec
			while(Timers.at(i).TimeRemaining <= 0 && !Timers.at(i).bPendingDelete)
			{
				if (Timers.at(i).OwningObject->GetPendingDelete())
				{
					//OwningObject->Destroy was called recently.
					Timers.at(i).bPendingDelete = true;
					break;
				}

				Timers.at(i).EventHandler();

				if (Timers.at(i).bPendingDelete || !VALID_OBJECT(Timers.at(i).OwningObject))
				{
					//Either RemoveTimer was called within EventHandler or the owning object was destroyed.
					Timers.at(i).bPendingDelete = true; //Notify parent loop to skip
					break;
				}

				//check time remaining one more time in case the EventHandler called SetTimer to itself
				if (Timers.at(i).TimeRemaining > 0)
				{
					break;
				}

				if (Timers.at(i).TimeInterval <= 0)
				{
					//Not repeating... remove timer
					Timers.at(i).bPendingDelete = true;
					break;
				}

				Timers.at(i).TimeRemaining += Timers.at(i).TimeInterval;
			} //End while (TimeRemaining < 0 && !bPendingDelete)
		} //End for (i < Timers.size())
	} //End HandleTick
} //Namespace SD

#endif