/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Directories.h
  Contains utilities to obtain engine-specific relative directories.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef DIRECTORIES_H
#define DIRECTORIES_H

#include "File.h"

#if INCLUDE_FILE

#define BASE_DIR FileUtils::GetBaseDirectory()

//All directory locations are relative to the Engine's base directory
#define BINARIES_DIR TXT("Binaries") DIR_SEPARATOR OUTPUT_LOCATION DIR_SEPARATOR
#define DOCUMENTS_DIR TXT("Documents") DIR_SEPARATOR
#define SOURCE_DIR TXT("Source") DIR_SEPARATOR

#endif
#endif