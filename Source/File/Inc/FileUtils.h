/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  FileUtils.h
  Abstract class containing generic utility functions related to file
  systems.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef FILEUTILS_H
#define FILEUTILS_H

#include "File.h"

#if INCLUDE_FILE

namespace SD
{
	class FileUtils : public BaseUtils
	{
		DECLARE_CLASS(FileUtils)


		/*
		=====================
		  Properties
		=====================
		*/

	protected:
		/* Base directory the engine is located (in absolute address). */
		static DString BaseDirectory;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Converts the given relative path to absolute path.  The path is relative to the base directory.
		  This function also guarantees that the results ends with a directory separator character.
		 */
		static DString ToAbsolutePath (const DString& relativePath);

		/**
		  Splits the full file name from the path.
		 */
		static void ExtractFileName (const DString& fullAddress, DString& outPath, DString& outFileName);

		/**
		  Extracts the file name from its extension (the period is trimmed).
		 */
		static void ExtractFileExtension (const DString& fullFileName, DString& outFileName, DString& outExtension);

		/**
		  Copies the contents of one file to another (regardless if destination already exists or not).
		  The source and destination expects path and file extensions.
		 */
		static void CopyFileContents (const DString& source, const DString& destination);


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		static DString GetBaseDirectory ();


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  Finds the engine's base absolute directory, and records the value.
		 */
		static DString FindBaseDirectory ();
	};
}

#endif
#endif