/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  FileUtils.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "FileClasses.h"

#if INCLUDE_FILE

using namespace std;

namespace SD
{
	DString FileUtils::BaseDirectory = FindBaseDirectory();

	IMPLEMENT_ABSTRACT_CLASS(FileUtils, BaseUtils)

	DString FileUtils::ToAbsolutePath (const DString& relativePath)
	{
		DString result = OS_FullPath(relativePath);

		if (result.At(result.Length() - 1) != DIR_SEPARATOR_CHAR)
		{
			result.String.push_back(DIR_SEPARATOR_CHAR);
		}

		return result;
	}

	void FileUtils::ExtractFileName (const DString& fullAddress, DString& outPath, DString& outFullFileName)
	{
		INT idx = static_cast<int>(fullAddress.String.find_last_of(DIR_SEPARATOR));
		if (idx == static_cast<int>(string::npos))
		{
			//No path was given.
			outPath.Clear();
			outFullFileName = fullAddress;
			return;
		}

		outPath = fullAddress.SubString(0, idx);
		outFullFileName = fullAddress.SubString(idx + 1);
	}

	void FileUtils::ExtractFileExtension (const DString& fullFileName, DString& outFileName, DString& outExtension)
	{
		size_t idx = fullFileName.String.find_last_of('.');
		if (idx == string::npos)
		{
			//No extension was given.
			outExtension.Clear();
			outFileName = fullFileName;
			return;
		}

		outFileName = fullFileName.String.substr(0, idx);
		outExtension = fullFileName.String.substr(idx + 1);
	}

	void FileUtils::CopyFileContents (const DString& source, const DString& destination)
	{
		//This seems to be the most simplistic way to copy one file to another:  http://stackoverflow.com/questions/10195343/copy-a-file-in-a-sane-safe-and-efficient-way
		ifstream sourceStream(source.ToCString());
		ofstream destinationStream(destination.ToCString());

		destinationStream << sourceStream.rdbuf();

		sourceStream.close();
		destinationStream.close();
	}

	DString FileUtils::GetBaseDirectory ()
	{
		return BaseDirectory;
	}

	DString FileUtils::FindBaseDirectory ()
	{
		DString exeLocation = OS_GetProcessLocation();

		//Validate location
		INT idx = exeLocation.FindSubText(BINARIES_DIR);
		if (idx == static_cast<int>(string::npos) || idx >= exeLocation.Length() - 1)
		{
			Engine::GetEngine()->FatalError(TXT("Invalid binaries location.  The binaries are expected to be located within:  ") + DString(BINARIES_DIR));
			return TXT("");
		}

		//Move up two directories since the binaries is located 2 levels within base directory 'Binaries/OutputLocation'
		exeLocation = exeLocation.SubString(0, idx - 1); //includes directory separator

		//Collapse directories (ie:  '/../')
		DString results = FileUtils::ToAbsolutePath(exeLocation);

		//Update the working directory to base directory
		if (!OS_SetWorkingDirectory(results))
		{
			Engine::GetEngine()->FatalError(TXT("Unable to set working directory to:  ") + results);
		}

		return results;
	}
}

#endif