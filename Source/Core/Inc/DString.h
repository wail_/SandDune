/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DString.h
  Short for Dune String, this class interfaces with string conversions,
  and contains string-related utility functions.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef DSTRING_H
#define DSTRING_H

#include "Configuration.h"

#if INCLUDE_CORE

#include "DProperty.h"

//For a very useful article about character encodings, see:  http://www.codeproject.com/Articles/2995/The-Complete-Guide-to-C-Strings-Part-I-Win-Chara 

#ifndef _TCHAR_DEFINED
	//Define TCHAR for non-Windows platforms
	#if USE_WIDE_STRINGS
		//Note:  Not all compilers understand wchar_t; you may have to define what wchar_t is.
		typedef wchar_t TCHAR;
	#else
		typedef char TCHAR;
	#endif

	#define _TCHAR_DEFINED
#endif

#if USE_WIDE_STRINGS
	typedef std::wstring TString;
#else
	typedef std::string TString;
#endif

#if 0
#if 1
#elif USE_UTF32
	typedef std::u32string TString;
#elif USE_UTF16
	typedef std::basic_string<Uint16> TString;
#elif USE_UTF8
	typedef std::basic_string<unsigned char> TString;
#else
	typedef std::string TString;
#endif
#endif

//Define TXT macro
/* Note:  Other character encodings were not tested.  It's very likely there are compatibility issues when changing
encodings.  The macro was added to make it easier to globally change hard-coded quotes throughout the solution. */
#if USE_WIDE_STRINGS
	#define TXT(msg) L##msg
#elif USE_UTF32
	#define TXT(msg) U##msg
#elif USE_UTF16
	#define TXT(msg) u##msg
#elif USE_UTF8
	#define TXT(msg) u8##msg
#else
	#define TXT(msg) msg
#endif

namespace SD
{
	class DString : public DProperty
	{


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* Contains the contents of this DString. */
		TString String;


		/*
		=====================
		  Constructors
		=====================
		*/

	public:
		DString ();
		DString (const TCHAR* inText);
		DString (const DString& inString);
		DString (const TString& inString);
		DString (const sf::String& inString);
		explicit DString (const unsigned int num);
		explicit DString (const int num);
		explicit DString (const float num);
		virtual ~DString ();


		/*
		=====================
		  Operators
		=====================
		*/

	public:
		virtual void operator= (const DString& inString);
		virtual void operator= (const TCHAR* inStr);


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual DString ToString () const override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Generates a TCHAR array from this string.
		 */
		virtual const TCHAR* ToCString () const;

		/**
		  Returns TCHAR at specified index.
		 */
		virtual const TCHAR At (INT idx) const;

		/**
		  Returns true if the text is either 1, true, or Yes.
		  Note:  This is implemented in Localization.cpp to handle language translations.
		 */
		virtual bool ToBool () const;

		/**
		  Compares this string to the other string.  Returns 0, if the strings are identical.
		  Returns negative if the other string is larger than this string.
		 */
		virtual int Compare (const DString& otherString, bool bCaseSensitive = true) const;

		/**
		  Searches for matching text within string.  Starts search from startPos.
		  Returns index where text first appears.  Returns negative if not found.
		 */
		virtual INT FindSubText (const DString& search, INT startPos = 0, bool bCaseSensitive = false) const;

		/**
		  The first string is populated by the text content before the selected index.
		  The second string is populated by the text content after the selected index.
		  The character splitting the string is not included in either string.
		 */
		virtual void SplitString (INT splitIndex, DString& outFirstSegment, DString& outSecondSegment) const;

		/**
		  Splits the string into segments for each delimiter found within the string.
		 */
		virtual void ParseString (TCHAR delimiter, std::vector<DString>& outSegments) const;

		/**
		  Returns text between start and end indices.
		  If endIdx is less than startIdx, then the rest of the string after startIdx is returned.
		 */
		virtual DString SubString (INT startIdx, INT endIdx = -1) const;

		/**
		  Returns text from start index, and the number of characters following start index.
		 */
		virtual DString SubStringCount (INT startIdx, INT count) const;

		/**
		  Returns the first set of characters from this string (up to count).
		 */
		virtual DString Left (INT count) const;

		/**
		  Returns the last set of characters from this string (up to count).
		 */
		virtual DString Right (INT count) const;

		/**
		  Returns the number of characters within this string.
		 */
		virtual INT Length () const;

		/**
		  Returns true if this string does not contain any text.
		 */
		virtual bool IsEmpty () const;

		/**
		  Removes all text content from string.
		 */
		virtual void Clear ();

		/**
		  Inserts a character at the specified index.
		 */
		virtual void Insert (INT idx, const TCHAR character);

		/**
		  Inserts a string within this string where the first letter of the inserted
		  string is found at the specified index.
		 */
		virtual void Insert (INT idx, DString text);

		/**
		  Removes character(s) starting from specified index up to count.
		 */
		virtual void Remove (INT idx, INT count = 1);

		/**
		  Replaces all found instances of searchFor with ReplaceWith
		 */
		virtual void StrReplace (const DString& searchFor, const DString& replaceWith, bool bCaseSensitive = false);

		/**
		  Similar to FormatString (template function), this finds an instance of '%s', and replaces it with the value of the next entry from the vector.
		  For compile-time list assembly, use the FormatString overload function that uses an initializer_list.
		 */
		static DString FormatString (DString text, const std::vector<DString>& values);

		/**
		  Converts all characters to upper-case letters.
		 */
		virtual void ToUpper ();

		/**
		  Converts all characters to lower-case letters.
		 */
		virtual void ToLower ();

		/**
		  Removes all white spaces from the beginning and end of the string.
		 */
		virtual void TrimSpaces ();

		/**
		  Searches this string until it finds the first character that does not match with specified string.  Returns the index when the strings do not match.
		  Searching "Sand Dune" within "Sand Pit" should return 5.
		  Searching "DString within "String should return 0.
		  Searching "Entity" within "EntityComponent" should return 6.
		  Searching "Engine" within "Engine" should return INT_INDEX_NONE.
		  If bCaseSensitive is false, the performance of this function is reduced since it'll create copies of 2 strings.
		 */
		virtual INT FindFirstMismatchingIdx (const DString& compare, bool bCaseSensitive) const;

#pragma region "Conversions"
		std::wstring ToWStr () const;
		std::string ToStr () const;

#ifdef PLATFORM_WINDOWS
		//Windows-specific conversions
		//Use this macro to get the various windows LP strings (the LPCWSTR gets cleared whenever the string goes off scope).
		//Strings would expire when doing:
		//LPCSTR myVar((DString + DString).ToCString());
		//LPCSTR myVar(TXT("Text"));
		//	return myVar; //expires after returning
		//For details see:  http://stackoverflow.com/questions/16559641/c-converting-between-string-and-lpcwstr
#define StrToLPCWSTR(source) source.ToWStr().c_str()
#define StrToLPCSTR(source) source.ToStr().c_str()
#endif
#pragma endregion


		/*
		=====================
		  Templates
		=====================
		*/

		/**
		  Generates a string where "%s" text is replaced by the value of the specified DProperty.
		  Expected params:  char* and a list of char*.
		 */
		template <class s>
		static DString Format (s text, std::initializer_list<const char*> list)
		{
			DString result(text);
			INT curIdx = result.FindSubText(TXT("%s"), 0, true);
			unsigned int listIdx = 0;

			while (curIdx >= 0)
			{
				result.Remove(curIdx, 2);

				//initializer_list<char*>::iterator arg = list.begin()
				std::initializer_list<const char*>::iterator arg = list.begin() + listIdx;
				DString value(*arg);

				result.Insert(curIdx, value);
				listIdx++;
				curIdx += value.Length();

				curIdx = result.FindSubText(TXT("%s"), curIdx, true);
			}

			return result;
		}

		/**
		  Generates a string from standard cpp data type.
		 */
		template <class Type>
		static DString MakeString (Type value)
		{
#if USE_WIDE_STRINGS
			return to_wstring(value);
#else
			return to_string(value);
#endif
		}
	};

#pragma region "External Operators"
	bool operator== (const DString& left, const DString& right);
	bool operator== (const DString& left, const TString& right);
	bool operator== (const TString& left, const DString& right);
	bool operator== (const DString& left, const TCHAR* right);
	bool operator== (const TCHAR* left, const DString& right);

	//Can't use operator~= for case insensitive compare
	bool operator!= (const DString& left, const DString& right);
	bool operator!= (const DString& left, const TString& right);
	bool operator!= (const TString& left, const DString& right);
	bool operator!= (const DString& left, const TCHAR* right);
	bool operator!= (const TCHAR* left, const DString& right);

	DString operator+ (const DString& left, const DString& right);
	DString operator+ (const DString& left, const TString& right);
	DString operator+ (const TString& left, const DString& right);
	DString operator+ (const DString& left, const TCHAR* right);
	DString operator+ (const TCHAR* left, const DString& right);

	DString& operator+= (DString& left, const DString& right);
	DString& operator+= (DString& left, const TString& right);
	TString& operator+= (TString& left, const DString& right);
	DString& operator+= (DString& left, const TCHAR* right);
#pragma endregion
}

#endif
#endif