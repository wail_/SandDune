/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  EngineComponent.h
  The Engine Component contains methods to extend the Engine's functionality.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef ENGINECOMPONENT_H
#define ENGINECOMPONENT_H

#include "Core.h"

#if INCLUDE_CORE

namespace SD
{
	class DClass;

	class EngineComponent
	{
		DECLARE_ENGINE_COMPONENT(EngineComponent)
		

		/*
		=====================
		  Properties
		=====================
		*/

	protected:
		/* If true, then this EngineComponent will register itself to the Engine's Tick cycle,
		and receive Pre and Post Tick Updates. */
		bool bTickingComponent;


		/*
		=====================
		  Constructors
		=====================
		*/

	protected:
		EngineComponent ();
		virtual ~EngineComponent ();


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Allows engine component to register parts of the Engine's Object Hash Table to group specific objects
		  for faster object iteration.  This is called prior to PreInitializeComponent.
		 */
		virtual void RegisterObjectHash ();

		/**
		  Called prior to majority of essential object instantiation.  Resource Pools are not
		  guaranteed to be instantiated at this point.  Object Hash values are already registered,
		  and is safe to instantiate objects and entities.
		 */
		virtual void PreInitializeComponent ();

		/**
		  Called after essential objects are instantiated.  Resource Pools are registered, and is
		  safe to import resources such as textures and fonts.
		 */
		virtual void InitializeComponent ();

		/**
		  Called after essential objects and resources are imported.
		 */
		virtual void PostInitializeComponent ();

		/**
		  Allows Engine Component react to incoming log message, and modify the message.
		 */
		virtual void RecordLog (DString& outFullMessage, const DString& logType);

		/**
		  Called before the Engine executes its tick cycle.  The render pipeline did not execute yet, and
		  all TickComponents did not call receive their Tick updates yet.
		  You must set bTickingComponent to true in the constructor to be able to receive updates.
		 */
		virtual void PreTick (FLOAT deltaSec);

		/**
		  Called after the Engine cycles through all TickComponents.  It's not guaranteed if the objects are
		  displayed or not at the time this function is called.
		  You must set bTickingComponent to true in the constructor to be able to receive updates.
		 */
		virtual void PostTick (FLOAT deltaSec);

		/**
		  Invoked whenever the engine is about to shutdown.
		 */
		virtual void ShutdownComponent ();


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		virtual bool GetTickingComponent () const;

	friend class Engine;
	friend class DClass;
	};
}

#endif
#endif