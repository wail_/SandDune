/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  EngineIntegrityTester.h
  This object assists the EngineIntegrityUnitTester in ensuring the objects and properties
  instantiated from its test is also cleaned up.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef ENGINEINTEGRITYTESTER_H
#define ENGINEINTEGRITYTESTER_H

#include "Object.h"
#include "UnitTester.h"

#if INCLUDE_CORE

#ifdef DEBUG_MODE

namespace SD
{
	class EngineIntegrityTester : public Object
	{
		DECLARE_CLASS(EngineIntegrityTester)


		/*
		=====================
		  Properties
		=====================
		*/
	private:
		/* Reference to all instantiated objects this unit tester created. */
		std::vector<Object*> InstantiatedObjects;

		/* Total number of registered objects to the hash table before this unit test created objects. */
		INT TotalObjectsBeforeTest = 0;

		/* Properties for the property iterator to find. */
		INT PropIterINT;
		FLOAT PropIterFLOAT;
		DString PropIterString;
		Vector3 PropIterVector;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void Destroy () override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		virtual bool TestClassIterator (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags);
		virtual bool TestObjectInstantiation (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags);
		virtual bool TestObjectIterator (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags);
		virtual bool TestPropertyIterator (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags);
		virtual bool TestCleanUp (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags);
		virtual bool TestComponents (const UnitTester* tester, UnitTester::EUnitTestFlags testFlags);
	};
}

#endif //debug_mode

#endif //INCLUDE_CORE
#endif //include guard