/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TransformationComponent.h
  Transformation Component determines the owning entity's location coordinates, rotation offset, and
  scaling attributes.  These aspects are to modify how the entity is rendered to window, and may
  influence various interactions with other entities (such as collision and AI).

  There are a variety of coordinate systems, and those systems are implemented in subclasses.
  Base TransformationComponent assumes the owning entity is always
  relevant, and resides in upper left corner of screen.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef TRANSFORMATIONCOMPONENT_H
#define TRANSFORMATIONCOMPONENT_H

#include "EntityComponent.h"

#if INCLUDE_CORE

namespace SD
{
	class TransformationComponent : public EntityComponent
	{
		DECLARE_CLASS(TransformationComponent)
	};
}

#endif
#endif