/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ComponentIterator.h
  The ComponentIterator will iterate through the components of a specified entity.

  Warning:  Be careful not it change the owning Entity's component list while an iterator is
  cycling for that may cause other components in next cycle to be skipped.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef COMPONENTITERATOR_H
#define COMPONENTITERATOR_H

#include "Core.h"

#if INCLUDE_CORE

namespace SD
{
	//forward declaration
	class Entity;
	class EntityComponent;

	class ComponentIterator
	{


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* If true, then this iterator will find components within components. */
		bool bRecursive;

	protected:
		/* Entity that owns all of the components this iterator will pass through. */
		const Entity* TargetEntity;

		/* Index of the SelectedComponent in the Entity's Component list. */
		unsigned int ComponentIndex;

		/* Iterator that'll be iterating through components within components. */
		ComponentIterator* RecursiveIterator;


		/*
		=====================
		  Constructors
		=====================
		*/

	public:
		ComponentIterator ();
		ComponentIterator (const Entity* inSelectedEntity, bool bInRecursive);
		virtual ~ComponentIterator ();
		


		/*
		=====================
		  Operators
		=====================
		*/

	public:
		void operator++ (); //++iter
		void operator++ (int); //iter++


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		virtual void SetTargetEntity (const Entity* newTargetEntity);


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		virtual const Entity* GetTargetEntity () const;
		virtual EntityComponent* GetSelectedComponent () const;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  Kicks off the iteration sequence.
		  Note:  this is not virtual since this is called through constructors.
		 */
		void SelectFirstComponent ();

		/**
		  Redirects SelectedObject to the next object in the linked list.  If none found, then it'll iterate
		  down the object hash table.
		 */
		virtual void FindNextComponent ();
	};
}

#endif
#endif