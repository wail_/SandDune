/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Stopwatch.h
  A debugging utility that simply prints out the lifespan of the
  instantiated object.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef STOPWATCH_H
#define STOPWATCH_H

#include "Core.h"
#if INCLUDE_CORE

#ifdef DEBUG_MODE
namespace SD
{
	class Stopwatch
	{


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* Name of this stopwatch to help the user identify what is this recording. */
		DString Name;

	protected:
		/* Timestamp when the timer started.  If negative, then the timer is paused. */
		FLOAT StartTime;

		/* Previously recorded elapsed time from pausing. */
		FLOAT RecordedTime;


		/*
		=====================
		  Constructors
		=====================
		*/

	public:
		Stopwatch ();
		Stopwatch (const DString& stopwatchName);
		Stopwatch (const Stopwatch& copyConstructor);
		virtual ~Stopwatch ();


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Suspends timer without losing progress of elapsed time.
		 */
		virtual void PauseTime ();

		virtual void ResumeTime ();
	
		/**
		  Sets the recorded time back to 0, and resets StartTime to the current time.
		 */
		virtual void ResetTime ();


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		/**
		  Retrieves the total recorded time this stopwatch recorded (in milliseconds).
		 */
		virtual FLOAT GetElapsedTime () const;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  Retrieves the current timestamp from the engine.
		 */
		virtual FLOAT GetSystemTime () const;

		/**
		  Logic to run when the stopwatch was destroyed.
		 */
		virtual void DestroyStopwatch ();
	};
}

#endif
#endif
#endif