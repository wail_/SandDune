/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SDFunctionTester.h
  Object responsible for assisting the DatatypeUnitTester's SDFunction test.

  In short, this object will bind a couple SDFunctions to its event handlers.
  The test will only become successful if all functions were invoked.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef SDFUNCTIONTESTER_H
#define SDFUNCTIONTESTER_H

#include "Object.h"

#if INCLUDE_CORE

#ifdef DEBUG_MODE

namespace SD
{
	class SDFunctionTester : public Object
	{
		DECLARE_CLASS(SDFunctionTester)


		/*
		=====================
		  Properties
		=====================
		*/

	protected:
		/* Various flags that all must be true to be considered a successful test. */
		bool bTestedVoid;
		bool bTestedParamINT;
		bool bTestedMultiParam;
		bool bTestedReturn;
		bool bTestedReturnMultiParam;
		bool bTestedReturnOutParam;

		/* Increments every time one of its event handlers were called. */
		INT FunctionCounter;

	private:
		const DatatypeUnitTester* DatatypeTester;
		UnitTester::EUnitTestFlags TestFlags;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;


		/*
		=====================
		  Method
		=====================
		*/

	public:
		/**
		  Executes test.  Returns true if all callbacks were invoked once, and the test was successful.
		 */
		virtual bool RunTest (const DatatypeUnitTester* tester, UnitTester::EUnitTestFlags testFlags);


		/*
		=====================
		  Event Handlers
		=====================
		*/

	protected:
		virtual void HandleVoidFunction ();
		virtual void HandleParamFunction (INT paramValueTest);
		virtual void HandleMultiParamFunction (INT param1, FLOAT param2, DString param3, unsigned int param4);
		virtual FLOAT HandleReturnFunction ();
		virtual BOOL HandleReturnMultiParamFunction (INT param1, Object* param2, std::string param3);
		virtual INT HandleReturnOutParamFunction (INT param1, INT& outParam2);
	};
}

#endif //debug_mode

#endif
#endif