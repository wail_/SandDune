/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Vector3.h
  Self-contained 3D vector class with some utilities.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef VECTOR3_H
#define VECTOR3_H

#include "Configuration.h"
#if INCLUDE_CORE

namespace SD
{
	class Vector2;

	class Vector3 : public DProperty
	{


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		FLOAT X;
		FLOAT Y;
		FLOAT Z;


		/*
		=====================
		  Constructors
		=====================
		*/

	public:
		Vector3 ();

		Vector3 (const float x, const float y, const float z);

		Vector3 (const FLOAT x, const FLOAT y, const FLOAT z);

		Vector3 (const Vector3& copyVector);

		
		/*
		=====================
		  Operators
		=====================
		*/

	public:
		void operator= (const Vector3& copyVector);


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		DString ToString () const override;

		
		/*
		=====================
		  Methods
		=====================
		*/

	public:
		static Vector3 SFMLtoSD (const sf::Vector3f sfVector);
		static sf::Vector3f SDtoSFML (const Vector3 sdVector);

		/**
		  Returns the length of this vector.
		 */
		FLOAT VSize (bool bIncludeZ = true) const;

		/**
		  Adjusts this vector's size so the length of the vector is equal to the specified length.
		 */
		void SetLengthTo (FLOAT targetLength);

		FLOAT Dot (const Vector3& otherVector) const;

		/**
		  Gets the orthogonal vector to this vector and the given vector.
		 */
		Vector3 CrossProduct (const Vector3& otherVector) const;

		void Normalize ();

		/**
		  Returns true of all axis of this vector are the equal.
		 */
		bool IsUniformed () const;

		/**
		  Returns true if all axis is 0.
		 */
		bool IsEmpty () const;

		/**
		  Returns a Vector2 from this vector's components.
		 */
		Vector2 ToVector2 () const;
	};

#pragma region "External Operators"
	bool operator== (const Vector3& left, const Vector3& right);
	bool operator!= (const Vector3& left, const Vector3& right);
	Vector3 operator+ (const Vector3& left, const Vector3& right);
	Vector3& operator+= (Vector3& left, const Vector3& right);
	Vector3 operator- (const Vector3& left, const Vector3& right);
	Vector3& operator-= (Vector3& left, const Vector3& right);
	Vector3 operator* (const Vector3& left, const Vector3& right);
	Vector3& operator*= (Vector3& left, const Vector3& right);
	Vector3 operator/ (const Vector3& left, const Vector3& right);
	Vector3& operator/= (Vector3& left, const Vector3& right);

	template<class T> Vector3 operator* (const Vector3& left, const T& right)
	{
		return Vector3(left.X * right, left.Y * right, left.Z * right);
	}

	template<class T> Vector3 operator* (const T& left, const Vector3& right)
	{
		return Vector3(left * right.X, left * right.Y, left * right.Z);
	}

	template<class T> Vector3 operator*= (Vector3& left, const T& right)
	{
		left.X *= right;
		left.Y *= right;
		left.Z *= right;

		return left;
	}

	template<class T> Vector3 operator/ (const Vector3& left, const T& right)
	{
		if (right != 0)
		{
			return Vector3(left.X / right, left.Y / right, left.Z / right);
		}

		return left;
	}

	template<class T> Vector3& operator/= (Vector3& left, const T& right)
	{
		if (right != 0)
		{
			left.X /= right;
			left.Y /= right;
			left.Z /= right;
		}

		return left;
	}
#pragma endregion
}

#endif
#endif