/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Rectangle.h
  A datatype responsible for tracking four borders (left, top, right, bottom).

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "Configuration.h"
#if INCLUDE_CORE

#include "CoreDatatypes.h"

namespace SD
{
	class Vector2;

	template <class T>
	class Rectangle : public DProperty
	{


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		T Left;
		T Top;
		T Right;
		T Bottom;


		/*
		=====================
		  Constructors
		=====================
		*/

	public:
		Rectangle ()
		{
			Left = static_cast<T>(0);
			Top = static_cast<T>(0);
			Right = static_cast<T>(0);
			Bottom = static_cast<T>(0);
		}

		Rectangle (const T& inLeft, const T& inTop, const T& inRight, const T& inBottom)
		{
			Left = inLeft;
			Top = inTop;
			Right = inRight;
			Bottom = inBottom;
		}

		Rectangle (const Rectangle& copyRectangle)
		{
			Left = copyRectangle.Left;
			Top = copyRectangle.Top;
			Right = copyRectangle.Right;
			Bottom = copyRectangle.Bottom;
		}

		
		/*
		=====================
		  Operators
		=====================
		*/

	public:
		void operator= (const Rectangle& copyRectangle)
		{
			Left = copyRectangle.Left;
			Top = copyRectangle.Top;
			Right = copyRectangle.Right;
			Bottom = copyRectangle.Bottom;
		}


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual DString ToString () const override
		{
			return (TXT("Left(") + Left.ToString() + TXT(") Top(") + Top.ToString() + TXT(") Right(") + Right.ToString() + TXT(") Bottom(") + Bottom.ToString() + TXT(")"));
		}

		
		/*
		=====================
		  Methods
		=====================
		*/

	public:
		T CalculateArea () const
		{
			return GetWidth() * GetHeight();
		}

		T CalculatePerimeter () const
		{
			return (GetWidth() * 2) + (GetHeight() * 2);
		}

		/**
		  Returns true if this rectangle is a square.
		 */
		bool IsSquare () const
		{
			return (GetWidth() == GetHeight());
		}

		/**
		  Returns true if all borders of this rectangle is zero.
		 */
		bool IsEmpty () const
		{
			return (GetWidth() == 0 && GetHeight() == 0);
		}

		/**
		  Calculates the overlapping rectangle.
		  Returns an empty rectangle if the two rectangles do not overlap.
		 */
		Rectangle GetOverlappingRectangle (const Rectangle& otherRectangle) const
		{
			if (!Overlaps(otherRectangle))
			{
				return Rectangle();
			}

			Rectangle result;
			result.Left = Utils::Max(Left, otherRectangle.Left);
			result.Top = Utils::Max(Top, otherRectangle.Top);
			result.Right = Utils::Min(Right, otherRectangle.Right);
			result.Bottom = Utils::Min(Bottom, otherRectangle.Bottom);

			return result;
		}

		/**
		  Returns true if this rectangle overlaps with other rectangle.
		 */
		bool Overlaps (const Rectangle& otherRectangle) const
		{
			if (Top >= otherRectangle.Bottom || Bottom <= otherRectangle.Top)
			{
				//Rectangle is below/above the other rectangle.
				return false;
			}

			if (Left >= otherRectangle.Right || Right <= otherRectangle.Left)
			{
				//Rectangle is left/right of other rectangle.
				return false;
			}

			return true;
		}

		/**
		  Returns true if the given point is within this rectangle.
		 */
		bool EncompassesPoint (const Vector2& targetPoint) const
		{
			return (targetPoint.X >= Left && targetPoint.X <= Right && targetPoint.Y <= Top && targetPoint.Y >= Bottom);
		}

		/**
		  Returns true if any of its borders are inside-out.
		 */
		bool ContainsInvalidBorders () const
		{
			return (Left > Right || Top > Bottom);
		}


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		T GetWidth () const
		{
			return T::Abs((Right - Left));
		}

		T GetHeight () const
		{
			return T::Abs((Bottom - Top));
		}


		/*
		=====================
		  Templates
		=====================
		*/

	public:
		template <class Type> Rectangle operator* (const Type& scaler)
		{
			return Rectangle(Left * scaler, Top * scaler, Right * scaler, Bottom * scaler);
		}

		template <class Type> void operator*= (const Type& scaler)
		{
			Left *= scaler;
			Top *= scaler;
			Right *= scaler;
			Bottom *= scaler;
		}

		template <class Type> Vector2 operator/ (const Type& scaler)
		{
			if (scaler != 0)
			{
				return Rectangle(Left / scaler, Top / scaler, Right / scaler, Bottom / scaler);
			}

			return this;
		}

		template <class Type> void operator/= (const Type& scaler)
		{
			if (scaler != 0)
			{
				Left /= scaler;
				Top /= scaler;
				Right /= scaler;
				Bottom /= scaler;
			}
		}
	};

#pragma region "External Operators"
	template<class T>
	bool operator== (const Rectangle<T>& left, const Rectangle<T>& right)
	{
		return (left.Top == right.Top && left.Right == right.Right && left.Bottom == right.Bottom && left.Left == right.Left);
	}

	template<class T>
	bool operator!= (const Rectangle<T>& left, const Rectangle<T>& right)
	{
		return !(left == right);
	}

	template<class T>
	Rectangle<T> operator+ (const Rectangle<T>& left, const Rectangle<T>& right)
	{
		return Rectangle<T>(left.Left + right.Left, left.Top + right.Top, left.Right + right.Right, left.Bottom + right.Bottom);
	}

	template<class T>
	Rectangle<T>& operator+= (Rectangle<T>& left, const Rectangle<T>& right)
	{
		left.Left += right.Left;
		left.Top += right.Top;
		left.Right += right.Right;
		left.Bottom += right.Bottom;
		return left;
	}

	template<class T>
	Rectangle<T> operator- (const Rectangle<T>& left, const Rectangle<T>& right)
	{
		return Rectangle<T>(left.Left - right.Left, left.Top - right.Top, left.Right - right.Right, left.Bottom - right.Bottom);
	}

	template<class T>
	Rectangle<T>& operator-= (Rectangle<T>& left, const Rectangle<T>& right)
	{
		left.Left -= right.Left;
		left.Top -= right.Top;
		left.Right -= right.Right;
		left.Bottom -= right.Bottom;
		return left;
	}

	template<class T>
	Rectangle<T> operator* (const Rectangle<T>& left, const Rectangle<T>& right)
	{
		return Rectangle<T>(left.Left * right.Left, left.Top * right.Top, left.Right * right.Right, left.Bottom * right.Bottom);
	}

	template<class T>
	Rectangle<T>& operator*= (Rectangle<T>& left, const Rectangle<T>& right)
	{
		left.Left *= right.Left;
		left.Top *= right.Top;
		left.Right *= right.Right;
		left.Bottom *= right.Bottom;
		return left;
	}

	template<class T>
	Rectangle<T> operator/ (const Rectangle<T>& left, const Rectangle<T>& right)
	{
		Rectangle<T> result(left);
		result /= right;
		return result;
	}

	template<class T>
	Rectangle<T>& operator/= (Rectangle<T>& left, const Rectangle<T>& right)
	{
		if (right.Left != 0.f)
		{
			left.Left /= right.Left;
		}

		if (right.Top != 0.f)
		{
			left.Top /= right.Top;
		}

		if (right.Right != 0.f)
		{
			left.Right /= right.Right;
		}

		if (right.Bottom != 0.f)
		{
			left.Bottom /= right.Bottom;
		}

		return left;
	}
#pragma endregion
}

#endif
#endif