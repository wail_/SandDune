/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  INT.h
  A class that represents a 32 bit number.  Contains numeric utility
  functions, and supports typecasting to various standard C++ numbers.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef INT_H
#define INT_H

#include "Configuration.h"
#include "DProperty.h"

#if INCLUDE_CORE

//Max possible value of 32bit unsigned int
#define UINT_INDEX_NONE 4294967295
#define INT_INDEX_NONE -1

namespace SD
{
	class FLOAT;

	class INT : public DProperty
	{


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		int Value = 0;


		/*
		=====================
		  Constructors
		=====================
		*/

	public:
		INT ();
		INT (const int newValue);
		INT (const INT& copyInt);
		explicit INT (const unsigned int& newValue);
		explicit INT (const FLOAT& newValue);
		explicit INT (const DString& text);

#ifdef PLATFORM_64BIT
		explicit INT (const size_t newValue);
#endif

		
		/*
		=====================
		  Operators
		=====================
		*/

	public:
		virtual void operator= (const INT& copyInt);
		virtual void operator= (const int otherInt);
		virtual void operator= (const unsigned int otherInt);

		virtual INT operator++ (); //++iter
		virtual INT operator++ (int); //iter++

		virtual INT operator- () const;
		virtual INT operator-- ();
		virtual INT operator-- (int);
#if 0
		virtual bool operator== (const INT& otherInt) const;
		virtual bool operator!= (const INT& otherInt) const;
		virtual bool operator< (const INT& otherInt) const;
		virtual bool operator<= (const INT& otherInt) const;
		virtual bool operator> (const INT& otherInt) const;
		virtual bool operator>= (const INT& otherInt) const;

		virtual INT operator>> (const INT& otherInt) const;
		virtual INT operator<< (const INT& otherInt) const;
		virtual INT operator& (const INT& otherInt) const;
		virtual INT operator^ (const INT& otherInt) const;
		virtual INT operator| (const INT& otherInt) const;
		virtual void operator&= (const INT& otherInt);
		virtual void operator^= (const INT& otherInt);
		virtual void operator|= (const INT& otherInt);


		virtual INT operator+ (const INT& otherInt) const;
		virtual void operator+= (const INT& otherInt);

		virtual INT operator- (const INT& otherInt) const;
		virtual INT operator- () const; //-Value
		virtual void operator-= (const INT& otherInt);

		virtual INT operator* (const INT& otherInt) const;
		virtual void operator*= (const INT& otherInt);

		virtual INT operator/ (const INT& otherInt) const;
		virtual void operator/= (const INT& otherInt);
		virtual void operator%= (const INT& otherInt);
		virtual INT operator% (const INT& otherInt) const;
#endif


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual DString ToString () const override;

		
		/*
		=====================
		  Methods
		=====================
		*/

	public:
		static INT MakeINT (float value);

		/**
		  Returns a FLOAT from this INT.
		 */
		virtual FLOAT ToFLOAT () const;

		bool IsEven () const;
		bool IsOdd () const;

		/**
		  Returns a string representation of this number with leading zeroes if the min digits requirement is not met.
		 */
		virtual DString ToStringMinDigits (const INT minDigits) const;

		/**
		  Returns the absolute value of this INT
		 */
		static INT Abs (INT value);
		virtual void Abs ();

		/**
		  Converts the value to an int.  If greater than INT_MAX, then the value is set to the max int.
		 */
		virtual int GetInt (unsigned int target);

		/**
		  Returns a converted value of this INT to an unsigned int.  Negative values are converted to zero.
		 */
		virtual unsigned int ToUnsignedInt () const;

		virtual INT NumDigits () const;
	};

#pragma region "External Operators"
	bool operator== (const INT& left, const INT& right);
	bool operator== (const INT& left, const int& right);
	bool operator== (const int& left, const INT& right);

	bool operator!= (const INT& left, const INT& right);
	bool operator!= (const INT& left, const int& right);
	bool operator!= (const int& left, const INT& right);

	bool operator< (const INT& left, const INT& right);
	bool operator< (const INT& left, const int& right);
	bool operator< (const int& left, const INT& right);
	bool operator<= (const INT& left, const INT& right);
	bool operator<= (const INT& left, const int& right);
	bool operator<= (const int& left, const INT& right);
	bool operator> (const INT& left, const INT& right);
	bool operator> (const INT& left, const int& right);
	bool operator> (const int& left, const INT& right);
	bool operator>= (const INT& left, const INT& right);
	bool operator>= (const INT& left, const int& right);
	bool operator>= (const int& left, const INT& right);

	INT operator>> (const INT& left, const INT& right);
	INT operator>> (const INT& left, const int& right);
	INT operator>> (const int& left, const INT& right);
	INT& operator>>= (INT& left, const INT& right);
	INT& operator>>= (INT& left, const int& right);
	int& operator>>= (int& left, const INT& right);
	INT operator<< (const INT& left, const INT& right);
	INT operator<< (const INT& left, const int& right);
	INT operator<< (const int& left, const INT& right);
	INT& operator<<= (INT& left, const INT& right);
	INT& operator<<= (INT& left, const int& right);
	int& operator<<= (int& left, const INT& right);
	INT operator& (const INT& left, const INT& right);
	INT operator& (const INT& left, const int& right);
	INT operator& (const int& left, const INT& right);
	INT operator^ (const INT& left, const INT& right);
	INT operator^ (const INT& left, const int& right);
	INT operator^ (const int& left, const INT& right);
	INT operator| (const INT& left, const INT& right);
	INT operator| (const INT& left, const int& right);
	INT operator| (const int& left, const INT& right);
	INT& operator&= (INT& left, const INT& right);
	INT& operator&= (INT& left, const int& right);
	int& operator&= (int& left, const INT& right);
	INT& operator^= (INT& left, const INT& right);
	INT& operator^= (INT& left, const int& right);
	int& operator^= (int& left, const INT& right);
	INT& operator|= (INT& left, const INT& right);
	INT& operator|= (INT& left, const int& right);
	int& operator|= (int& left, const INT& right);

	INT operator+ (const INT& left, const INT& right);
	INT operator+ (const INT& left, const int& right);
	INT operator+ (const int& left, const INT& right);
	INT& operator+= (INT& left, const INT& right);
	INT& operator+= (INT& left, const int& right);
	int& operator+= (int& left, const INT& right);

	INT operator- (const INT& left, const INT& right);
	INT operator- (const INT& left, const int& right);
	INT operator- (const int& left, const INT& right);
	INT& operator-= (INT& left, const INT& right);
	INT& operator-= (INT& left, const int& right);
	int& operator-= (int& left, const INT& right);

	INT operator* (const INT& left, const INT& right);
	INT operator* (const INT& left, const int& right);
	INT operator* (const int& left, const INT& right);
	INT& operator*= (INT& left, const INT& right);
	INT& operator*= (INT& left, const int& right);
	int& operator*= (int& left, const INT& right);

	INT operator/ (const INT& left, const INT& right);
	INT operator/ (const INT& left, const int& right);
	INT operator/ (const int& left, const INT& right);
	INT& operator/= (INT& left, const INT& right);
	INT& operator/= (INT& left, const int& right);
	int& operator/= (int& left, const INT& right);

	INT operator% (const INT& left, const INT& right);
	INT operator% (const INT& left, const int& right);
	INT operator% (const int& left, const INT& right);
	INT& operator%= (INT& left, const INT& right);
	INT& operator%= (INT& left, const int& right);
	int& operator%= (int& left, const INT& right);


#if 0
	bool operator== (int a, const INT& b);
	bool operator!= (int a, const INT& b);
	bool operator< (int a, const INT& b);
	bool operator<= (int a, const INT& b);
	bool operator> (int a, const INT& b);
	bool operator>= (int a, const INT& b);

	INT operator>> (int a, const INT& b);
	INT operator<< (int a, const INT& b);
	INT operator& (int a, const INT& b);
	INT operator^ (int a, const INT& b);
	INT operator| (int a, const INT& b);

	INT operator+ (int a, const INT& b);
	INT operator- (int a, const INT& b);
	INT operator* (int a, const INT& b);
	INT operator/ (int a, const INT& b);
	INT operator% (int a, const INT& b);
#endif
#pragma endregion
}

#endif
#endif