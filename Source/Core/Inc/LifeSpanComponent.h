/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  LifeSpanComponent.h
  Component responsible for timing out its owning entity.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef LIFESPANCOMPONENT_H
#define LIFESPANCOMPONENT_H

#include "EntityComponent.h"
#if INCLUDE_CORE

namespace SD
{
	class LifeSpanComponent : public EntityComponent
	{
		DECLARE_CLASS(LifeSpanComponent)


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* When this value is no longer positive, then this component will destroy the owning entity. */
		FLOAT LifeSpan;

		/* If true, LifeSpan will continuously drain from Tick.  Setting this false will pause the timer.*/
		bool bAging;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual void BeginObject () override;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  Destroys the owning entity because the LifeSpan expired.
		 */
		virtual void Expire ();


		/*
		=====================
		  Event Handlers
		=====================
		*/

	protected:
		virtual void HandleTick (FLOAT deltaSec);
	};
}

#endif
#endif