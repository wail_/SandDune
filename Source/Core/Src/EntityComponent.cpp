/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  EntityComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "CoreClasses.h"

#if INCLUDE_CORE

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(EntityComponent, Entity)

	void EntityComponent::InitProps ()
	{
		Super::InitProps();

		Owner = nullptr;
	}

	void EntityComponent::AddComponentModifier (EntityComponent* newComponent)
	{
		Super::AddComponentModifier(newComponent);

		if (VALID_OBJECT(Owner))
		{
			//Notify the parent components to also adjust their modifiers
			Owner->AddComponentModifier(newComponent);
		}
	}

	bool EntityComponent::RemoveComponentModifier (EntityComponent* oldComponent)
	{
		if (Super::RemoveComponentModifier(oldComponent) && VALID_OBJECT(Owner))
		{
			Owner->RemoveComponentModifier(oldComponent);
			return true;
		}

		return false;
	}

	void EntityComponent::Destroy ()
	{
		DetachSelfFromOwner();

		Super::Destroy();
	}

	unsigned int EntityComponent::CalculateHashID () const
	{
		return (Engine::GetEngine()->GetComponentHashNumber());
	}

	Entity* EntityComponent::GetRootEntity () const
	{
		if (dynamic_cast<EntityComponent*>(Owner))
		{
			return (dynamic_cast<EntityComponent*>(Owner)->GetRootEntity());
		}

		return Owner;
	}

	bool EntityComponent::CanBeAttachedTo (Entity* ownerCandidate) const
	{
		return (VALID_OBJECT(ownerCandidate) && ownerCandidate != this);
	}

	void EntityComponent::DetachSelfFromOwner ()
	{
		if (!VALID_OBJECT(Owner))
		{
			return; //already detached
		}

		Owner->RemoveComponent(this);
	}

	Entity* EntityComponent::GetOwner () const
	{
		return Owner;
	}

	void EntityComponent::AttachTo (Entity* newOwner)
	{
		Owner = newOwner;
	}

	void EntityComponent::ComponentDetached ()
	{
		Owner = nullptr;
	}
}

#endif