/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  FLOAT.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "CoreClasses.h"

#if INCLUDE_CORE

using namespace std;

namespace SD
{
	FLOAT::FLOAT ()
	{
		Value = 0.f;
	}

	FLOAT::FLOAT (const float newValue)
	{
		Value = newValue;
	}

	FLOAT::FLOAT (const FLOAT& copyFloat)
	{
		Value = copyFloat.Value;
	}

	FLOAT::FLOAT (const INT& newValue)
	{
		Value = newValue.ToFLOAT().Value;
	}

	FLOAT::FLOAT (const DString& text)
	{
		try
		{
			Value = stof(text.String);
		}
		catch(invalid_argument&)
		{
			LOG1(LOG_WARNING, TXT("Cannot construct a FLOAT from %s.  Defaulting to 0."), text);
			Value = 0.f;
		}
		catch(out_of_range&)
		{
			LOG2(LOG_WARNING, TXT("There's not enough space to fit %s within a float.  Defaulting to FLT_MAX_10_EXP(%s)."), text, FLOAT(FLT_MAX_10_EXP));
			Value = FLT_MAX_10_EXP;
		}
	}

	void FLOAT::operator= (const FLOAT& copyFloat)
	{
		Value = copyFloat.Value;
	}

	void FLOAT::operator= (const float otherFloat)
	{
		Value = otherFloat;
	}

	FLOAT FLOAT::operator++ ()
	{
		return ++Value;
	}

	FLOAT FLOAT::operator++ (int)
	{
		return Value++;
	}

	FLOAT FLOAT::operator- () const
	{
		return -Value;
	}

	FLOAT FLOAT::operator-- ()
	{
		return --Value;
	}

	FLOAT FLOAT::operator-- (int)
	{
		return Value--;
	}

#if 0
	bool FLOAT::operator== (const FLOAT& otherFloat) const
	{
		return (Value == otherFloat.Value);
	}

	bool FLOAT::operator!= (const FLOAT& otherFloat) const
	{
		return (Value != otherFloat.Value);
	}

	bool FLOAT::operator< (const FLOAT& otherFloat) const
	{
		return (Value < otherFloat.Value);
	}

	bool FLOAT::operator<= (const FLOAT& otherInt) const
	{
		return (Value <= otherInt.Value);
	}

	bool FLOAT::operator> (const FLOAT& otherFloat) const
	{
		return (Value > otherFloat.Value);
	}

	bool FLOAT::operator>= (const FLOAT& otherFloat) const
	{
		return (Value >= otherFloat.Value);
	}

	FLOAT FLOAT::operator+ (const FLOAT& otherFloat) const
	{
		return FLOAT(Value + otherFloat.Value);
	}

	void FLOAT::operator+= (const FLOAT& otherFloat)
	{
		Value += otherFloat.Value;
	}

	FLOAT FLOAT::operator++ ()
	{
		return ++Value;
	}

	FLOAT FLOAT::operator++ (int)
	{
		return Value++;
	}

	FLOAT FLOAT::operator- (const FLOAT& otherFloat) const
	{
		return FLOAT(Value - otherFloat.Value);
	}

	FLOAT FLOAT::operator- () const
	{
		return -Value;
	}

	void FLOAT::operator-= (const FLOAT& otherInt)
	{
		Value -= otherInt.Value;
	}

	FLOAT FLOAT::operator-- ()
	{
		return --Value;
	}

	FLOAT FLOAT::operator-- (int)
	{
		return Value--;
	}

	FLOAT FLOAT::operator* (const FLOAT& otherFloat) const
	{
		return FLOAT(Value * otherFloat.Value);
	}

	void FLOAT::operator*= (const FLOAT& otherFloat)
	{
		Value *= otherFloat.Value;
	}

	FLOAT FLOAT::operator/ (const FLOAT& otherFloat) const
	{
#ifdef DEBUG_MODE
		if (otherFloat == 0)
		{
			LOG(LOG_WARNING, TXT("Attempting to divide ") + ToString() + TXT(" by zero!"));
			return FLOAT(Value);
		}
#endif
		return (otherFloat.Value != 0) ? FLOAT(Value / otherFloat.Value) : FLOAT(Value);
	}

	void FLOAT::operator/= (const FLOAT& otherFloat)
	{
		if (otherFloat.Value != 0)
		{
			Value /= otherFloat.Value;
		}
#ifdef DEBUG_MODE
		else
		{
			LOG(LOG_WARNING, TXT("Attempting to divide ") + ToString() + TXT(" by zero!"));
		}
#endif
	}

	void FLOAT::operator%= (const FLOAT& otherFloat)
	{
		Value = fmod(Value, otherFloat.Value);
	}

	FLOAT FLOAT::operator% (const FLOAT& otherFloat) const
	{
		return FLOAT(fmod(Value, otherFloat.Value));
	}
#endif

	DString FLOAT::ToString () const
	{
		return DString::MakeString(Value);
	}

	FLOAT FLOAT::MakeFloat (int value)
	{
		return INT(value).ToFLOAT();
	}

	FLOAT FLOAT::MakeFloat (unsigned int value)
	{
		return INT(value).ToFLOAT();
	}

#ifdef PLATFORM_64BIT
	FLOAT FLOAT::MakeFloat (const size_t& value)
	{
		return INT(value).ToFLOAT();
	}
#endif

	INT FLOAT::ToINT () const
	{
		return INT(static_cast<int>(trunc(Value)));
	}

	FLOAT FLOAT::Abs (const FLOAT value)
	{
		return FLOAT(abs(value.Value));
	}

	void FLOAT::Abs ()
	{
		abs(Value);
	}

	FLOAT FLOAT::Round (const FLOAT value)
	{
		return FLOAT(round(value.Value));
	}

	void FLOAT::Round ()
	{
		Value = round(Value);
	}

	void FLOAT::Roundf (INT numDecimals)
	{
		float multiplier = pow(10.f, numDecimals.ToFLOAT().Value);
		Value = round(Value * multiplier)/multiplier;
	}

	FLOAT FLOAT::RoundUp (const FLOAT value)
	{
		return FLOAT(ceil(value.Value));
	}

	void FLOAT::RoundUp ()
	{
		Value = ceil(Value);
	}

	FLOAT FLOAT::RoundDown (const FLOAT value)
	{
		return FLOAT(floor(value.Value));
	}

	void FLOAT::RoundDown ()
	{
		Value = floor(Value);
	}

	float FLOAT::GetFloat (double target)
	{
		Value = (float)target;
		return Value;
	}

	double FLOAT::ToDouble () const
	{
		return static_cast<double>(Value);
	}

#pragma region "External Operators"
	bool operator== (const FLOAT& left, const FLOAT& right)
	{
		return (abs(left.Value - right.Value) < FLOAT_TOLERANCE);
	}

	bool operator== (const FLOAT& left, const float& right)
	{
		return (abs(left.Value - right) < FLOAT_TOLERANCE);
	}

	bool operator== (const float& left, const FLOAT& right)
	{
		return (abs(left - right.Value) < FLOAT_TOLERANCE);
	}

	bool operator!= (const FLOAT& left, const FLOAT& right)
	{
		return !(left.Value == right.Value);
	}

	bool operator!= (const FLOAT& left, const float& right)
	{
		return !(left.Value == right);
	}

	bool operator!= (const float& left, const FLOAT& right)
	{
		return !(left == right.Value);
	}

	bool operator< (const FLOAT& left, const FLOAT& right)
	{
		return (left.Value < right.Value);
	}

	bool operator< (const FLOAT& left, const float& right)
	{
		return (left.Value < right);
	}

	bool operator< (const float& left, const FLOAT& right)
	{
		return (left < right.Value);
	}

	bool operator<= (const FLOAT& left, const FLOAT& right)
	{
		return (left.Value <= right.Value);
	}

	bool operator<= (const FLOAT& left, const float& right)
	{
		return (left.Value <= right);
	}

	bool operator<= (const float& left, const FLOAT& right)
	{
		return (left <= right.Value);
	}

	bool operator> (const FLOAT& left, const FLOAT& right)
	{
		return (left.Value > right.Value);
	}

	bool operator> (const FLOAT& left, const float& right)
	{
		return (left.Value > right);
	}

	bool operator> (const float& left, const FLOAT& right)
	{
		return (left > right.Value);
	}

	bool operator>= (const FLOAT& left, const FLOAT& right)
	{
		return (left.Value >= right.Value);
	}

	bool operator>= (const FLOAT& left, const float& right)
	{
		return (left.Value >= right);
	}

	bool operator>= (const float& left, const FLOAT& right)
	{
		return (left >= right.Value);
	}

	FLOAT operator+ (const FLOAT& left, const FLOAT& right)
	{
		return FLOAT(left.Value + right.Value);
	}

	FLOAT operator+ (const FLOAT& left, const float& right)
	{
		return FLOAT(left.Value + right);
	}

	FLOAT operator+ (const float& left, const FLOAT& right)
	{
		return FLOAT(left + right.Value);
	}

	FLOAT& operator+= (FLOAT& left, const FLOAT& right)
	{
		left.Value += right.Value;
		return left;
	}

	FLOAT& operator+= (FLOAT& left, const float& right)
	{
		left.Value += right;
		return left;
	}

	float& operator+= (float& left, const FLOAT& right)
	{
		left += right.Value;
		return left;
	}

	FLOAT operator- (const FLOAT& left, const FLOAT& right)
	{
		return FLOAT(left.Value - right.Value);
	}

	FLOAT operator- (const FLOAT& left, const float& right)
	{
		return FLOAT(left.Value - right);
	}

	FLOAT operator- (const float& left, const FLOAT& right)
	{
		return FLOAT(left - right.Value);
	}

	FLOAT& operator-= (FLOAT& left, const FLOAT& right)
	{
		left.Value -= right.Value;
		return left;
	}

	FLOAT& operator-= (FLOAT& left, const float& right)
	{
		left.Value -= right;
		return left;
	}

	float& operator-= (float& left, const FLOAT& right)
	{
		left -= right.Value;
		return left;
	}

	FLOAT operator* (const FLOAT& left, const FLOAT& right)
	{
		return FLOAT(left.Value * right.Value);
	}

	FLOAT operator* (const FLOAT& left, const float& right)
	{
		return FLOAT(left.Value * right);
	}

	FLOAT operator* (const float& left, const FLOAT& right)
	{
		return FLOAT(left * right.Value);
	}

	FLOAT& operator*= (FLOAT& left, const FLOAT& right)
	{
		left.Value *= right.Value;
		return left;
	}

	FLOAT& operator*= (FLOAT& left, const float& right)
	{
		left.Value *= right;
		return left;
	}

	float& operator*= (float& left, const FLOAT& right)
	{
		left *= right.Value;
		return left;
	}

	FLOAT operator/ (const FLOAT& left, const FLOAT& right)
	{
#ifdef DEBUG_MODE
		if (right.Value == 0.f)
		{
			LOG1(LOG_WARNING, TXT("Attempting to divide %s by zero!"), left);
			return left;
		}
#endif

		return (right.Value != 0.f) ? FLOAT(left.Value / right.Value) : left;
	}

	FLOAT operator/ (const FLOAT& left, const float& right)
	{
#ifdef DEBUG_MODE
		if (right == 0.f)
		{
			LOG1(LOG_WARNING, TXT("Attempting to divide %s by zero!"), left);
			return left;
		}
#endif

		return (right != 0.f) ? FLOAT(left.Value / right) : left;
	}

	FLOAT operator/ (const float& left, const FLOAT& right)
	{
#ifdef DEBUG_MODE
		if (right.Value == 0.f)
		{
			LOG1(LOG_WARNING, TXT("Attempting to divide %s by zero!"), FLOAT(left));
			return FLOAT(left);
		}
#endif

		return (right.Value != 0.f) ? FLOAT(left / right.Value) : FLOAT(left);
	}

	FLOAT& operator/= (FLOAT& left, const FLOAT& right)
	{
		if (right.Value != 0.f)
		{
			left.Value /= right.Value;
		}
#ifdef DEBUG_MODE
		else
		{
			LOG1(LOG_WARNING, TXT("Attempting to divide %s by zero!"), left);
		}
#endif
		
		return left;
	}

	FLOAT& operator/= (FLOAT& left, const float& right)
	{
		if (right != 0.f)
		{
			left.Value /= right;
		}
#ifdef DEBUG_MODE
		else
		{
			LOG1(LOG_WARNING, TXT("Attempting to divide %s by zero!"), left);
		}
#endif
		
		return left;
	}

	float& operator/= (float& left, const FLOAT& right)
	{
		if (right.Value != 0.f)
		{
			left /= right.Value;
		}
#ifdef DEBUG_MODE
		else
		{
			LOG1(LOG_WARNING, TXT("Attempting to divide %s by zero!"), FLOAT(left));
		}
#endif
		
		return left;
	}

	FLOAT operator% (const FLOAT& left, const FLOAT& right)
	{
#ifdef DEBUG_MODE
		if (right.Value == 0.f)
		{
			LOG1(LOG_WARNING, TXT("Attempting to modulo %s by zero!"), left);
			return left;
		}
#endif

		return (right.Value != 0.f) ? FLOAT(fmod(left.Value, right.Value)) : left;
	}

	FLOAT operator% (const FLOAT& left, const float& right)
	{
#ifdef DEBUG_MODE
		if (right == 0.f)
		{
			LOG1(LOG_WARNING, TXT("Attempting to modulo %s by zero!"), left);
			return left;
		}
#endif

		return (right != 0.f) ? FLOAT(fmod(left.Value, right)) : left;
	}

	FLOAT operator% (const float& left, const FLOAT& right)
	{
#ifdef DEBUG_MODE
		if (right.Value == 0.f)
		{
			LOG1(LOG_WARNING, TXT("Attempting to modulo %s by zero!"), FLOAT(left));
			return left;
		}
#endif

		return (right.Value != 0.f) ? FLOAT(fmod(left, right.Value)) : FLOAT(left);
	}

	FLOAT& operator%= (FLOAT& left, const FLOAT& right)
	{
		if (right.Value != 0.f)
		{
			left.Value = fmod(left.Value, right.Value);
		}
#ifdef DEBUG_MODE
		else
		{
			LOG1(LOG_WARNING, TXT("Attempting to modulo %s by zero!"), left);
		}
#endif
		
		return left;
	}

	FLOAT& operator%= (FLOAT& left, const float& right)
	{
		if (right != 0.f)
		{
			left.Value = fmod(left.Value, right);
		}
#ifdef DEBUG_MODE
		else
		{
			LOG1(LOG_WARNING, TXT("Attempting to modulo %s by zero!"), left);
		}
#endif
		
		return left;
	}

	float& operator%= (float& left, const FLOAT& right)
	{
		if (right.Value != 0.f)
		{
			left = fmod(left, right.Value);
		}
#ifdef DEBUG_MODE
		else
		{
			LOG1(LOG_WARNING, TXT("Attempting to modulo %s by zero!"), FLOAT(left));
		}
#endif
		
		return left;
	}

#if 0
	bool operator== (float a, const FLOAT& b)
	{
		return (a == b.Value);
	}

	bool operator!= (float a, const FLOAT& b)
	{
		return (a != b.Value);
	}

	bool operator< (float a, const FLOAT& b)
	{
		return (a < b.Value);
	}

	bool operator<= (float a, const FLOAT& b)
	{
		return (a <= b.Value);
	}
	bool operator> (float a, const FLOAT& b)
	{
		return (a > b.Value);
	}

	bool operator>= (float a, const FLOAT& b)
	{
		return (a >= b.Value);
	}

	FLOAT operator+ (float a, const FLOAT& b)
	{
		return FLOAT(a + b.Value);
	}

	FLOAT operator- (float a, const FLOAT& b)
	{
		return FLOAT(a - b.Value);
	}

	FLOAT operator* (float a, const FLOAT& b)
	{
		return FLOAT(a * b.Value);
	}

	FLOAT operator/ (float a, const FLOAT& b)
	{
#ifdef DEBUG_MODE
		if (b == 0)
		{
			LOG1(LOG_WARNING, TXT("Attempting to divide %s by zero!"), FLOAT(a));
			return FLOAT(a);
		}
#endif
		return (b != 0) ? FLOAT(a/b.Value) : FLOAT(a);
	}

	FLOAT operator% (float a, const FLOAT& b)
	{
		return FLOAT(fmod(a, b.Value));
	}
#endif
#pragma endregion
}

#endif