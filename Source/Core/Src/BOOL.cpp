/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  BOOL.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "CoreClasses.h"

#if INCLUDE_CORE

using namespace std;

namespace SD
{
	int BOOL::LanguageID = -1;
	DString BOOL::ToTrueText = TXT("True");
	DString BOOL::ToFalseText = TXT("False");

	BOOL::BOOL ()
	{
		Value = false;
	}

	BOOL::BOOL (const bool newValue)
	{
		Value = newValue;
	}

	BOOL::BOOL (const BOOL& copyBool)
	{
		Value = copyBool.Value;
	}

	BOOL::BOOL (const INT& intValue)
	{
		Value = (intValue == 0) ? false : true;
	}

	BOOL::BOOL (const FLOAT& floatValue)
	{
		Value = (floatValue == 0) ? false : true;
	}

	BOOL::BOOL (const DString& stringValue)
	{
		Value = stringValue.ToBool();
	}

	void BOOL::operator= (const BOOL& copyBool)
	{
		Value = copyBool.Value;
	}

	void BOOL::operator= (const bool otherBool)
	{
		Value = otherBool;
	}

	BOOL::operator bool () const
	{
		return Value;
	}

	bool BOOL::operator ! () const
	{
		return !Value;
	}

#if !INCLUDE_LOCALIZATION
	DString BOOL::ToString () const
	{
		return (Value) ? TXT("True") : TXT("False");
	}
#endif

	FLOAT BOOL::ToFLOAT () const
	{
		return (Value) ? FLOAT(1.f) : FLOAT(0.f);
	}

	INT BOOL::ToINT () const
	{
		return (Value) ? INT(1) : INT(0);
	}

#pragma region "External Operators"
	bool operator== (const BOOL& left, const BOOL& right)
	{
		return (left.Value == right.Value);
	}

	bool operator== (const BOOL& left, const bool& right)
	{
		return (left.Value == right);
	}

	bool operator== (const bool& left, const BOOL& right)
	{
		return (left == right.Value);
	}

	bool operator!= (const BOOL& left, const BOOL& right)
	{
		return (left.Value != right.Value);
	}

	bool operator!= (const BOOL& left, const bool& right)
	{
		return (left.Value != right);
	}

	bool operator!= (const bool& left, const BOOL& right)
	{
		return (left != right.Value);
	}
#pragma endregion
}

#endif