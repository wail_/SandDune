/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Matrix.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "CoreClasses.h"

#if INCLUDE_CORE

using namespace std;

namespace SD
{
	Matrix::Matrix (INT numRows, INT numColumns)
	{
		Rows = numRows;
		Columns = numColumns;
	}

	Matrix::Matrix (INT numRows, INT numColumns, const vector<FLOAT>& inData)
	{
		Rows = numRows;
		Columns = numColumns;
		Data = inData;
	}

	Matrix::Matrix (const Matrix& copyMatrix)
	{
		Rows = copyMatrix.Rows;
		Columns = copyMatrix.Columns;
		Data = copyMatrix.Data;
	}

	Matrix::~Matrix ()
	{
	
	}

	void Matrix::operator= (const Matrix& copyMatrix)
	{
		Rows = copyMatrix.Rows;
		Columns = copyMatrix.Columns;
		Data = copyMatrix.Data;
	}

	DString Matrix::ToString () const
	{
		if (!IsValid())
		{
			return TXT("<Invalid Matrix>");
		}

		DString result = result.Format(TXT("%sx%s ("), {Rows.ToString().ToCString(), Columns.ToString().ToCString()});
		for (unsigned int i = 0; i < Data.size(); i++)
		{
			result += Data.at(i).ToString() + TXT(", ");
		}

		//Replace the trailing ", " with ")"
		result = result.SubString(0, result.Length() - 3);
		return result + TXT(")");
	}

	Matrix Matrix::GetVoidMatrix ()
	{
		vector<FLOAT> resultData;
		resultData.push_back(0.f);

		return Matrix(1, 1, resultData);
	}

	FLOAT Matrix::DotProduct (const vector<FLOAT>& left, const vector<FLOAT>& right)
	{
		CHECK(left.size() == right.size());

		FLOAT result = 0.f;
		for (unsigned int i = 0; i < left.size(); i++)
		{
			result += (left.at(i) * right.at(i));
		}

		return result;
	}

#ifdef DEBUG_MODE
	void Matrix::LogMatrix () const
	{
		if (!IsValid())
		{
			LOG3(LOG_DEBUG, TXT("<Invalid Matrix>    ---    CurRows: %s, CurColumns: %s, DataSize: %s"), GetNumRows(), GetNumColumns(), INT(Data.size()));
			return;
		}

		for (INT curRow = 0; curRow < GetNumRows(); curRow++)
		{
			DString rowText = TXT("[ ");
			for (INT curColumn = 0; curColumn < GetNumColumns(); curColumn++)
			{
				rowText += TXT(" ") + GetDataElement(curRow, curColumn).ToString() + TXT(" ");
			}

			rowText += TXT(" ]");
			LOG(LOG_DEFAULT, rowText);
		}
	}
#endif

	bool Matrix::IsValid () const
	{
		return (Data.size() > 0 && Columns > 0 && Rows > 0 && static_cast<int>(Data.size()) == (Rows * Columns));
	}

	bool Matrix::IsSquare () const
	{
		return Rows == Columns;
	}

	bool Matrix::IsUpperTriangularMatrix () const
	{
		/*
		An example of a upper triangular matrix
		[a	b	c	d	e]
		[0	f	g	h	i]
		[0	0	j	k	l]
		[0	0	0	m	n]
		[0	0	0	0	x]
		*/
		if (!IsSquare() || !IsValid())
		{
			return false;
		}

		for (INT curColumn = 0; curColumn < Columns; curColumn++)
		{
			for (INT curRow = curColumn + 1; curRow < Rows; curRow++)
			{
				if (GetDataElement(curRow, curColumn) != 0.f)
				{
					return false;
				}
			}
		}

		return true;
	}

	bool Matrix::IsLowerTriangularMatrix () const
	{
		/*
		An example of a lower triangular matrix
		[a	0	0	0	0]
		[b	c	0	0	0]
		[d	e	f	0	0]
		[g	h	i	j	0]
		[k	l	m	n	x]
		*/
		if (!IsSquare() || !IsValid())
		{
			return false;
		}

		for (INT curRow = 0; curRow < Rows; curRow++)
		{
			for (INT curColumn = curRow + 1; curColumn < Columns; curColumn++)
			{
				if (GetDataElement(curRow, curColumn) != 0.f)
				{
					return false;
				}
			}
		}

		return true;
	}

	bool Matrix::IsUnitriangularMatrix () const
	{
		/*
		An example of a unitriangular matrix
		[1	0	0	0	0]
		[A	1	0	0	0]
		[B	C	1	0	0]
		[D	E	F	1	0]
		[G	H	I	J	1]
		*/
		if (!IsSquare() || !IsValid())
		{
			return false;
		}

		//Check main diagonal first before checking if it's either lower or upper (for increased liklihood of returning false early).
		for (INT curRow = 0; curRow < Rows; curRow++)
		{
			if (GetDataElement(curRow, curRow) != 1.f)
			{
				return false;
			}
		}

		return (IsUpperTriangularMatrix() || IsLowerTriangularMatrix());
	}

	bool Matrix::IsUnitMatrix () const
	{
		if (!IsUpperTriangularMatrix() || !IsLowerTriangularMatrix())
		{
			return false;
		}

		//Do the same as unitriangular matrix without having to check lower/upper triangular matrices (for performance)
		for (INT curRow = 0; curRow < Rows; curRow++)
		{
			if (GetDataElement(curRow, curRow) != 1.f)
			{
				return false;
			}
		}

		return true;
	}

	bool Matrix::GetIdentityMatrix (Matrix& outIdentity) const
	{
		if (!IsValid() || !IsSquare())
		{
			return false;
		}

		/*
		The identity matrix should have ones diagonally (from top left corner to bottom right corner).
		All other elements should be zero.
		1	0	0	0
		0	1	0	0
		0	0	1	0
		0	0	0	1
		*/
		outIdentity = Matrix(Rows, Columns);
		outIdentity.Data.resize((outIdentity.GetNumRows() * outIdentity.GetNumColumns()).ToUnsignedInt());
		for (unsigned int i = 0; i < Data.size(); i++)
		{
			//The pattern when 1's appear are...
			//0, 3 (for 2x2)
			//0, 4, 8 (for 3x3)
			//0, 5, 10, 15 (for 4x4)
			outIdentity.Data.at(i) = (i % (Rows+1) == 0) ? 1.f : 0.f;
		}

		return true;
	}

	FLOAT Matrix::CalculateDeterminant () const
	{
		if (!IsValid())
		{
			LOG(LOG_WARNING, TXT("Cannot compute the determinant of an invalid matrix."));
			return 0.f;
		}

		if (!IsSquare())
		{
			LOG2(LOG_WARNING, TXT("Cannot compute a determinant for non squared matrices.  Current dimensions are %sx%s"), GetNumRows(), GetNumColumns());
			return 0.f;
		}

		if (GetNumRows() > 4)
		{
			LOG2(LOG_WARNING, TXT("Computing determinants for %sx%s matrices are not supported.  Only matrices up to 4x4 are supported."), GetNumRows(), GetNumColumns());
			return 0.f;
		}

		switch (GetNumRows().Value)
		{
			case(1):
				return Data.at(0);
			case(2):
				return CalcDeterminant2x2();
			case(3):
				return CalcDeterminant3x3();
			case(4):
				return CalcDeterminant4x4();
		}

		return 0.f;
	}

	bool Matrix::CalculateAdjugate (Matrix& outAdjugate) const
	{
		if (!IsValid())
		{
			LOG(LOG_WARNING, TXT("Cannot compute the adjugate of an invalid matrix."));
			return false;
		}

		if (!IsSquare())
		{
			LOG1(LOG_WARNING, TXT("Computing adjugates for nonsquared matrices are not supported:  %s"), ToString());
			return false;
		}

		if (GetNumRows() > 4)
		{
			LOG2(LOG_WARNING, TXT("Computing adjugates for matrices with dimensions greater than 4x4 is not supported.  Dimensions for current matrix is:  %sx%s"), GetNumRows(), GetNumColumns());
			return false;
		}

		outAdjugate = *this;
		switch (GetNumRows().Value)
		{
			case(1):
				break;
			case(2):
				CalcAdjugate2x2(outAdjugate);
				break;
			case(3):
				CalcAdjugate3x3(outAdjugate);
				break;
			case(4):
				CalcAdjugate4x4(outAdjugate);
		}

		return true;
	}

	bool Matrix::CalculateInverse (Matrix& outInverse) const
	{
		if (!IsValid())
		{
			LOG(LOG_WARNING, TXT("Cannot compute the inverse of an invalid matrix."));
			return false;
		}

		if (!IsSquare())
		{
			LOG1(LOG_WARNING, TXT("Computing inverses for nonsquared matrices are not supported:  %s"), ToString());
			return false;
		}

		if (GetNumRows() > 4)
		{
			LOG2(LOG_WARNING, TXT("Computing inverses for matrices with dimensions greater than 4x4 is not supported.  Dimensions for current matrix is:  %sx%s"), GetNumRows(), GetNumColumns());
			return false;
		}

		outInverse = *this;
		switch (GetNumRows().Value)
		{
			case(1):
				if (Data.at(0) != 0.f)
				{
					outInverse.Data.at(0) = 1/Data.at(0);
					return true;
				}

				return false;

			case(2):
				return CalcInverse2x2(outInverse);
			case(3):
				return CalcInverse3x3(outInverse);
			case(4):
				return CalcInverse4x4(outInverse);
		}

		return false;
	}

	Matrix Matrix::GetZeroMatrix () const
	{
		Matrix results(GetNumRows(), GetNumColumns());
		unsigned int matrixSize = (results.GetNumRows() * results.GetNumColumns()).ToUnsignedInt();
		results.Data.resize(matrixSize);
		for (unsigned int i = 0; i < matrixSize; i++)
		{
			results.Data.at(i) = 0.f;
		}

		return results;
	}

	Matrix Matrix::GetComatrix () const
	{
		/*
		A Comatrix will have a checkboard effect of signed numbers.  Example listed below:
		Original Matrix		Comatrix Signage
		[a	b	c	d]		[+	-	+	-] =>	[ a	-b	 c	-d]
		[e	f	g	h]		[-	+	-	+] =>	[-e	 f	-g	 h]
		[i	j	k	l]		[+	-	+	-] =>	[ i	-j	 k	-l]
		[m	n	o	p]		[-	+	-	+] =>	[-m	 n	-o	 p]
		*/
		Matrix result(*this);

		for (INT curColumn = 0; curColumn < GetNumColumns(); curColumn++)
		{
			INT negOffset = ((curColumn%2) == 0) ? 1 : 0;
			for (INT curRow = negOffset; curRow < GetNumRows(); curRow += 2)
			{
				result.Data.at(result.GetDataIndex(curRow, curColumn).ToUnsignedInt()) *= -1.f;
			}
		}

		return result;
	}

	Matrix Matrix::GetSubMatrix (INT rowIdx, INT columnIdx) const
	{
		if (rowIdx < 0 || columnIdx < 0)
		{
			LOG2(LOG_WARNING, TXT("Cannot obtain a submatrix using negative indices (%s, %s)"), rowIdx, columnIdx);
			return Matrix::GetVoidMatrix();
		}

		if (!IsValid())
		{
			LOG(LOG_WARNING, TXT("Cannot obtain a submatrix using an invalid matrix."));
			return Matrix::GetVoidMatrix();
		}

		if (Rows * Columns == 1)
		{
			LOG1(LOG_WARNING, TXT("Cannot obtain a submatrix from a 1x1 matrix:  %s"), ToString());
			return Matrix::GetVoidMatrix();
		}

		Matrix result(Rows - 1, Columns - 1);
		vector<FLOAT> subData;

		//Caution, the order of the nested loops are important since the data pushed into the subData depends on that order.
		for (INT curColumn = 0; curColumn < GetNumColumns(); curColumn++)		
		{
			if (curColumn == columnIdx)
			{
				continue;
			}

			for (INT curRow = 0; curRow < GetNumRows(); curRow++)
			{
				if (curRow == rowIdx)	
				{
					continue;
				}

				subData.push_back(GetDataElement(curRow, curColumn));
			}
		}

		result.Data = subData;
		return result;
	}

	bool Matrix::GetMatrixOfMinors (Matrix& outMatrixOfMinors) const
	{
		if (!IsValid() || !IsSquare() || GetNumRows() < 2)
		{
			return false;
		}

		if (GetNumRows() > 4)
		{
			LOG2(LOG_WARNING, TXT("Unable to get a matrix of minors for a matrix of size %sx%s since this function supports up to matrices of size 4x4"), GetNumRows(), GetNumColumns());
			return false;
		}

		outMatrixOfMinors = Matrix(GetNumRows(), GetNumColumns());
		vector<FLOAT> minorMatrix;

		for (INT curColumn = 0; curColumn < GetNumColumns(); curColumn++)
		{
			for (INT curRow = 0; curRow < GetNumRows(); curRow++)
			{
				FLOAT minorDeterminant = GetSubMatrix(curRow, curColumn).CalculateDeterminant();

				minorMatrix.push_back(minorDeterminant);
			}
		}

		outMatrixOfMinors.Data = minorMatrix;
		return true;
	}

	INT Matrix::GetNumRows () const
	{
		return Rows;
	}

	INT Matrix::GetNumColumns () const
	{
		return Columns;
	}

	vector<FLOAT> Matrix::GetRow (INT rowIdx) const
	{
		vector<FLOAT> result;
		GetRow(rowIdx, result);

		return result;
	}

	void Matrix::GetRow (INT rowIdx, std::vector<FLOAT>& outRowData) const
	{
		/*
		To obtain row idx 1
		0	0	0	0	0
		x	x	x	x	x
		0	0	0	0	0
		*/
		outRowData.clear();
		outRowData.resize(Columns.ToUnsignedInt());
		for (INT i = 0; i < Columns; i++)
		{
			outRowData.at(i.ToUnsignedInt()) = Data.at((rowIdx + (Rows * i)).ToUnsignedInt());
		}
	}

	vector<FLOAT> Matrix::GetColumn (INT columnIdx) const
	{
		vector<FLOAT> result;
		GetColumn(columnIdx, result);

		return result;
	}

	void Matrix::GetColumn (INT columnIdx, std::vector<FLOAT>& outColumnData) const
	{
		/*
		To obtain column idx 1
		0	x	0	0	0
		0	x	0	0	0
		0	x	0	0	0
		*/
		outColumnData.clear();
		outColumnData.resize(Rows.ToUnsignedInt());
		INT firstElementIdx = Rows * columnIdx; //Data element at which the targetted column begins
		for (INT i = 0; i < Rows; i++)
		{
			outColumnData.at(i.ToUnsignedInt()) = Data.at((i + firstElementIdx).ToUnsignedInt());
		}
	}

	FLOAT Matrix::GetDataElement (INT rowIdx, INT columnIdx) const
	{
		/*
		To obtain column idx 2, row idx 1
		0	0	0	0	0
		0	0	x	0	0
		0	0	0	0	0
		*/
		return Data.at(GetDataIndex(rowIdx, columnIdx).ToUnsignedInt());
	}

	INT Matrix::GetDataIndex (INT rowIdx, INT columnIdx) const
	{
		return (columnIdx * Rows) + rowIdx;
	}

	FLOAT Matrix::CalcDeterminant2x2 () const
	{
		/*
		Determinant of a 2x2 matrix is:
		[a	b] = ad - bc
		[c	d]
		*/
		CHECK(GetNumColumns() == 2 && GetNumRows() == 2);
		return ((Data.at(0) * Data.at(3)) - (Data.at(2) * Data.at(1)));
	}

	FLOAT Matrix::CalcDeterminant3x3 () const
	{
		/*
		Determinant of a 3x3 matrix is:
		[0	3	6] [a	b	c] = a[e	f] - b[d	f] + c[d	e] = a(ei - fh) - b(di - fg) + c(dh - eg)
		[1	4	7] [d	e	f]	  [h	i]	  [g	i]	  [g	h]
		[2	5	8] [g	h	i]
		*/
		CHECK(GetNumColumns() == 3 && GetNumRows() == 3);
		return	(Data.at(0) * ((Data.at(4) * Data.at(8)) - (Data.at(7) * Data.at(5)))) -
				(Data.at(3) * ((Data.at(1) * Data.at(8)) - (Data.at(7) * Data.at(2)))) +
				(Data.at(6) * ((Data.at(1) * Data.at(5)) - (Data.at(4) * Data.at(2))));
	}

	FLOAT Matrix::CalcDeterminant4x4 () const
	{
		/*
		Complexity for determinants of increasing dimensions increases exponentially.  It may still seem safer/stable to hardcode the formula rather than figuring out the upper triangle.  Possibly performance increase?
		Determinant of a 4x4 matrix is:
		[a	b	c	d] = a	[f	g	h] - b	[e	g	h] + c	[e	f	h] - d	[e	f	g]
		[e	f	g	h]		[j	k	l]		[i	k	l]		[i	j	l]		[i	j	k]
		[i	j	k	l]		[n	o	p]		[m	o	p]		[m	n	p]		[m	n	o]
		[m	n	o	p]

		Alternatively, we can try placing this long formula which is faster than creating sub matrices:  http://stackoverflow.com/questions/2922690/calculating-an-nxn-matrix-determinant-in-c-sharp
		*/
		CHECK(GetNumColumns() == 4 && GetNumRows() == 4);
		vector<FLOAT> subMatrix;
		subMatrix.resize(9);

		//A
		subMatrix.at(0) = Data.at(5);
		subMatrix.at(1) = Data.at(6);
		subMatrix.at(2) = Data.at(7);
		subMatrix.at(3) = Data.at(9);
		subMatrix.at(4) = Data.at(10);
		subMatrix.at(5) = Data.at(11);
		subMatrix.at(6) = Data.at(13);
		subMatrix.at(7) = Data.at(14);
		subMatrix.at(8) = Data.at(15);
		const Matrix a(3, 3, subMatrix);

		//B
		subMatrix.at(0) = Data.at(1);
		subMatrix.at(1) = Data.at(2);
		subMatrix.at(2) = Data.at(3);
		const Matrix b(3, 3, subMatrix);

		//C
		subMatrix.at(3) = Data.at(5);
		subMatrix.at(4) = Data.at(6);
		subMatrix.at(5) = Data.at(7);
		const Matrix c(3, 3, subMatrix);

		//D
		subMatrix.at(6) = Data.at(9);
		subMatrix.at(7) = Data.at(10);
		subMatrix.at(8) = Data.at(11);
		const Matrix d(3, 3, subMatrix);

		return ((Data.at(0) * a.CalcDeterminant3x3()) - (Data.at(4) * b.CalcDeterminant3x3()) + (Data.at(8) * c.CalcDeterminant3x3()) - (Data.at(12) * d.CalcDeterminant3x3()));
	}

	void Matrix::CalcAdjugate2x2 (Matrix& outAdjugate) const
	{
		/*
		[a	b]	=>	[ d	-b]
		[c	d]		[-c	 a]
		*/
		outAdjugate.Data.at(0) = Data.at(3);
		outAdjugate.Data.at(1) = Data.at(1) * -1.f;
		outAdjugate.Data.at(2) = Data.at(2) * -1.f;
		outAdjugate.Data.at(3) = Data.at(0);
	}

	//Conversions for matrices 3x3 and 4x4 were generated from:  http://www.wolframalpha.com/input/?i=adjugate
	void Matrix::CalcAdjugate3x3 (Matrix& outAdjugate) const
	{
		/*
		[a	b	c]	=>	[ei-fh		ch-bi		bf-ce]
		[d	e	f]		[fg-di		ai-cg		cd-af]
		[g	h	i]		[dh-eg		bg-ah		ae-bd]
		*/
		//Switching to unchecked vector accessors for (very minor) performance increase (and for less typing)
		//1st Column
		outAdjugate.Data[0] = (Data[4] * Data[8]) - (Data[7] * Data[5]);
		outAdjugate.Data[1] = (Data[7] * Data[2]) - (Data[1] * Data[8]);
		outAdjugate.Data[2] = (Data[1] * Data[5]) - (Data[4] * Data[2]);

		//2nd Column
		outAdjugate.Data[3] = (Data[6] * Data[5]) - (Data[3] * Data[8]);
		outAdjugate.Data[4] = (Data[0] * Data[8]) - (Data[6] * Data[2]);
		outAdjugate.Data[5] = (Data[3] * Data[2]) - (Data[0] * Data[5]);

		//3rd Column
		outAdjugate.Data[6] = (Data[3] * Data[7]) - (Data[6] * Data[4]);
		outAdjugate.Data[7] = (Data[6] * Data[1]) - (Data[0] * Data[7]);
		outAdjugate.Data[8] = (Data[0] * Data[4]) - (Data[3] * Data[1]);
	}

	void Matrix::CalcAdjugate4x4 (Matrix& outAdjugate) const
	{
		/*
		Indices			Letter mapping
		[0	4	8	12][a	b	c	d]	=>	[fkp-flo-gjp+gln+hjo-hkn	-bkp+blo+cjp-cln-djo+dkn	bgp-bho-cfp+chn+dfo-dgn		-bgl+bhk+cfl-chj-dfk+dgj]
		[1	5	9	13][e	f	g	h]		[-ekp+elo+gip-glm-hio+hkm	akp-alo-cip+clm+dio-dkm		-agp+aho+cep-chm-deo+dgm	agl-ahk-cel+chi+dek-dgi	]
		[2	6	10	14][i	j	k	l]		[ejp-eln-fip+flm+hin-hjm	-ajp+aln+bip-blm-din+djm	afp-ahn-bep+bhm+den-dfm		-afl+ahj+bel-bhi-dej+dfi]
		[3	7	11	15][m	n	o	p]		[-ejo+ekn+fio-fkm-gin+gjm	ajo-akn-bio+bkm+cin-cjm		-afo+agn+beo-bgm-cen+cfm	afk-agj-bek+bgi+cej-cfi	]
											....erm....this is error prone
		*/

		//						first term							second term
		//						third term							fourth term
		//						fifth term							sixth term
		//1st column
		outAdjugate.Data[0] =	(Data[5] * Data[10] * Data[15]) -	(Data[5] * Data[14] * Data[11]) -
								(Data[9] * Data[6] * Data[15]) +	(Data[9] * Data[14] * Data[7]) +
								(Data[13] * Data[6] * Data[11]) -	(Data[13] * Data[10] * Data[7]);
		outAdjugate.Data[1] =	(-Data[1] * Data[10] * Data[15]) +	(Data[1] * Data[14] * Data[11]) +
								(Data[9] * Data[2] * Data[15]) -	(Data[9] * Data[14] * Data[3]) -
								(Data[13] * Data[2] * Data[11]) +	(Data[13] * Data[10] * Data[3]);
		outAdjugate.Data[2] =	(Data[1] * Data[6] * Data[15]) -	(Data[1] * Data[14] * Data[7]) -
								(Data[5] * Data[2] * Data[15]) +	(Data[5] * Data[14] * Data[3]) +
								(Data[13] * Data[2] * Data[7]) -	(Data[13] * Data[6] * Data[3]);
		outAdjugate.Data[3] =	(-Data[1] * Data[6] * Data[11]) +	(Data[1] * Data[10] * Data[7]) +
								(Data[5] * Data[2] * Data[11]) -	(Data[5] * Data[10] * Data[3]) -
								(Data[9] * Data[2] * Data[7]) +		(Data[9] * Data[6] * Data[3]);

		//2nd column
		outAdjugate.Data[4] =	(-Data[4] * Data[10] * Data[15]) +	(Data[4] * Data[14] * Data[11]) +
								(Data[8] * Data[6] * Data[15]) -	(Data[8] * Data[14] * Data[7]) -
								(Data[12] * Data[6] * Data[11]) +	(Data[12] * Data[10] * Data[7]);
		outAdjugate.Data[5] =	(Data[0] * Data[10] * Data[15]) -	(Data[0] * Data[14] * Data[11]) -
								(Data[8] * Data[2] * Data[15]) +	(Data[8] * Data[14] * Data[3]) +
								(Data[12] * Data[2] * Data[11]) -	(Data[12] * Data[10] * Data[3]);

		outAdjugate.Data[6] =	(-Data[0] * Data[6] * Data[15]) +	(Data[0] * Data[14] * Data[7]) +
								(Data[4] * Data[2] * Data[15]) -	(Data[4] * Data[14] * Data[3]) -
								(Data[12] * Data[2] * Data[7]) +	(Data[12] * Data[6] * Data[3]);
		outAdjugate.Data[7] =	(Data[0] * Data[6] * Data[11]) -	(Data[0] * Data[10] * Data[7]) -
								(Data[4] * Data[2] * Data[11]) +	(Data[4] * Data[10] * Data[3]) +
								(Data[8] * Data[2] * Data[7]) -		(Data[8] * Data[6] * Data[3]);

		//3rd column
		outAdjugate.Data[8] =	(Data[4] * Data[9] * Data[15]) -	(Data[4] * Data[13] * Data[11]) -
								(Data[8] * Data[5] * Data[15]) +	(Data[8] * Data[13] * Data[7]) +
								(Data[12] * Data[5] * Data[11]) -	(Data[12] * Data[9] * Data[7]);
		outAdjugate.Data[9] =	(-Data[0] * Data[9] * Data[15]) +	(Data[0] * Data[13] * Data[11]) +
								(Data[8] * Data[1] * Data[15]) -	(Data[8] * Data[13] * Data[3]) -
								(Data[12] * Data[1] * Data[11]) +	(Data[12] * Data[9] * Data[3]);
		outAdjugate.Data[10] =	(Data[0] * Data[5] * Data[15]) -	(Data[0] * Data[13] * Data[7]) -
								(Data[4] * Data[1] * Data[15]) +	(Data[4] * Data[13] * Data[3]) +
								(Data[12] * Data[1] * Data[7]) -	(Data[12] * Data[5] * Data[3]);
		outAdjugate.Data[11] =	(-Data[0] * Data[5] * Data[11]) +	(Data[0] * Data[9] * Data[7]) +
								(Data[4] * Data[1] * Data[11]) -	(Data[4] * Data[9] * Data[3]) -
								(Data[8] * Data[1] * Data[7]) +		(Data[8] * Data[5] * Data[3]);

		//4th column
		outAdjugate.Data[12] =	(-Data[4] * Data[9] * Data[14]) +	(Data[4] * Data[13] * Data[10]) +
								(Data[8] * Data[5] * Data[14]) -	(Data[8] * Data[13] * Data[6]) -
								(Data[12] * Data[5] * Data[10]) +	(Data[12] * Data[9] * Data[6]);
		outAdjugate.Data[13] =	(Data[0] * Data[9] * Data[14]) -	(Data[0] * Data[13] * Data[10]) -
								(Data[8] * Data[1] * Data[14]) +	(Data[8] * Data[13] * Data[2]) +
								(Data[12] * Data[1] * Data[10]) -	(Data[12] * Data[9] * Data[2]);
		outAdjugate.Data[14] =	(-Data[0] * Data[5] * Data[14]) +	(Data[0] * Data[13] * Data[6]) +
								(Data[4] * Data[1] * Data[14]) -	(Data[4] * Data[13] * Data[2]) -
								(Data[12] * Data[1] * Data[6]) +	(Data[12] * Data[5] * Data[2]);
		outAdjugate.Data[15] =	(Data[0] * Data[5] * Data[10]) -	(Data[0] * Data[9] * Data[6]) -
								(Data[4] * Data[1] * Data[10]) +	(Data[4] * Data[9] * Data[2]) +
								(Data[8] * Data[1] * Data[6]) -		(Data[8] * Data[5] * Data[2]);
		//wipe sweat
	}

	bool Matrix::CalcInverse2x2 (Matrix& outInverse) const
	{
		//Formula for inverses:  Adjugate / Determinant
		FLOAT deter = CalcDeterminant2x2();
		outInverse = *this;

		if (deter == 0.f)
		{
#ifdef DEBUG_MODE
			LOG1(LOG_DEFAULT, TXT("This matrix is not invertible:  %s"), ToString());
#endif
			return false;
		}

		CalcAdjugate2x2(outInverse);
		outInverse *= (1/deter);

		return true;
	}

	bool Matrix::CalcInverse3x3 (Matrix& outInverse) const
	{
		FLOAT deter = CalcDeterminant3x3();
		outInverse = *this;

		if (deter == 0.f)
		{
#ifdef DEBUG_MODE
			LOG1(LOG_DEFAULT, TXT("This matrix is not invertible:  %s"), ToString());
#endif
			return false;
		}

		CalcAdjugate3x3(outInverse);
		outInverse *= (1/deter);

		return true;
	}

	bool Matrix::CalcInverse4x4 (Matrix& outInverse) const
	{
		FLOAT deter = CalcDeterminant4x4();
		outInverse = *this;

		if (deter == 0.f)
		{
#ifdef DEBUG_MODE
			LOG1(LOG_DEFAULT, TXT("This matrix is not invertible:  %s"), ToString());
#endif
			return false;
		}

		CalcAdjugate4x4(outInverse);
		outInverse *= (1/deter);

		return true;
	}

	bool operator== (const Matrix& left, const Matrix& right)
	{
		return (left.GetNumRows() == right.GetNumRows() && left.GetNumColumns() == right.GetNumColumns() && left.Data == right.Data);
	}

	bool operator!= (const Matrix& left, const Matrix& right)
	{
		return !(left == right);
	}

	Matrix operator+ (const Matrix& left, const Matrix& right)
	{
		if (left.GetNumRows() != right.GetNumRows() || left.GetNumColumns() != right.GetNumColumns())
		{
			LOG4(LOG_WARNING, TXT("Unable to get the sum of matrices with mismatching dimensions (%sx%s) != (%sx%s)"), left.GetNumRows(), left.GetNumColumns(), right.GetNumRows(), right.GetNumColumns());
			return Matrix::GetVoidMatrix();
		}

		Matrix result(left.GetNumRows(), left.GetNumColumns());
		result.Data.resize((result.GetNumRows() * result.GetNumColumns()).ToUnsignedInt());
		for (unsigned int i = 0; i < left.Data.size(); i++)
		{
			result.Data.at(i) = left.Data.at(i) + right.Data.at(i);
		}

		return result;
	}

	Matrix operator+= (Matrix& left, const Matrix& right)
	{
		if (left.GetNumRows() != right.GetNumRows() || left.GetNumColumns() != right.GetNumColumns())
		{
			LOG4(LOG_WARNING, TXT("Unable to get the sum of matrices with mismatching dimensions (%sx%s) != (%sx%s)"), left.GetNumRows(), left.GetNumColumns(), right.GetNumRows(), right.GetNumColumns());
			return left;
		}

		for (unsigned int i = 0; i < left.Data.size(); i++)
		{
			left.Data.at(i) += right.Data.at(i);
		}

		return left;
	}

	Matrix operator- (const Matrix& left, const Matrix& right)
	{
		if (left.GetNumRows() != right.GetNumRows() || left.GetNumColumns() != right.GetNumColumns())
		{
			LOG4(LOG_WARNING, TXT("Unable to get the difference of matrices with mismatching dimensions (%sx%s) != (%sx%s)"), left.GetNumRows(), left.GetNumColumns(), right.GetNumRows(), right.GetNumColumns());
			return Matrix::GetVoidMatrix();
		}

		Matrix result(left.GetNumRows(), left.GetNumColumns());
		result.Data.resize((result.GetNumRows() * result.GetNumColumns()).ToUnsignedInt());
		for (unsigned int i = 0; i < left.Data.size(); i++)
		{
			result.Data.at(i) = left.Data.at(i) - right.Data.at(i);
		}

		return result;
	}

	Matrix operator-= (Matrix& left, const Matrix& right)
	{
		if (left.GetNumRows() != right.GetNumRows() || left.GetNumColumns() != right.GetNumColumns())
		{
			LOG4(LOG_WARNING, TXT("Unable to get the difference of matrices with mismatching dimensions (%sx%s) != (%sx%s)"), left.GetNumRows(), left.GetNumColumns(), right.GetNumRows(), right.GetNumColumns());
			return left;
		}

		for (unsigned int i = 0; i < left.Data.size(); i++)
		{
			left.Data.at(i) -= right.Data.at(i);
		}

		return left;
	}

	Matrix operator* (const Matrix& left, const FLOAT& right)
	{
		Matrix results(left.GetNumRows(), left.GetNumColumns());
		results.Data.resize(left.Data.size());
		for (unsigned int i = 0; i < left.Data.size(); i++)
		{
			results.Data.at(i) = left.Data.at(i) * right;
		}

		return results;
	}

	Matrix operator* (const FLOAT& left, const Matrix& right)
	{
		Matrix results(right.GetNumRows(), right.GetNumColumns());
		results.Data.resize(right.Data.size());
		for (unsigned int i = 0; i < right.Data.size(); i++)
		{
			results.Data.at(i) = right.Data.at(i) * left;
		}

		return results;
	}

	Matrix& operator*= (Matrix& left, const FLOAT& right)
	{
		for (unsigned int i = 0; i < left.Data.size(); i++)
		{
			left.Data.at(i) *= right;
		}

		return left;
	}

	Matrix operator* (const Matrix& left, const Matrix& right)
	{
		Matrix results(Matrix(left.GetNumRows(), right.GetNumColumns()));
		if (left.GetNumColumns() != right.GetNumRows())
		{
			//Can't multiply these matrices
			LOG2(LOG_WARNING, TXT("Matrix %s is not compatible for matrix multiplication with %s"), left, right);
			return results;
		}

		results.Data.resize((results.GetNumRows() * results.GetNumColumns()).ToUnsignedInt());

		/* Matrix multiplication
		[a	c	e]		[g	j]		[ag + ch + ei		aj + ck + el]
		[b	d	f]	X	[h	k]	=	[bg + dh + fi		bj + dk + fl]
						[i	l]
		*/
		vector<FLOAT> leftRow;
		vector<FLOAT> rightColumn;
		for (INT curRow = 0; curRow < left.GetNumRows(); curRow++)
		{
			left.GetRow(curRow, leftRow);

			for (INT curColumn = 0; curColumn < right.GetNumColumns(); curColumn++)
			{
				right.GetColumn(curColumn, rightColumn);
				results.Data.at((curRow + (curColumn * results.GetNumRows())).ToUnsignedInt()) = Matrix::DotProduct(leftRow, rightColumn);
			}
		}

		return results;
	}

	Matrix& operator*= (Matrix& left, const Matrix& right)
	{
		//Create a matrix copy since we'll be reading from left matrix (left.GetRow) while writing to it.
		left = left * right;

		return left;
	}
}

#endif