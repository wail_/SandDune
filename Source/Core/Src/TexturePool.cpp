/*
=====================================================================
  <Any copyright stuff here>

  TexturePool.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "CoreClasses.h"

#if INCLUDE_CORE

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(TexturePool, ResourcePool)

	TexturePool* TexturePool::TexturePoolInstance = nullptr;

	bool TexturePool::CanBeCreated ()
	{
		return (!VALID_OBJECT(TexturePoolInstance));
	}

	void TexturePool::BeginObject ()
	{
		Super::BeginObject();

		IfGraphicsInstance(graphicsInstance)
		{
			graphicsInstance->RegisterResourcePool(this);
			TexturePoolInstance = this;
		}
		else
		{
			LOG(LOG_CRITICAL, TXT("Unable to register Texture Pool!  Couldn't find an instance of the graphics engine!"));
		}
	}

	void TexturePool::ReleaseResources ()
	{
		for (unsigned int i = 0; i < ImportedTextures.size(); i++)
		{
			ImportedTextures.at(i).TextureInstance->Release();
		}

		ImportedTextures.clear();
	}

	void TexturePool::RegisterTexture (Texture* newTexture, const DString& textureName)
	{
		SMaterialResource newResource;
		newResource.TextureInstance = newTexture;
		newResource.TextureName = textureName;

		ImportedTextures.push_back(newResource);
	}

	Texture* TexturePool::FindTexture (const DString& textureName)
	{
		for (unsigned int i = 0; i < ImportedTextures.size(); i++)
		{
			if (ImportedTextures.at(i).TextureName == textureName)
			{
				return ImportedTextures.at(i).TextureInstance;
			}
		}

		return nullptr;
	}

	bool TexturePool::RemoveTexture (Texture* target)
	{
		for (unsigned int i = 0; i < ImportedTextures.size(); i++)
		{
			if (ImportedTextures.at(i).TextureInstance == target)
			{
				ImportedTextures.erase(ImportedTextures.begin() + i);
				return true;
			}
		}

		return false;
	}

	TexturePool* TexturePool::GetTexturePool ()
	{
		return TexturePoolInstance;
	}
}

#endif