/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ObjectIterator.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "CoreClasses.h"

#if INCLUDE_CORE

using namespace std;

namespace SD
{
	ObjectIterator::ObjectIterator ()
	{
		TargetedHash = Engine::GetEngine()->GetObjectHashNumber();
		bMultiHashIDs = true; //Typically specifying searching through objects typically means that one wants to find everything.
		FindFirstObject();
	}

	ObjectIterator::ObjectIterator (unsigned int targetedHash, bool bUseMultiHashIDs)
	{
		if (targetedHash != 0 && !Utils::IsPowerOf2(targetedHash))
		{
			LOG1(LOG_WARNING, TXT("Invalid object hash value (%s).  Object hash values must be in powers of 2.  Defaulting iterator to 0."), DString(targetedHash));
			TargetedHash = 0;
		}
		else
		{
			TargetedHash = targetedHash;
		}

		bMultiHashIDs = bUseMultiHashIDs;
		FindFirstObject();
	}

	ObjectIterator::~ObjectIterator ()
	{
	}

	void ObjectIterator::operator++ ()
	{
		FindNextObject();
	}

	void ObjectIterator::operator++ (int)
	{
		FindNextObject();
	}

	unsigned int ObjectIterator::GetTargetedHash () const
	{
		return TargetedHash;
	}

	void ObjectIterator::FindFirstObject ()
	{
		Engine* engine = Engine::GetEngine();

		ObjTableIdx = CalcTableIdx();
		if (ObjTableIdx >= engine->ObjectHashTable.size())
		{
			LOG3(LOG_WARNING, TXT("Created an ObjectIterator with an invalid TargetedHash (%s).  Computing the table index returned %s.  The Engine's hash table size is %s"), DString(TargetedHash), DString(ObjTableIdx), DString(static_cast<int>(Engine::GetEngine()->ObjectHashTable.size())));
			SelectedObject = nullptr;
		}
		else
		{
			SelectedObject = engine->ObjectHashTable.at(ObjTableIdx).LeadingObject;

			if (bMultiHashIDs)
			{
				while (SelectedObject == nullptr && ++ObjTableIdx < engine->ObjectHashTable.size())
				{
					SelectedObject = engine->ObjectHashTable.at(ObjTableIdx).LeadingObject;
				}
			}
		}
	}

	unsigned int ObjectIterator::CalcTableIdx () const
	{
		double result = log2(TargetedHash);
		return static_cast<unsigned int>(trunc(result));
	}

	void ObjectIterator::FindNextObject ()
	{
		do
		{
			if (SelectedObject == nullptr)
			{
				return; //This iterator is done
			}

			SelectedObject = SelectedObject->NextObject;
			if (bMultiHashIDs)
			{
				//Reached the end of this table.  Iterate down the hash table
				while (SelectedObject == nullptr && ++ObjTableIdx < Engine::GetEngine()->ObjectHashTable.size())
				{
					SelectedObject = Engine::GetEngine()->ObjectHashTable.at(ObjTableIdx).LeadingObject;
				}
			}
		}
		while (!VALID_OBJECT(SelectedObject)); //Don't return pending delete objects.  Keep iterating until we ran out of objects or when we find an available object
	}
}

#endif