/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SDFunctionTester.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "CoreClasses.h"

#if INCLUDE_CORE

#ifdef DEBUG_MODE

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(SDFunctionTester, Object)

	void SDFunctionTester::InitProps ()
	{
		Super::InitProps();

		bTestedVoid = false;
		bTestedParamINT = false;
		bTestedMultiParam = false;
		bTestedReturn = false;
		bTestedReturnMultiParam = false;
		bTestedReturnOutParam = false;
		FunctionCounter = 0;
	}

	bool SDFunctionTester::RunTest (const DatatypeUnitTester* tester, UnitTester::EUnitTestFlags testFlags)
	{
		DatatypeTester = tester;
		TestFlags = testFlags;
		DatatypeTester->SetTestCategory(TestFlags, TXT("SDFunctionTester"));

		//reset member variables
		bTestedVoid = false;
		bTestedParamINT = false;
		bTestedMultiParam = false;
		bTestedReturn = false;
		bTestedReturnMultiParam = false;
		bTestedReturnOutParam = false;
		FunctionCounter = 0;

		SDFunction<void> voidFunction = SDFunction<void>(bind(&SDFunctionTester::HandleVoidFunction, this), this, TXT("SDFunctionTester::HandleVoidFunction"));
		if (!voidFunction.IsBounded())
		{
			DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  Failed to bind %s to SDFunction."), {voidFunction.ToString()});
			return false;
		}
		voidFunction();

		if (!bTestedVoid)
		{
			DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  %s was not called."), {voidFunction.ToString()});
			return false;
		}

		SDFunction<void, int> paramFunction = SDFunction<void, int>(bind(&SDFunctionTester::HandleParamFunction, this, placeholders::_1), this, TXT("SDFunctionTester::HandleParamFunction"));
		if (!paramFunction.IsBounded())
		{
			DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  Failed to bind %s to SDFunction."), {paramFunction.ToString()});
			return false;
		}
		paramFunction(5);

		if (!bTestedParamINT)
		{
			DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  Either %s was not called, or the event handler did not receive the parameter it was expecting."), {paramFunction.ToString()});
			return false;
		}

		SDFunction<void, INT, FLOAT, DString, unsigned int> multiParamFunction = SDFUNCTION_4PARAM(this, SDFunctionTester, HandleMultiParamFunction, void, INT, FLOAT, DString, unsigned int);
		if (!multiParamFunction.IsBounded())
		{
			DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  Failed to bind %s to SDFunction."), {multiParamFunction.ToString()});
			return false;
		}
		multiParamFunction(5, 5.f, TXT("Five"), 5);

		if (!bTestedMultiParam)
		{
			DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  Either %s was not called, or the event handler did not receive the parameters it was expecting."), {multiParamFunction.ToString()});
			return false;
		}

		SDFunction<FLOAT> returnFunction = SDFUNCTION(this, SDFunctionTester, HandleReturnFunction, FLOAT);
		if (!returnFunction.IsBounded())
		{
			DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  Failed to bind %s to SDFunction."), {returnFunction.ToString()});
			return false;
		}
		FLOAT returnValue = returnFunction();

		if (returnValue != 5.f)
		{
			DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  The expected return value for %s is 5.f.  Instead it returned %s"), {returnFunction.ToString(), returnValue.ToString()});
			return false;
		}

		if (!bTestedReturn)
		{
			DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  %s was not called."), {returnFunction.ToString()});
			return false;
		}

		SDFunction<BOOL, INT, Object*, string> returnMultiParamFunction = SDFUNCTION_3PARAM(this, SDFunctionTester, HandleReturnMultiParamFunction, BOOL, INT, Object*, string);
		if (!returnMultiParamFunction.IsBounded())
		{
			DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  Failed to bind %s to SDFunction."), {returnMultiParamFunction.ToString()});
			return false;
		}

		BOOL otherReturnValue = returnMultiParamFunction(5, this, TXT("Five"));
		if (!otherReturnValue)
		{
			DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  The expected return value for %s is true.  Instead it returned %s"), {returnMultiParamFunction.ToString(), otherReturnValue.ToString()});
			return false;
		}

		if (!bTestedReturnMultiParam)
		{
			DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  Either the %s was not called, or the event handler did not receive the function parameters it was expecting."), {returnMultiParamFunction.ToString()});
			return false;
		}

		SDFunction<INT, INT, INT&> returnOutParamFunction = SDFUNCTION_2PARAM(this, SDFunctionTester, HandleReturnOutParamFunction, INT, INT, INT&);
		if (!returnOutParamFunction.IsBounded())
		{
			DatatypeTester->UnitTestError(TestFlags, TXT("Function unit testered failed.  Failed to bind %s to SDFunction"), {returnOutParamFunction.ToString()});
			return false;
		}

		INT outParam;
		INT returnOutValue = returnOutParamFunction(5, outParam);
		if (returnOutValue != 5)
		{
			DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  The expected return value for %s is 5.  Instead it returned %s"), {returnOutParamFunction.ToString(), returnOutValue.ToString()});
			return false;
		}

		if (outParam != 5)
		{
			DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  %s is expected to set second parameter to 5.  Instead the second parameter is currently %s"), {returnOutParamFunction.ToString(), outParam.ToString()});
			return false;
		}

		if (!bTestedReturnOutParam)
		{
			DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  Either the %s was not called, or the event handler did not receive the function parameters it was expecting."), {returnOutParamFunction.ToString()});
			return false;
		}

		if (FunctionCounter != 6)
		{
			DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  The total number of events invoked should have been 6.  Instead %s event handlers were invoked."), {FunctionCounter.ToString()});
			return false;
		}

		DatatypeTester->CompleteTestCategory(testFlags);
		DatatypeTester->ExecuteSuccessSequence(testFlags, TXT("SDFunctionTester"));

		return true;
	}

	void SDFunctionTester::HandleVoidFunction ()
	{
		UNITTESTER_LOG(TestFlags, TXT("SDFunctionTester::HandleVoidFunction event handler was invoked."));
		bTestedVoid = true;
		FunctionCounter++;
	}

	void SDFunctionTester::HandleParamFunction (INT paramValueTest)
	{
		UNITTESTER_LOG1(TestFlags, TXT("SDFunctionTester::HandleParamFunction event handler was invoked with param %s"), paramValueTest);

		if (paramValueTest != 5)
		{
			DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  The expected parameter for HandleParamFunction is 5.  Instead it received:  %s"), {paramValueTest.ToString()});
			return;
		}

		bTestedParamINT = true;
		FunctionCounter++;
	}

	void SDFunctionTester::HandleMultiParamFunction (INT param1, FLOAT param2, DString param3, unsigned int param4)
	{
		UNITTESTER_LOG4(TestFlags, TXT("SDFunctionTester::HandleMultiParamFunction event handler was invoked with params:  %s, %s, %s, %s"), param1, param2, param3, DString(param4));

		if (param1 != 5 || param2 != 5.f || param3 != TXT("Five"), param4 != 5)
		{
			DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  The expected parameters for HandleMultiParamFunction is 5, 5.5, \"Five\", 5.  Instead it received:  %s, %s, %s, %s"), {param1.ToString(), param2.ToString(), param3.ToString(), DString(param4)});
			return;
		}

		bTestedMultiParam = true;
		FunctionCounter++;
	}

	FLOAT SDFunctionTester::HandleReturnFunction ()
	{
		UNITTESTER_LOG(TestFlags, TXT("SDFunctionTester::HandleReturnFunction event handler was invoked."));
		bTestedReturn = true;
		FunctionCounter++;

		return 5.f;
	}

	BOOL SDFunctionTester::HandleReturnMultiParamFunction (INT param1, Object* param2, std::string param3)
	{
		UNITTESTER_LOG3(TestFlags, TXT("SDFunctionTester::HandleReturnMultiParamFunction event handler was invoked with params:  %s, %s, %s"), param1, param2->ToString(), DString(param3));

		if (param1 != 5, param2 != this, param3 != TXT("Five"))
		{
			DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  The expected parameters for HandleReturnMultiParamFunction is 5, %s, \"Five\".  Instead it received:  %s, %s, %s.  Function is returning false."), {ToString(), param1.ToString(), param2->ToString(), DString(param3)});
			return false;
		}

		bTestedReturnMultiParam = true;
		FunctionCounter++;
		
		return true;
	}

	INT SDFunctionTester::HandleReturnOutParamFunction (INT param1, INT& outParam2)
	{
		UNITTESTER_LOG1(TestFlags, TXT("SDFunctionTester::HandleReturnOutParamFunction event handler was invoked with param:  %s"), param1);

		if (param1 != 5)
		{
			DatatypeTester->UnitTestError(TestFlags, TXT("Function unit tester failed.  The expected parameter for HandleReturnOutParamFunction is 5.  Instead it received:  %s.  Function is returning 0."), {param1.ToString()});
			return 0;
		}

		bTestedReturnOutParam = true;
		FunctionCounter++;

		outParam2 = 5;
		return 5;
	}
}

#endif
#endif