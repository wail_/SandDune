/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  DPropertyExtension.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "CoreClasses.h"

#if INCLUDE_CORE

using namespace std;

namespace SD
{
	DPropertyExtension::DPropertyExtension ()
	{
		NextProperty = nullptr;
		OwningProperty = nullptr;
	}

	DPropertyExtension::DPropertyExtension (const DPropertyExtension& copyProperty)
	{
		//Each property extension should maintain their own linked list
		NextProperty = nullptr;
		OwningProperty = nullptr;
	}

	DPropertyExtension::~DPropertyExtension ()
	{
		if (NextProperty != nullptr)
		{
			delete NextProperty;
			NextProperty = nullptr;
		}
	}

	void DPropertyExtension::BindPropertyToObject (Object* propertyOwner)
	{
#ifdef DEBUG_MODE
		//Exclude this property safety check from Release builds for performance reasons.
		for (PropertyIterator<DPropertyExtension> iter(propertyOwner); iter.GetSelectedProperty() != nullptr; iter++)
		{
			//Warn the developer that this property is already registered to this Object's property list which would cause an infinite loop crash
			if (iter.GetSelectedProperty() == this)
			{
				Engine::GetEngine()->FatalError(GetOwningProperty()->ToString() + TXT(" is already registered to ") + propertyOwner->ToString() + TXT(".  Registering a property extension to a common owner multiple times will cause an infinite loop for release builds."));
				return;
			}
		}
#endif
		//Insert this property at the beginning of the object's property linked list
		DPropertyExtension* oldFirstProp = propertyOwner->FirstProperty;
		propertyOwner->FirstProperty = this;
		NextProperty = oldFirstProp;
	}

	void DPropertyExtension::SetOwningProperty (DProperty* newOwningProperty)
	{
		OwningProperty = newOwningProperty;
	}

	DPropertyExtension* DPropertyExtension::GetNextProperty () const
	{
		return NextProperty;
	}

	DProperty* DPropertyExtension::GetOwningProperty () const
	{
		return OwningProperty;
	}
}

#endif