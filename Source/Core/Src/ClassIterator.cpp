/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ClassIterator.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "CoreClasses.h"

#if INCLUDE_CORE

using namespace std;

namespace SD
{
	ClassIterator::ClassIterator ()
	{
		InitialClass = Object::SStaticClass();
		InitializeIterator();
	}

	ClassIterator::ClassIterator (const DClass* newInitialClass)
	{
		InitialClass = newInitialClass;
		InitializeIterator();
	}

	ClassIterator::~ClassIterator ()
	{
	}

	void ClassIterator::operator++ ()
	{
		SelectNextClass();
	}

	void ClassIterator::operator++ (int)
	{
		SelectNextClass();
	}

	void ClassIterator::InitializeIterator ()
	{
		SelectedClass = InitialClass;

		SClassIteratorInfo firstClass;

		//Insert the first child class to avoid base case on first iteration
		firstClass.ChildIndex = -1; //haven't selected this class yet
		firstClass.CurrentClass = SelectedClass;

		ParentClasses.push_back(firstClass);
	}

	void ClassIterator::SelectNextClass ()
	{
		int lastParentIdx = (int)ParentClasses.size() - 1;

		//No more classes to iterate through
		if (lastParentIdx < 0)
		{
			SelectedClass = nullptr;
			return;
		}

		const DClass* curClass = ParentClasses.at(lastParentIdx).CurrentClass; //only concerned with last element

		ParentClasses.at(lastParentIdx).ChildIndex++;
		if (ParentClasses.at(lastParentIdx).ChildIndex >= static_cast<int>(curClass->Children.size()))
		{
			//already iterated through all child classes, go to next neighboring class
			ParentClasses.pop_back();
			SelectNextClass(); //try again with the next previous ParentClass
			return;
		}

		//if (curParent->Children.size() > 0)
		//child class of parent has more children
		if (curClass->Children.at(ParentClasses.at(lastParentIdx).ChildIndex)->Children.size() > 0)
		{
			//Insert subclass (setup vector for next iteration)
			SClassIteratorInfo newParentInfo;
			newParentInfo.ChildIndex = -1; //haven't selected this class yet
			newParentInfo.CurrentClass = curClass->Children.at(ParentClasses.at(lastParentIdx).ChildIndex);
			ParentClasses.push_back(newParentInfo);
		}

		SelectedClass = curClass->Children.at(ParentClasses.at(lastParentIdx).ChildIndex);
	}
}

#endif