/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ComponentIterator.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "CoreClasses.h"

#if INCLUDE_CORE

using namespace std;

namespace SD
{
	ComponentIterator::ComponentIterator ()
	{
		bRecursive = true;
		TargetEntity = nullptr;
		ComponentIndex = UINT_INDEX_NONE;
		RecursiveIterator = nullptr;
	}

	ComponentIterator::ComponentIterator (const Entity* inSelectedEntity, bool bInRecursive)
	{
		bRecursive = bInRecursive;
		TargetEntity = inSelectedEntity;
		ComponentIndex = UINT_INDEX_NONE;
		RecursiveIterator = nullptr;
		SelectFirstComponent();
	}

	ComponentIterator::~ComponentIterator ()
	{
		if (RecursiveIterator)
		{
			delete RecursiveIterator;
			RecursiveIterator = nullptr;
		}
	}

	void ComponentIterator::operator++ ()
	{
		FindNextComponent();
	}
	void ComponentIterator::operator++ (int)
	{
		FindNextComponent();
	}

	void ComponentIterator::SetTargetEntity (const Entity* newTargetEntity)
	{
		TargetEntity = newTargetEntity;
		ComponentIndex = UINT_INDEX_NONE;
		SelectFirstComponent();
	}

	const Entity* ComponentIterator::GetTargetEntity () const
	{
		return TargetEntity;
	}

	EntityComponent* ComponentIterator::GetSelectedComponent () const
	{
		if (!VALID_OBJECT(TargetEntity) || ComponentIndex == UINT_INDEX_NONE)
		{
			return nullptr;
		}

		if (RecursiveIterator)
		{
			return RecursiveIterator->GetSelectedComponent();
		}

		if (ComponentIndex >= TargetEntity->Components.size())
		{
			return nullptr;
		}

		return TargetEntity->Components.at(ComponentIndex);
	}

	void ComponentIterator::SelectFirstComponent ()
	{
		ComponentIndex = 0;
	}

	void ComponentIterator::FindNextComponent ()
	{
		if (!VALID_OBJECT(TargetEntity) || ComponentIndex > TargetEntity->Components.size())
		{
			return;
		}		
	
		if (RecursiveIterator != nullptr)
		{
			RecursiveIterator->FindNextComponent(); //Redirect to sub component iterator
		}
		else if (bRecursive && TargetEntity->Components.at(ComponentIndex)->Components.size() > 0)
		{
			//Current selected component has sub components.  Iterate those components
			RecursiveIterator = new ComponentIterator(TargetEntity->Components.at(ComponentIndex), true);
		}
		else
		{
			ComponentIndex++;
		}

		if (RecursiveIterator && !RecursiveIterator->GetSelectedComponent())
		{
			//SubComponentIterator finished iterating
			delete RecursiveIterator;
			RecursiveIterator = nullptr;
			ComponentIndex++;
		}
	}
}

#endif