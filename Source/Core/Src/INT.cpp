/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  INT.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "CoreClasses.h"

#if INCLUDE_CORE

using namespace std;

namespace SD
{
	INT::INT ()
	{
		Value = 0;
	}

	INT::INT (const int newValue)
	{
		Value = newValue;
	}

	INT::INT (const INT& copyInt)
	{
		Value = copyInt.Value;
	}

	INT::INT (const unsigned int& newValue)
	{
		Value = GetInt(newValue);
	}

	INT::INT (const FLOAT& newValue)
	{
		Value = newValue.ToINT().Value;
	}

	INT::INT (const DString& text)
	{
		try
		{
			Value = stoi(text.String);
		}
		catch(out_of_range&)
		{
			LOG2(LOG_WARNING, TXT("There's not enough space to fit %s within an INT.  Defaulting to INT_MAX(%s)."), text, INT(INT_MAX));
			Value = INT_MAX;
		}
		catch(invalid_argument&)
		{
			LOG1(LOG_WARNING, TXT("Cannot construct an INT from %s.  Defaulting to 0."), text);
			Value = 0;
		}
	}

#ifdef PLATFORM_64BIT
	INT::INT (const size_t newValue)
	{
		Value = static_cast<int>(newValue);
	}
#endif

	void INT::operator= (const INT& copyInt)
	{
		Value = copyInt.Value;
	}

	void INT::operator= (const int otherInt)
	{
		Value = otherInt;
	}

	void INT::operator= (const unsigned int otherInt)
	{
		Value = GetInt(otherInt);
	}

	INT INT::operator++ ()
	{
		return ++Value;
	}

	INT INT::operator++ (int)
	{
		return Value++;
	}

	INT INT::operator- () const
	{
		return -Value;
	}

	INT INT::operator-- ()
	{
		return --Value;
	}

	INT INT::operator-- (int)
	{
		return Value--;
	}

#if 0
	bool INT::operator== (const INT& otherInt) const
	{
		return (Value == otherInt.Value);
	}

	bool INT::operator!= (const INT& otherInt) const
	{
		return (Value != otherInt.Value);
	}

	bool INT::operator< (const INT& otherInt) const
	{
		return (Value < otherInt.Value);
	}

	bool INT::operator<= (const INT& otherInt) const
	{
		return (Value <= otherInt.Value);
	}

	bool INT::operator> (const INT& otherInt) const
	{
		return (Value > otherInt.Value);
	}

	bool INT::operator>= (const INT& otherInt) const
	{
		return (Value >= otherInt.Value);
	}

	INT INT::operator>> (const INT& otherInt) const
	{
		return INT(Value >> otherInt.Value);
	}

	INT INT::operator<< (const INT& otherInt) const
	{
	
		return INT(Value << otherInt.Value);
	}

	INT INT::operator& (const INT& otherInt) const
	{
		return INT(Value & otherInt.Value);
	}

	INT INT::operator^ (const INT& otherInt) const
	{
		return INT(Value ^ otherInt.Value);
	}

	INT INT::operator| (const INT& otherInt) const
	{
		return INT(Value | otherInt.Value);
	}

	void INT::operator&= (const INT& otherInt)
	{
		Value &= otherInt.Value;
	}

	void INT::operator^= (const INT& otherInt)
	{
		Value ^= otherInt.Value;
	}

	void INT::operator|= (const INT& otherInt)
	{
		Value |= otherInt.Value;
	}

	INT INT::operator+ (const INT& otherInt) const
	{
		return INT(Value + otherInt.Value);
	}

	void INT::operator+= (const INT& otherInt)
	{
		Value += otherInt.Value;
	}

	INT INT::operator++ ()
	{
		return ++Value;
	}

	INT INT::operator++ (int)
	{
		return Value++;
	}

	INT INT::operator- (const INT& otherInt) const
	{
		return INT(Value - otherInt.Value);
	}

	INT INT::operator- () const
	{
		return -Value;
	}

	void INT::operator-= (const INT& otherInt)
	{
		Value -= otherInt.Value;
	}

	INT INT::operator-- ()
	{
		return --Value;
	}

	INT INT::operator-- (int)
	{
		return Value--;
	}

	INT INT::operator* (const INT& otherInt) const
	{
		return INT(Value * otherInt.Value);
	}

	void INT::operator*= (const INT& otherInt)
	{
		Value *= otherInt.Value;
	}

	INT INT::operator/ (const INT& otherInt) const
	{
#ifdef DEBUG_MODE
		if (otherInt == 0)
		{
			LOG1(LOG_WARNING, TXT("Attempting to divide %s by zero!"), ToString());
			return INT(Value);
		}
#endif

		return (otherInt.Value != 0) ? INT(Value / otherInt.Value) : INT(Value);
	}

	void INT::operator/= (const INT& otherInt)
	{
		if (otherInt.Value != 0)
		{
			Value /= otherInt.Value;
		}
#ifdef DEBUG_MODE
		else
		{
			LOG1(LOG_WARNING, TXT("Attempting to divide %s by zero!"), ToString());
		}
#endif
	}

	void INT::operator%= (const INT& otherInt)
	{
		if (otherInt != 0)
		{
			Value %= otherInt.Value;
		}
#ifdef DEBUG_MODE
		else
		{
			LOG1(LOG_WARNING, TXT("Attempting to modulo %s by zero!"), ToString());
		}
#endif
	}

	INT INT::operator% (const INT& otherInt) const
	{
		if (otherInt != 0)
		{
			return INT(Value % otherInt.Value);
		}

#ifdef DEBUG_MODE
		LOG1(LOG_WARNING, TXT("Attempting to modulo %s by zero!"), ToString());
#endif
		return INT(Value);
	}
#endif

	DString INT::ToString () const
	{
		return DString::MakeString(Value);
	}

	INT INT::MakeINT (float value)
	{
		return INT(FLOAT(value).ToINT());
	}

	FLOAT INT::ToFLOAT () const
	{
		return FLOAT(static_cast<float>(Value));
	}

	bool INT::IsEven () const
	{
		return (Value%2) == 0;
	}

	bool INT::IsOdd () const
	{
		return !IsEven();
	}

	DString INT::ToStringMinDigits (const INT minDigits) const
	{
		INT curDigits = NumDigits();
		INT numZeroesNeeded = Utils::Max<INT>(0, minDigits - curDigits);

		return std::string(numZeroesNeeded.Value, '0') + ToString();
	}

	INT INT::Abs (INT value)
	{
		return abs(value.Value);
	}

	void INT::Abs ()
	{
		abs(Value);
	}

	int INT::GetInt (unsigned int target)
	{
		if (target > INT_MAX)
		{
			Value = INT_MAX;
		}
		else
		{
			Value = target;
		}

		return Value;
	}

	unsigned int INT::ToUnsignedInt () const
	{
		int result = Value;

		if (result < 0)
		{
			//Set the value to 0 so that the signed bit will not convert this negative number to a large unsigned int
			result = 0;
		}

		return static_cast<unsigned int>(result);
	}

	INT INT::NumDigits () const
	{
		return (Value != 0) ? static_cast<int>(log10((double)abs(Value))) + 1 : 1;
	}

#pragma region "External Operators"
	bool operator== (const INT& left, const INT& right)
	{
		return (left.Value == right.Value);
	}

	bool operator== (const INT& left, const int& right)
	{
		return (left.Value == right);
	}

	bool operator== (const int& left, const INT& right)
	{
		return (left == right.Value);
	}

	bool operator!= (const INT& left, const INT& right)
	{
		return (left.Value != right.Value);
	}

	bool operator!= (const INT& left, const int& right)
	{
		return (left.Value != right);
	}

	bool operator!= (const int& left, const INT& right)
	{
		return (left != right.Value);
	}

	bool operator< (const INT& left, const INT& right)
	{
		return (left.Value < right.Value);
	}

	bool operator< (const INT& left, const int& right)
	{
		return (left.Value < right);
	}

	bool operator< (const int& left, const INT& right)
	{
		return (left < right.Value);
	}

	bool operator<= (const INT& left, const INT& right)
	{
		return (left.Value <= right.Value);
	}

	bool operator<= (const INT& left, const int& right)
	{
		return (left.Value <= right);
	}

	bool operator<= (const int& left, const INT& right)
	{
		return (left <= right.Value);
	}

	bool operator> (const INT& left, const INT& right)
	{
		return (left.Value > right.Value);
	}

	bool operator> (const INT& left, const int& right)
	{
		return (left.Value > right);
	}

	bool operator> (const int& left, const INT& right)
	{
		return (left > right.Value);
	}

	bool operator>= (const INT& left, const INT& right)
	{
		return (left.Value >= right.Value);
	}

	bool operator>= (const INT& left, const int& right)
	{
		return (left.Value >= right);
	}

	bool operator>= (const int& left, const INT& right)
	{
		return (left >= right.Value);
	}

	INT operator>> (const INT& left, const INT& right)
	{
		return INT(left.Value >> right.Value);
	}

	INT operator>> (const INT& left, const int& right)
	{
		return INT(left.Value >> right);
	}

	INT operator>> (const int& left, const INT& right)
	{
		return INT(left >> right.Value);
	}

	INT& operator>>= (INT& left, const INT& right)
	{
		left.Value >>= right.Value;
		return left;
	}

	INT& operator>>= (INT& left, const int& right)
	{
		left.Value >>= right;
		return left;
	}

	int& operator>>= (int& left, const INT& right)
	{
		left >>= right.Value;
		return left;
	}

	INT operator<< (const INT& left, const INT& right)
	{
		return INT(left.Value << right.Value);
	}

	INT operator<< (const INT& left, const int& right)
	{
		return INT(left.Value << right);
	}

	INT operator<< (const int& left, const INT& right)
	{
		return INT(left << right.Value);
	}

	INT& operator<<= (INT& left, const INT& right)
	{
		left.Value <<= right.Value;
		return left;
	}

	INT& operator<<= (INT& left, const int& right)
	{
		left.Value <<= right;
		return left;
	}

	int& operator<<= (int& left, const INT& right)
	{
		left <<= right.Value;
		return left;
	}

	INT operator& (const INT& left, const INT& right)
	{
		return INT(left.Value & right.Value);
	}

	INT operator& (const INT& left, const int& right)
	{
		return INT(left.Value & right);
	}

	INT operator& (const int& left, const INT& right)
	{
		return INT(left & right.Value);
	}

	INT operator^ (const INT& left, const INT& right)
	{
		return INT(left.Value & right.Value);
	}

	INT operator^ (const INT& left, const int& right)
	{
		return INT(left.Value & right);
	}

	INT operator^ (const int& left, const INT& right)
	{
		return INT(left & right.Value);
	}

	INT operator| (const INT& left, const INT& right)
	{
		return INT(left.Value | right.Value);
	}

	INT operator| (const INT& left, const int& right)
	{
		return INT(left.Value | right);
	}

	INT operator| (const int& left, const INT& right)
	{
		return INT(left | right.Value);
	}

	INT& operator&= (INT& left, const INT& right)
	{
		left.Value &= right;
		return left;
	}

	INT& operator&= (INT& left, const int& right)
	{
		left.Value &= right;
		return left;
	}

	int& operator&= (int& left, const INT& right)
	{
		left &= right.Value;
		return left;
	}

	INT& operator^= (INT& left, const INT& right)
	{
		left.Value ^= right.Value;
		return left;
	}

	INT& operator^= (INT& left, const int& right)
	{
		left.Value ^= right;
		return left;
	}

	int& operator^= (int& left, const INT& right)
	{
		left ^= right.Value;
		return left;
	}

	INT& operator|= (INT& left, const INT& right)
	{
		left.Value |= right.Value;
		return left;
	}

	INT& operator|= (INT& left, const int& right)
	{
		left.Value |= right;
		return left;
	}

	int& operator|= (int& left, const INT& right)
	{
		left |= right.Value;
		return left;
	}

	INT operator+ (const INT& left, const INT& right)
	{
		return INT(left.Value + right.Value);
	}

	INT operator+ (const INT& left, const int& right)
	{
		return INT(left.Value + right);
	}

	INT operator+ (const int& left, const INT& right)
	{
		return INT(left + right.Value);
	}

	INT& operator+= (INT& left, const INT& right)
	{
		left.Value += right.Value;
		return left;
	}

	INT& operator+= (INT& left, const int& right)
	{
		left.Value += right;
		return left;
	}

	int& operator+= (int& left, const INT& right)
	{
		left += right.Value;
		return left;
	}

	INT operator- (const INT& left, const INT& right)
	{
		return INT(left.Value - right.Value);
	}

	INT operator- (const INT& left, const int& right)
	{
		return INT(left.Value - right);
	}

	INT operator- (const int& left, const INT& right)
	{
		return INT(left - right.Value);
	}

	INT& operator-= (INT& left, const INT& right)
	{
		left.Value -= right.Value;
		return left;
	}

	INT& operator-= (INT& left, const int& right)
	{
		left.Value -= right;
		return left;
	}

	int& operator-= (int& left, const INT& right)
	{
		left -= right.Value;
		return left;
	}

	INT operator* (const INT& left, const INT& right)
	{
		return INT(left.Value * right.Value);
	}

	INT operator* (const INT& left, const int& right)
	{
		return INT(left.Value * right);
	}

	INT operator* (const int& left, const INT& right)
	{
		return INT(left * right.Value);
	}

	INT& operator*= (INT& left, const INT& right)
	{
		left.Value *= right.Value;
		return left;
	}

	INT& operator*= (INT& left, const int& right)
	{
		left.Value *= right;
		return left;
	}

	int& operator*= (int& left, const INT& right)
	{
		left *= right.Value;
		return left;
	}

	INT operator/ (const INT& left, const INT& right)
	{
#ifdef DEBUG_MODE
		if (right.Value == 0)
		{
			LOG1(LOG_WARNING, TXT("Attempting to divide %s by zero!"), left);
			return left;
		}
#endif

		return (right.Value != 0) ? INT(left.Value / right.Value) : left;
	}

	INT operator/ (const INT& left, const int& right)
	{
#ifdef DEBUG_MODE
		if (right == 0)
		{
			LOG1(LOG_WARNING, TXT("Attempting to divide %s by zero!"), left);
			return left;
		}
#endif

		return (right != 0) ? INT(left.Value / right) : left;
	}

	INT operator/ (const int& left, const INT& right)
	{
#ifdef DEBUG_MODE
		if (right.Value == 0)
		{
			LOG1(LOG_WARNING, TXT("Attempting to divide %s by zero!"), INT(left));
			return INT(left);
		}
#endif

		return (right.Value != 0) ? INT(left / right.Value) : INT(left);
	}

	INT& operator/= (INT& left, const INT& right)
	{
		if (right.Value != 0)
		{
			left.Value /= right.Value;
		}
#ifdef DEBUG_MODE
		else
		{
			LOG1(LOG_WARNING, TXT("Attempting to divide %s by zero!"), left);
		}
#endif
		
		return left;
	}

	INT& operator/= (INT& left, const int& right)
	{
		if (right != 0)
		{
			left.Value /= right;
		}
#ifdef DEBUG_MODE
		else
		{
			LOG1(LOG_WARNING, TXT("Attempting to divide %s by zero!"), left);
		}
#endif
		
		return left;
	}

	int& operator/= (int& left, const INT& right)
	{
		if (right.Value != 0)
		{
			left /= right.Value;
		}
#ifdef DEBUG_MODE
		else
		{
			LOG1(LOG_WARNING, TXT("Attempting to divide %s by zero!"), INT(left));
		}
#endif
		
		return left;
	}

	INT operator% (const INT& left, const INT& right)
	{
#ifdef DEBUG_MODE
		if (right.Value == 0)
		{
			LOG1(LOG_WARNING, TXT("Attempting to modulo %s by zero!"), left);
			return left;
		}
#endif

		return (right.Value != 0) ? INT(left.Value % right.Value) : left;
	}

	INT operator% (const INT& left, const int& right)
	{
#ifdef DEBUG_MODE
		if (right == 0)
		{
			LOG1(LOG_WARNING, TXT("Attempting to modulo %s by zero!"), left);
			return left;
		}
#endif

		return (right != 0) ? INT(left.Value % right) : left;
	}

	INT operator% (const int& left, const INT& right)
	{
#ifdef DEBUG_MODE
		if (right.Value == 0)
		{
			LOG1(LOG_WARNING, TXT("Attempting to modulo %s by zero!"), INT(left));
			return left;
		}
#endif

		return (right.Value != 0) ? INT(left % right.Value) : INT(left);
	}

	INT& operator%= (INT& left, const INT& right)
	{
		if (right.Value != 0)
		{
			left.Value %= right.Value;
		}
#ifdef DEBUG_MODE
		else
		{
			LOG1(LOG_WARNING, TXT("Attempting to modulo %s by zero!"), left);
		}
#endif
		
		return left;
	}

	INT& operator%= (INT& left, const int& right)
	{
		if (right != 0)
		{
			left.Value %= right;
		}
#ifdef DEBUG_MODE
		else
		{
			LOG1(LOG_WARNING, TXT("Attempting to modulo %s by zero!"), left);
		}
#endif
		
		return left;
	}

	int& operator%= (int& left, const INT& right)
	{
		if (right.Value != 0)
		{
			left %= right.Value;
		}
#ifdef DEBUG_MODE
		else
		{
			LOG1(LOG_WARNING, TXT("Attempting to modulo %s by zero!"), INT(left));
		}
#endif
		
		return left;
	}

#if 0
	bool operator== (int a, const INT& b)
	{
		return (a == b.Value);
	}

	bool operator!= (int a, const INT& b)
	{
		return (a != b.Value);
	}

	bool operator< (int a, const INT& b)
	{
		return (a < b.Value);
	}

	bool operator<= (int a, const INT& b)
	{
		return (a <= b.Value);
	}

	bool operator> (int a, const INT& b)
	{
		return (a > b.Value);
	}

	bool operator>= (int a, const INT& b)
	{
		return (a >= b.Value);
	}

	INT operator>> (int a, const INT& b)
	{
		return INT(a >> b.Value);
	}

	INT operator<< (int a, const INT& b)
	{
		return INT(a << b.Value);
	}

	INT operator& (int a, const INT& b)
	{
		return INT(a & b.Value);
	}

	INT operator^ (int a, const INT& b)
	{
		return INT(a ^ b.Value);
	}

	INT operator| (int a, const INT& b)
	{
		return INT(a | b.Value);
	}

	INT operator+ (int a, const INT& b)
	{
		return INT(a + b.Value);
	}

	INT operator- (int a, const INT& b)
	{
		return INT(a - b.Value);
	}

	INT operator* (int a, const INT& b)
	{
		return INT(a * b.Value);
	}

	INT operator/ (int a, const INT& b)
	{
#ifdef DEBUG_MODE
		if (b == 0)
		{
			LOG1(LOG_WARNING, TXT("Attempting to divide %s by zero!"), INT(a).ToString());
			return INT(a);
		}
#endif
		return (b != 0) ? INT(a/b.Value) : INT(a);
	}

	INT operator% (int a, const INT& b)
	{
		return INT(a%b.Value);
	}
#endif
#pragma endregion
}

#endif