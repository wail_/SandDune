/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Object.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "CoreClasses.h"

#if INCLUDE_CORE

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS_NO_PARENT(Object)

	Object::Object ()
	{
		FirstProperty = nullptr;
	}
	
	Object::Object (const Object& copyConstructor)
	{
		//Each object will maintain their own list of properties.
		FirstProperty = nullptr;
	}

	Object::~Object ()
	{
		if (FirstProperty != nullptr)
		{
			delete FirstProperty;
			FirstProperty = nullptr;
		}
	}

	DString Object::ToString () const
	{
		return GetUniqueName();
	}

	void Object::InitProps ()
	{
#ifdef DEBUG_MODE
		DebugName = TXT("");
#endif
		bPendingDelete = false;
		ObjectHash = -1;
		ID = -1;

		NextObject = nullptr;
	}

	void Object::BeginObject ()
	{
		ObjectHash = CalculateHashID();
		RegisterObject();
	}

	void Object::Destroy ()
	{
		bPendingDelete = true;
	}

	bool Object::IsDefaultObject () const
	{
		return (GetDefaultObject() == this);
	}

	unsigned int Object::GetObjectHash () const
	{
		return ObjectHash;
	}

	DString Object::GetUniqueName () const
	{
#ifdef DEBUG_MODE
		if (!DebugName.IsEmpty())
		{
			return DebugName;
		}
#endif
		if (ID == 0)
		{
			return TXT("Default_") + StaticClass()->ToString();
		}

		return StaticClass()->ToString() + TXT("_") + ID.ToString();
	}

	DString Object::GetName () const
	{
		return StaticClass()->Name();
	}

	DString Object::GetFriendlyName () const
	{
		return GetUniqueName();
	}

	const Object* Object::GetDefaultObject () const
	{
		return StaticClass()->GetDefaultObject();
	}

	bool Object::GetPendingDelete () const
	{
		return bPendingDelete;
	}

	unsigned int Object::CalculateHashID () const
	{
		return Engine::GetEngine()->GetObjectHashNumber();
	}

	void Object::PostEngineInitialize () const
	{
		//Do nothing
	}

	void Object::CleanUpInvalidPointers ()
	{
		//Do nothing
	}

	void Object::InitializeObject ()
	{
		InitProps();

		if (!IsDefaultObject())
		{
			BeginObject();
		}
	}

	void Object::RegisterObject ()
	{
		//adds self to Engine's object list
		Engine::GetEngine()->RegisterObject(this);

		//Generate a unique name for this object
		CalculateUniqueName();
	}
}

#endif