/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GraphicsEngineComponent.h
  EngineComponent responsible for launching the application's window, and
  running the render pipeline.  This component is also responsible for
  housing the ResourcePools.

  This is essentially the GraphicsCore.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef GRAPHICSENGINECOMPONENT_H
#define GRAPHICSENGINECOMPONENT_H

#include "Graphics.h"
#include "ResourcePool.h"
#include "RenderTarget.h"

#if INCLUDE_GRAPHICS

namespace SD
{
	class GraphicsEngineComponent : public EngineComponent
	{
		DECLARE_ENGINE_COMPONENT(GraphicsEngineComponent)


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		//Global draw order defines entities are drawn in ascending order.
		static const FLOAT DrawOrderBackground;
		static const FLOAT DrawOrderMainScene;
		static const FLOAT DrawOrderForeground;
		static const FLOAT DrawOrderUI;

	protected:
		/* Main application's window. */
		Window* WindowHandle;

		/* List of windows that'll be updated as fast as the engine's update rate. */
		std::vector<Window*> ExternalWindows;

		/* Dimensions of the window handle size. */
		INT WindowHeight;
		INT WindowWidth;

		unsigned int RenderComponentHashNumber;

		bool bFullScreen = false;

		std::vector<ResourcePool*> RegisteredResourcePools;

		/* Reference to the main render target. */
		RenderTarget* PrimaryRenderTarget = nullptr;

		/* Color to turn to when clearing the back buffer. */
		Color BackBufferResetColor;

	private:
		/* Becomes true if this engine is currently accessing the ExternallWindows loop. */
		bool bLockExternalWindows;

		/* List of window handles that are pending to be destroyed. */
		std::vector<Window*> PendingDestroyExternalWindows;


		/*
		=====================
		  Constructors
		=====================
		*/

	public:
		GraphicsEngineComponent ();


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void RegisterObjectHash () override;

		virtual void PreInitializeComponent () override;

		virtual void InitializeComponent () override;

		virtual void PreTick (FLOAT deltaSec) override;

		virtual void PostTick (FLOAT deltaSec) override;

		virtual void ShutdownComponent () override;


		/*
		=====================
		  Methods
		=====================
		*/

		/**
		  Adds the resource pool to the resource pools vector (for resource cleanup)
		 */
		virtual void RegisterResourcePool (ResourcePool* newResourcePool);

		/**
		  Creates an OS window handle of the specified class, and will be automatically render as fast as the engine's update rate.
		 */
		virtual Window* CreateExternalWindow (const DClass* windowClass);

		virtual void DestroyExternalWindow (Window* targetWindow);


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		virtual const RenderTarget* GetPrimaryRenderTarget () const;
		virtual Window* GetWindowHandle () const;

		/**
		  Returns the render window of the Window Handle.
		 */
		virtual sf::RenderWindow* GetRenderWindow () const;
		virtual void GetWindowSize (INT& outWidth, INT& outHeight) const;

		virtual unsigned int GetRenderComponentHashNumber () const;


		/*
		=====================
		  Event Handlers
		=====================
		*/

	protected:
		virtual void HandleWindowEvent (const sf::Event newEvent);
	};
}

#endif
#endif