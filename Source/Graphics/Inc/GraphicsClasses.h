/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GraphicsClasses.h
  Contains all header includes for the Graphics module.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef GRAPHICSCLASSES_H
#define GRAPHICSCLASSES_H

#include "Configuration.h"
#if INCLUDE_GRAPHICS

#define MODULE_GRAPHICS 1

#include "Graphics.h"
#include "GraphicsEngineComponent.h"
#include "ResourcePool.h"
#include "TexturePool.h"
#include "FontPool.h"
#include "RenderTarget.h"
#include "Camera.h"
#include "ResolutionComponent.h"
#include "AbsTransformComponent.h"
#include "TransformationRenderInterface.h"
#include "RenderComponent.h"
#include "SpriteComponent.h"
#include "SolidColorRenderComponent.h"

#ifdef DEBUG_MODE
#include "GraphicsUnitTester.h"
#include "SpriteTester.h"
#endif

#endif
#endif