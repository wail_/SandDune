/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SpriteTester.h
  Tests various aspects of the Graphics Module.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef SPRITETESTER_H
#define SPRITETESTER_H

#include "Graphics.h"

#if INCLUDE_GRAPHICS

#ifdef DEBUG_MODE
namespace SD
{
	class SpriteTester : public Entity
	{
		DECLARE_CLASS(SpriteTester)


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* Multiplies the swelling speed as the sprite grows/shrinks */
		FLOAT SwellMultiplier;

		UnitTester::EUnitTestFlags TestFlags;

	protected:
		/* Time this entity existed (used to calculate which stage in animation should be rendered). */
		FLOAT LivingTime;

		/* Index at which testing stage is this entity currently at.  Increments at every "swell"
		[0]:  Repeated sprite no sub division
		[1]:  Repeated sprite sub division 2 (1x2)
		[2]:  Repeated sprite sub division 4 (2x2) (top right corner displayed)
		[3]:  Repeated sprite sub division 8 (4x4)
		[4]:  Scaled sprite no sub division
		[5]:  Scaled sprite sub division 2 (1x2)
		[6]:  Scaled sprite sub division 4 (2x2) (top right corner displayed)
		[7]:  Scaled sprite sub division 8 (4x4)
		*/
		INT TestingStage;

		TickComponent* Tick;

		SpriteComponent* Sprite;

		AbsTransformComponent* Transformation;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual void BeginObject () override;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  Returns the scaling properties for the sprite based on this tester's properties.
		 */
		virtual FLOAT CalculateSpriteScale () const;

		/**
		  Increments the test's life time.  Returns true if it needs to execute the next test stage.
		 */
		virtual bool TickTestStage (FLOAT deltaSec);


		/*
		=====================
		  Event Handler
		=====================
		*/

	protected:
		virtual void HandleTick (FLOAT deltaSec);

	};
}

#endif
#endif
#endif