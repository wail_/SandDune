/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SpriteComponent.h
  A render component that's responsible for drawing a single sprite.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef SPRITECOMPONENT_H
#define SPRITECOMPONENT_H

#include "RenderComponent.h"

#if INCLUDE_GRAPHICS

namespace SD
{
	class SpriteComponent : public RenderComponent
	{
		DECLARE_CLASS(SpriteComponent)


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* Reference to the instanced SFML's sprite class. */
		sf::Sprite* Sprite;

	protected:
		/* Texture to render for the sprite. */
		const Texture* SpriteTexture;

		/* Scale multipliers based on displaying subdivision of a sprite. */
		FLOAT SubDivideScaleX;
		FLOAT SubDivideScaleY;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual void BeginObject () override;
		virtual void Destroy () override;
		virtual void Render (const Camera* targetCamera, const Window* targetWindow) override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Sets the sfml's sprite sub rectangle based on the split and display values.
		  numHorizontal/VerticalSegments determine the number of equal parts.  (2 = splitting in half, 4 = splitting in quarters)
		  horizontal/verticalIndex determines which segment to display.  Reads from left->right and top->bottom.
				If horizontalIndex is 4, then it'll display the fourth segment from the left.
				If verticalIndex is 0, then it'll display the top segment.
		  Function returns false if the specified parameters are not valid and no changes were applied to the sprite.
		  Note:  Calling this will be overriding SetDrawCoordinates if it returns true.
		 */
		virtual bool SetSubDivision (INT numHorizontalSegments, INT numVerticalSegments, INT horizontalIndex, INT verticalIndex);

		/**
		  Scales the draw coordinates using multipliers relative to the sprite size.
		  @Param ScaleMultipliers:  Determines how much of the texture is drawn.  Setting 0.5 will draw half the texture.
		  @Param posMultipliers:  Determines draw position offset.  Setting to 0.5 will move the center of the texture to top left corner.
		  Note:  Calling this will be overriding SetSubDivision.
		 */
		virtual void SetDrawCoordinatesMultipliers (FLOAT xScaleMultiplier, FLOAT yScaleMultiplier, FLOAT xPosMultiplier = 0.f, FLOAT yPosMultiplier = 0.f);

		/**
		  Same as SetDrawCoordinates(FLOATs), but this uses absolute coordinates instead of multipliers.
		  U = x position, V = y position, UL = x Length, VL = y Length.
		  Note:  Calling this will be overriding SetSubDivision.
		 */
		virtual void SetDrawCoordinates (INT u, INT v, INT ul, INT vl);

		/**
		  Updates the texture to render for this sprite.  Note:  You should update the DrawCoordinates or SubDivision values after changing the texture.
		 */
		virtual void SetSpriteTexture (const Texture* newTexture);

		/**
		  Returns the absolute size of the whole sprite. (This does not consider the transformation interface's size coordinates).
		 */
		virtual void GetSpriteSize (INT& outWidth, INT& outHeight) const;
		virtual Vector2 GetSpriteSize () const;


		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		virtual const Texture* GetSpriteTexture () const;
	};
}

#endif
#endif