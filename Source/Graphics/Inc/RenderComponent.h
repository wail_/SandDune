/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  RenderComponent.h
  A component class responsible for drawing content.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef RENDERCOMPONENT_H
#define RENDERCOMPONENT_H

#include "EntityComponent.h"

#if INCLUDE_GRAPHICS

namespace SD
{
	class RenderComponent : public EntityComponent
	{
		DECLARE_CLASS(RenderComponent)


		/*
		=====================
		  Properties
		=====================
		*/

	public:
		/* Transformation that may be manipulating how this object is rendered. */
		TransformationRenderInterface* TransformationInterface;

		/* List of render windows this render component may draw to.  By default, this will only draw to the main render window (specified in GraphicsEngineComponent). */
		std::vector<const Window*> RelevantRenderWindows;

	protected:
		/* The draw order value for this component where lower values are drawn before the higher values. */
		FLOAT DrawOrder;

		/* If true, then this component may be rendered. */
		bool bVisible;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;
		virtual bool AddComponent (EntityComponent* newComponent, bool bLogOnFail = true, bool bDeleteOnFail = true) override;

	protected:
		unsigned int CalculateHashID () const override;
		virtual void CleanUpInvalidPointers () override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Implementation that renders something to the specified targetWindow.
		 */
		virtual void Render (const Camera* targetCamera, const Window* targetWindow) = 0;

		/**
		  Returns true if the entity should even be added to the RenderTarget's draw order array.
		 */
		virtual bool IsRelevant (const Camera* targetCamera, const Window* renderWindow);

		/**
		  Retrieves the draw order value of this render component.
		 */
		virtual FLOAT CalculateDrawOrder (const Camera* targetCamera);		

		virtual void SetDrawOrder (FLOAT newDrawOrder);
		virtual void SetVisibility (bool bNewVisible);


		/*
		=====================
		  Accessors
		=====================
		*/

		virtual FLOAT GetDrawOrder () const;
		virtual bool GetVisibility () const;
	};
}

#endif
#endif