/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TransformationRenderInterface.h
  An interface that allows TransformationComponents to manipulate how
  a rendered object is displayed on the screen.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef TRANSFORMATIONRENDERINTERFACE_H
#define TRANSFORMATIONRENDERINTERFACE_H

#include "Graphics.h"

#if INCLUDE_GRAPHICS

namespace SD
{
	class Camera;

	class TransformationRenderInterface
	{


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  Sets the render object's pivot/origin point to determine the anchor point the object rotates about and scales from.
		 */
		virtual void ApplyPivotPoint (sf::Transformable* renderObject) = 0;

		/**
		  Manipulates the render object's draw position based on the given camera.
		 */
		virtual void ApplyDrawPosition (sf::Transformable* renderObject, const Camera* camera) = 0;
		
		/**
		  Manipulates the render object's draw size based on the given camera.
		 */
		virtual void ApplyDrawSize (sf::Transformable* renderObject, const Camera* camera) = 0;

		/**
		  Manipulates the render object's draw rotation based on the given camera.
		 */
		virtual void ApplyDrawRotation (sf::Transformable* renderObject, const Camera* camera) = 0;
	};
}

#endif
#endif