/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ResourcePool.h
  ResourcePool is the parent class of all resource managers.  A resource pool
  is responsible for tracking a reference to a particular resource and allow
  external objects to import or reuse resources.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef RESOURCEPOOL_H
#define RESOURCEPOOL_H

#include "Graphics.h"

#if INCLUDE_CORE

namespace SD
{
	class ResourcePool : public Object
	{
		DECLARE_CLASS(ResourcePool)


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void BeginObject () override;
		virtual void Destroy () override;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		virtual void ReleaseResources () = 0;
	};
}

#endif
#endif