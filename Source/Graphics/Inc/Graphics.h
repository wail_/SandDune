/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Graphics.h
  Contains important file includes and definitions for the Graphics module.

  The Graphics module is responsible for handling the graphics pipeline.
  This contains the GraphicsEngine, ResourcePools, and various utilities
  such as SpriteComponents and RenderTargets.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef GRAPHICS_H
#define GRAPHICS_H

#include "Configuration.h"
#if INCLUDE_GRAPHICS

#if !(INCLUDE_CORE)
#error The Graphics module requires the Core module.  Please enable INCLUDE_CORE in Configuration.h.
#endif

//Include library-specific includes
#if (INCLUDE_SFMLGRAPHICS)
#include "SFMLGraphicsClasses.h"
#else
#error The Graphics module requires a Graphics Library to be used to define various resource types such as Textures and Fonts.
#endif

#if !(INCLUDE_GEOMETRY2D)
#error The Graphics module requires the Geometry 2D module.  Please enable INCLUDE_GEOMETRY2D in Configuration.h.
#endif

#if !(INCLUDE_FILE)
#error The Graphics module requires the File module.  Please enable INCLUDE_FILE in Configuration.h.
#endif

//Include required modules
#include "CoreClasses.h"
#include "Geometry2DClasses.h"
#include "FileClasses.h"

#define LOG_GRAPHICS TXT("Graphics")

#endif
#endif