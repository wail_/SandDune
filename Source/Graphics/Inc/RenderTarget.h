/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  RenderTarget.h
  Responsible for generating a scene for the associated camera.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef RENDERTARGET_H
#define RENDERTARGET_H

#include "Graphics.h"

#if INCLUDE_GRAPHICS

//Some arbitrary max limit a render target may update itself (in frames per second)
#define MAX_FRAME_RATE 256

namespace SD
{
	class RenderComponent;
	class Camera;

	class RenderTarget : public Entity
	{
		DECLARE_CLASS(RenderTarget)


		/*
		=====================
		  Data Types
		=====================
		*/

	protected:
		struct SRenderOrderData
		{
			RenderComponent* RenderComponent;
			FLOAT RenderOrder;
		};


		/*
		=====================
		  Properties
		=====================
		*/

	protected:
		/* Determines the maximum frame rate for this render target.  0 = no limit, negative = disabled.  (in seconds). */
		FLOAT FrameRate;

		/* The camera that determines the view matrices for this render target. */
		Camera* AssociatedCamera;

		/* Window handle this render target will draw objects to. */
		Window* RenderWindow;

	private:
		/* Time needed to elapse before this render target updates its scene. */
		FLOAT TimeRemaining;


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual void InitProps () override;

		/**
		  Registers itself to the graphics engine.
		 */
		virtual void BeginObject () override;
		virtual void Destroy () override;

	protected:
		virtual void CleanUpInvalidPointers () override;


		/*
		=====================
		  Methods
		=====================
		*/

	public:
		/**
		  When invoked, then this render target will update its scene after rendering other entities.
		 */
		virtual void GenerateScene ();

		/**
		  Sets the associated camera for the RenderTarget to determine the view matrices when generating a scene.
		 */
		virtual void SetAssociatedCamera (Camera* newCamera);

		/**
		  Updates the FrameRate value.  Notifies the engine how frequently should this render target render (in seconds).
		  Set it to 0, to disable frame rate caps.  Set to negative values to disable automatic scene updates.
		  If bResetElapsedTime is true, then the time remaining to generate a new scene will reset to this RenderTarget's new frame rate.
		 */
		virtual void SetFrameRate (FLOAT newFrameRate, bool bResetElapsedTime = true);

		virtual void SetRenderWindow (Window* newRenderWindow);

		
		/*
		=====================
		  Accessors
		=====================
		*/

	public:
		FLOAT GetFrameRate () const;
		virtual Camera* GetAssociatedCamera () const;
		virtual Window* GetRenderwindow () const;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		/**
		  Inserts the new render component to the vector of SRenderOrderData.
		  The vector's order is preserved so that the RenderOrder is ascending.
		 */
		void InsertRenderComponent (std::vector<SRenderOrderData>& outRenderList, RenderComponent* newComponent);


		/*
		=====================
		  Event Handlers
		=====================
		*/

	protected:
		virtual void HandleTick (FLOAT deltaSec);
	};
}

#endif
#endif