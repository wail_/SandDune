/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  GraphicsEngineComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GraphicsClasses.h"

#if INCLUDE_GRAPHICS

using namespace std;

namespace SD
{
	IMPLEMENT_ENGINE_COMPONENT(GraphicsEngineComponent)

	const FLOAT GraphicsEngineComponent::DrawOrderBackground = 100000.f;
	const FLOAT GraphicsEngineComponent::DrawOrderMainScene = 200000.f;
	const FLOAT GraphicsEngineComponent::DrawOrderForeground = 300000.f;
	const FLOAT GraphicsEngineComponent::DrawOrderUI = 400000.f;

	GraphicsEngineComponent::GraphicsEngineComponent () : Super()
	{
		RenderComponentHashNumber = 0;
		bTickingComponent = true;
		BackBufferResetColor = Color(); //Default to black
	}

	void GraphicsEngineComponent::RegisterObjectHash ()
	{
		Super::RegisterObjectHash();

		RenderComponentHashNumber = Engine::GetEngine()->RegisterObjectHash(TXT("Render Component"));
	}

	void GraphicsEngineComponent::PreInitializeComponent ()
	{
		Super::PreInitializeComponent();

#ifdef DEBUG_MODE
		LOG(LOG_DEBUG, TXT("Initializing Graphics Engine. . ."));
#endif

		WindowHandle = Window::CreateObject();
		WindowHandle->Resource = new sf::RenderWindow(sf::VideoMode(800, 600), PROJECT_NAME);

		//obtain window dimensions
		sf::Vector2u windowSize = WindowHandle->Resource->getSize();
		WindowHeight = windowSize.y;
		WindowWidth = windowSize.x;

		WindowHandle->RegisterPollingDelegate(SDFUNCTION_1PARAM(this, GraphicsEngineComponent, HandleWindowEvent, void, const sf::Event));

		//Create the various resource pools
		if (TexturePool::GetTexturePool() == nullptr)
		{
			TexturePool::CreateObject();
		}

		if (FontPool::GetFontPool() == nullptr)
		{
			FontPool::CreateObject();
		}

		//Create the main rendertarget
		PrimaryRenderTarget = RenderTarget::CreateObject();
		PrimaryRenderTarget->SetRenderWindow(WindowHandle);
	}

	void GraphicsEngineComponent::InitializeComponent ()
	{
		Super::InitializeComponent();

		TexturePool::GetTexturePool()->CreateAndImportTexture(TXT("DebuggingTexture.jpg"), TXT("DebuggingTexture"));
	}

	void GraphicsEngineComponent::PreTick (FLOAT deltaSec)
	{
		Super::PreTick(deltaSec);

		//Check for input and any external window events prior to rendering.
		WindowHandle->PollWindowEvents();

		if (WindowHandle->Resource != nullptr) //Needed in case PollWindowEvents destroyed this window handle
		{
			WindowHandle->Resource->clear(BackBufferResetColor.Source);
		}

		bLockExternalWindows = true;
		for (Window* curWindow : ExternalWindows)
		{
			curWindow->PollWindowEvents();
			curWindow->Resource->clear(BackBufferResetColor.Source);
		}
		bLockExternalWindows = false;

		for (unsigned int i = 0; i < PendingDestroyExternalWindows.size(); i++)
		{
			DestroyExternalWindow(PendingDestroyExternalWindows.at(i));
		}
		PendingDestroyExternalWindows.clear();
	}

	void GraphicsEngineComponent::PostTick (FLOAT deltaSec)
	{
		Super::PostTick(deltaSec);

		WindowHandle->Resource->display();

		for (Window* curWindow : ExternalWindows)
		{
			curWindow->Resource->display();
		}
	}

	void GraphicsEngineComponent::ShutdownComponent ()
	{
		if (VALID_OBJECT(PrimaryRenderTarget))
		{
			PrimaryRenderTarget->Destroy();
			PrimaryRenderTarget = nullptr;
		}

		if (VALID_OBJECT(WindowHandle))
		{
			WindowHandle->Destroy();
			WindowHandle = nullptr;
		}

		if (VALID_OBJECT(TexturePool::GetTexturePool()))
		{
			TexturePool::GetTexturePool()->Destroy();
		}

		if (VALID_OBJECT(FontPool::GetFontPool()))
		{
			FontPool::GetFontPool()->Destroy();
		}

		Super::ShutdownComponent();
	}

	void GraphicsEngineComponent::RegisterResourcePool (ResourcePool* newResourcePool)
	{
		RegisteredResourcePools.push_back(newResourcePool);
	}

	Window* GraphicsEngineComponent::CreateExternalWindow (const DClass* windowClass)
	{
		if (windowClass == nullptr)
		{
			LOG(LOG_WARNING, TXT("Cannot instantiate an external window without specifying which window class to use."));
			return nullptr;
		}
		CHECK(windowClass->GetDefaultObject() != nullptr)

		if (!windowClass->IsChildOf(windowClass))
		{
			LOG1(LOG_WARNING, TXT("The specified class in CreateExternalWindow parameter is not a window class:  it's a %s"), windowClass->ToString());
			return nullptr;
		}

		Window* newWindow = dynamic_cast<Window*>(windowClass->GetDefaultObject()->CreateObjectOfMatchingClass());
		CHECK(newWindow != nullptr)

		ExternalWindows.push_back(newWindow);
		return newWindow;
	}

	void GraphicsEngineComponent::DestroyExternalWindow (Window* targetWindow)
	{
		if (bLockExternalWindows)
		{
			PendingDestroyExternalWindows.push_back(targetWindow);
			return;
		}

		for (unsigned int i = 0; i < ExternalWindows.size(); i++)
		{
			if (ExternalWindows.at(i) == targetWindow)
			{
				targetWindow->Destroy();
				ExternalWindows.erase(ExternalWindows.begin() + i);
				break;
			}
		}
	}

	const RenderTarget* GraphicsEngineComponent::GetPrimaryRenderTarget () const
	{
		return PrimaryRenderTarget;
	}

	Window* GraphicsEngineComponent::GetWindowHandle () const
	{
		return WindowHandle;
	}

	sf::RenderWindow* GraphicsEngineComponent::GetRenderWindow () const
	{
		return (WindowHandle != nullptr) ? WindowHandle->Resource : nullptr;
	}

	void GraphicsEngineComponent::GetWindowSize (INT& outWidth, INT& outHeight) const
	{
		outWidth = WindowWidth;
		outHeight = WindowHeight;
	}

	unsigned int GraphicsEngineComponent::GetRenderComponentHashNumber () const
	{
		return RenderComponentHashNumber;
	}

	void GraphicsEngineComponent::HandleWindowEvent (const sf::Event newEvent)
	{
		if (newEvent.type == sf::Event::Closed)
		{
			Engine::GetEngine()->bShuttingDown = true;
		}
	}
}

#endif