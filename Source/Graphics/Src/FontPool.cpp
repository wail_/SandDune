/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  FontPool.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GraphicsClasses.h"

#if INCLUDE_GRAPHICS

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(FontPool, ResourcePool)
	IMPLEMENT_RESOURCE_POOL(Font)

	void FontPool::InitProps ()
	{
		Super::InitProps();

		DefaultFont = nullptr;
	}

	void FontPool::BeginObject ()
	{
		Super::BeginObject();

		if (FontPoolInstance == nullptr)
		{
			FontPoolInstance = this;
		}

		DefaultFont = FindFont(TXT("DefaultFont"));
		if (DefaultFont == nullptr)
		{
			//Use GetFontPool instead of self to refer to the single font pool instance for proper clean up.
			DefaultFont = FontPool::GetFontPool()->CreateAndImportFont(TXT("QuattrocentoSans-Regular.ttf"), TXT("DefaultFont"));
		}
	}

	void FontPool::Destroy ()
	{
		if (FontPoolInstance == this)
		{
			FontPoolInstance = nullptr;
		}

		Super::Destroy();
	}

	void FontPool::ReleaseResources ()
	{
		for (unsigned int i = 0; i < ImportedResources.size(); i++)
		{
			ImportedResources.at(i).Font->Release();
		}

		DefaultFont = nullptr;
		ImportedResources.clear();
	}

	Font* FontPool::CreateAndImportFont (DString fileName, DString fontName)
	{
		if (!IsValidName(fontName))
		{
			LOG1(LOG_GRAPHICS, TXT("Unable to create font since the specified name (%s) is either invalid or taken."), fontName);
			return nullptr;
		}

		//Import texture
		fileName = FileUtils::GetBaseDirectory() + FONT_LOCATION + DIR_SEPARATOR + fileName;
		sf::Font* newFont = new sf::Font();

		if (!newFont->loadFromFile(fileName.ToCString()))
		{
			LOG1(LOG_WARNING, TXT("Unable to create font.  Could not find %s"), fileName);
			return nullptr;
		}

		Font* result = Font::CreateObject();
		if (!VALID_OBJECT(result))
		{
			delete newFont;
			return nullptr;
		}

		result->SetResource(newFont);
		if (!ImportFont(result, fontName))
		{
			//Failed to import.  destroy font
			result->Destroy(); //releases resource
			return nullptr;
		}

		return result;
	}

	Font* FontPool::GetDefaultFont () const
	{
		return DefaultFont;
	}
}

#endif