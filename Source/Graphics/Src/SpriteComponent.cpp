/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  SpriteComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GraphicsClasses.h"

#if INCLUDE_GRAPHICS

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(SpriteComponent, RenderComponent)

	void SpriteComponent::InitProps ()
	{
		Super::InitProps();

		Sprite = nullptr;

		SpriteTexture = nullptr;

		SubDivideScaleX = 1.f;
		SubDivideScaleY = 1.f;
	}

	void SpriteComponent::BeginObject ()
	{
		Super::BeginObject();

		Sprite = new sf::Sprite();

		if (SpriteTexture != nullptr) //SpriteTexture could be set early from InitProps
		{
			Sprite->setTexture(*(SpriteTexture->GetTextureResource()));
		}
	}

	void SpriteComponent::Destroy ()
	{
		if (Sprite != nullptr)
		{
			delete Sprite;
			Sprite = nullptr;
		}

		Super::Destroy();
	}

	void SpriteComponent::Render (const Camera* targetCamera, const Window* targetWindow)
	{
		if (!SpriteTexture)
		{
			return;
		}

		if (TransformationInterface != nullptr)
		{
			TransformationInterface->ApplyPivotPoint(Sprite);
			TransformationInterface->ApplyDrawPosition(Sprite, targetCamera);
			TransformationInterface->ApplyDrawSize(Sprite, targetCamera);
			TransformationInterface->ApplyDrawRotation(Sprite, targetCamera);
		}

		//Apply sprite scalings
		Vector2 spriteScale = Vector2(SubDivideScaleX, SubDivideScaleY);
		spriteScale *= Vector2::SFMLtoSD(Sprite->getScale()); //Apply transformation interface's scale.  Note:  Can't multiply two sf::Vectors together.

		spriteScale /= GetSpriteSize();

		Sprite->setScale(Vector2::SDtoSFML(spriteScale));

		targetWindow->Resource->draw(*Sprite);
	}

	bool SpriteComponent::SetSubDivision (INT numHorizontalSegments, INT numVerticalSegments, INT horizontalIndex, INT verticalIndex)
	{
		if (numHorizontalSegments <= 0 || numVerticalSegments <= 0 || horizontalIndex < 0 || verticalIndex < 0)
		{
			return false;
		}

		if (horizontalIndex >= numHorizontalSegments || verticalIndex >= numVerticalSegments)
		{
			return false;
		}

		sf::Vector2u totalSize = Sprite->getTexture()->getSize();

		sf::IntRect sfRect;
		sfRect.width = totalSize.x/numHorizontalSegments.Value;
		sfRect.height = totalSize.y/numVerticalSegments.Value;
		sfRect.left = horizontalIndex.Value * sfRect.width;
		sfRect.top = verticalIndex.Value * sfRect.height;

		Sprite->setTextureRect(sfRect);

		SubDivideScaleX = numHorizontalSegments.ToFLOAT();
		SubDivideScaleY = numVerticalSegments.ToFLOAT();

		return true;
	}

	void SpriteComponent::SetDrawCoordinatesMultipliers (FLOAT xScaleMultiplier, FLOAT yScaleMultiplier, FLOAT xPosMultiplier, FLOAT yPosMultiplier)
	{
		if (!VALID_OBJECT(SpriteTexture))
		{
			return;
		}

		sf::Vector2f totalSize = sf::Vector2f(Sprite->getTexture()->getSize());
		sf::FloatRect sfRect;
		sfRect.width = totalSize.x * xScaleMultiplier.Value;
		sfRect.height = totalSize.y * yScaleMultiplier.Value;
		sfRect.left = totalSize.x * xPosMultiplier.Value;
		sfRect.top = totalSize.y * yPosMultiplier.Value;

		Sprite->setTextureRect(sf::IntRect(sfRect));

		SubDivideScaleX = 1/xScaleMultiplier;
		SubDivideScaleY = 1/yScaleMultiplier;
	}

	void SpriteComponent::SetDrawCoordinates (INT u, INT v, INT ul, INT vl)
	{
		sf::IntRect sfRect;
		sfRect.width = ul.Value;
		sfRect.height = vl.Value;
		sfRect.left = u.Value;
		sfRect.top = v.Value;

		Sprite->setTextureRect(sfRect);

		if (VALID_OBJECT(SpriteTexture))
		{
			sf::Vector2u totalSize = Sprite->getTexture()->getSize();
			SubDivideScaleX = INT(totalSize.x).ToFLOAT() / ul.ToFLOAT();
			SubDivideScaleY = INT(totalSize.y).ToFLOAT() / vl.ToFLOAT();
		}
	}

	void SpriteComponent::SetSpriteTexture (const Texture* newTexture)
	{
		SpriteTexture = newTexture;

		if (SpriteTexture != nullptr)
		{
			Sprite->setTexture(*(SpriteTexture->GetTextureResource()));

			//Need to update draw coordinates since setting the texture initially would update the the sprite's draw rectangle, but subsequent texture updates will not.
			//For example, if a sprite initializes to use a 256x256 texture, the sprite's textureRect value is 256,256,0,0 (zero being positions).  Later if that same
			//	sprite changed to a 128x128 texture, the sprite's textureRect is still 256,256,0,0.
			Vector2 newTextureSize;
			newTexture->GetDimensions(newTextureSize);
			sf::IntRect curRect = Sprite->getTextureRect();
			SetDrawCoordinates(curRect.left, curRect.top, (newTextureSize.X/SubDivideScaleX).ToINT(), (newTextureSize.Y/SubDivideScaleY).ToINT());
		}
	}

	void SpriteComponent::GetSpriteSize (INT& outWidth, INT& outHeight) const
	{
		outWidth = 0;
		outHeight = 0;

		if (VALID_OBJECT(SpriteTexture))
		{
			outWidth = SpriteTexture->GetWidth();
			outHeight = SpriteTexture->GetHeight();
		}
	}

	Vector2 SpriteComponent::GetSpriteSize () const
	{
		INT width;
		INT height;
		GetSpriteSize(width, height);

		return Vector2(width.ToFLOAT(), height.ToFLOAT());
	}

	const Texture* SpriteComponent::GetSpriteTexture () const
	{
		return SpriteTexture;
	}
}

#endif