/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  RenderComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GraphicsClasses.h"

#if INCLUDE_GRAPHICS

using namespace std;

namespace SD
{
	IMPLEMENT_ABSTRACT_CLASS(RenderComponent, EntityComponent)

	void RenderComponent::InitProps ()
	{
		Super::InitProps();

		TransformationInterface = nullptr;
		if (GraphicsEngineComponent::Get() != nullptr)
		{
			RelevantRenderWindows.push_back(GraphicsEngineComponent::Get()->GetWindowHandle());
			SetDrawOrder(GraphicsEngineComponent::Get()->DrawOrderMainScene);
			SetVisibility(true);
		}
	}

	bool RenderComponent::AddComponent (EntityComponent* newComponent, bool bLogOnFail, bool bDeleteOnFail)
	{
		if (!Super::AddComponent(newComponent, bLogOnFail, bDeleteOnFail))
		{
			return false;
		}

		//Ensure that the newly created sub render component has the same draw order as its parent.
		RenderComponent* subRenderComponent = dynamic_cast<RenderComponent*>(newComponent);
		if (VALID_OBJECT(subRenderComponent))
		{
			subRenderComponent->SetDrawOrder(DrawOrder);
		}

		return true;
	}

	unsigned int RenderComponent::CalculateHashID () const
	{
		return (GraphicsEngineComponent::Get()->GetRenderComponentHashNumber());
	}

	void RenderComponent::CleanUpInvalidPointers ()
	{
		Super::CleanUpInvalidPointers();

		Object* transformationObject = dynamic_cast<Object*>(TransformationInterface);
		if (transformationObject != nullptr && transformationObject->GetPendingDelete())
		{
			TransformationInterface = nullptr;
		}

		CLEANUP_INVALID_POINTER_ARRAY(RelevantRenderWindows)
	}

	bool RenderComponent::IsRelevant (const Camera* targetCamera, const Window* renderWindow)
	{
		//Find a render window match with specified target window
		for (unsigned int i = 0; i < RelevantRenderWindows.size(); i++)
		{
			if (RelevantRenderWindows.at(i) == renderWindow)
			{
				return GetVisibility();
			}
		}

		return false;
	}

	FLOAT RenderComponent::CalculateDrawOrder (const Camera* targetCamera)
	{
		return DrawOrder;
	}

	void RenderComponent::SetDrawOrder (FLOAT newDrawOrder)
	{
		DrawOrder = newDrawOrder;

		//Update any owned components to this new draw order
		for (unsigned int i = 0; i < Components.size(); i++)
		{
			RenderComponent* subRenderComponent = dynamic_cast<RenderComponent*>(Components.at(i));

			if (VALID_OBJECT(subRenderComponent))
			{
				subRenderComponent->SetDrawOrder(newDrawOrder);	
			}
		}
	}

	void RenderComponent::SetVisibility (bool bNewVisible)
	{
		bVisible = bNewVisible;
	}

	FLOAT RenderComponent::GetDrawOrder () const
	{
		return DrawOrder;
	}

	bool RenderComponent::GetVisibility () const
	{
		return bVisible;
	}
}

#endif