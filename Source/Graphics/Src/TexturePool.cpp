/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TexturePool.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GraphicsClasses.h"

#if INCLUDE_GRAPHICS

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(TexturePool, ResourcePool)
	IMPLEMENT_RESOURCE_POOL(Texture)

	void TexturePool::BeginObject ()
	{
		Super::BeginObject();

		if (TexturePoolInstance == nullptr)
		{
			TexturePoolInstance = this;
		}
	}

	void TexturePool::Destroy ()
	{
		if (TexturePoolInstance == this)
		{
			TexturePoolInstance = nullptr;
		}

		Super::Destroy();
	}

	void TexturePool::ReleaseResources ()
	{
		for (unsigned int i = 0; i < ImportedResources.size(); i++)
		{
			ImportedResources.at(i).Texture->Release();
		}

		ImportedResources.clear();
	}

	Texture* TexturePool::CreateAndImportTexture (DString fileName, DString textureName)
	{
		if (!IsValidName(textureName))
		{
			LOG1(LOG_GRAPHICS, TXT("Unable to create texture since the specified name (%s) is either invalid or taken."), textureName);
			return nullptr;
		}

		//Import texture
		fileName = FileUtils::GetBaseDirectory() + TEXTURE_LOCATION + DIR_SEPARATOR + fileName;
		sf::Texture* newTexture = new sf::Texture();

		if (!newTexture->loadFromFile(fileName.ToCString()))
		{
			LOG1(LOG_WARNING, TXT("Unable to create texture.  Could not find %s"), fileName);
			return nullptr;
		}

		Texture* result = Texture::CreateObject();
		if (!VALID_OBJECT(result))
		{
			delete newTexture;
			return nullptr;
		}

		result->SetResource(newTexture);

		sf::Vector2u dimensions = result->GetTextureResource()->getSize();
		int width = dimensions.x;
		int height = dimensions.y;

#ifdef REQPOWEROF2
		//Typically sprites are automatically padded so that they are in powers of 2.
		if (!Utils::IsPowerOf2(width) || !Utils::IsPowerOf2(height))
		{
			LOG3(LOG_WARNING, TXT("Unable to import texture %s.  The dimensions of that image is not in powers of two (%sx%s)"), fileName, INT(width), INT(height));
			textureReference->Destroy();
			delete newTexture;
			return nullptr;
		}
#endif

		SetTextureAttributes(result, width, height, textureName);

		if (!ImportTexture(result, textureName))
		{
			//Failed to import.  destroy texture
			result->Destroy(); //releases resource
			return nullptr;
		}

		return result;
	}

	void TexturePool::SetTextureAttributes (Texture* target, INT width, INT height, const DString& textureName)
	{
		target->Width = width;
		target->Height = height;
		target->TextureName = textureName;
		target->SetRepeat(true); //By default:  textures should be repeating since smeared textures are typically not desired.
	}
}

#endif