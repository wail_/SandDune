/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  ResolutionComponent.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GraphicsClasses.h"

#if INCLUDE_GRAPHICS

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(ResolutionComponent, EntityComponent)

	void ResolutionComponent::InitProps ()
	{
		Super::InitProps();

		WindowHandle = nullptr;
	}

	void ResolutionComponent::Destroy ()
	{
		if (VALID_OBJECT(WindowHandle))
		{
			SetWindowHandle(nullptr); //Unregister polling delegate
		}

		Super::Destroy();
	}

	void ResolutionComponent::CleanUpInvalidPointers ()
	{
		Super::CleanUpInvalidPointers();

		CLEANUP_INVALID_POINTER(WindowHandle)
	}

	void ResolutionComponent::SetWindowHandle (Window* newWindowHandle)
	{
		if (VALID_OBJECT(WindowHandle))
		{
			WindowHandle->UnregisterPollingDelegate(SDFUNCTION_1PARAM(this, ResolutionComponent, HandleWindowEvent, void, const sf::Event));
		}

		WindowHandle = newWindowHandle;

		if (VALID_OBJECT(WindowHandle))
		{
			WindowHandle->RegisterPollingDelegate(SDFUNCTION_1PARAM(this, ResolutionComponent, HandleWindowEvent, void, const sf::Event));
		}
	}

	Window* ResolutionComponent::GetWindowHandle () const
	{
		return WindowHandle;
	}

	void ResolutionComponent::HandleWindowEvent (const sf::Event newEvent)
	{
		if (!OnResolutionChange.IsBounded())
		{
			return;
		}

		if (newEvent.type == sf::Event::Resized)
		{
			OnResolutionChange.Execute(INT(newEvent.size.width), INT(newEvent.size.height));
		}
	}
}

#endif