/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  RenderTarget.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "GraphicsClasses.h"

#if INCLUDE_GRAPHICS

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(RenderTarget, Entity)

	void RenderTarget::InitProps ()
	{
		Super::InitProps();

		FrameRate = 0.f;
		AssociatedCamera = nullptr;
		RenderWindow = nullptr;

		TimeRemaining = 0.f;
	}

	void RenderTarget::BeginObject ()
	{
		Super::BeginObject();

		if (!VALID_OBJECT(AssociatedCamera))
		{
			AssociatedCamera = Camera::CreateObject();
		}

		TickComponent* tick = TickComponent::CreateObject();
		if (AddComponent(tick))
		{
			tick->SetTickHandler(SDFUNCTION_1PARAM(this, RenderTarget, HandleTick, void, FLOAT));
		}
	}

	void RenderTarget::Destroy ()
	{
		if (VALID_OBJECT(AssociatedCamera))
		{
			AssociatedCamera->Destroy();
			AssociatedCamera = nullptr;
		}

		Super::Destroy();
	}

	void RenderTarget::CleanUpInvalidPointers ()
	{
		Super::CleanUpInvalidPointers();

		CLEANUP_INVALID_POINTER(AssociatedCamera)
		CLEANUP_INVALID_POINTER(RenderWindow)
	}

	void RenderTarget::GenerateScene ()
	{
		vector<SRenderOrderData> renderList;

		RenderComponent* renderedEntity = nullptr;
		for (ObjectIterator iter(GraphicsEngineComponent::Get()->GetRenderComponentHashNumber()); iter.SelectedObject; iter++)
		{
			renderedEntity = dynamic_cast<RenderComponent*>(iter.SelectedObject);

			if (VALID_OBJECT(renderedEntity) && renderedEntity->IsRelevant(AssociatedCamera, RenderWindow))
			{
				InsertRenderComponent(renderList, renderedEntity);
			}
		}

		//draw the components
		for (unsigned int i = 0; i < renderList.size(); i++)
		{
			renderList.at(i).RenderComponent->Render(AssociatedCamera, RenderWindow);
		}

		TimeRemaining = FrameRate;
	}

	void RenderTarget::SetAssociatedCamera (Camera* newCamera)
	{
		AssociatedCamera = newCamera;
	}

	void RenderTarget::SetFrameRate (FLOAT newFrameRate, bool bResetElapsedTime)
	{
		FrameRate = newFrameRate;

		if (bResetElapsedTime)
		{
			TimeRemaining = FrameRate;
		}
	}

	void RenderTarget::SetRenderWindow (Window* newRenderWindow)
	{
		RenderWindow = newRenderWindow;
	}

	FLOAT RenderTarget::GetFrameRate () const
	{
		return FrameRate;
	}

	Camera* RenderTarget::GetAssociatedCamera () const
	{
		return AssociatedCamera;
	}

	Window* RenderTarget::GetRenderwindow () const
	{
		return RenderWindow;
	}

	void RenderTarget::InsertRenderComponent (vector<SRenderOrderData>& outRenderList, RenderComponent* newComponent)
	{
		FLOAT newComponentDrawOrder = newComponent->CalculateDrawOrder(AssociatedCamera);
		SRenderOrderData newOrderData;
		newOrderData.RenderComponent = newComponent;
		newOrderData.RenderOrder = newComponentDrawOrder;

		unsigned int i = 0;
		for (; i < outRenderList.size(); i++)
		{
			if (outRenderList.at(i).RenderOrder > newComponentDrawOrder)
			{
				break;
			}
		}

		outRenderList.insert(outRenderList.begin() + i, newOrderData);
	}

	void RenderTarget::HandleTick (FLOAT deltaSec)
	{
		if (FrameRate < 0)
		{
			return;
		}

		TimeRemaining -= deltaSec;
		if (TimeRemaining <= 0.f)
		{
			GenerateScene();
		}
	}
}

#endif