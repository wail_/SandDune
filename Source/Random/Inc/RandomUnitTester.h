/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  RandomUnitTester.h
  Unit tester that'll be validating various random related utility functions.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#ifndef RANDOMUNITTESTER_H
#define RANDOMUNITTESTER_H

#include "Random.h"

#if INCLUDE_RANDOM

#ifdef DEBUG_MODE

namespace SD
{
	class RandomUnitTester : public UnitTester
	{
		DECLARE_CLASS(RandomUnitTester)


		/*
		=====================
		  Inherited
		=====================
		*/

	public:
		virtual bool RunTests (EUnitTestFlags testFlags) const override;


		/*
		=====================
		  Implementation
		=====================
		*/

	protected:
		virtual bool TestUtilityFunctions (EUnitTestFlags testFlags) const;
	};
}

#endif //debug_mode

#endif
#endif