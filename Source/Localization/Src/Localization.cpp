/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  Localization.cpp

  Implements various functions from depending modules since the depending modules
  (such as Core) also depended on some of its functionality to be localized.

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "Configuration.h"

#if INCLUDE_LOCALIZATION

#include "LocalizationClasses.h"

using namespace std;

namespace SD
{
	bool DString::ToBool () const
	{
		TextTranslator* translator = TextTranslator::GetTranslator();
		if (!VALID_OBJECT(translator))
		{
			LOG1(LOG_WARNING, TXT("Unable to convert %s to bool since it was unable to get a handle to the text translator."), ToString());
			return false;
		}

		vector<DString> translations;
		translator->GetTranslations(TXT("TrueText"), translations, TXT("Core"), TXT("DString"));
		if (translations.size() <= 0)
		{
			LOG1(LOG_LOCALIZATION, TXT("String To Bool is not localized to the current language:  %s"), translator->GetSelectedLanguageName());
			return false;
		}

		DString trimmedString(String);
		trimmedString.TrimSpaces();

		//Return true if any of the localized text matches current string
		for (unsigned int i = 0; i < translations.size(); i++)
		{
			if (trimmedString.Compare(translations.at(i), false) == 0)
			{
				return true;
			}
		}

		return false;
	}

	DString BOOL::ToString () const
	{
		TextTranslator* translator = TextTranslator::GetTranslator();
		if (!VALID_OBJECT(translator))
		{
			LOG1(LOG_WARNING, TXT("Unable to convert %s to string since it was unable to get a handle to the text translator."), ToString());
			return false;
		}

		if (translator->GetSelectedLanguage() != LanguageID)
		{
			//Selected language changed.  Update translations
			LanguageID = translator->GetSelectedLanguage();
			ToTrueText = translator->TranslateText(TXT("TBoolToString"), TXT("Core"), TXT("BOOL"));
			ToFalseText = translator->TranslateText(TXT("FBoolToString"), TXT("Core"), TXT("BOOL"));

			if (ToTrueText.IsEmpty() || ToFalseText.IsEmpty())
			{
				LOG1(LOG_LOCALIZATION, TXT("Bool to String is not localized to the current language:  %s"), translator->GetSelectedLanguageName());
				ToTrueText = TXT("True");
				ToFalseText = TXT("False");
			}
		}

		return ((Value) ? ToTrueText : ToFalseText);
	}
}
#endif