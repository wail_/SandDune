/*
=====================================================================
  MIT License

  Copyright (c) 2016 Eric Eberhart
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

  ------------------------------------------------------------------------------

  TextTranslator.cpp

  Author:  Eric "Ant" Eberhart
=====================================================================
*/

#include "Configuration.h"

#if INCLUDE_LOCALIZATION

#include "LocalizationClasses.h"

using namespace std;

namespace SD
{
	IMPLEMENT_CLASS(TextTranslator, Object)

	void TextTranslator::InitProps ()
	{
		Super::InitProps();

		SelectedLanguage = English;

		SLanguageMapping newLanguageMapping;
		newLanguageMapping.Language = English;
		newLanguageMapping.TextSuffix = TXT("-ENG");
		newLanguageMapping.FriendlyName = TXT("English");
		LanguageMappings.push_back(newLanguageMapping);

		newLanguageMapping.Language = Debugging;
		newLanguageMapping.TextSuffix = TXT("-XXX");
		newLanguageMapping.FriendlyName = TXT("Debugging");
		LanguageMappings.push_back(newLanguageMapping);
	}

	void TextTranslator::BeginObject ()
	{
		Super::BeginObject();

		DefaultReader = ConfigWriter::CreateObject();
		if (!VALID_OBJECT(DefaultReader))
		{
			Engine::GetEngine()->FatalError(TXT("Failed to initialize TextTranslator.  Unable to create a ConfigWriter for the DefaultReader variable."));
			Destroy();
			return;
		}

		SelectDefaultLanguage();		
#ifndef PROJECT_NAME
		DString fileName = GetFullFileFormat(TXT("TextTranslator"));
#else
		DString fileName = GetFullFileFormat(PROJECT_NAME);
#endif

		DefaultReader->SetReadOnly(true);
		DefaultReader->OpenFile(fileName, false);
		if (!DefaultReader->IsFileOpened())
		{
			LOG1(LOG_WARNING, TXT("Failed to initialize TextTranslator.  Unable to open or find a file named %s"), fileName);
			DefaultReader->Destroy();
			DefaultReader = nullptr;
		}
	}

	void TextTranslator::Destroy ()
	{
		if (VALID_OBJECT(DefaultReader))
		{
			DefaultReader->Destroy();
			DefaultReader = nullptr;
		}

		Super::Destroy();
	}

	TextTranslator* TextTranslator::GetTranslator ()
	{
		return LocalizationEngineComponent::Get()->GetTranslator();
	}

	DString TextTranslator::TranslateText (const DString& localizationKey, const DString& baseFileName, const DString& sectionName) const
	{
		DString fileName = GetFullFileFormat(baseFileName);
		ConfigWriter* curFile = GetFileReader(fileName);

		if (!VALID_OBJECT(curFile) || !curFile->IsFileOpened())
		{
			LOG2(LOG_WARNING, TXT("Unable to translate \"%s\" since it could not find an available file named \"%s\""), localizationKey, fileName);
		}

		DString result = curFile->GetPropertyText(sectionName, localizationKey);

		if (result.IsEmpty())
		{
			//Key is not translated for current language.
			LOG3(LOG_LOCALIZATION, TXT("Unable to find translated text for key (%s) within section (%s) within file (%s)."), localizationKey, sectionName, baseFileName);
		}

		if (curFile != DefaultReader)
		{
			curFile->Destroy();
		}

		return result;
	}

	void TextTranslator::GetTranslations (const DString& localizationKey, vector<DString>& outTranslations, const DString& baseFileName, const DString& sectionName) const
	{
		DString fileName = GetFullFileFormat(baseFileName);
		ConfigWriter* curFile = GetFileReader(fileName);

		if (!VALID_OBJECT(curFile) || !curFile->IsFileOpened())
		{
			LOG2(LOG_WARNING, TXT("Unable to translate \"%s\" since it could not find an available file named \"%s\""), localizationKey, fileName);
		}

		curFile->GetArrayValues<DString>(sectionName, localizationKey, outTranslations);

		if (outTranslations.size() <= 0)
		{
			//Key is not translated for current language.
			LOG3(LOG_LOCALIZATION, TXT("Unable to find translated text for key (%s) within section (%s) within file (%s)."), localizationKey, sectionName, baseFileName);
		}

		if (curFile != DefaultReader)
		{
			curFile->Destroy();
		}
	}

	void TextTranslator::SelectLanguage (ELanguages newLanguage)
	{
		for (unsigned int i = 0; i < LanguageMappings.size(); i++)
		{
			if (LanguageMappings.at(i).Language == newLanguage)
			{
				SelectedLanguageMappingIdx = i;
				SelectedLanguage = newLanguage;
				return;
			}
		}

		LOG(LOG_WARNING, TXT("Unable to set language since there isn't a language mapping associated with specified language."));
	}

	TextTranslator::ELanguages TextTranslator::GetSelectedLanguage () const
	{
		return SelectedLanguage;
	}

	DString TextTranslator::GetSelectedLanguageName () const
	{
		return LanguageMappings[SelectedLanguageMappingIdx].FriendlyName;
	}

	void TextTranslator::GetLanguageMappings (vector<SLanguageMapping>& outLanguageMappings) const
	{
		outLanguageMappings = LanguageMappings;
	}

	TextTranslator::SLanguageMapping TextTranslator::GetSelectedLanguageData () const
	{
		if (SelectedLanguageMappingIdx != UINT_INDEX_NONE)
		{
			return LanguageMappings[SelectedLanguageMappingIdx];
		}

		LOG(LOG_WARNING, TXT("Failed to get the selected language data since SelectedLanguageMappingIdx variable is not yet set."));
		return SLanguageMapping();
	}

	const ConfigWriter* TextTranslator::GetDefaultReader () const
	{
		return DefaultReader;
	}

	DString TextTranslator::GetFullFileFormat (const DString& baseFileName) const
	{
		if (baseFileName.IsEmpty())
		{
			return DString(LOCALIZATION_LOCATION) + DIR_SEPARATOR + PROJECT_NAME + GetSelectedLanguageData().TextSuffix + LOCALIZATION_EXT;
		}

		return DString(LOCALIZATION_LOCATION) + DIR_SEPARATOR + baseFileName + GetSelectedLanguageData().TextSuffix + LOCALIZATION_EXT;
	}

	void TextTranslator::SelectDefaultLanguage ()
	{
		SelectLanguage(DEFAULT_LANGUAGE);
	}

	ConfigWriter* TextTranslator::GetFileReader (const DString& fullFileName) const
	{
		//Use default reader if the file name is not specified
		if (fullFileName == GetFullFileFormat(TXT("")))
		{
			return DefaultReader;
		}

		ConfigWriter* newReader = ConfigWriter::CreateObject();
		newReader->SetReadOnly(true);
		newReader->OpenFile(fullFileName, false);

		return newReader;
	}
}

#endif